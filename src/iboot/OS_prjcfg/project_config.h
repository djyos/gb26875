/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#ifndef __PROJECT_CONFFIG_H__
#define __PROJECT_CONFFIG_H__

#include <stdint.h>
#include <stddef.h>
//manual config start
//此处填写手动配置，DIDE生成配置文件时，不会被修改
//manual config end

#define    CN_RUNMODE_IBOOT                0                //IBOOT模式运行
#define    CN_RUNMODE_APP                  2                //由IBOOT加载的APP
#define    CFG_RUNMODE                     CN_RUNMODE_IBOOT           //由IBOOT加载的APP
#define    CN_RUNMODE_BOOTSELF             1                       //无须IBOOT，自启动模式APP
//*******************************  Configure black box  ******************************************//
#define    CFG_MODULE_ENABLE_BLACK_BOX     true
//*******************************  Configure device file system  ******************************************//
#define CFG_DEVFILE_LIMIT       10        // "设备数量",定义设备数量
#define    CFG_MODULE_ENABLE_DEVICE_FILE_SYSTEM  true
//*******************************  Configure file system  ******************************************//
#define CFG_CLIB_BUFFERSIZE            512        // "C库文件用户态缓冲区尺寸"
#define    CFG_MODULE_ENABLE_FILE_SYSTEM   true
//*******************************  Configure int  ******************************************//
#define    CFG_MODULE_ENABLE_INT           true
//*******************************  Configure kernel object system  ******************************************//
#define CFG_OBJECT_LIMIT        8     // "对象数初始值"，用完会自动扩充
#define CFG_HANDLE_LIMIT        8     // "句柄数初始值"，用完会自动扩充
#define    CFG_MODULE_ENABLE_KERNEL_OBJECT_SYSTEM  true
//*******************************  Configure lock  ******************************************//
#define CFG_LOCK_LIMIT          40        // "锁的数量",定义锁的数量，包含信号量和互斥量
#define    CFG_MODULE_ENABLE_LOCK          true
//*******************************  Configure memory pool  ******************************************//
#define CFG_MEMPOOL_LIMIT       10        // "内存池数量限值",
#define    CFG_MODULE_ENABLE_MEMORY_POOL   true
//*******************************  Configure message queue  ******************************************//
#define    CFG_MODULE_ENABLE_MESSAGE_QUEUE true
//*******************************  Configure multiplex  ******************************************//
#define    CFG_MODULE_ENABLE_MULTIPLEX     true
//*******************************  Configure ring buffer and line buffer  ******************************************//
#define    CFG_MODULE_ENABLE_RING_BUFFER_AND_LINE_BUFFER  true
//*******************************  Configure watch dog  ******************************************//
#define CFG_WDT_LIMIT           10        // "看门狗数量",允许养狗数量
#define    CFG_MODULE_ENABLE_WATCH_DOG     true
//*******************************  Configure xip app file system  ******************************************//
#define    CFG_MODULE_ENABLE_XIP_APP_FILE_SYSTEM  true
//*******************************  Configure cpu onchip timer  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_TIMER  true
//*******************************  Configure cpu onchip systime  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_SYSTIME  true
//*******************************  Configure cpu drive inner flash  ******************************************//
#define CFG_EFLASH_PART_FORMAT     false        // 分区选项,是否需要擦除该芯片。
#define CFG_EFLASH_PAGE_SIZE                 512        // 片内flash的页大小，单位字节。
#define CFG_EFLASH_SMALL_SECT_PAGE_NUM       64           // 片内flash的小扇区中，有多少页。
#define CFG_EFLASH_LARGE_SECT_PAGE_NUM       256           // 片内flash的大扇区中，有多少页。
#define CFG_EFLASH_NORMAL_SECT_PAGE_NUM      512           // 片内flash的标准扇区中，有多少页。
#define CFG_EFLASH_PLANE_SMALL_SECT_NUM      4           // 片内flash的主存储块中，有多少小扇区。
#define CFG_EFLASH_PLANE_LARGE_SECT_NUM      1           // 片内flash的主存储块中，有多少大扇区。
#define CFG_EFLASH_PLANE_NORMAL_SECT_NUM     7         // 片内flash的主存储块中，有多少标准扇区。
#define CFG_EFLASH_PLANE_NUM                 1           // 片内flash的主存储块个数。
#define CFG_EFLASH_MAPPED_START_ADDR         0x8000000   // 片内flash的映射起始地址。
#define    CFG_MODULE_ENABLE_CPU_DRIVE_INNER_FLASH  true
//*******************************  Configure emflash insatall xip  ******************************************//
#define CFG_EFLASH_XIP_PART_START      6            // 分区起始，填写块号，块号从0开始计算
#define CFG_EFLASH_XIP_PART_END        -1           // 分区结束，-1表示最后一块，如果结束块是6，起始块是0，则该分区使用的块为0，1，2，3，4，5块
#define CFG_EFLASH_XIP_PART_FORMAT     false        // 分区选项,是否需要格式化该分区。
#define CFG_EFLASH_XIPFSMOUNT_NAME   "xip-app"      // 需安装的文件系统的mount的名字，NULL表示该flash不挂载文件系统
#define    CFG_MODULE_ENABLE_EMFLASH_INSATALL_XIP  true
//*******************************  Configure cpu onchip dma  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_DMA  true
//*******************************  Configure cpu onchip gpio  ******************************************//
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_GPIO  true
//*******************************  Configure cpu onchip qspi  ******************************************//
#define CFG_QSPI_CLOCK_PRESCALER    6     // 设置QSPI的时钟为AHB时钟的1/(CFG_QSPI_CLOCK_PRESCALER + 1)
#define CFG_QSPI_FIFO_THRESHOLD_LEVEL    4     // 设置QSPI的FIFO的阈值为CFG_QSPI_FIFO_THRESHOLD_LEVEL
#define CFG_QSPI_FLASH_SIZE              24    // Flash 中的字节数 = 2^[FSIZE+1]
#define CFG_QSPI_CHIP_SELECT_HIGH_TIME   4    // 片选高电平时间
#define CFG_QSPI_FLASH_ID    1     // 该QSPI支持控制两个flash，这里选择，接的是哪个flash。
#define CFG_QSPI_DDR_HOLD    1      // 1：使用模拟延迟来延迟数据输出,2：数据输出延迟 1/4 个 QUADSPI 输出时钟周期。
#define CFG_QSPI_CK_MODE     0     // 时钟模式0或者3
#define CFG_QSPI_ALTERNATE_BYTES_MODE     0     // 交替字节模式 0:无交替字节,1:单线传输交替字节 ,2:双线传输交替字节 ,3:四线传输交替字节
#define CFG_QSPI_ALTERNATE_BYTES_SIZE     0     // 交替字节长度 0:8 位交替字节,1:16 位交替字节 ,2:24 位交替字节 ,3:32 位交替字节
#define CFG_QSPI_DUAL_FLASH_MODE    false     // 是否开启双闪存模式。
#define CFG_QSPI_SAMPLE_SHIFT       true      // 是否采样移位半个周期。
#define CFG_QSPI_SIOO               false      // 是否仅发送指令一次模式。
#define CFG_QSPI_DDR_ENABLE         false      // 是否使能DDR。
#define CFG_QSPI_TIMEOUT           5000    // QSPI执行的超时时间
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_SDCARD  true
//*******************************  Configure STM32 commond code  ******************************************//
#define    CFG_MODULE_ENABLE_STM32_COMMOND_CODE  true
//*******************************  Configure kernel  ******************************************//
#define CFG_INIT_STACK_SIZE     4096      // "初始化栈空间",定义初始化过程使用的栈空间，一般按默认值就可以了
#define CFG_EVENT_LIMIT         15        // "事件数量限值",事件数量
#define CFG_EVENT_TYPE_LIMIT    15        // "事件类型数限值",事件类型数
#define CFG_MAINSTACK_LIMIT     4096      // "main函数栈尺寸",main函数运行所需的栈尺寸
#define CFG_IDLESTACK_LIMIT     1024      // "IDLE事件栈尺寸",IDLE事件处理函数运行的栈尺寸，一般按默认值就可以了
#define CFG_IDLE_MONITOR_CYCLE  30        // "IDLE监视周期",监视IDLE事件持续低于1/16 CPU占比的时间，秒数，0=不监视
#define CFG_OS_TINY             false     // "tiny版内核",true=用于资源紧缺的场合，内核会裁剪掉：事件类型名字，事件处理时间统计。
#define    CFG_MODULE_ENABLE_KERNEL        true
//*******************************  Configure board config  ******************************************//
#define    CFG_MODULE_ENABLE_BOARD_CONFIG  true
//*******************************  Configure misc  ******************************************//
#define    CFG_MODULE_ENABLE_MISC          true
//*******************************  Configure Software Timers  ******************************************//
#define CFG_TIMERS_LIMIT        5         // "定时器数量",可创建的定时器数量（不包含图形界面的定时器）
#define CFG_TIMER_SOUCE_HARD    true      // "硬件定时器提供时钟源",选择专用硬件还是tick做时钟源
#define    CFG_MODULE_ENABLE_SOFTWARE_TIMERS  true
//*******************************  Configure time  ******************************************//
#define CFG_LOCAL_TIMEZONE      8        // "时区",北京时间是东8区
#define    CFG_MODULE_ENABLE_TIME          true
//*******************************  Configure debug information  ******************************************//
#define    CFG_MODULE_ENABLE_DEBUG_INFORMATION  true
//*******************************  Configure flash  ******************************************//
#define    CFG_MODULE_ENABLE_FLASH         true
//*******************************  Configure heap  ******************************************//
#define CFG_DYNAMIC_MEM true    // "全功能动态分配",即使选false，也允许使用malloc-free分配内存，但使用有差别，详见 《……用户手册》内存分配章节
#define    CFG_MODULE_ENABLE_HEAP          true
//*******************************  Configure uart device file  ******************************************//
#define    CFG_MODULE_ENABLE_UART_DEVICE_FILE  true
//*******************************  Configure cpu onchip uart  ******************************************//
#define CFG_UART1_SENDBUF_LEN       64        //  "UART1发送环形缓冲区大小",
#define CFG_UART1_RECVBUF_LEN       64        //  "UART1接收环形缓冲区大小",
#define CFG_UART1_DMABUF_LEN        64        //  "UART1 DMA环形缓冲区大小",
#define CFG_UART2_SENDBUF_LEN       64        //  "UART2发送环形缓冲区大小",
#define CFG_UART2_RECVBUF_LEN       64        //  "UART2接收环形缓冲区大小",
#define CFG_UART2_DMABUF_LEN        64        //  "UART2 DMA环形缓冲区大小",
#define CFG_UART3_SENDBUF_LEN       64        //  "UART3发送环形缓冲区大小",
#define CFG_UART3_RECVBUF_LEN       64        //  "UART3接收环形缓冲区大小",
#define CFG_UART3_DMABUF_LEN        64        //  "UART3 DMA环形缓冲区大小",
#define CFG_UART4_SENDBUF_LEN       64        //  "UART4发送环形缓冲区大小",
#define CFG_UART4_RECVBUF_LEN       64        //  "UART4接收环形缓冲区大小",
#define CFG_UART4_DMABUF_LEN        64        //  "UART4 DMA环形缓冲区大小",
#define CFG_UART5_SENDBUF_LEN       64        //  "UART5发送环形缓冲区大小",
#define CFG_UART5_RECVBUF_LEN       64        //  "UART5接收环形缓冲区大小",
#define CFG_UART5_DMABUF_LEN        64        //  "UART5 DMA环形缓冲区大小",
#define CFG_UART6_SENDBUF_LEN       64        //  "UART6发送环形缓冲区大小",
#define CFG_UART6_RECVBUF_LEN       64        //  "UART6接收环形缓冲区大小",
#define CFG_UART6_DMABUF_LEN        64        //  "UART6 DMA环形缓冲区大小",
#define CFG_UART7_SENDBUF_LEN       64        //  "UART7发送环形缓冲区大小",
#define CFG_UART7_RECVBUF_LEN       64        //  "UART7接收环形缓冲区大小",
#define CFG_UART7_DMABUF_LEN        64        //  "UART7 DMA环形缓冲区大小",
#define CFG_UART8_SENDBUF_LEN       64        //  "UART8发送环形缓冲区大小",
#define CFG_UART8_RECVBUF_LEN       64        //  "UART8接收环形缓冲区大小",
#define CFG_UART8_DMABUF_LEN        64        //  "UART8 DMA环形缓冲区大小",
#define CFG_UART1_ENABLE           true          //  "是否使用UART1",
#define CFG_UART1_ENABLE_DMA       true          //  "UART1使能DMA",
#define CFG_UART2_ENABLE           false         //  "是否使用UART2",
#define CFG_UART2_ENABLE_DMA       false         //  "UART2使能DMA",
#define CFG_UART3_ENABLE           true         //  "是否使用UART3",
#define CFG_UART3_ENABLE_DMA       false         //  "UART3使能DMA",
#define CFG_UART4_ENABLE           true         //  "是否使用UART4",
#define CFG_UART4_ENABLE_DMA       true         //  "UART4使能DMA",
#define CFG_UART5_ENABLE           true         //  "是否使用UART5",
#define CFG_UART5_ENABLE_DMA       true         //  "UART5使能DMA",
#define CFG_UART6_ENABLE           true         //  "是否使用UART6",
#define CFG_UART6_ENABLE_DMA       true         //  "UART6使能DMA",
#define CFG_UART7_ENABLE           true         //  "是否使用UART7",
#define CFG_UART7_ENABLE_DMA       true         //  "UART7使能DMA",
#define CFG_UART8_ENABLE           false         //  "是否使用UART8",
#define CFG_UART8_ENABLE_DMA       false         //  "UART8使能DMA",
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_UART  true
//*******************************  Configure tcpip  ******************************************//
#define     CFG_NETPKG_MEMSIZE          0x4000    // "数据包缓冲区尺寸"
#define    CFG_MODULE_ENABLE_TCPIP         true
//*******************************  Configure sock  ******************************************//
#define     CFG_SOCKET_NUM              10        // "socket数限值"，占一个 tagItem 结构
#define    CFG_MODULE_ENABLE_SOCK          true
//*******************************  Configure ppp  ******************************************//
#define    CFG_MODULE_ENABLE_PPP           true
//*******************************  Configure dhcp  ******************************************//
#define     CFG_DHCPD_ENABLE            false     // "DHCP 服务器使能"
#define     CFG_DHCPC_ENABLE            true      // "DHCP 客户端使能"
#define     CFG_DHCP_RENEWTIME          3600      // "renew timer",秒数
#define     CFG_DHCP_REBINDTIME         3600      // "rebind timer",秒数
#define     CFG_DHCP_LEASETIME          3600      // "lease timer",秒数
#define     CFG_DHCPD_IPNUM             0x40      // "IP池尺寸",64
#define     CFG_DHCPD_IPSTART           "192.168.0.2"      // "DHCP起始IP",
#define     CFG_DHCPD_SERVERIP          "192.168.0.253"    // "DHCP SERVER IP"
#define     CFG_DHCPD_ROUTERIP          "192.168.0.253"    // "DHCP ROUTER SERVER IP"
#define     CFG_DHCPD_NETIP             "255.255.255.0"    // "DHCP MASK IP"
#define     CFG_DHCPD_DNS               "192.168.0.253"    // "DHCP DNSSERVER IP"
#define     CFG_DHCPD_DOMAINNAME       "domain"          // "DHCP domain name"
#define    CFG_MODULE_ENABLE_DHCP          true
//*******************************  Configure arp  ******************************************//
#define     CFG_ARP_HASHLEN             32        // "ARP哈希表长度"，占用一个指针
#define    CFG_MODULE_ENABLE_ARP           true
//*******************************  Configure cpu onchip ETH  ******************************************//
#define CFG_ETH_NETCARD_NAME    "STM32F7_ETH"     //  "网卡名称",
#define CFG_ETH_LOOP_CYCLE      1000           //  "网卡轮询周期(uS)",中断模式无须填写
#define CFG_ETH_LOOP_ENABLE     true              //  "网卡接收是否轮询",
#define CFG_ETH_HARD_MAC_ADDR   true              //  "硬件生成Mac地址",
#define CFG_ETH_MAC_ADDR0      00             //  "MAC ADDR0",若选中"硬件生成Mac地址",则无须填写
#define CFG_ETH_MAC_ADDR1      02             //  "MAC ADDR1",若选中"硬件生成Mac地址",则无须填写
#define CFG_ETH_MAC_ADDR2      03             //  "MAC ADDR2",若选中"硬件生成Mac地址",则无须填写
#define CFG_ETH_MAC_ADDR3      04             //  "MAC ADDR3",若选中"硬件生成Mac地址",则无须填写
#define CFG_ETH_MAC_ADDR4      05             //  "MAC ADDR4",若选中"硬件生成Mac地址",则无须填写
#define CFG_ETH_MAC_ADDR5      06             //  "MAC ADDR5",若选中"硬件生成Mac地址",则无须填写
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_MAC  true
//*******************************  Configure udp  ******************************************//
#define     CFG_UDP_CBNUM               10        // "UDP socket数限值"，占用一个 tagUdpCB 结构
#define     CFG_UDP_HASHLEN             4         // "udp socket 哈希表长度"，占用一个指针
#define     CFG_UDP_PKGMSGLEN        1472         // udp最大包长度
#define     CFG_UDP_BUFLENDEFAULT    0x2000       // 8KB
#define    CFG_MODULE_ENABLE_UDP           true
//*******************************  Configure tpl  ******************************************//
#define     CFG_TPL_PROTONUM            5         // "支持的传输协议数"，占用一个 tagTplProtoItem 结构
#define    CFG_MODULE_ENABLE_TPL           true
//*******************************  Configure ftp  ******************************************//
#define     CFG_FTPD_ENABLE            false     // "ftp 服务器使能",暂未实现
#define     CFG_FTPC_ENABLE            true      // "ftp 客户端使能"
#define    CFG_MODULE_ENABLE_FTP           true
//*******************************  Configure telnet  ******************************************//
#define     CFG_TELNETD_ENABLE          true      // "telnet 服务器使能"
#define     CFG_TELNETC_ENABLE          false     // "telnet 客户端使能"
#define    CFG_MODULE_ENABLE_TELNET        true
//*******************************  Configure router  ******************************************//
#define CFG_IP_STRMAX           20   // 最大路由条目数
#define    CFG_MODULE_ENABLE_ROUTER        true
//*******************************  Configure loader  ******************************************//
#define CFG_UPDATEIBOOT_EN      false         // "是否支持在线更新Iboot"，
#define    CFG_START_APP_IS_VERIFICATION   false                // "启动app时是否执行校验功能"，
#define  CFG_APP_RUNMODE  EN_DIRECT_RUN       // "APP运行模式",EN_DIRECT_RUN=直接从flash中运行；EN_FORM_FILE=从文件系统加载到内存运行，
#define  CFG_APP_VERIFICATION   VERIFICATION_NULL     // "APP校验方法",
#define CFG_IBOOT_VERSION_SMALL       00          // "Iboot版本号:低",xx.xx.__，APP忽略
#define CFG_IBOOT_VERSION_MEDIUM      00          // "Iboot版本号:中",xx.__.xx，APP忽略
#define CFG_IBOOT_VERSION_LARGE       01          // "Iboot版本号:高",__.xx.xx，APP忽略
#define CFG_IBOOT_UPDATE_NAME      "/fat/iboot.bin"             // "待升级iboot默认存储路径"
#define CFG_APP_UPDATE_NAME        "/fat/app.bin"              // "待升级app默认存储路径"
#define CFG_FORCED_UPDATE_PATH     "/fat/djyapp.bin"             // "强制升级的文件路径"
#define    CFG_MODULE_ENABLE_LOADER        true
//*******************************  Configure stdio  ******************************************//
#define CFG_STDIO_STDIN_MULTI      true           // "是否支持多种输入设备",
#define CFG_STDIO_STDOUT_FOLLOW    true           // "stdout是否跟随stdin",
#define CFG_STDIO_STDERR_FOLLOW    true           // "stderr是否跟随stdin",
#define CFG_STDIO_FLOAT_PRINT      true           // "支持浮点打印"
#define CFG_STDIO_STDIOFILE        true           // "支持标准IO文件"
#define CFG_STDIO_IN_NAME              "/dev/UART1"      // "标准输入设备名",
#define CFG_STDIO_OUT_NAME             "/dev/UART1"      // "标准输出设备名",
#define CFG_STDIO_ERR_NAME             "/dev/UART1"      // "标准err输出设备名",
#define    CFG_MODULE_ENABLE_STDIO         true
//*******************************  Configure shell  ******************************************//
#define CFG_SHELL_STACK            0x1000        // "执行shell命令的栈尺寸",
#define CFG_ADD_ROUTINE_SHELL      true          // "是否添加常规shell命令",
#define CFG_ADD_EXPAND_SHELL       true          // "是否添加拓展shell命令",
#define CFG_ADD_GLOBAL_FUN         false         // "添加全局函数到shell",
#define CFG_SHOW_ADD_SHEELL        true          // "显示在编译窗口添加的shell命令",
#define    CFG_MODULE_ENABLE_SHELL         true
//*******************************  Configure network config  ******************************************//
#define CFG_STATIC_IP       false              //  "使用静态IP?",
#define CFG_SELECT_NETCARD  "STM32F7_ETH"     //  "网卡名称",必须与选中的网卡驱动中配置的名称相同
#define CFG_MY_IPV4         "192.168.0.179"   //  "静态IP",
#define CFG_MY_SUBMASK      "255.255.255.0"   //  "子网掩码",
#define CFG_MY_GATWAY       "192.168.0.1"     //  "网关",
#define CFG_MY_DNS          "192.168.0.1"     //  "DNS",
#define    CFG_MODULE_ENABLE_NETWORK_CONFIG  true
//*******************************  Configure cpu onchip iwdt  ******************************************//
#define CFG_BOOT_TIME_LIMIT         30000000          //  "启动加载超限时间",允许保护启动加载过程才需要配置此项
#define CFG_IWDG_PRESCALER           128               //  iwdg的分配系数。
#define CFG_RELOAD_CYCLE             4000              //  自动重装载值,看门狗溢出时间为Tout=((4× 2^prer) × rlr) /32
#define CFG_WDT_TIM                 TIM12      //  启动加载用到的定时器
#define CFG_DEFEND_ON_BOOT           false            //  "保护启动过程",启动加载过程如果出现死机，看门狗将复位
#define    CFG_MODULE_ENABLE_CPU_ONCHIP_IWDT  true
//*******************************  Core Clock  ******************************************//
#define    CFG_CORE_MCLK                   (216.0*Mhz)       //主频，内核要用，必须定义
//*******************************  Configure yaf2 file system  ******************************************//
#define CFG_YAF_ECC                       0                     //  YAF文件系统文件使能设备ECC功能。0不使用ecc，YAF2_ENABLE_DEVICE_ECC使用ecc
#define CFG_YAF_INSTALL_OPTION           MS_INSTALLCREAT                 //  YAF文件系统安装选项，安装时是否格式化整个文件系统；
#define CFG_YAF_MOUNT_POINT              "yaf2"                    //  "name",YAF文件系统安装目录
#define    CFG_MODULE_ENABLE_YAF2_FILE_SYSTEM  true
//*******************************  DjyosProduct Configuration  ******************************************//
#define    PRODUCT_MANUFACTURER_NAME       ""                //厂商名称
#define    PRODUCT_PRODUCT_CLASSIFY        ""                //产品分类
#define    PRODUCT_PRODUCT_MODEL           ""                //产品型号
#define    PRODUCT_VERSION_LARGE           0                 //版本号,__.xx.xx
#define    PRODUCT_VERSION_MEDIUM          0                 //版本号,xx.__.xx
#define    PRODUCT_VERSION_SMALL           0                 //版本号,xx.xx.__
#define    PRODUCT_PRODUCT_MODEL_CODE      ""                //产品型号编码
#define    PRODUCT_PASSWORD                ""                //产品秘钥
#define    PRODUCT_OTA_ADDRESS             ""                //OTA服务器地址
#define    PRODUCT_BOARD_TYPE              "qucanji"         //板件类型
#define    PRODUCT_CPU_TYPE                "stm32f767xx"     //CPU类型


#endif
