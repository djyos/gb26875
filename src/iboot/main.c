
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */

#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include <stat.h>
#include <dbug.h>
#include <Iboot_info.h>
#include "project_config.h"
extern u32 gc_pAppRange;

static bool_t updateapp(char *addr,u32 file_size, char *file_name)
{
    FILE *xipapp = 0;
    u32 res, residue_size = (s32)file_size;
    char xipapppath[MutualPathLen];
    char percentage_last = 0, percentage = 0;
    if(strlen(file_name) > 40)
    {
        printf("Filename is too long  %s ,\r\n", file_name);
        return false;
    }
    sprintf(xipapppath, "%s%s", "/xip-app/", file_name);
    xipapp = fopen(xipapppath, "w+");
    if(xipapp)
    {
        printf("open file success   %s ,\r\n", xipapppath);
        printf("\r\nloading       ");
        while(1)
        {
            percentage = 100 - ((char)((residue_size * 100)/ file_size));
            if(percentage != percentage_last)
            {
                printf("\b\b\b%2d%%",percentage);
                percentage_last = percentage;
            }
            if(residue_size > 1024)
            {
                res = fwrite(addr, 1, 1024, xipapp);
                if(res != 1024)
                    printf("write file xip-app error  \r\n ");
                addr += res;
                residue_size -= res;
            }
            else
            {
                res = fwrite(addr, 1, residue_size, xipapp);
                if(res != residue_size)
                    printf("write file xip-app error  \r\n ");
                addr += res;
                residue_size -= res;
            }
            if(residue_size == 0)
            {
                info_printf("IAP","app update success.\r\n");
                break;
            }
            if((s32)residue_size < 0)
            {
                info_printf("IAP","app update fail.\r\n");
                break;
            }
        }
        fclose(xipapp);
        Iboot_ClearRunIbootUpdateApp();
        if(residue_size == 0)
        {
            Iboot_SetUpdateRunModet(1);
            Iboot_UpdateToRun();
        }
    }
    else
        printf("open file fail,\r\n");

    return true;
}
ptu32_t djy_main(void)
{
    FILE *fp = NULL;
    char app_info[MutualPathLen];
    struct stat test_stat;
    if(Iboot_GetUpdateApp() == true)
    {
        if(Iboot_GetMutualUpdatePath(app_info, sizeof(app_info)) == true)
        {
            if(Iboot_GetUpdateSource() == 0)
            {
                if((!stat(CFG_FORCED_UPDATE_PATH,&test_stat)) || (!stat(app_info,&test_stat)))
                {
                    if(test_stat.st_size <= gc_pAppRange)
                    {
                        Iboot_UpdateApp();
                    }
                }
                else
                    error_printf("xip","update file not found\r\n");
            }
            else if(Iboot_GetUpdateSource() == 1)
            {
                char *psram_update_addr = NULL;
                u8 app_info_size = 0;
                u32 file_size;
                char file_name[MutualPathLen];
                struct AppHead *apphead;

                memcpy(&psram_update_addr, app_info + app_info_size, sizeof(psram_update_addr));
                printf("psram_update_addr   = %x,\r\n", psram_update_addr);
                app_info_size += sizeof(psram_update_addr);
//                file_size = atoi(app_info + 11);
                memcpy(&file_size, app_info + app_info_size, sizeof(file_size));
                printf("file_size   = %d,\r\n",file_size);
                app_info_size += sizeof(file_size);


                strcpy(file_name, app_info + app_info_size);
                printf("file_name   = %s,\r\n",file_name);
                apphead = (struct AppHead *)psram_update_addr;
                if((apphead->app_bin_size != apphead->file_size) || (apphead->file_size != file_size))
                {
                    printf("ERROR: file size diff.apphead_appbinsize = %d, apphead_filesize = %d, file_size = %d!\r\n",apphead->app_bin_size, apphead->file_size, file_size);
                }
                else
                {
                    if(file_size < 0x180000)
                    {
                        if(XIP_AppFileCheck(psram_update_addr))
                        {

                            printf("File check success\r\n");
                            printf("Start erase flash\r\n");
                            if(!File_Format("/xip-app"))
                            {
                                printf("Erase flash succeed\r\n");
                                updateapp(psram_update_addr, file_size, file_name);
                            }
                        }
                        else
                        {
                            printf("File check fail\r\n");
                        }
                    }
                    else
                    {
                        printf("update file size too long\r\n");
                    }
                }
            }
            else
                error_printf("xip"," Update source error\r\n");
        }
        else
            error_printf("xip","app file info get fail\r\n");
    }
	while(1)
	{
		printf("hello world!\r\n");
		DJY_EventDelay(1000*1000);
	}
	return 0;
}
