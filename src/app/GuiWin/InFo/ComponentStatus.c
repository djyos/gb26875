/*
 * DataStatus.c
 *
 *  Created on: 2020年9月17日
 *      Author: cwj
 */

#include "stdint.h"
#include "string.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "WinSwitch.h"

/****************serial***************/
static u32 serial_connect_statues[5]={0,0,0,0,0};  //串口连接状态

void SetscStatues(u32 addr,u32 statues)
{
    if(addr>=5)
    {
        return;
    }
    serial_connect_statues[addr]=statues;
}
u32 GetscStatues(u32 addr)
{
    if(addr>=5)
    {
        return 0;
    }
    return serial_connect_statues[addr];
}
/************************************/


/****************ip***************/
static u32 ip_connect_statues[5]={0,0,0,0,0}; //ip连接状态
static u32 ip_connect_numb=0;                 //当前所使用的ip在界面上的下标
void SeticStatues(u32 addr,u32 statues)
{
    if(addr>=5)
    {
        return;
    }
    for(int i=0;i<5;i++)
    {
        if(addr == i)
        {
            ip_connect_statues[i]=statues;
        }
        else
        {
            ip_connect_statues[i]=0;
        }
    }

}
u32 GeticStatues(u32 addr)
{
    if(addr>=5)
    {
        return 0;
    }
    return ip_connect_statues[addr];
}

void SetConnectNumb(u32 addr)
{
    ip_connect_numb=addr;
}
u32 GetConnectNumb(void)
{
    return ip_connect_numb;
}
/*********************************/
