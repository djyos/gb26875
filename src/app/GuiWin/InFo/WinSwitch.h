#ifndef _GUIWIN_WINSWITCH_H_
#define _GUIWIN_WINSWITCH_H_
#include "stddef.h"
#include "gdd.h"
#include "DataStatus.h"
#include "ComponentStatus.h"
#define VK_DEL_EMPTY           0x19
#define MSG_REFRESH_UI     (MSG_WM_USER + 0x0001)
#define GUI_WIN_OPEN     0

typedef bool_t (*T_HmiCreate)(struct WindowMsg *pMsg);//界面控件创建
typedef bool_t (*T_HmiPaint)(struct WindowMsg *pMsg);//界面绘制
typedef enum WinType (*T_DoMsg)(struct WindowMsg *pMsg);//界面消息响应

typedef enum
{
    Gui_Save_Serial,  //保存串口配置
    Gui_Save_Ip,      //保存IP配置
    Gui_Ip_Test1,      //连接测试中
    Gui_Ip_Test2,      //连接测试结果
    Gui_Usb_Copy,     //拷贝日志
    Gui_Save_Password,
    Gui_Null,
}ClickResponse;

//按键手动控制定时器的状态机
typedef enum
{
    CtrlConnectTest,
    CtrlSerialSave,
    CtrlPasswordSave,
    CtrlNull,
}CtrlTimerMachine;

typedef enum      //提醒框
{
    Box_Save_Sucess,
    Box_Save_Error,
    Box_Ip_Set,
    Box_Ip_Connecting,
    Box_Ip_Sucess,
    Box_Ip_Error,
    Box_Save_Password,
    Box_Null,
}PromptBoxs;

enum WinType
{
    WIN_Main_WIN = 0,               //主窗口界面
    WIN_Home_Page,                  //首页
    WIN_Admin_Login,                //输入管理密码

    WIN_Serial_Win,                 //串口设置主页
    WIN_Serial_Edit,                //串口编辑页面
    WIN_Equipment_Type,             //选择设备类型
    WIN_Equipment_Brand,            //选择设备品牌
    WIN_Equipment_Model,            //选择设备型号

    WIN_Ip_Win,                     //IP设置主页
    WIN_Ip_Edit,                    //IP配置界面

    WIN_System_Win,                 //系统设置主页
    WIN_System_Password,            //修改密码
    WIN_System_Log,                 //日志主页
    WIN_System_BR,                  //备份和恢复
    WIN_System_CU,                  //检查更新

    WIN_Prompt_Box,                 //一些提醒框

    WIN_Max,                        //有效界面总数
    WIN_NotChange,                  //不改变窗口
    WIN_Error,                      //状态错误
};

typedef enum
{
    Sys_Log_Time,
    Sys_Log_Type,
    Sys_Log_Inf,
}GuiSysLogType;

struct GuiSysLog
{
    char SysLogTime[16];
    char SysLogType[16];
    char SysLogInf[64];
};

struct GuiWinCb
{
    T_HmiCreate HmiCreate;//界面控件创建
    T_HmiPaint HmiPaint;//界面绘制
    T_DoMsg DoMsg;//界面消息响应
};

//========================界面内注册接口定义===================================
int Register_Main_WIN();
int Register_Home_Page();
int Register_Admin_Login();
int Register_Serial_Win();
int Register_Serial_Edit();
int Register_Equipment_Type();
int Register_Equipment_Brand();
int Register_Equipment_Model();
int Register_Ip_Win();
int Register_Ip_Edit();
int Register_System_Win();
int Register_System_Password();
int Register_System_Log();
int Register_System_BR();
int Register_System_CU();
int Register_Prompt_Box();

enum WinType Get_SelectionWinType();

void Main_GuiWin(void);
bool_t HmiRefresh(struct WindowMsg *pMsg);
bool_t HmiNotify_Trans(struct WindowMsg *pMsg);
bool_t Refresh_GuiWin(u32 param2);
bool_t Jump_GuiWin(enum WinType nextInterface);
HWND Get_WindowsHwnd();
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg);
bool_t Refresh_SwitchWIn(HWND hwnd, u32 param2);
bool_t  Jump_Interface(HWND hwnd,enum WinType nextInterface);
int Init_WinSwitch();

void SetLoginJump(enum WinType page);
enum WinType GetLoginJump(void);
char *GetSyscType(void);
char *GetSyscBrand(void);
char *GetSyscModel(void);
HWND GetSerialBoxHwnd(void);
void SetClickResponse(ClickResponse type);
void DisplayPicture_Init(void);
bool_t GetPingFlag(void);
void SetGuiSysLog(GuiSysLogType type,u32 info,char *date,u32 len);
#endif /* _GUIWIN_WINSWITCH_H_ */
