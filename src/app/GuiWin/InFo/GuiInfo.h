/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef IBOOT_GUI_INFO_H_
#define IBOOT_GUI_INFO_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "widget/gdd_progressbar.h"
#include "widget/gdd_textbox.h"
#include "string.h"
#ifndef GUI_INFO_FILE_C_
#endif

#define LCDW_32  1
#define LCDH_32  1

//界面元素枚举
enum widgettype
{
    widget_type_background,
    widget_type_number,
    widget_type_button,
    widget_type_picture,
    widget_type_pictureground,
    widget_type_serialconnect,
    widget_type_ipconnect,
    widget_type_text,
    widget_type_grouptext,
    widget_type_ctext1,
    widget_type_ctext2,
    widget_type_ctext3,
    widget_type_ctext4,
    widget_type_ctext5,
    widget_type_itext1,
    widget_type_itext2,
    widget_type_itext3,
    widget_type_itext4,
    widget_type_itext5,
    widget_type_textbox,
    widget_type_job,
    widget_type_groupbutton,
    widget_type_groupbutton1,
    widget_type_groupbutton2,
    widget_type_groupbutton3,
    widget_type_groupbutton4,
    widget_type_groupbutton5,
};

//界面元素信息
struct GUIINFO
{
    RECT position;//坐标，大小信息
    const char* name;
    enum widgettype type;
    ptu32_t userParam;
} ;

//=================Bmp枚举=======================

struct BmpInfo
{
    s32 bmpsize;
    char *Buf;
};

enum Bmptype
{
    BMP_MainInterface1=0,  //开机界面图标
    BMP_MainInterface2,  //开机界面图标
    BMP_MainInterface3,  //开机界面图标
    BMP_MainInterface4,  //开机界面图标
    BMP_Connectioning,   //连接中
    BMP_Disconnect,      //断开连接
    BMP_HomePageText,    //首页字体
    BMP_RunningState1,   //运行状态-选中
    BMP_RunningState0,   //运行状态-未选中
    BMP_PortConfiguration1,      //串口配置-选中
    BMP_PortConfiguration0,      //串口配置-未选中
    BMP_ServerConfiguration1,    //服务器配置-选中
    BMP_ServerConfiguration0,    //服务器配置-未选中
    BMP_SystemSettings1,         //系统设置-选中
    BMP_SystemSettings0,         //系统设置-未选中
    BMP_TopBox,                  //顶部框
    BMP_SmallFrame,              //复用小框
    BMP_BigFrame,                //复用小框
    BMP_BRRemind,                //备份/恢复提醒框

    BMP_AboutMachText,           //关于本机
    BMP_BRText,                  //备份/恢复
    BMP_CheckUpdateText,         //检查更新
    BMP_IPConfiText,             //IP配置
    BMP_LogText,                 //日志
    BMP_ModifPassText,           //修改密码
    BMP_SysSetText,              //系统设置
    BMP_LoginText,               //请输入管理密码

    BMP_NetworkStatusBox,        //网络状态框
    BMP_PowerStatusBox,          //电源状态框
    BMP_SerialStatusBox,         //串口连接状态框
    BMP_ServerStatusBox,         //服务器连接状态框
    BMP_SystemInformationBox,    //系统信息框

    BMP_PortCom1,                //COM1框
    BMP_PortCom2,                //COM2框
    BMP_PortCom3,                //COM3框
    BMP_PortCom4,                //COM4框
    BMP_PortCom5,                //COM5框
    BMP_PortEdit,                //设置串口
    BMP_Directory,               //选择目录
    BMP_ChooseBackground,        //配置背景板
    BMP_ChooseClose,             //配置关闭按钮
    BMP_ChooseSave,              //配置确认按钮
    BMP_Check,                   //勾选
    BMP_Save,                    //保存
    BMP_Back,                    //不保存并返回

    BMP_Gateway,                 //本地网关
    BMP_IpBox1,                  //ip1框
    BMP_IpBox2,                  //ip2框
    BMP_IpBox3,                  //ip3框
    BMP_IpBox4,                  //ip4框
    BMP_IpBox5,                  //ip5框

    BMP_ExportButton,            //日志导出按钮
    BMP_LogBackButton,           //日志返回按钮
    BMP_LogLeftButton,           //日志左滑按钮
    BMP_LogRightButton,          //日志右滑按钮
    BMP_LogBox,                  //日志首页框
    BMP_LogRemind,               //日志提醒标志
    BMP_LogRemind2,              //日志提醒标志

    BMP_KeyBoard,                //键盘背景

    BMP_IpConnecting,           //正在连接提示框
    BMP_IpConnectError,          //连接失败提醒框
    BMP_SaveSerialError,         //保存失败提醒框
    BMP_IpConnectSucess,         //连接成功提醒框

    BMP_MAXNNUM,   //图标数量
    Bmp_NULL,
};




const char * Get_BmpBuf(enum Bmptype type);

void Set_TextBox_hwnd(HWND BoxHwnd);
HWND Get_TextBox_hwnd(void);
bool_t TextBox_Close(HWND hwnd);
#ifdef __cplusplus
}
#endif

#endif /* IBOOT_GUI_INFO_H_ */
