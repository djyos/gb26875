//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum SystemBRWidgeId{
    SYS_BACKGROUND,

    SysTopBox,
    SysPageText,
    SysRunningState0,
    SysPortConfiguration0,
    SysServerConfiguration0,
    SysSystemSettings1,

    SysBigFrame,
    BRText,

    BRButton1,
    BRButton2,
    BRRemind,
    BRBack,

    SYS_MAXNUM,//总数量
};

//==========================s========config======================================
static const  struct GUIINFO  SystemBRCfgTab[SYS_MAXNUM] =
{
    [SYS_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [SysTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [SysPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text1",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [SysRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [SysPortConfiguration0] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration0,
    },
    [SysServerConfiguration0] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration0,
    },
    [SysSystemSettings1] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings1,
    },

    [SysBigFrame] = {
        .position = {0/LCDW_32,80/LCDH_32,(0+1024)/LCDW_32,(80+520)/LCDH_32},
        .name = "大框",
        .type = widget_type_picture,
        .userParam = BMP_BigFrame,
    },
    [BRText] = {
        .position = {29/LCDW_32,81/LCDH_32,(29+120)/LCDW_32,(81+27)/LCDH_32},
        .name = "备份恢复",
        .type = widget_type_picture,
        .userParam = BMP_BRText,
    },

    [BRRemind] = {
         .position = {34/LCDW_32,524/LCDH_32,(34+360)/LCDW_32,(524+50)/LCDH_32},
         .name = "提醒框",
         .type = widget_type_picture,
         .userParam = BMP_BRRemind,
     },

    [BRButton1] = {
        .position = {55/LCDW_32,133/LCDH_32,(55+270)/LCDW_32,(133+60)/LCDH_32},
        .name = "备份",
        .type = widget_type_groupbutton,
        .userParam = Bmp_NULL,
    },
    [BRButton2] = {
        .position = {55/LCDW_32,219/LCDH_32,(55+270)/LCDW_32,(219+60)/LCDH_32},
        .name = "恢复",
        .type = widget_type_groupbutton,
        .userParam = Bmp_NULL,
    },

    [BRBack] = {
        .position = {916/LCDW_32,532/LCDH_32,(916+80)/LCDW_32,(532+36)/LCDH_32},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_LogBackButton,
    },

};

//按钮控件创建函数
static bool_t  SystemBRButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }
        else if(buttoninfo->type == widget_type_groupbutton)
        {
            GDD_SetFillColor(hdc,RGB(255,255,255));
            GDD_FillRect(hdc,&rc);
            GDD_SetDrawColor(hdc,RGB(0,198,255));
            GDD_DrawRect(hdc,&rc);
            GDD_SetTextColor(hdc,RGB(60,60,60));
            GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);

            rc.left+=235;
            rc.top+=17;
            bmp = Get_BmpBuf(BMP_Directory);
            GDD_DrawBMP(hdc,rc.left,rc.top,bmp);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_SystemBR(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,SystemBRButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<SYS_MAXNUM;i++)
    {
        switch (SystemBRCfgTab[i].type)
        {
            case  widget_type_button :
            case  widget_type_groupbutton :
            {

                Widget_CreateButton(SystemBRCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SystemBRCfgTab[i].position.left, SystemBRCfgTab[i].position.top,\
                                 GDD_RectW(&SystemBRCfgTab[i].position),GDD_RectH(&SystemBRCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SystemBRCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}
//绘制消息处函数
static bool_t HmiPaint_SystemBR(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<SYS_MAXNUM;i++)
        {
            switch (SystemBRCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,SystemBRCfgTab[i].userParam);
                    GDD_FillRect(hdc,&SystemBRCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SystemBRCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SystemBRCfgTab[i].position.left,\
                                SystemBRCfgTab[i].position.top,bmp);
                    }
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_SystemBR(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case SysRunningState0:
                NextUi= WIN_Home_Page;
                break;
            case SysPortConfiguration0:
                NextUi= WIN_Serial_Win  ;
                break;
            case SysServerConfiguration0:
                NextUi= WIN_Ip_Win  ;
                break;
//            case SysSystemSettings1:
//                NextUi= WIN_System_Win  ;
//                break;
            case BRBack:
                NextUi= WIN_System_Win  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_System_BR()
{
    return Register_NewWin(WIN_System_BR,HmiCreate_SystemBR,HmiPaint_SystemBR,HmiNotify_SystemBR);
}
