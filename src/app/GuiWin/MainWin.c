//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "Info/GuiInfo.h"
#include "Info/WinSwitch.h"

void GuiResponseInit(void);

#define CN_TIMER_CTRL               1

static u32 TimerCnt = 0;

//定时器调转页面不与点击页面冲突
static CtrlTimerMachine en_StatueMachine=CtrlNull;
CtrlTimerMachine GetStatueMachine(void)
{
    return en_StatueMachine;
}
void SetStatueMachine(CtrlTimerMachine s)
{
    en_StatueMachine=s;
}
HWND tg_pMainWinHwn;
struct WinTimer *tg_pCtrlTimer;

enum MainWinWidgeId{
    ENUM_BACKGROUND,
    EnumInterface1,
    EnumInterface2,
    EnumInterface3,
    EnumInterface4,
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MainWinCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [EnumInterface1] = {
        .position = {352/LCDW_32,180/LCDH_32,(352+321)/LCDW_32,(180+44)/LCDH_32},
        .name = "MainInterface1",
        .type = widget_type_picture,
        .userParam = BMP_MainInterface1,
    },
    [EnumInterface2] = {
        .position = {433/LCDW_32,298/LCDH_32,(433+157)/LCDW_32,(298+44)/LCDH_32},
        .name = "MainInterface2",
        .type = widget_type_picture,
        .userParam = BMP_MainInterface2,
    },
    [EnumInterface3] = {
        .position = {441/LCDW_32,350/LCDH_32,(441+147)/LCDW_32,(350+27)/LCDH_32},
        .name = "MainInterface3",
        .type = widget_type_picture,
        .userParam = BMP_MainInterface3,
    },
    [EnumInterface4] = {
        .position = {753/LCDW_32,555/LCDH_32,(753+241)/LCDW_32,(555+31)/LCDH_32},
        .name = "MainInterface4",
        .type = widget_type_picture,
        .userParam = BMP_MainInterface4,
    },
};

//参数大于0不刷新控件，参数等于2给定时器用
bool_t Refresh_GuiWin(u32 param2)
{
    return Refresh_SwitchWIn(tg_pMainWinHwn, param2);
}
bool_t Jump_GuiWin(enum WinType nextInterface)
{
    return Jump_Interface(tg_pMainWinHwn,nextInterface);
}

HWND Get_WindowsHwnd()
{
    return tg_pMainWinHwn;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_MainWin(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
         //   {MSG_PAINT,BmpButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_MainWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (MainWinCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,MainWinCfgTab[i].userParam);
                    GDD_FillRect(hdc,&MainWinCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(MainWinCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,MainWinCfgTab[i].position.left,\
                                MainWinCfgTab[i].position.top,bmp);
                    }
                    break;
                default:    break;
            }
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_MainWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    return NextUi;
}


void IpConnectTimer(void)
{
    en_StatueMachine = CtrlConnectTest;
    TimerCnt=0;
    GDD_ResetTimer(tg_pCtrlTimer,800);
}

void SerialSaveTimer(void)
{
    en_StatueMachine = CtrlSerialSave;
    TimerCnt=0;
    GDD_ResetTimer(tg_pCtrlTimer,800);
}

void PasswordSaveTimer(void)
{
    en_StatueMachine = CtrlPasswordSave;
    TimerCnt=0;
    GDD_ResetTimer(tg_pCtrlTimer,800);
}

static bool_t HmiTimer(struct WindowMsg *pMsg)
{
    HWND hwnd;
    hwnd=pMsg->hwnd;

    switch(en_StatueMachine)
    {
        case CtrlConnectTest:
            TimerCnt ++;
            if(1 == TimerCnt)
            {
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Prompt_Box, 1);
            }
            else if(3 <= TimerCnt)
            {
                TimerCnt=0;
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Ip_Edit, 2);
                GDD_StopTimer(tg_pCtrlTimer);
            }
            break;
        case CtrlSerialSave:
            TimerCnt ++;
            if(1 == TimerCnt)
            {
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Prompt_Box, 1);
            }
            else if(3 <= TimerCnt)
            {
                TimerCnt=0;
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Serial_Edit, 2);
                GDD_StopTimer(tg_pCtrlTimer);
            }
            break;
        case CtrlPasswordSave:
            TimerCnt ++;
            if(1 == TimerCnt)
            {
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_Prompt_Box, 1);
            }
            else if(3 <= TimerCnt)
            {
                TimerCnt=0;
                GDD_PostMessage(hwnd, MSG_REFRESH_UI,WIN_System_Password, 2);
                GDD_StopTimer(tg_pCtrlTimer);
            }
            break;
        default:
            break;
    }
    return true;
}

void Main_GuiWin(void)
{
    //图片初始化
    DisplayPicture_Init();

    //界面注册
    Init_WinSwitch();

    //按键响应的业务处理
    GuiResponseInit();

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTable[] =
    {
        {MSG_CREATE,HmiCreate_MainWin},         //主窗口创建消息
        {MSG_PAINT,HmiPaint_MainWin},           //绘制消息
        {MSG_NOTIFY,HmiNotify_Trans},        //子控件发来的通知消息
        {MSG_REFRESH_UI,HmiRefresh},               //刷新窗口消息
        {MSG_TIMER,HmiTimer},                   //定时器消息响应函数
    };

    static struct MsgTableLink  s_gBmpDemoMsgLink;

    s_gBmpDemoMsgLink.MsgNum = sizeof(s_gBmpMsgTable) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLink.myTable = (struct MsgProcTable *)&s_gBmpMsgTable;
    tg_pMainWinHwn = GDD_CreateGuiApp("GUIapp",&s_gBmpDemoMsgLink, 0x2000, CN_WINBUF_PARENT,0);
    tg_pCtrlTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CTRL,100);
}

int Register_Main_WIN()
{
    return Register_NewWin(WIN_Main_WIN,HmiCreate_MainWin,HmiPaint_MainWin,HmiNotify_MainWin);
}


#include "shell.h"

void jumpwin(char *param)
{
    u32 number=atoi(param);
    if(number>14)
    {
        Jump_GuiWin(WIN_Home_Page);
    }
    else
    {
        Jump_GuiWin(number);
    }
}
void xf_ping(char *param)
{
    s32 t = xq_ping(param);
    printf("xfping: %dms", t);
}
void reset_timer(void)
{
    en_StatueMachine = CtrlConnectTest;
    GDD_ResetTimer(tg_pCtrlTimer,5000);
}
void stop_timer(void)
{
    en_StatueMachine = CtrlNull;
    GDD_StopTimer(tg_pCtrlTimer);
}
ADD_TO_ROUTINE_SHELL(gui, jumpwin, "开始测试线程");
ADD_TO_ROUTINE_SHELL(xfping, xf_ping, "ping服务器");
ADD_TO_ROUTINE_SHELL(rtimer, reset_timer, "开启定时器");
ADD_TO_ROUTINE_SHELL(stimer, stop_timer, "关闭定时器");
