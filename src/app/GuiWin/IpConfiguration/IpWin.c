//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdlib.h>
#include <time.h>

#include "stdint.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "project_config.h"     //本文件由IDE中配置界面生成，存放在APP的工程目录中。
                                //允许是个空文件，所有配置将按默认值配置。
#include <iboot_Info.h>
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum IpWinWidgeId{
    IP_BACKGROUND,

    IpTopBox,
    IpPageText,
    IpRunningState0,
    IpPortConfiguration0,
    IpServerConfiguration1,
    IpSystemSettings0,

    IpBoxTop,
    IpBox1,
    IpBox2,
    IpBox3,
    IpBox4,
    IpBox5,

    IpConnect1,
    IpConnect2,
    IpConnect3,
    IpConnect4,
    IpConnect5,
    IpEdit1,
    IpEdit2,
    IpEdit3,
    IpEdit4,
    IpEdit5,

    IpBack,

    CardAddress,
    Gateway,
    Mask,
    Mac,
    IpAddress1,
    IpPort1,
    IpAddress2,
    IpPort2,
    IpAddress3,
    IpPort3,
    IpAddress4,
    IpPort4,
    IpAddress5,
    IpPort5,


    IP_MAXNUM,//总数量
};

//==================================config======================================
static const  struct GUIINFO IpWinCfgTab[IP_MAXNUM] =
{
    [IP_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [IpTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [IpPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [IpRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [IpPortConfiguration0] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration0,
    },
    [IpServerConfiguration1] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration1,
    },
    [IpSystemSettings0] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings0,
    },

    [IpBoxTop] = {
        .position = {0/LCDW_32,86/LCDH_32,(0+1024)/LCDW_32,(86+162)/LCDH_32},
        .name = "box_top",
        .type = widget_type_picture,
        .userParam = BMP_Gateway,
    },
    [IpBox1] = {
        .position = {0/LCDW_32,243/LCDH_32,(0+324)/LCDW_32,(243+140)/LCDH_32},
        .name = "box1",
        .type = widget_type_picture,
        .userParam = BMP_IpBox1,
    },
    [IpBox2] = {
        .position = {354/LCDW_32,243/LCDH_32,(354+318)/LCDW_32,(243+140)/LCDH_32},
        .name = "box2",
        .type = widget_type_picture,
        .userParam = BMP_IpBox2,
    },
    [IpBox3] = {
        .position = {700/LCDW_32,243/LCDH_32,(700+324)/LCDW_32,(243+140)/LCDH_32},
        .name = "box3",
        .type = widget_type_picture,
        .userParam = BMP_IpBox3,
    },
    [IpBox4] = {
        .position = {0/LCDW_32,382/LCDH_32,(0+324)/LCDW_32,(382+140)/LCDH_32},
        .name = "box4",
        .type = widget_type_picture,
        .userParam = BMP_IpBox4,
    },
    [IpBox5] = {
        .position = {354/LCDW_32,382/LCDH_32,(354+318)/LCDW_32,(382+140)/LCDH_32},
        .name = "box5",
        .type = widget_type_picture,
        .userParam = BMP_IpBox5,
    },

    [IpConnect1] = {
        .position = {235/LCDW_32,266/LCDH_32,(235+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "cbutton1",
        .type = widget_type_groupbutton,
        .userParam = 0,
    },
    [IpConnect2] = {
        .position = {583/LCDW_32,266/LCDH_32,(583+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "cbutton2",
        .type = widget_type_groupbutton,
        .userParam = 1,
    },
    [IpConnect3] = {
        .position = {929/LCDW_32,266/LCDH_32,(929+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "cbutton3",
        .type = widget_type_groupbutton,
        .userParam = 2,
    },
    [IpConnect4] = {
        .position = {235/LCDW_32,405/LCDH_32,(235+30)/LCDW_32,(405+30)/LCDH_32},
        .name = "cbutton4",
        .type = widget_type_groupbutton,
        .userParam = 3,
    },
    [IpConnect5] = {
        .position = {583/LCDW_32,405/LCDH_32,(583+30)/LCDW_32,(405+30)/LCDH_32},
        .name = "cbutton5",
        .type = widget_type_groupbutton,
        .userParam = 4,
    },

    [IpEdit1] = {
        .position = {275/LCDW_32,266/LCDH_32,(275+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "ebutton1",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [IpEdit2] = {
        .position = {623/LCDW_32,266/LCDH_32,(623+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "ebutton2",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [IpEdit3] = {
        .position = {969/LCDW_32,266/LCDH_32,(969+30)/LCDW_32,(266+30)/LCDH_32},
        .name = "ebutton3",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [IpEdit4] = {
        .position = {275/LCDW_32,405/LCDH_32,(275+30)/LCDW_32,(405+30)/LCDH_32},
        .name = "ebutton4",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [IpEdit5] = {
        .position = {623/LCDW_32,405/LCDH_32,(623+30)/LCDW_32,(405+30)/LCDH_32},
        .name = "ebutton5",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [IpBack] = {
        .position = {916/LCDW_32,532/LCDH_32,(916+80)/LCDW_32,(532+36)/LCDH_32},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_LogBackButton,
    },

    [CardAddress] = {
        .position = {49/LCDW_32,136/LCDH_32,(49+250)/LCDW_32,(136+21)/LCDH_32},
        .name = "网卡地址:",
        .type = widget_type_text,
        .userParam = 1,
    },
    [Gateway] = {
        .position = {512/LCDW_32,136/LCDH_32,(512+250)/LCDW_32,(136+21)/LCDH_32},
        .name = "默认网关:",
        .type = widget_type_text,
        .userParam = 2,
    },
    [Mask] = {
        .position = {49/LCDW_32,184/LCDH_32,(49+250)/LCDW_32,(184+21)/LCDH_32},
        .name = "子网掩码:",
        .type = widget_type_text,
        .userParam = 3,
    },
    [Mac] = {
        .position = {512/LCDW_32,184/LCDH_32,(512+250)/LCDW_32,(184+21)/LCDH_32},
        .name = "MAC地址:",
        .type = widget_type_text,
        .userParam = 4,
    },

    [IpAddress1] = {
        .position = {30/LCDW_32,300/LCDH_32,(30+250)/LCDW_32,(300+20)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_itext1,
        .userParam = Gui_IpAddress,
    },
    [IpPort1] = {
        .position = {30/LCDW_32,330/LCDH_32,(30+250)/LCDW_32,(330+20)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext1,
        .userParam = Gui_Port,
    },

    [IpAddress2] = {
        .position = {378/LCDW_32,300/LCDH_32,(378+250)/LCDW_32,(300+20)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_itext2,
        .userParam = Gui_IpAddress,
    },
    [IpPort2] = {
        .position = {378/LCDW_32,330/LCDH_32,(378+250)/LCDW_32,(330+20)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext2,
        .userParam = Gui_Port,
    },

    [IpAddress3] = {
        .position = {724/LCDW_32,300/LCDH_32,(724+250)/LCDW_32,(300+20)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_itext3,
        .userParam = Gui_IpAddress,
    },
    [IpPort3] = {
        .position = {724/LCDW_32,330/LCDH_32,(724+250)/LCDW_32,(330+20)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext3,
        .userParam = Gui_Port,
    },

    [IpAddress4] = {
        .position = {30/LCDW_32,439/LCDH_32,(30+250)/LCDW_32,(439+20)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_itext4,
        .userParam = Gui_IpAddress,
    },
    [IpPort4] = {
        .position = {30/LCDW_32,469/LCDH_32,(30+250)/LCDW_32,(469+20)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext4,
        .userParam = Gui_Port,
    },

    [IpAddress5] = {
        .position = {378/LCDW_32,439/LCDH_32,(378+250)/LCDW_32,(439+20)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_itext5,
        .userParam = Gui_IpAddress,
    },
    [IpPort5] = {
        .position = {378/LCDW_32,469/LCDH_32,(378+250)/LCDW_32,(469+20)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext5,
        .userParam = Gui_Port,
    },

};

static char NetworkCardAddress[32]="192.168.1.166";
static char DefaultGateway[32]="192.168.1.0";
static char SubnetMask[32]="255.255.255.0";
static char MacAddress[32]="00:00:00:00";

void IpWinInit(void)
{
    u8 *mac_result=NetDevGetMac(NetDevGet("STM32F7_ETH"));
    snprintf(MacAddress,sizeof(MacAddress),"%02x:%02x:%02x:%02x:%02x:%02x",mac_result[0],mac_result[1],mac_result[2],mac_result[3],mac_result[4],mac_result[5]);
//    printf("MacAddress is %s\r\n\r\n",MacAddress);

    struct RoutItem4 *ss=NetDev_GetIPv4RoutEntry(NetDevGet("STM32F7_ETH"));
    char *ip_result=inet_ntoa(__Router_PickIPv4(ss));

    snprintf(NetworkCardAddress,sizeof(NetworkCardAddress),"%s",ip_result);
//    printf("NetworkCardAddress is %s\r\n",ip_result);

}

//按钮控件创建函数
static bool_t  IpWinButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }
        else if(buttoninfo->type == widget_type_groupbutton)
        {
            if(1 == GeticStatues(buttoninfo->userParam))
            {
                bmp = Get_BmpBuf(BMP_Connectioning);
            }
            else
            {
                bmp = Get_BmpBuf(BMP_Disconnect);
            }
            GDD_DrawBMP(hdc,0,0,bmp);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_IpWin(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,IpWinButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<IP_MAXNUM;i++)
    {
        switch (IpWinCfgTab[i].type)
        {
            case  widget_type_button :
            case  widget_type_groupbutton :
            {

                Widget_CreateButton(IpWinCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 IpWinCfgTab[i].position.left, IpWinCfgTab[i].position.top,\
                                 GDD_RectW(&IpWinCfgTab[i].position),GDD_RectH(&IpWinCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&IpWinCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}
extern struct GuiIp GuiIpConfig[5];
//绘制消息处函数
static bool_t HmiPaint_IpWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    char buf[56]={0};
    u32 addr=0;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));

        for(int i=0;i<IP_MAXNUM;i++)
        {
            switch (IpWinCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,IpWinCfgTab[i].userParam);
                    GDD_FillRect(hdc,&IpWinCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(IpWinCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,IpWinCfgTab[i].position.left,\
                                IpWinCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :

                    memset(buf,0,56);

                    if(IpWinCfgTab[i].userParam == 1)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,NetworkCardAddress);
                    }
                    else if(IpWinCfgTab[i].userParam == 2)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,DefaultGateway);
                    }
                    else if(IpWinCfgTab[i].userParam == 3)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,SubnetMask);
                    }
                    else if(IpWinCfgTab[i].userParam == 4)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,MacAddress);
                    }

                    GDD_DrawText(hdc,buf,-1,&IpWinCfgTab[i].position,DT_VCENTER|DT_LEFT);

                    break;

                case  widget_type_itext1 :
                case  widget_type_itext2 :
                case  widget_type_itext3 :
                case  widget_type_itext4 :
                case  widget_type_itext5 :

                    addr=IpWinCfgTab[i].type-widget_type_itext1;

                    memset(buf,0,56);

                    if(IpWinCfgTab[i].userParam == Gui_IpAddress)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,GuiIpConfig[addr].gip_address);
                    }
                    else if(IpWinCfgTab[i].userParam == Gui_Port)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",IpWinCfgTab[i].name,GuiIpConfig[addr].gport);
                    }

                    GDD_DrawText(hdc,buf,-1,&IpWinCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_IpWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case IpRunningState0:
                NextUi= WIN_Home_Page;
                break;
            case IpPortConfiguration0:
                NextUi= WIN_Serial_Win  ;
                break;
//            case IpServerConfiguration1:
//                NextUi= WIN_Ip_Win  ;
//                break;
            case IpSystemSettings0:
                NextUi= WIN_System_Win  ;
                break;
            case IpBack:
                NextUi= WIN_Home_Page  ;
                break;
            case IpEdit1:
            case IpEdit2:
            case IpEdit3:
            case IpEdit4:
            case IpEdit5:
                SetIpNumb(id-IpEdit1);
                NextUi= WIN_Ip_Edit  ;
                break;
            case IpConnect1:
            case IpConnect2:
            case IpConnect3:
            case IpConnect4:
            case IpConnect5:
                if((u32)(id-IpConnect1)!=GetConnectNumb())
                {
                    tra_breakout();
                    SetTcpUrl(id-IpConnect1);
                    SetConnectNumb(id-IpConnect1);
                }
                NextUi= WIN_Ip_Win  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Ip_Win()
{
    return Register_NewWin(WIN_Ip_Win,HmiCreate_IpWin,HmiPaint_IpWin,HmiNotify_IpWin);
}
