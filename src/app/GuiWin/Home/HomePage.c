//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include "string.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum HomePageWidgeId{
    HOME_BACKGROUND,

    HomeTopBox,
    HomePageText,
    HomeRunningState1,
    HomePortConfiguration0,
    HomeServerConfiguration0,
    HomeSystemSettings0,

    HomeSerialStatusBox,
    HomeServerStatusBox,
    HomeNetworkStatusBox,
    HomePowerStatusBox,
    HomeSystemInformationBox,

    ComButton1,
    ComButton2,
    ComButton3,
    ComButton4,
    ComButton5,
    ServerButton1,
    ServerButton2,
    ServerButton3,
    ServerButton4,
    ServerButton5,

    ComTextA1,
    ComTextB1,
    ComTextA2,
    ComTextB2,
    ComTextA3,
    ComTextB3,
    ComTextA4,
    ComTextB4,
    ComTextA5,
    ComTextB5,
    ServerTextA1,
    ServerTextB1,
    ServerTextA2,
    ServerTextB2,
    ServerTextA3,
    ServerTextB3,
    ServerTextA4,
    ServerTextB4,
    ServerTextA5,
    ServerTextB5,
    MetworkState,
    MainElectricity,
    PrepareElectricity,
    SysInformation1,
    SysInformation2,
    SysInformation3,
    SysInformation4,
    SysInformation5,

    HOME_MAXNUM,//总数量
};

static char EquipIdent[32]={'S','T','M','3','2','F','7',0};      //设备标识
static char VersionNumber[32]={'0','.','0','.','0',0};          //版本号
void SetHomeInfo(char *equipment,int version1,int version2,int version3)
{
    snprintf(EquipIdent,sizeof(EquipIdent),"设备标识:%s",equipment);
    snprintf(VersionNumber,sizeof(VersionNumber),"固定版本号:%d.%d.%d",version1,version2,version3);
}
//==================================config======================================
static const  struct GUIINFO HomePageCfgTab[HOME_MAXNUM] =
{
    [HOME_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [HomeTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [HomePageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+258)/LCDW_32,(17+37)/LCDH_32},
        .name = "text",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [HomeRunningState1] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState1,
    },
    [HomePortConfiguration0] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration0,
    },
    [HomeServerConfiguration0] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration0,
    },
    [HomeSystemSettings0] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings0,
    },

    [HomeSerialStatusBox] = {
        .position = {16/LCDW_32,81/LCDH_32,(16+367)/LCDW_32,(81+517)/LCDH_32},
        .name = "box1",
        .type = widget_type_picture,
        .userParam = BMP_SerialStatusBox,
    },
    [HomeServerStatusBox] = {
        .position = {390/LCDW_32,81/LCDH_32,(390+367)/LCDW_32,(81+517)/LCDH_32},
        .name = "box2",
        .type = widget_type_picture,
        .userParam = BMP_ServerStatusBox,
    },
    [HomeNetworkStatusBox] = {
        .position = {761/LCDW_32,81/LCDH_32,(761+247)/LCDW_32,(81+112)/LCDH_32},
        .name = "box3",
        .type = widget_type_picture,
        .userParam = BMP_NetworkStatusBox,
    },
    [HomePowerStatusBox] = {
        .position = {761/LCDW_32,190/LCDH_32,(761+247)/LCDW_32,(190+123)/LCDH_32},
        .name = "box4",
        .type = widget_type_picture,
        .userParam = BMP_PowerStatusBox,
    },
    [HomeSystemInformationBox] = {
        .position = {761/LCDW_32,315/LCDH_32,(761+247)/LCDW_32,(315+283)/LCDH_32},
        .name = "box5",
        .type = widget_type_picture,
        .userParam = BMP_SystemInformationBox,
    },

    [ComButton1] = {
        .position = {323/LCDW_32,148/LCDH_32,(323+30)/LCDW_32,(148+30)/LCDH_32},
        .name = "ComButton1",
        .type = widget_type_serialconnect,
        .userParam = Bmp_NULL,
    },
    [ComButton2] = {
        .position = {323/LCDW_32,238/LCDH_32,(323+30)/LCDW_32,(238+30)/LCDH_32},
        .name = "ComButton2",
        .type = widget_type_serialconnect,
        .userParam = Bmp_NULL,
    },
    [ComButton3] = {
        .position = {323/LCDW_32,328/LCDH_32,(323+30)/LCDW_32,(328+30)/LCDH_32},
        .name = "ComButton3",
        .type = widget_type_serialconnect,
        .userParam = Bmp_NULL,
    },
    [ComButton4] = {
        .position = {323/LCDW_32,418/LCDH_32,(323+30)/LCDW_32,(418+30)/LCDH_32},
        .name = "ComButton4",
        .type = widget_type_serialconnect,
        .userParam = Bmp_NULL,
    },
    [ComButton5] = {
        .position = {323/LCDW_32,508/LCDH_32,(323+30)/LCDW_32,(508+30)/LCDH_32},
        .name = "ComButton5",
        .type = widget_type_serialconnect,
        .userParam = Bmp_NULL,
    },

    [ServerButton1] = {
        .position = {698/LCDW_32,148/LCDH_32,(698+30)/LCDW_32,(148+30)/LCDH_32},
        .name = "ServerButton1",
        .type = widget_type_ipconnect,
        .userParam = Bmp_NULL,
    },
    [ServerButton2] = {
        .position = {698/LCDW_32,238/LCDH_32,(698+30)/LCDW_32,(238+30)/LCDH_32},
        .name = "ServerButton2",
        .type = widget_type_ipconnect,
        .userParam = Bmp_NULL,
    },
    [ServerButton3] = {
        .position = {698/LCDW_32,328/LCDH_32,(698+30)/LCDW_32,(328+30)/LCDH_32},
        .name = "ServerButton3",
        .type = widget_type_ipconnect,
        .userParam = Bmp_NULL,
    },
    [ServerButton4] = {
        .position = {698/LCDW_32,418/LCDH_32,(698+30)/LCDW_32,(418+30)/LCDH_32},
        .name = "ServerButton4",
        .type = widget_type_ipconnect,
        .userParam = Bmp_NULL,
    },
    [ServerButton5] = {
        .position = {698/LCDW_32,508/LCDH_32,(698+30)/LCDW_32,(508+30)/LCDH_32},
        .name = "ServerButton5",
        .type = widget_type_ipconnect,
        .userParam = Bmp_NULL,
    },

    [ComTextA1] = {
        .position = {51/LCDW_32,142/LCDH_32,(51+251)/LCDW_32,(142+21)/LCDH_32},
        .name = "无",
        .type = widget_type_ctext1,
        .userParam = Gui_SysType,
    },
    [ComTextB1] = {
        .position = {51/LCDW_32,170/LCDH_32,(51+251)/LCDW_32,(170+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext1,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextA2] = {
        .position = {51/LCDW_32,232/LCDH_32,(51+251)/LCDW_32,(232+21)/LCDH_32},
        .name = "无",
        .type = widget_type_ctext2,
        .userParam = Gui_SysType,
    },
    [ComTextB2] = {
        .position = {51/LCDW_32,260/LCDH_32,(51+251)/LCDW_32,(260+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext2,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextA3] = {
        .position = {51/LCDW_32,322/LCDH_32,(51+251)/LCDW_32,(322+21)/LCDH_32},
        .name = "无",
        .type = widget_type_ctext3,
        .userParam = Gui_SysType,
    },
    [ComTextB3] = {
        .position = {51/LCDW_32,350/LCDH_32,(51+251)/LCDW_32,(350+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext3,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextA4] = {
        .position = {51/LCDW_32,412/LCDH_32,(51+251)/LCDW_32,(412+21)/LCDH_32},
        .name = "无",
        .type = widget_type_ctext4,
        .userParam = Gui_SysType,
    },
    [ComTextB4] = {
        .position = {51/LCDW_32,440/LCDH_32,(51+251)/LCDW_32,(440+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext4,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextA5] = {
        .position = {51/LCDW_32,502/LCDH_32,(51+251)/LCDW_32,(502+21)/LCDH_32},
        .name = "无",
        .type = widget_type_ctext5,
        .userParam = Gui_SysType,
    },
    [ComTextB5] = {
        .position = {51/LCDW_32,530/LCDH_32,(51+251)/LCDW_32,(530+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext5,
        .userParam = Gui_EquipmentAddress,
    },

    [ServerTextA1] = {
        .position = {425/LCDW_32,142/LCDH_32,(425+251)/LCDW_32,(142+21)/LCDH_32},
        .name = "IP:",
        .type = widget_type_itext1,
        .userParam = Gui_IpAddress,
    },
    [ServerTextB1] = {
        .position = {425/LCDW_32,170/LCDH_32,(425+251)/LCDW_32,(170+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext1,
        .userParam = Gui_Port,
    },
    [ServerTextA2] = {
        .position = {425/LCDW_32,232/LCDH_32,(425+251)/LCDW_32,(232+21)/LCDH_32},
        .name = "IP:",
        .type = widget_type_itext2,
        .userParam = Gui_IpAddress,
    },
    [ServerTextB2] = {
        .position = {425/LCDW_32,260/LCDH_32,(425+251)/LCDW_32,(260+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext2,
        .userParam = Gui_Port,
    },
    [ServerTextA3] = {
        .position = {425/LCDW_32,322/LCDH_32,(425+251)/LCDW_32,(322+21)/LCDH_32},
        .name = "IP:",
        .type = widget_type_itext3,
        .userParam = Gui_IpAddress,
    },
    [ServerTextB3] = {
        .position = {425/LCDW_32,350/LCDH_32,(425+251)/LCDW_32,(350+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext3,
        .userParam = Gui_Port,
    },
    [ServerTextA4] = {
        .position = {425/LCDW_32,412/LCDH_32,(425+251)/LCDW_32,(412+21)/LCDH_32},
        .name = "IP:",
        .type = widget_type_itext4,
        .userParam = Gui_IpAddress,
    },
    [ServerTextB4] = {
        .position = {425/LCDW_32,440/LCDH_32,(425+251)/LCDW_32,(440+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext4,
        .userParam = Gui_Port,
    },
    [ServerTextA5] = {
        .position = {425/LCDW_32,502/LCDH_32,(425+251)/LCDW_32,(502+21)/LCDH_32},
        .name = "IP:",
        .type = widget_type_itext5,
        .userParam = Gui_IpAddress,
    },
    [ServerTextB5] = {
        .position = {425/LCDW_32,530/LCDH_32,(425+251)/LCDW_32,(530+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_itext5,
        .userParam = Gui_Port,
    },
    [MetworkState] = {
        .position = {783/LCDW_32,127/LCDH_32,(783+210)/LCDW_32,(127+21)/LCDH_32},
        .name = "已连接到有线网络",
        .type = widget_type_text,
        .userParam = 0,
    },
    [MainElectricity] = {
        .position = {783/LCDW_32,231/LCDH_32,(783+210)/LCDW_32,(231+21)/LCDH_32},
        .name = "主电    使用中",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [PrepareElectricity] = {
        .position = {783/LCDW_32,263/LCDH_32,(783+210)/LCDW_32,(263+21)/LCDH_32},
        .name = "备电    使用中",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SysInformation1] = {
        .position = {783/LCDW_32,363/LCDH_32,(783+210)/LCDW_32,(363+21)/LCDH_32},
        .name = EquipIdent,
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SysInformation2] = {
        .position = {783/LCDW_32,393/LCDH_32,(783+210)/LCDW_32,(393+21)/LCDH_32},
        .name = VersionNumber,
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SysInformation3] = {
        .position = {783/LCDW_32,423/LCDH_32,(783+210)/LCDW_32,(423+21)/LCDH_32},
        .name = "上次备份时间:无",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SysInformation4] = {
        .position = {783/LCDW_32,453/LCDH_32,(783+210)/LCDW_32,(453+21)/LCDH_32},
        .name = "USB存储设备:未连接",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SysInformation5] = {
        .position = {783/LCDW_32,483/LCDH_32,(783+210)/LCDW_32,(483+21)/LCDH_32},
        .name = "本机存储:YAF",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },

};

//按钮控件创建函数
static bool_t  HomePageButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_HomePage(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,HomePageButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<HOME_MAXNUM;i++)
    {
        switch (HomePageCfgTab[i].type)
        {
            case  widget_type_button :
            {

                Widget_CreateButton(HomePageCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 HomePageCfgTab[i].position.left, HomePageCfgTab[i].position.top,\
                                 GDD_RectW(&HomePageCfgTab[i].position),GDD_RectH(&HomePageCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&HomePageCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}

extern struct GuiSerial GuiSerialConfig[5];
extern struct GuiIp GuiIpConfig[5];
//绘制消息处函数
static bool_t HmiPaint_HomePage(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    char buf[56]={0};
    u32 addr=0;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));

        for(int i=0;i<HOME_MAXNUM;i++)
        {
            switch (HomePageCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,HomePageCfgTab[i].userParam);
                    GDD_FillRect(hdc,&HomePageCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(HomePageCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,HomePageCfgTab[i].position.left,\
                                HomePageCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :
                    if(HomePageCfgTab[i].userParam == 0)
                    {
                        if(!GetPingFlag())
                        {
                            GDD_DrawText(hdc,"网络未连接",-1,&HomePageCfgTab[i].position,DT_VCENTER|DT_LEFT);
                        }
                        else
                        {
                            GDD_DrawText(hdc,HomePageCfgTab[i].name,-1,&HomePageCfgTab[i].position,DT_VCENTER|DT_LEFT);
                        }
                    }
                    else
                    {
                        GDD_DrawText(hdc,HomePageCfgTab[i].name,-1,&HomePageCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    }
                    break;

                case widget_type_serialconnect:
                    if(1 == GetscStatues(i-ComButton1))
                    {
                        bmp = Get_BmpBuf(BMP_Connectioning);
                    }
                    else
                    {
                        bmp = Get_BmpBuf(BMP_Disconnect);
                    }
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,HomePageCfgTab[i].position.left,\
                                HomePageCfgTab[i].position.top,bmp);
                    }
                    break;
                case widget_type_ipconnect:
                    if(1 == GeticStatues(i-ServerButton1))
                    {
                        bmp = Get_BmpBuf(BMP_Connectioning);
                    }
                    else
                    {
                        bmp = Get_BmpBuf(BMP_Disconnect);
                    }
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,HomePageCfgTab[i].position.left,\
                                HomePageCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_ctext1 :
                case  widget_type_ctext2 :
                case  widget_type_ctext3 :
                case  widget_type_ctext4 :
                case  widget_type_ctext5 :

                    addr=HomePageCfgTab[i].type-widget_type_ctext1;

                    memset(buf,0,56);

                    if(HomePageCfgTab[i].userParam == Gui_SysType)
                    {
                        if(GuiSerialConfig[addr].gsys_type[0] == 0)
                        {
                            snprintf(buf,sizeof(buf),"%s",HomePageCfgTab[i].name);
                        }
                        else
                        {
                            snprintf(buf,sizeof(buf),"%s",GuiSerialConfig[addr].gsys_type);
                        }
                    }
                    else if(HomePageCfgTab[i].userParam == Gui_EquipmentAddress)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",HomePageCfgTab[i].name,GuiSerialConfig[addr].gequipment_address);
                    }

                    GDD_DrawText(hdc,buf,-1,&HomePageCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                case  widget_type_itext1 :
                case  widget_type_itext2 :
                case  widget_type_itext3 :
                case  widget_type_itext4 :
                case  widget_type_itext5 :

                    addr=HomePageCfgTab[i].type-widget_type_itext1;

                    memset(buf,0,56);

                    if(HomePageCfgTab[i].userParam == Gui_IpAddress)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",HomePageCfgTab[i].name,GuiIpConfig[addr].gip_address);
                    }
                    else if(HomePageCfgTab[i].userParam == Gui_Port)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",HomePageCfgTab[i].name,GuiIpConfig[addr].gport);
                    }

                    GDD_DrawText(hdc,buf,-1,&HomePageCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType LoginJump=WIN_NotChange;
void SetLoginJump(enum WinType page) //记录登录成功所要跳转的页面
{
    LoginJump=page;
}
enum WinType GetLoginJump(void) //登录成功所要跳转的页面
{
    return LoginJump;
}
static enum WinType HmiNotify_HomePage(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
//            case HomeRunningState1:
//                NextUi= WIN_Home_Page;
//                break;
            case HomePortConfiguration0:
                SetLoginJump(WIN_Serial_Win);
                NextUi= WIN_Admin_Login  ;
                break;
            case HomeServerConfiguration0:
                SetLoginJump(WIN_Ip_Win);
                NextUi= WIN_Admin_Login  ;
                break;
            case HomeSystemSettings0:
                SetLoginJump(WIN_System_Win);
                NextUi= WIN_Admin_Login  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Home_Page()
{
    return Register_NewWin(WIN_Home_Page,HmiCreate_HomePage,HmiPaint_HomePage,HmiNotify_HomePage);
}
