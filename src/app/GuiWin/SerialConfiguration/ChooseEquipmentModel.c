//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum EquipmentModelWidgeId{
    SERIAL_BACKGROUND,

    SerialChooseBackground,
    SerialText,

    EquipmentModel1,
    EquipmentModel2,
    EquipmentModel3,
    EquipmentModel4,
    EquipmentModel5,
    EquipmentModel6,
    EquipmentModel7,

    Close,
    Save,


    SERIAL_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO EquipmentModelCfgTab[SERIAL_MAXNUM] =
{
    [SERIAL_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },

    [SerialChooseBackground] = {
        .position = {56/LCDW_32,41/LCDH_32,(56+568)/LCDW_32,(41+282)/LCDH_32},
        .name = "choose_background",
        .type = widget_type_picture,
        .userParam = BMP_ChooseBackground,
    },

    [SerialText] = {
        .position = {75/LCDW_32,47/LCDH_32,(75+160)/LCDW_32,(47+34)/LCDH_32},
        .name = "请选择设备型号",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },

    [EquipmentModel1] = {
        .position = {75/LCDW_32,91/LCDH_32,(75+160)/LCDW_32,(91+34)/LCDH_32},
        .name = "JB-TZJERNB2121",
        .type = widget_type_groupbutton,
        .userParam = 1,
    },
    [EquipmentModel2] = {
        .position = {251/LCDW_32,91/LCDH_32,(251+160)/LCDW_32,(91+34)/LCDH_32},
        .name = "JB-TZJERNB2122",
        .type = widget_type_groupbutton,
        .userParam = 2,
    },
    [EquipmentModel3] = {
        .position = {75/LCDW_32,135/LCDH_32,(75+160)/LCDW_32,(135+34)/LCDH_32},
        .name = "JB-TZJERNB2123",
        .type = widget_type_groupbutton,
        .userParam = 3,
    },
    [EquipmentModel4] = {
        .position = {251/LCDW_32,135/LCDH_32,(251+160)/LCDW_32,(135+34)/LCDH_32},
        .name = "JB-TZJERNB214",
        .type = widget_type_groupbutton,
        .userParam = 4,
    },
    [EquipmentModel5] = {
        .position = {75/LCDW_32,179/LCDH_32,(75+160)/LCDW_32,(179+34)/LCDH_32},
        .name = "JB-TZJERNB215",
        .type = widget_type_groupbutton,
        .userParam = 5,
    },
    [EquipmentModel6] = {
        .position = {251/LCDW_32,179/LCDH_32,(251+160)/LCDW_32,(179+34)/LCDH_32},
        .name = "JB-TZJERNB216",
        .type = widget_type_groupbutton,
        .userParam = 6,
    },
    [EquipmentModel7] = {
        .position = {75/LCDW_32,223/LCDH_32,(75+160)/LCDW_32,(223+34)/LCDH_32},
        .name = "JB-TZJERNB217",
        .type = widget_type_groupbutton,
        .userParam = 7,
    },

     [Close] = {
         .position = {575/LCDW_32,61/LCDH_32,(575+30)/LCDW_32,(61+30)/LCDH_32},
         .name = "取消",
         .type = widget_type_button,
         .userParam = BMP_ChooseClose,
     },
     [Save] = {
         .position = {320/LCDW_32,261/LCDH_32,(320+40)/LCDW_32,(261+40)/LCDH_32},
         .name = "保存",
         .type = widget_type_button,
         .userParam = BMP_ChooseSave,
     },


};

static u32 SyscModelFlag=0;

//按钮控件创建函数
static bool_t  EquipmentModelButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_groupbutton)
        {
            GDD_SetFillColor(hdc,RGB(255,255,255));
            GDD_FillRect(hdc,&rc);
            GDD_SetDrawColor(hdc,RGB(0,198,255));
            GDD_DrawRect(hdc,&rc);
            GDD_SetTextColor(hdc,RGB(60,60,60));
            GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);

            if(buttoninfo->userParam == SyscModelFlag)
            {
                rc.right-=29;
                bmp = Get_BmpBuf(BMP_Check);
                GDD_DrawBMP(hdc,rc.right,0,bmp);
            }
        }
        else if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_EquipmentModel(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,EquipmentModelButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<SERIAL_MAXNUM;i++)
    {
        switch (EquipmentModelCfgTab[i].type)
        {
            case  widget_type_button :
            case  widget_type_groupbutton :
            {

                Widget_CreateButton(EquipmentModelCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 EquipmentModelCfgTab[i].position.left, EquipmentModelCfgTab[i].position.top,\
                                 GDD_RectW(&EquipmentModelCfgTab[i].position),GDD_RectH(&EquipmentModelCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&EquipmentModelCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_EquipmentModel(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));

        for(int i=0;i<SERIAL_MAXNUM;i++)
        {
            switch (EquipmentModelCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,EquipmentModelCfgTab[i].userParam);
                    GDD_FillRect(hdc,&EquipmentModelCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(EquipmentModelCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,EquipmentModelCfgTab[i].position.left,\
                                EquipmentModelCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :
                    GDD_DrawText(hdc,EquipmentModelCfgTab[i].name,-1,&EquipmentModelCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static char SyscModel[48]={0};
char *GetSyscModel(void)
{
    return SyscModel;
}

static enum WinType HmiNotify_EquipmentModel(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case EquipmentModel1:
            case EquipmentModel2:
            case EquipmentModel3:
            case EquipmentModel4:
            case EquipmentModel5:
            case EquipmentModel6:
            case EquipmentModel7:
                SyscModelFlag=id-EquipmentModel1+1;
                NextUi= WIN_Equipment_Model  ;
                break;
            case Save:
                if(SyscModelFlag != 0)
                {
                    strcpy(SyscModel,EquipmentModelCfgTab[SyscModelFlag+EquipmentModel1-1].name);
                }
                SyscModelFlag=0;
                NextUi= WIN_Serial_Edit  ;
                break;
            case Close:
                SyscModelFlag=0;
                NextUi= WIN_Serial_Edit  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Equipment_Model()
{
    return Register_NewWin(WIN_Equipment_Model,HmiCreate_EquipmentModel,HmiPaint_EquipmentModel,HmiNotify_EquipmentModel);
}
