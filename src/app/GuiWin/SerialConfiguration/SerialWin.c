//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum SerialWinWidgeId{
    SERIAL_BACKGROUND,

    SerialTopBox,
    SerialPageText,
    SerialRunningState0,
    SerialPortConfiguration1,
    SerialServerConfiguration0,
    SerialSystemSettings0,

    SerialCom1,
    SerialCom2,
    SerialCom3,
    SerialCom4,
    SerialCom5,

    SerialConnect1,
    SerialConnect2,
    SerialConnect3,
    SerialConnect4,
    SerialConnect5,
    SerialEdit1,
    SerialEdit2,
    SerialEdit3,
    SerialEdit4,
    SerialEdit5,

    SerialBack,

    ComTextRate1,
    ComTextBit1,
    ComTextCheck1,
    ComTextStop1,
    ComTextAddress1,
    ComTextEquipment1,

    ComTextRate2,
    ComTextBit2,
    ComTextCheck2,
    ComTextStop2,
    ComTextAddress2,
    ComTextEquipment2,

    ComTextRate3,
    ComTextBit3,
    ComTextCheck3,
    ComTextStop3,
    ComTextAddress3,
    ComTextEquipment3,

    ComTextRate4,
    ComTextBit4,
    ComTextCheck4,
    ComTextStop4,
    ComTextAddress4,
    ComTextEquipment4,

    ComTextRate5,
    ComTextBit5,
    ComTextCheck5,
    ComTextStop5,
    ComTextAddress5,
    ComTextEquipment5,

    SERIAL_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO SerialWinCfgTab[SERIAL_MAXNUM] =
{
    [SERIAL_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [SerialTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [SerialPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [SerialRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [SerialPortConfiguration1] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration1,
    },
    [SerialServerConfiguration0] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration0,
    },
    [SerialSystemSettings0] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings0,
    },

    [SerialCom1] = {
        .position = {17/LCDW_32,86/LCDH_32,(17+330)/LCDW_32,(86+241)/LCDH_32},
        .name = "com1",
        .type = widget_type_picture,
        .userParam = BMP_PortCom1,
    },
    [SerialCom2] = {
        .position = {347/LCDW_32,86/LCDH_32,(347+330)/LCDW_32,(86+241)/LCDH_32},
        .name = "com2",
        .type = widget_type_picture,
        .userParam = BMP_PortCom2,
    },
    [SerialCom3] = {
        .position = {677/LCDW_32,86/LCDH_32,(677+330)/LCDW_32,(86+241)/LCDH_32},
        .name = "com3",
        .type = widget_type_picture,
        .userParam = BMP_PortCom3,
    },
    [SerialCom4] = {
        .position = {23/LCDW_32,326/LCDH_32,(23+318)/LCDW_32,(326+238)/LCDH_32},
        .name = "com4",
        .type = widget_type_picture,
        .userParam = BMP_PortCom4,
    },
    [SerialCom5] = {
        .position = {347/LCDW_32,326/LCDH_32,(347+330)/LCDW_32,(326+241)/LCDH_32},
        .name = "com5",
        .type = widget_type_picture,
        .userParam = BMP_PortCom5,
    },

    [SerialConnect1] = {
        .position = {247/LCDW_32,108/LCDH_32,(247+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "cbutton1",
        .type = widget_type_groupbutton,
        .userParam = 0,
    },
    [SerialConnect2] = {
        .position = {577/LCDW_32,108/LCDH_32,(577+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "cbutton2",
        .type = widget_type_groupbutton,
        .userParam = 1,
    },
    [SerialConnect3] = {
        .position = {908/LCDW_32,108/LCDH_32,(908+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "cbutton3",
        .type = widget_type_groupbutton,
        .userParam = 2,
    },
    [SerialConnect4] = {
        .position = {247/LCDW_32,348/LCDH_32,(247+30)/LCDW_32,(348+30)/LCDH_32},
        .name = "cbutton4",
        .type = widget_type_groupbutton,
        .userParam = 3,
    },
    [SerialConnect5] = {
        .position = {577/LCDW_32,348/LCDH_32,(577+30)/LCDW_32,(348+30)/LCDH_32},
        .name = "cbutton5",
        .type = widget_type_groupbutton,
        .userParam = 4,
    },

    [SerialEdit1] = {
        .position = {287/LCDW_32,108/LCDH_32,(287+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "ebutton1",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [SerialEdit2] = {
        .position = {617/LCDW_32,108/LCDH_32,(617+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "ebutton2",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [SerialEdit3] = {
        .position = {948/LCDW_32,108/LCDH_32,(948+30)/LCDW_32,(108+30)/LCDH_32},
        .name = "ebutton3",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [SerialEdit4] = {
        .position = {287/LCDW_32,348/LCDH_32,(287+30)/LCDW_32,(348+30)/LCDH_32},
        .name = "ebutton4",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [SerialEdit5] = {
        .position = {617/LCDW_32,348/LCDH_32,(617+30)/LCDW_32,(348+30)/LCDH_32},
        .name = "ebutton5",
        .type = widget_type_button,
        .userParam = BMP_PortEdit,
    },
    [SerialBack] = {
        .position = {916/LCDW_32,532/LCDH_32,(916+80)/LCDW_32,(532+36)/LCDH_32},
        .name = "back",
        .type = widget_type_button,
        .userParam = BMP_LogBackButton,
    },

    [ComTextRate1] = {
        .position = {40/LCDW_32,123/LCDH_32,(40+140)/LCDW_32,(123+20)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_ctext1,
        .userParam = Gui_Bitrate,
    },
    [ComTextBit1] = {
        .position = {40/LCDW_32,153/LCDH_32,(40+287)/LCDW_32,(153+20)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_ctext1,
        .userParam = Gui_DataBit,
    },
    [ComTextCheck1] = {
        .position = {40/LCDW_32,183/LCDH_32,(40+287)/LCDW_32,(183+20)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_ctext1,
        .userParam = Gui_ParityBit,
    },
    [ComTextStop1] = {
        .position = {40/LCDW_32,213/LCDH_32,(40+287)/LCDW_32,(213+20)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_ctext1,
        .userParam = Gui_StopBit,
    },
    [ComTextAddress1] = {
        .position = {40/LCDW_32,243/LCDH_32,(40+287)/LCDW_32,(243+20)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext1,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextEquipment1] = {
        .position = {40/LCDW_32,273/LCDH_32,(40+287)/LCDW_32,(273+20)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_ctext1,
        .userParam = Gui_SysType,
    },

    [ComTextRate2] = {
        .position = {370/LCDW_32,123/LCDH_32,(370+140)/LCDW_32,(123+20)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_ctext2,
        .userParam = Gui_Bitrate,
    },
    [ComTextBit2] = {
        .position = {370/LCDW_32,153/LCDH_32,(370+287)/LCDW_32,(153+20)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_ctext2,
        .userParam = Gui_DataBit,
    },
    [ComTextCheck2] = {
        .position = {370/LCDW_32,183/LCDH_32,(370+287)/LCDW_32,(183+20)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_ctext2,
        .userParam = Gui_ParityBit,
    },
    [ComTextStop2] = {
        .position = {370/LCDW_32,213/LCDH_32,(370+287)/LCDW_32,(213+20)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_ctext2,
        .userParam = Gui_StopBit,
    },
    [ComTextAddress2] = {
        .position = {370/LCDW_32,243/LCDH_32,(370+287)/LCDW_32,(243+20)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext2,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextEquipment2] = {
        .position = {370/LCDW_32,273/LCDH_32,(370+287)/LCDW_32,(273+20)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_ctext2,
        .userParam = Gui_SysType,
    },

    [ComTextRate3] = {
        .position = {700/LCDW_32,123/LCDH_32,(700+140)/LCDW_32,(123+20)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_ctext3,
        .userParam = Gui_Bitrate,
    },
    [ComTextBit3] = {
        .position = {700/LCDW_32,153/LCDH_32,(700+287)/LCDW_32,(153+20)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_ctext3,
        .userParam = Gui_DataBit,
    },
    [ComTextCheck3] = {
        .position = {700/LCDW_32,183/LCDH_32,(700+287)/LCDW_32,(183+20)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_ctext3,
        .userParam = Gui_ParityBit,
    },
    [ComTextStop3] = {
        .position = {700/LCDW_32,213/LCDH_32,(700+287)/LCDW_32,(213+20)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_ctext3,
        .userParam = Gui_StopBit,
    },
    [ComTextAddress3] = {
        .position = {700/LCDW_32,243/LCDH_32,(700+287)/LCDW_32,(243+20)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext3,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextEquipment3] = {
        .position = {700/LCDW_32,273/LCDH_32,(700+287)/LCDW_32,(273+20)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_ctext3,
        .userParam = Gui_SysType,
    },

    [ComTextRate4] = {
        .position = {40/LCDW_32,363/LCDH_32,(40+140)/LCDW_32,(363+20)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_ctext4,
        .userParam = Gui_Bitrate,
    },
    [ComTextBit4] = {
        .position = {40/LCDW_32,393/LCDH_32,(40+287)/LCDW_32,(393+20)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_ctext4,
        .userParam = Gui_DataBit,
    },
    [ComTextCheck4] = {
        .position = {40/LCDW_32,423/LCDH_32,(40+287)/LCDW_32,(423+20)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_ctext4,
        .userParam = Gui_ParityBit,
    },
    [ComTextStop4] = {
        .position = {40/LCDW_32,453/LCDH_32,(40+287)/LCDW_32,(453+20)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_ctext4,
        .userParam = Gui_StopBit,
    },
    [ComTextAddress4] = {
        .position = {40/LCDW_32,483/LCDH_32,(40+287)/LCDW_32,(483+20)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext4,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextEquipment4] = {
        .position = {40/LCDW_32,513/LCDH_32,(40+287)/LCDW_32,(513+20)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_ctext4,
        .userParam = Gui_SysType,
    },

    [ComTextRate5] = {
        .position = {370/LCDW_32,363/LCDH_32,(370+140)/LCDW_32,(363+20)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_ctext5,
        .userParam = Gui_Bitrate,
    },
    [ComTextBit5] = {
        .position = {370/LCDW_32,393/LCDH_32,(370+287)/LCDW_32,(393+20)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_ctext5,
        .userParam = Gui_DataBit,
    },
    [ComTextCheck5] = {
        .position = {370/LCDW_32,423/LCDH_32,(370+287)/LCDW_32,(423+20)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_ctext5,
        .userParam = Gui_ParityBit,
    },
    [ComTextStop5] = {
        .position = {370/LCDW_32,453/LCDH_32,(370+287)/LCDW_32,(453+20)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_ctext5,
        .userParam = Gui_StopBit,
    },
    [ComTextAddress5] = {
        .position = {370/LCDW_32,483/LCDH_32,(370+287)/LCDW_32,(483+20)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_ctext5,
        .userParam = Gui_EquipmentAddress,
    },
    [ComTextEquipment5] = {
        .position = {370/LCDW_32,513/LCDH_32,(370+287)/LCDW_32,(513+20)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_ctext5,
        .userParam = Gui_SysType,
    },
};

//按钮控件创建函数
static bool_t  SerialWinButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }
        else if(buttoninfo->type == widget_type_groupbutton)
        {
            if(1 == GetscStatues(buttoninfo->userParam))
            {
                bmp = Get_BmpBuf(BMP_Connectioning);
            }
            else
            {
                bmp = Get_BmpBuf(BMP_Disconnect);
            }
            GDD_DrawBMP(hdc,0,0,bmp);
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_SerialWin(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,SerialWinButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<SERIAL_MAXNUM;i++)
    {
        switch (SerialWinCfgTab[i].type)
        {
            case  widget_type_button :
            case  widget_type_groupbutton :
            {

                Widget_CreateButton(SerialWinCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialWinCfgTab[i].position.left, SerialWinCfgTab[i].position.top,\
                                 GDD_RectW(&SerialWinCfgTab[i].position),GDD_RectH(&SerialWinCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialWinCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}
extern struct GuiSerial GuiSerialConfig[5];
//绘制消息处函数
static bool_t HmiPaint_SerialWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    char buf[56]={0};
    u32 addr=0;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));

        for(int i=0;i<SERIAL_MAXNUM;i++)
        {
            switch (SerialWinCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,SerialWinCfgTab[i].userParam);
                    GDD_FillRect(hdc,&SerialWinCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SerialWinCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SerialWinCfgTab[i].position.left,\
                                SerialWinCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_ctext1 :
                case  widget_type_ctext2 :
                case  widget_type_ctext3 :
                case  widget_type_ctext4 :
                case  widget_type_ctext5 :

                    addr=SerialWinCfgTab[i].type-widget_type_ctext1;

                    memset(buf,0,56);

                    if(SerialWinCfgTab[i].userParam == Gui_Bitrate)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gbitrate);
                    }
                    else if(SerialWinCfgTab[i].userParam == Gui_DataBit)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gdata_bit);
                    }
                    else if(SerialWinCfgTab[i].userParam == Gui_ParityBit)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gparity_bit);
                    }
                    else if(SerialWinCfgTab[i].userParam == Gui_StopBit)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gstop_bit);
                    }
                    else if(SerialWinCfgTab[i].userParam == Gui_EquipmentAddress)
                    {
                        snprintf(buf,sizeof(buf),"%s%d",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gequipment_address);
                    }
                    else if(SerialWinCfgTab[i].userParam == Gui_SysType)
                    {
                        snprintf(buf,sizeof(buf),"%s%s",SerialWinCfgTab[i].name,GuiSerialConfig[addr].gsys_type);
                    }

                    GDD_DrawText(hdc,buf,-1,&SerialWinCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_SerialWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case SerialRunningState0:
                NextUi= WIN_Home_Page;
                break;
//            case SerialPortConfiguration1:
//                NextUi= WIN_Serial_Win  ;
//                break;
            case SerialServerConfiguration0:
                NextUi= WIN_Ip_Win  ;
                break;
            case SerialSystemSettings0:
                NextUi= WIN_System_Win  ;
                break;
            case SerialBack:
                NextUi= WIN_Home_Page  ;
                break;
            case SerialEdit1:
            case SerialEdit2:
            case SerialEdit3:
            case SerialEdit4:
            case SerialEdit5:
                SetComNumb(id-SerialEdit1);
                SerialFlagInit(id-SerialEdit1);
                NextUi= WIN_Serial_Edit  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Serial_Win()
{
    return Register_NewWin(WIN_Serial_Win,HmiCreate_SerialWin,HmiPaint_SerialWin,HmiNotify_SerialWin);
}
