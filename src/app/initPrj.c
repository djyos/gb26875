/****************************************************
 *  Automatically-generated file. Do not edit!	*
 ****************************************************/

#include "project_config.h"
#include "djyos.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include <Iboot_info.h>
#include <djyfs/filesystems.h>

extern ptu32_t djy_main(void);
//如果这个CN_MANUFACTURER_NAME名字要改，那djysrc里的Iboot_info.c中相应的名字也要改
const char CN_MANUFACTURER_NAME[] = PRODUCT_MANUFACTURER_NAME;


const struct ProductInfo Djy_Product_Info __attribute__ ((section(".DjyProductInfo"))) =
{
    .VersionNumber[0] = PRODUCT_VERSION_LARGE,
    .VersionNumber[1] = PRODUCT_VERSION_MEDIUM,
    .VersionNumber[2] = PRODUCT_VERSION_SMALL,
#if(CN_PTR_BITS < 64)
    .ManufacturerNameAddr      = (u32)(&CN_MANUFACTURER_NAME),
    .ManufacturerNamereserved32    = 0xffffffff,
#else
    .ManufacturerNameAddr      = (u64)(&CN_MANUFACTURER_NAME),
#endif
    .ProductClassify = PRODUCT_PRODUCT_CLASSIFY,
    .ProductType = PRODUCT_PRODUCT_MODEL,

    .TypeCode = PRODUCT_PRODUCT_MODEL_CODE,

    .ProductionTime = {0xff,0xff,0xff,0xff},
    .ProductionNumber = {0xff,0xff,0xff,0xff,0xff},
    .reserved8 = 0,
    .BoardType = PRODUCT_BOARD_TYPE,
    .CPU_Type = PRODUCT_CPU_TYPE,
    .Reserved ={
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,\
                0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
            },
};


ptu32_t __djy_main(void)
{
    //cpu onchip iwdt
    extern bool_t ModuleInstall_BrdWdt(void);
    ModuleInstall_BrdWdt();
    //end cpu onchip iwdt

    //touchscreen GT911
    extern bool_t ModuleInstall_GT911(void);
    ModuleInstall_GT911( );
    #if(CFG_MODULE_ENABLE_GRAPHICAL_DECORATE_DEVELOPMENT == true)
    extern bool_t GDD_AddInputDev(const char *InputDevName);
    GDD_AddInputDev(CFG_GT911_TOUCH_NAME);
    #endif
    //end touchscreen GT911

    djy_main();

	return 0;
}

void Sys_ModuleInit(void)
{
	uint16_t evtt_main;

    //shell
    extern void Stdio_KnlInOutInit(char * StdioIn, char *StdioOut);
    Stdio_KnlInOutInit(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME);
    extern s32 ModuleInstall_Shell(ptu32_t para);
    ModuleInstall_Shell(0);
    //end shell

    //----------------------------early----------------------------//
    //black box
    extern void ModuleInstall_BlackBox(void);
    ModuleInstall_BlackBox( );
    //end black box

    //device file system
    extern s32 ModuleInstall_dev(void);
    ModuleInstall_dev();    // 安装设备文件系统；
    //end device file system

    //message queue
    extern bool_t ModuleInstall_MsgQ(void);
    ModuleInstall_MsgQ ( );
    //end message queue

    //multiplex
    extern bool_t ModuleInstall_Multiplex(void);
    ModuleInstall_Multiplex ();
    //end multiplex

    //watch dog
    extern bool_t ModuleInstall_Wdt(void);
    ModuleInstall_Wdt();
    //end watch dog

    //xip iboot file system
    extern s32 ModuleInstall_XIP_IBOOT_FS(u32 opt, void *data);
    ModuleInstall_XIP_IBOOT_FS(0,NULL);
    //end xip iboot file system

    //cpu onchip timer
    extern bool_t ModuleInstall_HardTimer(void);
    ModuleInstall_HardTimer();
    //end cpu onchip timer

    //cpu onchip systime
    extern bool_t MoudleInit_Systime(ptu32_t para);
    MoudleInit_Systime(0);
    //end cpu onchip systime

    //cpu drive inner flash
    s32 ModuleInstall_EmbededFlash(u32 doformat);
    ModuleInstall_EmbededFlash(CFG_EFLASH_PART_FORMAT);
    //end cpu drive inner flash

    //emflash insatall xip
    extern s32 ModuleInstall_EmFlashInstallXIP(const char *TargetFs,s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_EmFlashInstallXIP(CFG_EFLASH_XIPFSMOUNT_NAME,CFG_EFLASH_XIP_PART_START,
    CFG_EFLASH_XIP_PART_END, CFG_EFLASH_XIP_PART_FORMAT);
    //end emflash insatall xip

    //cpu onchip qspi
    extern s32 ModuleInstall_QSPI(void);
    ModuleInstall_QSPI();
    //end cpu onchip qspi

    //gd5f1gq5x
    extern s32 ModuleInstall_Gd5f1g(void);
    ModuleInstall_Gd5f1g();
    //end gd5f1gq5x

    //yaf2 file system
    extern s32 ModuleInstall_YAF2(const char *target, u32 opt, void *data);
    ModuleInstall_YAF2(CFG_YAF_MOUNT_POINT, CFG_YAF_INSTALL_OPTION, CFG_YAF_ECC);
    //end yaf2 file system

    //gd5f1g install yaf
    extern bool_t ModuleInstall_Gd5f1gInstallYaf(const char *TargetFs,s32 bstart, s32 bend, u32 doformat);
    ModuleInstall_Gd5f1gInstallYaf(CFG_GD5F1G_YAFFSMOUNT_NAME, CFG_GD5F1G_YAF_PART_START,
    CFG_GD5F1G_YAF_PART_END, CFG_GD5F1G_YAF_PART_FORMAT);
    //end gd5f1g install yaf

    //stm32usb
    extern s32 ModuleInstall_USB(const char *TargetFs,u8 controller);
    ModuleInstall_USB(CFG_USB_UDISK_FS, CFG_USB_CONTROLLER_ID);
    //end stm32usb

    //djybus
    extern bool_t ModuleInstall_DjyBus(void);
    ModuleInstall_DjyBus ( );
    //end djybus

    //iicbus
    extern bool_t ModuleInstall_IICBus(void);
    ModuleInstall_IICBus ( );
    //end iicbus

    //cpu onchip iic
    bool_t ModuleInstall_IIC(ptu32_t port);
    #if CFG_IIC1_ENABLE== true
    ModuleInstall_IIC_Drv(CN_IIC1);
    #endif
    #if CFG_IIC2_ENABLE== true
    ModuleInstall_IIC_Drv(CN_IIC2);
    #endif
    #if CFG_IIC3_ENABLE== true
    ModuleInstall_IIC_Drv(CN_IIC3);
    #endif
    #if CFG_IIC4_ENABLE== true
    ModuleInstall_IIC_Drv(CN_IIC4);
    #endif
    //end cpu onchip iic

    //----------------------------medium----------------------------//
    //font
    extern bool_t ModuleInstall_Font(void);
    ModuleInstall_Font ( );
    //end font

    //kernel
    #if(CFG_OS_TINY == flase)
    extern s32 kernel_command(void);
    kernel_command();
    #endif
    //end kernel

    //Nls Charset
    extern ptu32_t ModuleInstall_Charset(ptu32_t para);
    ModuleInstall_Charset(0);
    extern void ModuleInstall_CharsetNls(const char * DefaultCharset);
    ModuleInstall_CharsetNls("C");
    //end Nls Charset

    //gb2312 charset
    extern bool_t ModuleInstall_CharsetGb2312(void);
    ModuleInstall_CharsetGb2312 ( );
    //end gb2312 charset

    //gb2312 dot
    extern void ModuleInstall_FontGB2312(void);
    ModuleInstall_FontGB2312();
    //end gb2312 dot

    //Software Timers
    extern bool_t ModuleInstall_Timer(void);
    ModuleInstall_Timer();
    //end Software Timers

    //utf8 charset
    extern bool_t ModuleInstall_CharsetUtf8(void);
    ModuleInstall_CharsetUtf8 ( );
    //end utf8 charset

    //cpu onchip rtc
    extern ptu32_t ModuleInstall_CpuRtc(ptu32_t para);
    ModuleInstall_CpuRtc(0);
    //end cpu onchip rtc

    //graphical kernel
    extern bool_t ModuleInstall_GK(void);
    ModuleInstall_GK();
    //end graphical kernel

    //cpu onchip lcd
    extern struct DisplayObj* ModuleInstall_LCD(const char *DisplayName,const char* HeapName);
    ModuleInstall_LCD(CFG_ONCHIP_DISPLAY_NAME,CFG_ONCHIP_LCD_HEAP_NAME);
    //end cpu onchip lcd

    //graphical decorate development
    extern void ModuleInstall_Gdd_AND_Desktop(void);
    ModuleInstall_Gdd_AND_Desktop();
    //end graphical decorate development

    //cpu onchip uart
    extern ptu32_t ModuleInstall_UART(ptu32_t SerialNo);
    #if CFG_UART1_ENABLE ==1
    ModuleInstall_UART(CN_UART1);
    #endif
    #if CFG_UART2_ENABLE ==1
    ModuleInstall_UART(CN_UART2);
    #endif
    #if CFG_UART3_ENABLE ==1
    ModuleInstall_UART(CN_UART3);
    #endif
    #if CFG_UART4_ENABLE ==1
    ModuleInstall_UART(CN_UART4);
    #endif
    #if CFG_UART5_ENABLE ==1
    ModuleInstall_UART(CN_UART5);
    #endif
    #if CFG_UART6_ENABLE ==1
    ModuleInstall_UART(CN_UART6);
    #endif
    #if CFG_UART7_ENABLE ==1
    ModuleInstall_UART(CN_UART7);
    #endif
    #if CFG_UART8_ENABLE ==1
    ModuleInstall_UART(CN_UART8);
    #endif
    //end cpu onchip uart

    //tcpip
    extern bool_t ModuleInstall_TcpIp(void);
    ModuleInstall_TcpIp( );
    //end tcpip

    DHCP_ClientInit( );

    //cpu onchip ETH
    extern bool_t ModuleInstall_ETH(void);
    ModuleInstall_ETH( );
    //end cpu onchip ETH

    //loader
    #if !defined (CFG_RUNMODE_BAREAPP)
    extern bool_t ModuleInstall_UpdateIboot(void);
    ModuleInstall_UpdateIboot( );
    #endif
    //end loader

    //human machine interface
    extern bool_t ModuleInstall_HmiIn(void);
    ModuleInstall_HmiIn();      //初始化人机界面输入模块
    //end human machine interface

    //touch
    extern bool_t ModuleInstall_Touch(void);
    ModuleInstall_Touch();    //初始化人机界面输入模块
    //end touch

    //isbus
    bool_t ModuleInstall_ISBUS(u32 HostStackSize,u32 SlaveStackSize);
    ModuleInstall_ISBUS(CFG_HOST_STACK_SIZE,CFG_SLAVE_STACK_SIZE);
    //end isbus

    //----------------------------later----------------------------//
    //stdio
    #if(CFG_STDIO_STDIOFILE == true)
    extern s32 ModuleInstall_STDIO(const char *in,const char *out, const char *err);
    ModuleInstall_STDIO(CFG_STDIO_IN_NAME,CFG_STDIO_OUT_NAME,CFG_STDIO_ERR_NAME);
    #endif
    //end stdio

    //network config
    extern void ModuleInstall_InitNet( );
    ModuleInstall_InitNet( );
    //end network config

    //ls88e6060
    extern ptu32_t ModuleInstall_LS88e6060(void);
    ModuleInstall_LS88e6060();
    //end ls88e6060

	evtt_main = DJY_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,0,
		__djy_main,NULL,CFG_MAINSTACK_LIMIT, "main function");
	//事件的两个参数暂设为0,如果用shell启动,可用来采集shell命令行参数
	DJY_EventPop(evtt_main,NULL,0,0,0,0);

    //heap
    #if ((CFG_DYNAMIC_MEM == true))
    extern bool_t Heap_DynamicModuleInit(void);
    Heap_DynamicModuleInit ( );
    #endif
    //end heap



	return ;
}
