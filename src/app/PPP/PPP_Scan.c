/*================================================================================
 * 文件名称：DebugShell.c
 * 文件描述：Shell调试命令组件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间:
 * 版本修订:
 * 修订人员:
 *================================================================================*/

// include 关系
//#include "InstCell.h"
//#include "DebugShell.h"

// TcpSop 组件中包含如下
#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include "stdlib.h"
#include "endian.h"
#include "string.h"
#include "os.h"
#include "shell.h"
#include <endian.h>
#include "PPP_Scan.h"
//#include "DebugShell.h"
#include <blackbox.h>
#include "pppdata.h"
#include <netbsp.h>

#define  CN_PPP_SCAN_TASK_STACK_LEN    0x1000
static u8  gs_PPP_Scan_TaskStack[CN_PPP_SCAN_TASK_STACK_LEN];

#define CHECK_LINK_TIME (50)
#define CHECK_RESTART_TIME (1)

#define CN_RESET_TIME      6
#define CN_MODEM_BOOTTIME  25

static u32 u32_PPP_Scan_Status;
static u32   dwRstCnt = 0;

tagTimerB  tTimeOut_PPP_Success;

//无线模块电源控制引脚 add by zhb 20170726(依据SR5333硬件改版)
extern void M4G_PowerSet(void);
extern void M4G_PowerClr(void);
extern void CPU_Reboot(u32 key);
extern void CPU_Reset(u32 key);

//=================================================
// 函数功能：重启无线模块
//=================================================
bool_t ModemReset(void)
{
    int i = 0;
    printf("ModemHardReset:");
    printf("low");
    //GPIO_SettoLow(GPIO_I,PIN10);
    M4G_PowerClr();
    for(i = 0;i<CN_RESET_TIME;i++)
    {
        printf(".");
        DJY_EventDelay(1000*mS);
    }
    //GPIO_SettoHigh(GPIO_I,PIN10);
    M4G_PowerSet();
    printf("high");
    for(i = 0;i<CN_MODEM_BOOTTIME;i++)
    {
        printf(".");
        DJY_EventDelay(1000*mS);
    }
    printf("done\n\r");
    return true;
}

//=================================================
// 函数功能：无线模块拨号状态钩子函数
//=================================================
//bool_t devEvent(enum NetDevEvent event)

bool_t devEvent(struct NetDev *handle,enum NetDevEvent event)
{
    switch(event)
    {
        // 模块和基站建立通讯
    case EN_NETDEVEVENT_LINKUP:
        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_LINKUP);
        break;
        // 模块获取到公网IP
    case EN_NETDEVEVENT_IPGET:
        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_IPGET);
        NetDev_SetDefault(handle);
        break;
        // 模块丢失公网IP
    case EN_NETDEVEVENT_IPRELEASE:
        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_IPRELEASE);
        break;
        // 模块和基站失去链接
    case EN_NETDEVEVENT_LINKDOWN:
        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_LINKDOWN);
        break;
    default:
        break;
    }

    return true;
}

// 这个函数不用了，但暂时保留
//=================================================
// 函数功能：如果拨号成功，创建TCP任务
//=================================================
void devEvent_PPP(enum NetDevEvent event)
{
    if(event)
        TcpStartNet();
}

enum _EN_PPP_Status
{
    EN_PPP_NOT_INITIAL = 0,             // 无线模块设备未输出化成功
    EN_PPP_INITIAL_SUCCESS,             // 无线模块初始化成功
    EN_PPP_LINK_START,                  // 无线模块开始拨号
    EN_PPP_LINK_START_OKCHECK,          // 无线模块拨号成功检查
    EN_PPP_LINK_SUCCESS,                // 无线模块拨号成功
    EN_PPP_SCAN_LINKOK,                 // 无线模块连接状态检查
    EN_PPP_LINK_NEED_RESET_CHECK,       // 无线模块重启标志位检查
    EN_PPP_LINK_RESTART,                // 无线模块重拨号
    EN_PPP_UMODEM_RESET,                // 无线模块重启
};
static bool_t flag = false;
//==================================================================
// 函数功能：持续扫描无线模块的状态，自动拨号。若掉线，则重新拨号
//==================================================================
bool_t PPP_Status_Scan(void)
{
//*******************************************************************************//
    //add the ppp device to the stack
    extern bool_t AtDial(char *devname,char *apn);
    extern bool_t ModemReset(void);
    extern bool_t PppDevAdd(char *netdev,char *iodev,const char *user,const char *passwd,const char *apn,\
                     bool_t (*fnModemReset)(void),bool_t (*fnModemAtregnet)(char *devname,char *apn),\
                     fnNetDevEventHook eventhook);
    extern s32 CUSTOM_DeviceReady(void);
    extern bool_t  AtDefaultDevSet(char *name);
    //if you want to auto check the simcard and apn,please set the APN to NULL
//*******************************************************************************//
    //start the ppp dial
    extern bool_t PppDevLinkStart(char *pppdevname);
    //waifor 80 seconds for the dial
    extern bool_t PppDevLinkIsOk(char *pppdevname);
    //stop the ppp dial
    extern bool_t PppDevLinkStop(char *pppdevname);
    //CPU_Reset the ppp
    extern bool_t PppDevLinkRst(char *pppdevname);
//*******************************************************************************//

    u32 u32PPPStatus = EN_PPP_NOT_INITIAL;    // 无线模块的状态
#if 0
    static u32 u32PPPStatus_Bak = EN_PPP_NOT_INITIAL;
#endif
    u32 times = CHECK_LINK_TIME;
    u32 restart_times = CHECK_RESTART_TIME;

//    u32 times_init = CHECK_INIT_TIME;

    printf("wait the usb dev up \n\r");
    DJY_EventDelay(2000*mS);

    M4G_PowerSet(); // 给4G模块上电
    EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_POWERON);
    Time_TimerBStart( &tTimeOut_PPP_Success,  CN_MS_T_600S );// 定时器
    while(1)
    {
        u32_PPP_Scan_Status = u32PPPStatus;
        DJY_EventDelay(1000*mS);
        if(Time_TimerBCheck( &tTimeOut_PPP_Success ))
        {
            if(M_GrGet_bGlobalRam(EN_BOOL_SN_EVC_IDLE))
            {
                EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_POWEROFF);
                DJY_EventDelay(5000*mS);
                CPU_Reset(CN_BOOT_LEGALKEY);
            }
        }
/*
        if(Get_gui_net_reset_flag())
        {
            flag = true;
            Set_gui_net_reset_flag(false);
        }
*/
        switch(u32PPPStatus)
        {
            case EN_PPP_NOT_INITIAL:
                if(0 == CUSTOM_DeviceReady())    //wait for the usb to be ready
                {
                    printf("done\n\r");
                    EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_READCONFIG);
                    u32PPPStatus = EN_PPP_INITIAL_SUCCESS;
                }
                else
                {
                    DJY_EventDelay(1000*mS);
                    //printf(".");
                    u32PPPStatus = EN_PPP_NOT_INITIAL;
                }
                break;

            case EN_PPP_INITIAL_SUCCESS: // 初始化成功(成功挂载无线模块到USB)，开始拨号
                DJY_EventDelay(200*mS);
                // 添加PPP设备
                PppDevAdd(NET_DEV_NAME,IO_DEV_NAME,"cyg","zhangqf",/*"gddwgs1.gd"*/NULL,ModemReset,AtDial,(fnNetDevEventHook)devEvent);
                // 将At设置恢复默认
                AtDefaultDevSet(AT_DEV_NAME);

                // 跳转到无线模块开始拨号
                u32PPPStatus = EN_PPP_LINK_START;
                break;

            case EN_PPP_LINK_START:// 无线模块开始拨号
                // 开始拨号,拨号前等待5s
                DJY_EventDelay(5000*mS);
                PppDevLinkStart(NET_DEV_NAME);
                // 将超时时间设置为 CHECK_LINK_TIME
                times = CHECK_LINK_TIME;

                u32PPPStatus = EN_PPP_LINK_START_OKCHECK;
                break;

            case EN_PPP_LINK_START_OKCHECK:// 检查拨号是否成功
                if(TRUE == PppDevLinkIsOk(NET_DEV_NAME))
                {
                    u32PPPStatus = EN_PPP_LINK_SUCCESS;
                    if(flag == true)
                        u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                else
                {
//                    sleep(1);
                    DJY_DelayUs(1000 * 1000);
                    times--;
                    u32PPPStatus = EN_PPP_LINK_START_OKCHECK;
                }
                if(times <= 0)
                {
                    u32PPPStatus = EN_PPP_LINK_RESTART;
                    if(flag == true)
                        u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                break;

            case EN_PPP_LINK_SUCCESS:
                // 如果拨号成功了，将无线模块重拨号次数恢复到3次。
                restart_times = CHECK_RESTART_TIME;
                Time_TimerBStop( &tTimeOut_PPP_Success);
                dwRstCnt = 0;
                u32PPPStatus = EN_PPP_SCAN_LINKOK;
                if(flag == true)
                    u32PPPStatus = EN_PPP_UMODEM_RESET;
                break;

            case EN_PPP_SCAN_LINKOK:
                // 如果无线模块不在线，就将无线模块在线标志清0，并跳转至重启状态
                if(PppDevLinkIsOk(NET_DEV_NAME))
                {
                    //create the tcp connection here
                    TcpSetModermState( 0 );
                    u32PPPStatus = EN_PPP_LINK_NEED_RESET_CHECK;
                    if(flag == true)
                        u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                else
                {
                    TcpSetModermState( 1 );
                    //printf("PPP 拨号掉线，TCP不再连接.\r\n");
                    if(0 == dwRstCnt)
                    {
                        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TRAILEDGE);
                        Time_TimerBReset( &tTimeOut_PPP_Success);
                    }
                    dwRstCnt++;
                    EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_HARDRESET);
                    u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                break;

            case EN_PPP_LINK_NEED_RESET_CHECK:
//                sleep(3);
//                DJY_DelayUs(3000 * 1000);
                DJY_EventDelay(3000*mS);
                if(TCP_Get_Modem_RstFlag() && TCP_Get_Modem_RstFlag2())
                {
                    //test for 1s the redial
//                    sleep(1);//we will stay in the ppp mode 100 seconds and then redial
//                    DJY_DelayUs(1000 * 1000);
                    DJY_EventDelay(1000*mS);
                    u32PPPStatus = EN_PPP_SCAN_LINKOK;
                    if(flag == true)
                        u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                else
                {
                    Tcp_Reset_iFailRstFlag2_All();
                    if(0 == dwRstCnt)
                    {
                        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TRAILEDGE);
                        Time_TimerBReset( &tTimeOut_PPP_Success);
                    }
                    dwRstCnt++;
                    EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_HARDRESET);
                    u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                break;
            case EN_PPP_LINK_RESTART:
                if(flag == true)
                    u32PPPStatus = EN_PPP_UMODEM_RESET;
                else
                {
                    TcpSetModermState( 1 );
                    //printf("PPP拨号，重置标志位被置位.TCP不再连接...\n\r");
                    printf("ppp dial restart.\n\r");
                    //if not ok then stop and CPU_Reset it
                    PppDevLinkStop(NET_DEV_NAME);
                    //if(TRUE == PppDevLinkIsOk((char*)NET_DEV_NAME))
                    //{
                    //    sleep(2);
                    //    break;
                    //}
                    u32PPPStatus = EN_PPP_LINK_START;
                    if(restart_times--<=0)
                    {
                        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_HARDRESET);
                        u32PPPStatus = EN_PPP_UMODEM_RESET;
                    }
                }

                break;
            case EN_PPP_UMODEM_RESET:
                TcpSetModermState( 1 );
                //printf("PPP拨号，重置标志位被置位.TCP不再连接...\n\r");
                printf("ppp dial failed\n\r");
                //if not ok then stop and CPU_Reset it
                PppDevLinkStop(NET_DEV_NAME);
                PppDevLinkRst(NET_DEV_NAME);

                if(0 == CUSTOM_DeviceReady())
                {
                    // 在无线模块rst，枚举成功以后，将重拨号次数恢复到3次。
                    restart_times = CHECK_RESTART_TIME;
                    u32PPPStatus = EN_PPP_LINK_START;
                }
                else
                {
                    u32PPPStatus = EN_PPP_UMODEM_RESET;
                }
                flag = false;
                break;

            default:
                break;
        }
#if 0
        if(u32PPPStatus != u32PPPStatus_Bak)
        {
            printf("PPP_Status changed from %d to %d. \r\n", u32PPPStatus_Bak, u32PPPStatus);
            u32PPPStatus_Bak = u32PPPStatus;
        }
#endif
    }
    return true;
}

//=================================================
// 函数功能：创建无线模块扫描任务
//=================================================
void PPP_Start( void )
{
    u16 UModemEvtt;
    UModemEvtt = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 1, (ptu32_t(*)(void))PPP_Status_Scan,
                           gs_PPP_Scan_TaskStack, CN_PPP_SCAN_TASK_STACK_LEN, "UModem_Scan");

    if(UModemEvtt==CN_EVTT_ID_INVALID)
    {
        printf("UModem_Scan event register failed.\r\n");
    }
    else
    {
        DJY_EventPop(UModemEvtt, NULL, 0, 0, 0, 0);
    }
}

//=================================================
// 函数功能：查看拨号状态
//=================================================
void Dbg_Show_PPP_Status(void)
{
    extern bool_t PppDevLinkIsOk(char *pppdevname);
    u32 bPPP_Status;
    bPPP_Status = PppDevLinkIsOk(NET_DEV_NAME);
    printf("PPP当前模块拨号状态是: %d. \n\r", bPPP_Status);
}

//=================================================
// 函数功能：查看无线模块硬件状态
//=================================================
void Dbg_Show_Modem_Status(void)
{
    extern s32 CUSTOM_DeviceReady(void);
    u32 bModem_Status;
    bModem_Status = (0 == CUSTOM_DeviceReady());
    printf("PPP当前模块硬件读取状态是: %d. \n\r", bModem_Status);
}

//=================================================
// 函数功能：查看PPP_Scan状态
//=================================================
void Dbg_Show_PPP_Scan_Status(void)
{
    printf("PPP当前模块状态机是: %d. \n\r", u32_PPP_Scan_Status);
}


ADD_TO_ROUTINE_SHELL(shppps,Dbg_Show_PPP_Status,"拨号状态 :COMMAND:shppps+enter");
