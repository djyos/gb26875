#ifndef __SUNRITIME_H__
#define __SUNRITIME_H__
#include "sunri_types.h"
// BCD码方式时标数据结构
typedef struct
{
    INT8U            byYear_L;                       // 年(低位)
    INT8U            byYear_H;                       // 年(高位) 默认为0x20
    INT8U            byMonth;                        // 月
    INT8U            byDay;                          // 日
    INT8U            byHour;                         // 时
    INT8U            byMinute;                       // 分
    INT8U            bySecond;                       // 秒
    INT8U            byMS_L;                         // 毫秒(低位)
    INT8U            byMS_H;                         // 毫秒(高位)
    INT8U            byWeek;                         // 星期
    INT8U            byRes[2];                       // 备用字节(配齐4的整数倍)
}tagTimeBCD;

// 以1970年1月1日0时0分0秒0毫秒为时钟基准开始的UTC时间数据结构
typedef struct
{
    INT32U           dwSecond;                       // 秒
    INT32U           dwMillisecond;                  // 毫秒
}tagTimeUTC;

// HEX码方式时标数据结构
typedef struct
{
    INT16U           wYear;                          // 年(00-65535)
    INT8U            byMonth;                        // 月(01-12)
    INT8U            byDay;                          // 日(01-31)
    INT8U            byHour;                         // 时(00-23)
    INT8U            byMinute;                       // 分(00-59)
    INT16U            wMS;                            // 毫秒(00-59,999)
    INT8U            byWeek;                         // 星期
}tagTimeHEX;

typedef struct {                                    // TAG方式时标数据结构
    BITS            Second:6;                       // 秒(00-59)
    BITS            Minute:6;                       // 分(00-59)
    BITS            Hour:5;                         // 时(00-23)
    BITS            Day:5;                          // 日(01-31)
    BITS            Month:4;                        // 月(01-12)
    BITS            Year:6;                         // 年(00-64)
}tagTimeTAG;


// C类定时器,用于反时限保护的定时
typedef struct
{
   INT32U   dwRetLimit  ;                           // 返回计时时限
   float    fCount      ;                           // 动作计数
   INT32U   dwRetCount  ;                           // 返回计数
   INT8U        bActEnabled ;                           // 动作计时是否计时启动
   INT8U        bRetEnabled ;                           // 返回计时是否计时启动
   INT8U        bExpired    ;                           // 动作计时时间是否到
   INT8U        bRetExpired ;                           // 返回计时时间是否到
//   INT32U     dwCnt;
}tagTimerC;


#endif

