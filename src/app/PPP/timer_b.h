/*================================================================================
 * 文件描述：公共函数
 * 文件版本: V1.00
 * 开发人员: 黎强
 * 定版时间:
 * 版本修订:
 * 修订人员:
 *================================================================================*/

#ifndef _TIMER_B_H_
#define _TIMER_B_H_

//#define tagTimerB   INT64
typedef struct TimerB
{
    s64  DeadLine;
    u32  TimeLimit;
}tagTimerB;
extern void     Time_TimerBStart( tagTimerB *ptTimerB, DWORD dwTimeLimit);
extern BOOL  Time_TimerBCheck( tagTimerB *ptTimerB );
extern void     Time_TimerBSetExpired(  tagTimerB *ptTimerB );
extern void     Time_TimerBReset( tagTimerB *ptTimerB );
extern void     Time_TimerBStop( tagTimerB *ptTimerB );
extern BOOL Time_TimerBIsStart( tagTimerB *ptTimerB );

/*================================================================================*/
#endif /* _TIMER_B_H_ */

