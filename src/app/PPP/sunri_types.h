//========================================================
// 文件模块说明:
// C6000数据类型定义
// 文件版本: v1.00
// 开发人员: lst
// 创建时间: 2011.06.01
// Copyright(c) 2011-2011  深圳鹏瑞软件有限公司
//========================================================
// 程序修改记录(最新的放在最前面):
//  <版本号> <修改日期>, <修改人员>: <修改功能概述>
//========================================================
//例如：
// V1.02 2004-07-14, 张三: 接收函数增加对报文长度有效性校核

#ifndef __SUNRI_TYPES_H__
#define __SUNRI_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BYTE
    #define BYTE unsigned  char
#endif

#ifndef DWORD
    #define DWORD UINT32
#endif

#ifndef WORD
    #define WORD UINT16
#endif

#ifndef UINT64
    #define UINT64 unsigned  long long
#endif

#ifndef SINT64
    #define SINT64 signed long long
#endif

#ifndef FLOAT32
    #define FLOAT32 float
#endif

#ifndef SINT32
    #define SINT32 int
#endif

#ifndef UINT32
    #define UINT32 unsigned int
#endif

#ifndef INT16U
#define INT16U  unsigned short
#endif

#ifndef UINT16
    #define UINT16 unsigned  short
#endif

#ifndef SINT16
    #define SINT16 short int
#endif

#ifndef UINT8
    #define UINT8 unsigned  char
#endif

#ifndef SINT8
    #define SINT8 signed  char
#endif

#ifndef VUINT32
    #define VUINT32 volatile UINT32
#endif

#ifndef VUINT16
    #define VUINT16 volatile UINT16
#endif

#ifndef VUINT8
    #define VUINT8 volatile UINT8
#endif

#ifndef BOOLEAN
#define BOOLEAN unsigned char
#endif

#ifndef INT8U
#define INT8U   unsigned char
#endif

#ifndef INT8S
#define INT8S    signed char
#endif

#ifndef INT16S
#define INT16S          short
#endif

#ifndef INT32U
#define INT32U  unsigned int
#endif

#ifndef INT32S
#define INT32S          int
#endif

#ifndef ULONG
#define ULONG   unsigned long
#endif

#ifndef LONG
#define LONG           long
#endif

#ifndef INT64
#define INT64          long long
#endif

#ifndef VULONG
#define VULONG  volatile unsigned long
#endif

#ifndef VINT16U
#define VINT16U volatile unsigned short
#endif

#ifndef VINT8U
#define VINT8U  volatile unsigned char
#endif


#ifndef FALSE
    #define FALSE             0
#endif
#ifndef TRUE
    #define TRUE              1
#endif

#ifndef NULL
    #define     NULL ((void*) 0)
#endif

#ifndef BOOL
    #define BOOL             UINT32
#endif

// 条件成立常用标志
#define CN_FLAG_TRUE        (0x5A)                  // 条件成立标志
#define CN_FLAG_FALSE       (0xA5)                  // 条件不成立标志
#define CN_REC_ACTION       (0x55)                  // 记录动作标志
#define CN_REC_RETURN       (0xAA)                  // 记录返回标志

// BIT位定义
#define DB0                 (0x01)
#define DB1                 (0x02)
#define DB2                 (0x04)
#define DB3                 (0x08)
#define DB4                 (0x10)
#define DB5                 (0x20)
#define DB6                 (0x40)
#define DB7                 (0x80)
#define DB8                 (0x100)
#define DB9                 (0x200)
#define DB10                (0x400)
#define DB11                (0x800)
#define DB12                (0x1000)
#define DB13                (0x2000)
#define DB14                (0x4000)
#define DB15                (0x8000)
#define DB16                (0x10000)
#define DB17                (0x20000)
#define DB18                (0x40000)
#define DB19                (0x80000)
#define DB20                (0x100000)
#define DB21                (0x200000)
#define DB22                (0x400000)
#define DB23                (0x800000)
#define DB24                (0x1000000)
#define DB25                (0x2000000)
#define DB26                (0x4000000)
#define DB27                (0x8000000)
#define DB28                (0x10000000)
#define DB29                (0x20000000)
#define DB30                (0x40000000)
#define DB31                (0x80000000)

#define BITS unsigned int
#define PSTR volatile char *
#define LPSTR volatile UINT8 *

#ifdef __cplusplus
    typedef SINT32          (*PFUNC)( ... );
#else
    typedef SINT32          (*PFUNC)( );
#endif

#ifdef __cplusplus
}
#endif

#endif //__SUNRI_TYPES_H__

