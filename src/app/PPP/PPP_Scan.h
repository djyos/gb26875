/*================================================================================
 * 文件名称：DebugShell.h
 * 文件描述：Shell调试命令组件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间: 
 * 版本修订:
 * 修订人员: 
 *================================================================================*/
 
#ifndef _PPP_SCAN_H
#define _PPP_SCAN_H

// include 包含关系
//#include "IEC103_Comm.h"
//#include "Resource.h"
#include "cpu_peri.h"
#include <netbsp.h>


#define IO_DEV_NAME     "usb-modem"
#define AT_DEV_NAME     "usb-at"
#define NET_DEV_NAME    "pppnet"

// 外部函数声明
extern bool_t ModemReset(void);
//extern bool_t devEvent(enNetDevEvent event);
extern bool_t devEvent(struct NetDev * handle,enum NetDevEvent event);
extern void devEvent_PPP(enum NetDevEvent event);
extern bool_t PPP_Status_Scan(void);
extern void PPP_Start( void );
extern void Dbg_Show_PPP_Scan_Status(void);

/*================================================================================*/
#endif // _DebugShell_H
