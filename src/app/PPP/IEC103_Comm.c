//================================================================================
// 文件描述：103组件底层通信封装模块
// 文件版本: V1.00
// 开发人员: 姜超
// 定版时间:
// 版本修订:
// 修订人员:
//================================================================================
#include "IEC103_Comm.h"
#include "string.h"
//#include "pubsever.h"
//#include "ParaVal.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include "project_config.h"     //本文件由IDE中配置界面生成，存放在APP的工程目录中。
                                //允许是个空文件，所有配置将按默认值配置。
//

// ===============================================================================================  
//  Get the pointer of PORT control variables   
// ===============================================================================================
BOOLEAN Fun_GetPortVariable( INT8U byPortNo, tagPPortVal *pptPortVal )
{
    if( byPortNo >=CN_NUM_PORT ) return FALSE;
    *pptPortVal = &g_tPortVal[byPortNo];
    (*pptPortVal)->ptPortSet = &g_ptPortSet[byPortNo];//在初始化里赋值

    return TRUE;
}
// ===============================================================================================  
//  
// ===============================================================================================

BOOLEAN Port_CheckCommERR( BYTE byPortNo )
{
    tagPortVal          *ptPortVal;

    if( FALSE==Fun_GetPortVariable( byPortNo, &ptPortVal) ) return FALSE;

    if( Time_TimerBCheck( &(ptPortVal->tTimerCommOK) ) )
    { // 通讯故障时间到
        if( ptPortVal->bCommOK )
        { // 正常转故障 (遥信合位为故障)
            ptPortVal->bCommOK = FALSE;
        }
        Time_TimerBStop( &(ptPortVal->tTimerCommOK) );
    }

    return TRUE;
}
// ===============================================================================================  
// 
// ===============================================================================================

BOOLEAN Port_CheckCommOK( BYTE byPortNo )
{
    tagPortVal          *ptPortVal;

    if( FALSE ==Fun_GetPortVariable( byPortNo, &ptPortVal) ) return FALSE;

    if( FALSE ==ptPortVal->bCommOK )
    { // 故障转正常 (遥信分位为正常)
        ptPortVal->bCommOK = TRUE;
    }
    Time_TimerBReset( &(ptPortVal->tTimerCommOK) ); // 重启计时器

    return TRUE;
}

// ============================================================================
// 函数功能：初始化端口控制变量
// 输入参数：bFirst=TRUE:第一次初始化
// 输出参数：无
// 返回值：  无
// ============================================================================
void Init_PortCtrl( UINT8 byPortNo, BOOLEAN bFirst )
{
#if (0)
    tagPSysSet    ptSysSet;
    tagPPortVal   ptPortVal;
    tagPPortSet   ptPortSet;
    tagPRecvCtrl  ptRecvCtrl;
    tagPSendCtrl  ptSendCtrl;
    tagPDataCtrl  ptDataCtrl;
    tagPEventCtrl ptEventCtrl;
    tagPMsgCtrl   ptMsgCtrl;
    tagPCmdCtrl   ptCmdCtrl;
    tagPCmdCtrl   ptLastCmdCtrl;
    UINT16        wTemp;
    UINT16        wPortType;
    UINT8         byLoop;

    for( byLoop = 0; byLoop < CN_NUM_PORT; byLoop++ )
    {

        if( ( 0xFF != byPortNo ) && ( byLoop != byPortNo ) )
        {
            continue;
        }
        ptPortVal = &g_tPortVal[byLoop];

        ptPortVal->ptPortSet = &(g_ptPortSet[byLoop]);                                                                  // 通讯端口参数变量指针

        if( bFirst )
        {
            // 端口通讯状态
            ptPortVal->bCommOK = FALSE;
            Time_TimerBStart( &( ptPortVal->tTimerCommOK ), CN_TIMEOUT_COMM_ERROR_PORT*1000); // 由规约程序初始化时重新赋值

            ptPortVal->byCmdNum    = 0;                                                                          // 等待的命令数目
            ptPortVal->wResetTimes = 0;                                                                          // 通讯口复位次数
            //for( wTemp = 0; wTemp < 512; wTemp++ )
            // {
            // ptPortVal->dwRes[wTemp] = 0;
            //}
        }
        ptPortVal->bNeedSwitch      = FALSE;
        ptPortVal->wPollingTimes    = 1;                                                                             // 循环查询的次数
        ptPortVal->byCurRecvMsgCtrl = 0;
        Time_TimerBStart( &ptPortVal->tTimerPolling, 10);
        Time_TimerBStart( &ptPortVal->tTimerClock, 60*1000);
        for( wTemp = 0; wTemp < CN_NUM_PORT; wTemp++ )
        {
            ptPortVal->wCmdSta[wTemp] = CN_CMD_IDLE;
        }

        ptLastCmdCtrl             = &ptPortVal->tLastCmdCtrl;
        ptLastCmdCtrl->wCmdSource = 0xFFFF;
        ptLastCmdCtrl->wCmdCode   = CN_CMD_IDLE;
        Time_TimerBStop( &ptLastCmdCtrl->tCmdTimer );

        ptCmdCtrl             = &ptPortVal->tCmdCtrl;
        ptCmdCtrl->wCmdSource = 0xFFFF;
        ptCmdCtrl->wCmdCode   = CN_CMD_IDLE;
        ptCmdCtrl->wCmdLast   = CN_CMD_IDLE;
        Time_TimerBStop( &ptCmdCtrl->tCmdTimer );

        ptRecvCtrl             = &ptPortVal->tRecvCtrl;
        ptRecvCtrl->wRecvRead  = 0;
        ptRecvCtrl->wRecvWrite = 0;
        Time_TimerBStop( &ptRecvCtrl->tTimerRecv );
        Time_TimerBStop( &ptRecvCtrl->tTimerMsg );

        ptSendCtrl            = &ptPortVal->tSendCtrl;
        ptSendCtrl->wSendFlag = CN_STATUS_SEND_IDLE;
        ptSendCtrl->wSendRead = 0;
        ptSendCtrl->wSendLen  = 0;
        Time_TimerBStop( &ptSendCtrl->tTimerAbandon );
        Time_TimerBStart( &ptSendCtrl->tTimerLineIdle, 10 ); /* can send right now */

        ptDataCtrl              = &( ptPortVal->tDataCtrl );                    // 数据发送控制变量
        ptDataCtrl->wSendKind   = 0;                                            // 下次要发送的信号的类型
        ptDataCtrl->wOffsetYC   = 0;                                            // 下次要发送的遥测的起始点号
        ptDataCtrl->wOffsetYX   = 0;                                            // 下次要发送的遥信的起始点号
        ptDataCtrl->wOffsetYM   = 0;                                            // 下次要发送的遥脉的起始点号
        ptDataCtrl->wOffsetYC_B = 0;                                            // 下次要发送的遥测的起始点号(B类)
        ptDataCtrl->wOffsetYC_C = 0;                                            // 下次要发送的遥测的起始点号(C类)
        Time_TimerBStart( &( ptDataCtrl->tTimerYC ), 10 );
        Time_TimerBStart( &( ptDataCtrl->tTimerYX ), 10 );
        Time_TimerBStart( &( ptDataCtrl->tTimerYM ), 10);
        Time_TimerBStart( &( ptDataCtrl->tTimerYC_B ), 10 );
        Time_TimerBStart( &( ptDataCtrl->tTimerYC_C ), 10);

        ptEventCtrl                = &( ptPortVal->tEventCtrl );                // 事件发送控制变量
        ptEventCtrl->wEventKind    = CN_CMD_IDLE;
        ptEventCtrl->wEventQueueID = 0xFFFF;
        Time_TimerBStop( &( ptEventCtrl->tEventTimer ) );

        for( wTemp = 0; wTemp < CN_NUM_MSG_PORT; wTemp++ )
        {
            ptMsgCtrl              = &ptPortVal->tMsgCtrl[wTemp];
            ptMsgCtrl->byMsgStatus = CN_STATUS_RECEIVE_SYNCH;
            ptMsgCtrl->byMsgType   = 0;
            ptMsgCtrl->wLenCur     = 0;
            ptMsgCtrl->wLenAll     = 0;
        }
    }
#endif
    return;
}
// =====================================================================================
//
// ======================================================================================
void Sio_Port_Set(INT8U byPortNo)
{
//    USART_InitTypeDef  USART_InitStructure;
    //INT16U  wBaudRateID,wParityID;
    //INT32U  dwBaudRate;
    //INT16U  wParity;
    //INT16U  wUsed,wProtocolID;
    //BOOLEAN bFlag = TRUE;
        
    if( byPortNo >=CN_NUM_PORT_SIO ) return ;
#if (0)
    //波特率
    wBaudRateID   = TLVPara_GetParaValue( EN_TLV_PARA_SIO_PORTSET,(byPortNo*CN_LONG_TLV_CLASS_SIO_PORTSET+0));//读TLV参数区
    if(wBaudRateID>CN_NUM_COMBAUDRATE) bFlag = FALSE; //无效的波特率
    else
    {
        bFlag = TRUE;
        switch(wBaudRateID)
        {
            case 0:
                dwBaudRate = 300;break;
            case 1:
                dwBaudRate = 600;break;
            case 2:
                dwBaudRate = 1200;break;
            case 3:
                dwBaudRate = 2400;break;
            case 4:
                dwBaudRate = 4800;break;
            case 5:
                dwBaudRate = 9600;break;
            case 6:
                dwBaudRate = 19200;break;
            case 7:
                dwBaudRate = 38400;break;
            case 8:
                dwBaudRate = 57600;break;
            case 9:
                dwBaudRate = 115200;break;
            case 10:
                dwBaudRate = 230400;break;
            case 11:
                dwBaudRate = 460800;break;
            case 12:
                dwBaudRate = 921600;break;
            default:
                  break;                            
        }    
    }
    
    //校验位
    wParityID = TLVPara_GetParaValue( EN_TLV_PARA_SIO_PORTSET,(byPortNo*CN_LONG_TLV_CLASS_SIO_PORTSET+1));//读TLV参数区
    if(wParityID>CN_NUM_VERIFY) bFlag = FALSE; //无效的检验位ID
    switch( wParityID )
    {
        case 0:
            wParity = CN_UART_PARITY_NONE ;
            break;
        case 1:
            wParity = CN_UART_PARITY_EVEN;
            break;
        case 2:
            wParity = CN_UART_PARITY_ODD;
            break;
        default:
            wParity = CN_UART_PARITY_EVEN;
            break;  
    }
    
    //端口使用的规约ID
    wProtocolID = TLVPara_GetParaValue( EN_TLV_PARA_SIO_PORTSET,(byPortNo*CN_LONG_TLV_CLASS_SIO_PORTSET+2));//读TLV参数区
    if(wProtocolID<CN_NUM_PROTOL) 
      g_ptPortSet[byPortNo].wProtocolID = wProtocolID;
    
    //端口是否使用    
    wUsed = TLVPara_GetParaValue( EN_TLV_PARA_SIO_PORTSET,(byPortNo*CN_LONG_TLV_CLASS_SIO_PORTSET+3));//读TLV参数区
    if(wUsed == TRUE) g_ptPortSet[byPortNo].wUsed = TRUE;
    else              g_ptPortSet[byPortNo].wUsed = FALSE;
    g_ptPortSet[byPortNo].wPortType = CN_PORT_RS485;       
    g_ptPortSet[byPortNo].wPortUsage = EN_PORT_USAGE_FOR_MON;   
   if(bFlag== TRUE)
   {
        switch( byPortNo )
        {//参数读入
        case  0 :   //USART0,串口0
            g_tPortVal[0].DevFp = Driver_OpenDevice("USART1",O_RDWR,CN_TIMEOUT_FOREVER);
            Driver_CtrlDevice(g_tPortVal[0].DevFp,CN_UART_SET_BAUD,dwBaudRate,0);
            Driver_CtrlDevice(g_tPortVal[0].DevFp,CN_UART_START,0,0);
            //      USART_InitStructure.USART_BaudRate = dwBaudRate;
            //      USART_InitStructure.USART_Parity = wParity;
            //      Uart_HardInit(CN_UART_MON1, &USART_InitStructure);
            break;
        case  1 :   //USART1,串口1
            g_tPortVal[1].DevFp = Driver_OpenDevice("USART2",O_RDWR,CN_TIMEOUT_FOREVER);
            Driver_CtrlDevice(g_tPortVal[1].DevFp,CN_UART_SET_BAUD,dwBaudRate,0);
            Driver_CtrlDevice(g_tPortVal[1].DevFp,CN_UART_START,0,0);
            //      USART_InitStructure.USART_BaudRate = dwBaudRate;
            //      USART_InitStructure.USART_Parity = wParity;
            //      Uart_HardInit(CN_UART_MON2, &USART_InitStructure);
            break;
        default :
            break;
        }//end for "switch( byPortNo )"
    
  }

#endif

//  TLVPara_SaveEditToIIC(EN_TLV_PARA_SIO_PORTSET);
  return  ;
}
// ============================================================================
// 函数功能： 物理网口IP/MAC初始化
// 输入参数：无
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================

void Net_MAC_Init(INT8U byPortNo)
{

	//DWORD     dwLoop;
    INT8U     u8IP[4];
    INT8U     u8Mac[6];
    INT8U     u8SubNetMask[4];
    INT8U     u8GateWay[4];
    INT8U     u8DNS[4];    
    INT8U     u8Broad[4];

    tagHostAddrV4  ipv4addr;
    char ip[16];
    char submask[16];
    char dns[16];
    char gatway[16];
    char broad[16];

    u32 ret,r_addr;
    struct NetDev* dev;

#if (1)
        memset((void *)ip,0,16);
        memset((void *)submask,0,16);
        memset((void *)dns,0,16);
        memset((void *)gatway,0,16);
        memset((void *)broad,0,16);

// 获取装置IP地址
//        r_addr=CN_EVC_HMI_EVCIP_ADDR;
//        ret=AT24_ReadBytes(r_addr,u8IP,CN_EVC_HMI_LEN_EVCIP);
// 获取网关
//        r_addr=CN_EVC_HMI_GATE_ADDR;
//        ret=AT24_ReadBytes(r_addr,u8GateWay,CN_EVC_HMI_LEN_GATE);

    if(ret)
    {
        ;
    }
    
#if 1
// 手动设置IP
    u8IP[3] = 192;
    u8IP[2] = 168;
    u8IP[1] = 253;
    u8IP[0] = 3;
// 手动设置网关
    u8GateWay[3] = 192;
    u8GateWay[2] = 168;
    u8GateWay[1] = 253;
    u8GateWay[0] = 240;
#endif

// 获取子网掩码
        u8SubNetMask[3] = 255;
        u8SubNetMask[2] = 255;
        u8SubNetMask[1] = 255;
        u8SubNetMask[0] = 0;
// 获取DNS
        u8DNS[3] = u8IP[3];
        u8DNS[2] = u8IP[2];
        u8DNS[1] = u8IP[1];
        u8DNS[0] = 1;
// 获取组播地址    
        u8Broad[3] = u8IP[3];
        u8Broad[2] = u8IP[2];
        u8Broad[1] = u8IP[1];
        u8Broad[0] = 255;
// 获取MAC地址  
        u8Mac[0] = 0;
        u8Mac[1] = 0;               //读TLV参数区   
        u8Mac[2] = u8IP[3];         //读TLV参数区   
        u8Mac[3] = u8IP[2];         //读TLV参数区   
        u8Mac[4] = u8IP[1];         //读TLV参数区   
        u8Mac[5] = u8IP[0];
        
        dev = NetDevGet(CFG_SELECT_NETCARD);
        if(!(NetDevCtrl(dev, EN_NETDEV_SETMAC, (ptu32_t)u8Mac)))
        {
            printk("MAC SET failed\r\n");
        }

        printf("设置装置IP地址: %03d.%03d.%03d.%03d \r\n", u8IP[3], u8IP[2], u8IP[1], u8IP[0]);
        sprintf(ip, "%d.%d.%d.%d", u8IP[3],            \
                                   u8IP[2],            \
                                   u8IP[1],            \
                                   u8IP[0]);
        printf("设置装置网关: %03d.%03d.%03d.%03d \r\n", u8GateWay[3], u8GateWay[2], u8GateWay[1], u8GateWay[0]);
        sprintf(gatway, "%d.%d.%d.%d", u8GateWay[3],            \
                                       u8GateWay[2],            \
                                       u8GateWay[1],            \
                                       u8GateWay[0]); 
        printf("设置装置子网掩码: %03d.%03d.%03d.%03d \r\n", u8SubNetMask[3], u8SubNetMask[2], u8SubNetMask[1], u8SubNetMask[0]);
        sprintf(submask, "%d.%d.%d.%d", u8SubNetMask[3],            \
                                        u8SubNetMask[2],            \
                                        u8SubNetMask[1],            \
                                        u8SubNetMask[0]);  
        printf("设置装置DNS: %03d.%03d.%03d.%03d \r\n", u8DNS[3], u8DNS[2], u8DNS[1], u8DNS[0]);
        sprintf(dns, "%d.%d.%d.%d", u8DNS[3],            \
                                    u8DNS[2],            \
                                    u8DNS[1],            \
                                    u8DNS[0]);
        printf("设置装置组播地址: %03d.%03d.%03d.%03d \r\n", u8Broad[3], u8Broad[2], u8Broad[1], u8Broad[0]);
        sprintf(broad, "%d.%d.%d.%d", u8Broad[3],            \
                                      u8Broad[2],            \
                                      u8Broad[1],            \
                                      u8Broad[0]);     
        
        memset((void *)&ipv4addr,0,sizeof(ipv4addr));
        ipv4addr.ip      = inet_addr(ip);
        ipv4addr.submask = inet_addr(submask);
        ipv4addr.gatway  = inet_addr(gatway);
        ipv4addr.dns     = inet_addr(dns);
        ipv4addr.broad   = inet_addr(broad);
        
        if(RoutCreate(CFG_SELECT_NETCARD,EN_IPV_4,(void *)&ipv4addr,CN_ROUT_NONE))
        {
           printk("%s:Add %s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
        }
        else
        {
            printk("%s:Add %s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
        }

    // delet the unuse macro definition
    // wangrq @ 2070502
#if (CN_EVC_MODEM)
        ;
#else
        RoutSetDefault(EN_IPV_4,ipv4addr.ip);
#endif
#endif
#if (0)
    memset((void *)&ipv4addr,0,sizeof(ipv4addr));
    ipv4addr.ip      = inet_addr("192.168.253.1");
    ipv4addr.submask = inet_addr("255.255.255.0");
    ipv4addr.gatway  = inet_addr("192.168.253.241");
    ipv4addr.dns     = inet_addr("192.168.253.1");
    ipv4addr.broad   = inet_addr("192.168.253.255");
    if(RoutCreate(CFG_SELECT_NETCARD,EN_IPV_4,(void *)&ipv4addr,CN_ROUT_NONE))
    {
        printk("%s:Add %s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
    }
    else
    {
        printk("%s:Add %s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
    }
// 如果有无线模块，就不设置默认网关。
// 如果没有无线模块，需要通过网线连接DTU，就需要添加默认网关
#if (CN_EVC_MODEM)
    ;
#else
    RoutSetDefault(EN_IPV_4,ipv4addr.ip);
#endif
#endif

    return ;
}

void MAC_Set( )
{
//    DWORD     dwLoop;
//    INT16U    u16IP[4];
    INT8U     u8Mac[6];
//    INT16U    u16SubNetMask[4];

//    tagTLVTab   *ptTLVParaTab;                                          // 指向TLV参数表基址
//    WORD        *pwTLVUsing;                                             // TLV参数缓冲区基址(编辑区)

//    ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[EN_TLV_PARA_MAC_SET];         // 指针初始化
//    pwTLVUsing    = ptTLVParaTab->pwTLVUsing;
#if (0)
    u8Mac[0] = 0;
    u8Mac[1] = (INT8U)TLVPara_GetParaValue( EN_TLV_PARA_MAC_SET,0);//读TLV参数区    
    u8Mac[2] = (INT8U)TLVPara_GetParaValue( EN_TLV_PARA_MAC_SET,1);//读TLV参数区    
    u8Mac[3] = (INT8U)TLVPara_GetParaValue( EN_TLV_PARA_MAC_SET,2);//读TLV参数区    
    u8Mac[4] = (INT8U)TLVPara_GetParaValue( EN_TLV_PARA_MAC_SET,3);//读TLV参数区    
    u8Mac[5] = (INT8U)TLVPara_GetParaValue( EN_TLV_PARA_MAC_SET,4);//读TLV参数区

#endif

    struct NetDev* dev;
    dev = NetDevGet(CFG_SELECT_NETCARD);
    if(!(NetDevCtrl(dev, EN_NETDEV_SETMAC, (ptu32_t)u8Mac)))
    {
        printk("MAC SET failed\r\n");
    }
    return ;
}

// ============================================================================
// 函数功能： 物理网口IP/MAC设置
// 输入参数：无
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================
//
//void Net_Port_Set(INT8U byPortNo,UINT32 u32oldip)
//{
////    DWORD   dwLoop;
//    INT16U    u16IP[4];
////    INT16U    u16Mac[6];
//    INT16U    u16SubNetMask[4];
//
//    //tagTLVTab   *ptTLVParaTab;                                          // 指向TLV参数表基址
//    //WORD        *pwTLVUsing;                                             // TLV参数缓冲区基址(编辑区)
//
//    tagHostAddrV4  ipv4addr;
//    char ip[16];
//    char submask[16];
////    char dns[16];
////    char gatway[16];
////    char broad[16];
//
//    if( byPortNo >=CN_NUM_PORT_NET ) return ;
//    if(byPortNo==0)
//    {
////        ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[EN_TLV_PARA_NET_PORTSET1];         // 指针初始化
//    }
//    if(byPortNo==1)
//    {
////        ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[EN_TLV_PARA_NET_PORTSET2];         // 指针初始化
//    }
////    pwTLVUsing    = ptTLVParaTab->pwTLVUsing;
//
//    memset((void *)ip,0,16);
//    memset((void *)submask,0,16);
////    memset((void *)dns,0,16);
////    memset((void *)gatway,0,16);
////    memset((void *)broad,0,16);
////    byPortNo -= CN_NUM_PORT_SIO;
//
////for( dwLoop=0; dwLoop<CN_NUM_PORT_NET; dwLoop++ )
////{
////    if (dwLoop!=byPortNo) continue;
//            if(byPortNo==0)
//            {
//                u16IP[0]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,0);//读TLV参数区
//                u16IP[1]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,1);//读TLV参数区
//                u16IP[2]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,2);//读TLV参数区
//                u16IP[3]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,3);//读TLV参数区
//                u16SubNetMask[0] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,4);//读TLV参数区
//                u16SubNetMask[1] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,5);//读TLV参数区
//                u16SubNetMask[2] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,6);//读TLV参数区
//                u16SubNetMask[3] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,7);//读TLV参数区
//            }
//            if(byPortNo==1)
//            {
//                u16IP[0]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,0);//读TLV参数区
//                u16IP[1]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,1);//读TLV参数区
//                u16IP[2]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,2);//读TLV参数区
//                u16IP[3]  = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,3);//读TLV参数区
//                u16SubNetMask[0] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,4);//读TLV参数区
//                u16SubNetMask[1] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,5);//读TLV参数区
//                u16SubNetMask[2] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,6);//读TLV参数区
//                u16SubNetMask[3] = TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,7);//读TLV参数区
//            }
//
//
//
//        sprintf(ip,"%d.%d.%d.%d",u16IP[0],u16IP[1],u16IP[2],u16IP[3]);
//        sprintf(submask,"%d.%d.%d.%d",u16SubNetMask[0],u16SubNetMask[1],u16SubNetMask[2],u16SubNetMask[3]);
////        sprintf(gatway,"%d.%d.%d.%d",u16IP[0],u16IP[1],0,1);
////        sprintf(dns,"%d.%d.%d.%d",u16IP[0],u16IP[1],0,1);
////        sprintf(broad,"%d.%d.%d.%d",u16IP[0],u16IP[1],0,255);
////        sprintf(name,"%s_%d",gAppNetName,dwLoop);
//
//        memset((void *)&ipv4addr,0,sizeof(ipv4addr));
//        ipv4addr.ip      = inet_addr(ip);
//        ipv4addr.submask = inet_addr(submask);
//        ipv4addr.gatway  = INADDR_ANY;  //inet_addr(gatway);
//        ipv4addr.dns     = INADDR_ANY;  //inet_addr(dns);
//        ipv4addr.broad   = INADDR_ANY;  //inet_addr(broad);
////        if(RoutSet(CFG_SELECT_NETCARD,EN_IPV_4,u32oldip,(void *)&ipv4addr))
//        extern bool_t RouterItemChange(enum_ipv_t ver,ipaddr_t ipold,tagHostAddrV4 *newaddr);
//        if(RouterItemChange(EN_IPV_4,u32oldip,(void *)&ipv4addr))
//        {
//           printk("%s:config success\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
//        }
////    }
////    TLVPara_SaveEditToIIC(EN_TLV_PAoRA_NET_PORTSET);
//    return ;
//}
// ============================================================================
// 函数功能：通讯设置
// 输入参数：无
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================

void Comm_Port_Set(INT8U ucCommNo)
{
    INT16U    u16IP[4];
//   BYTE byIP[4];
//    char ip[4];
    tagParamNet       *ptParamNet;
    INT8U ulCommNo ;
    u32               ret,r_addr;  
    UINT8             byNetPort[2] = {0};
    if( ucCommNo >=CN_NUM_NET_MON+CN_NUM_PORT_SIO ) return ;
//    memset((void *)ip,0,4);
    ptParamNet = (tagParamNet *)(g_ptPortSet[ucCommNo].dwParam);
    ulCommNo = ucCommNo ;
    ucCommNo -=  CN_NUM_PORT_SIO ;

#if (0)
    // sibida
    //121,40,230,142

    // 主网主机IP地址
    u16IP[0]   = 0;//202;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+0));//读TLV参数区
    u16IP[1]   = 0;//104;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+1));//读TLV参数区
    u16IP[2]   = 0;//113;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+2));//读TLV参数区
    u16IP[3]   = 0;//211;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+3));//读TLV参数区
    
    ptParamNet->byMonIP[0]= u16IP[0] ;
    ptParamNet->byMonIP[1]= u16IP[1] ;
    ptParamNet->byMonIP[2]= u16IP[2] ;
    ptParamNet->byMonIP[3]= u16IP[3] ;
#endif
    // 主网从机IP地址
    u16IP[0]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+4));//读TLV参数区
    u16IP[1]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+5));//读TLV参数区
    u16IP[2]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+6));//读TLV参数区
    u16IP[3]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+7));//读TLV参数区
    ptParamNet->bySlaMonIP[0]= u16IP[0] ;
    ptParamNet->bySlaMonIP[1]= u16IP[1] ;
    ptParamNet->bySlaMonIP[2]= u16IP[2] ;
    ptParamNet->bySlaMonIP[3]= u16IP[3] ;
    // 从网主机IP地址
    u16IP[0]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+8));//读TLV参数区
    u16IP[1]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+9));//读TLV参数区
    u16IP[2]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+10));//读TLV参数区
    u16IP[3]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+11));//读TLV参数区
    ptParamNet->byMonIP2[0]= u16IP[0] ;
    ptParamNet->byMonIP2[1]= u16IP[1] ;
    ptParamNet->byMonIP2[2]= u16IP[2] ;
    ptParamNet->byMonIP2[3]= u16IP[3] ;
    // 从网从机IP地址
    u16IP[0]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+12));//读TLV参数区
    u16IP[1]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+13));//读TLV参数区
    u16IP[2]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+14));//读TLV参数区
    u16IP[3]   = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+15));//读TLV参数区
    ptParamNet->bySlaMonIP2[0]= u16IP[0] ;
    ptParamNet->bySlaMonIP2[1]= u16IP[1] ;
    ptParamNet->bySlaMonIP2[2]= u16IP[2] ;
    ptParamNet->bySlaMonIP2[3]= u16IP[3] ;

// 从IIC中读取端口号
    r_addr=CN_EVC_HMI_PORT_ADDR;
//    ret=AT24_ReadBytes(r_addr,byNetPort,CN_EVC_HMI_LEN_PORT);
    byNetPort[0] = 0x69;
    byNetPort[1] = 0x07;

    ptParamNet->wNetPort = M_BcdToHex(byNetPort[0])*100 + M_BcdToHex(byNetPort[1]);
    printf("main函数: 初始化端口号: %d .\r\n", ptParamNet->wNetPort);
    ptParamNet->bDblMac = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+17));//读TLV参数区
    ptParamNet->bDblNet = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+18));//读TLV参数区
    g_ptPortSet[ulCommNo].wProtocolID = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+19));//读TLV参数区
        
    ptParamNet->byMasPort = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+20));//读TLV参数区
    ptParamNet->bySlaPort = 0;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+21));//读TLV参数区   
    g_ptPortSet[ulCommNo].wUsed = 1;//TLVPara_GetParaValue( EN_TLV_PARA_COMM_PORTSET,(ucCommNo*CN_LONG_TLV_CLASS_COMM_SET+22));//读TLV参数区         
    g_ptPortSet[ulCommNo].wPortType = CN_PORT_NET;       
    g_ptPortSet[ulCommNo].wPortUsage = EN_PORT_USAGE_FOR_MON;   


    if(ret)
    {
    	;
    }
}
// ====================================================================================
// 底层没有提供获取 IP 地址的方式,根据网口编号获取IP地址
// ====================================================================================

s32 GetFlashNetIP(UINT8 byNum,UINT8* pbyBuf)       
{
    if(byNum == 0)
    {
        pbyBuf[0]   = 192;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,0);//读TLV参数区
        pbyBuf[1]   = 168;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,1);//读TLV参数区
        pbyBuf[2]   = 253;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,2);//读TLV参数区
        pbyBuf[3]   = 179;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET1,3);//读TLV参数区
    }
    if(byNum == 1)
    {
        pbyBuf[0]   = 192;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,0);//读TLV参数区
        pbyBuf[1]   = 168;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,1);//读TLV参数区
        pbyBuf[2]   = 253;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,2);//读TLV参数区
        pbyBuf[3]   = 179;//TLVPara_GetParaValue( EN_TLV_PARA_NET_PORTSET2,3);//读TLV参数区
    }

    return 0;
}
//=================预留恢复默认参数接口函数 ===========================================//
// ============================================================================
// 函数功能：通讯端口TLV参数恢复默认
// 输入参数：无
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================
s32 ResetTLV_SIO(void )
{
#if (0)
	//    tagPParamNet    ptParamNet;
    tagPParamSio    ptParamSio;
    tagPPortSet     ptPortSet;
    WORD            *pwTLVEdit;                                         // TLV参数缓冲区基址(编辑区)
    WORD            wTemp;
    DWORD           dwLoop;                                         // 临时变量
    //tagDWord_Byte4  tWB4;
    //tagWord_Byte2   tWB2;
    tagTLVTab       *ptTLVParaTab;                              // 指向TLV参数表基址
//    BYTE            byNetCfg[4]={222,111,112,201};            // 监控主机ip
    ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[CN_TLV_PARA_LOC_SIO_PORTSET];                                // 指针初始化
    pwTLVEdit = ptTLVParaTab->pwTLVEdit;

    // 恢复TLV参数默认
    for( dwLoop=0; dwLoop<CN_NUM_PORT_SIO; dwLoop++ )
    {
        ptPortSet = &(g_ptPortSet[dwLoop]);
        for( wTemp=0; wTemp<8; wTemp++ )
            ptPortSet->dwParam[wTemp] = 0;                      // 通讯参数(校验位、停止位等)
        for( wTemp=0; wTemp<CN_LEN_RES_PORT; wTemp++ )
            ptPortSet->dwRes[wTemp] = 0;                        // 通讯口保留参数
        ptParamSio = (tagPParamSio)(ptPortSet->dwParam);
        ptParamSio->byDataBit = 8;
        ptParamSio->byParity = 2;                               // 偶校验
        ptParamSio->byStopBit = 1;                              // 1位停止位
        ptPortSet->wProtocolID = EN_PROTOCOL_PORT_SIO_IEC_103;
        ptPortSet->wUsed = FALSE;
        ptPortSet->wPortType = CN_PORT_RS485;
        ptPortSet->wPortUsage = EN_PORT_USAGE_FOR_MON;
        ptPortSet->wRunMode  = 0;                               // 运行模式(BasicCAN/PeliCAN, Synch/Asyn, Server/Client)
        ptPortSet->wBaudRate = 9600;
        ptPortSet->dwAddBase  = 0;                              // 端口硬件地址基准

        ptParamSio->byDataBit = 8;
        ptParamSio->byParity  = 2;              // 偶校验
        ptParamSio->byStopBit = 1;              // 停止位1
        *pwTLVEdit++ = ptPortSet->wProtocolID;              // 规约代号
        *pwTLVEdit++ = ptPortSet->wUsed;                    // 是否使用<0-关, 1-开>
        *pwTLVEdit++ = ptPortSet->wPortType;                // 通讯口类型
        *pwTLVEdit++ = ptPortSet->wPortUsage;               // 端口用途<监控/测量、主/从>
        *pwTLVEdit++ = ptPortSet->wRunMode;                 // 运行模式(BasicCAN/PeliCAN, Synch/Asyn, Server/Client)
        *pwTLVEdit++ = ptPortSet->wBaudRate;                // 波特率
        tWB4.dwDword= ptPortSet->dwAddBase;
        *pwTLVEdit++ = tWB4.wWord[CN_WORD_LO];              // 端口硬件地址基准
        *pwTLVEdit++ = tWB4.wWord[CN_WORD_HI];              // 端口硬件地址基准

        tWB2.cByte[CN_BYTE_LO]=ptParamSio->byDataBit;
        tWB2.cByte[CN_BYTE_HI]=ptParamSio->byParity;
        *pwTLVEdit++ = tWB2.wWord;
        tWB2.cByte[CN_BYTE_LO]=ptParamSio->byStopBit;
        tWB2.cByte[CN_BYTE_HI]=0;
        *pwTLVEdit++ = tWB2.wWord;
        for( wTemp=1; wTemp<8; wTemp++ )                    // 通讯参数(校验位、停止位等)
        {
            tWB4.dwDword= ptPortSet->dwParam[wTemp];
            *pwTLVEdit++ = tWB4.wWord[CN_WORD_LO];
            *pwTLVEdit++ = tWB4.wWord[CN_WORD_HI];
        }
        for( wTemp=0; wTemp<CN_LEN_RES_PORT; wTemp++ )      // 通讯口保留参数
        {
            tWB4.dwDword= ptPortSet->dwRes[wTemp];
            *pwTLVEdit++ = tWB4.wWord[CN_WORD_LO];
            *pwTLVEdit++ = tWB4.wWord[CN_WORD_HI];
        }
    }
    TLVPara_SaveEditToIIC(EN_TLV_PARA_SIO_PORTSET);  

#endif
    return TRUE;
} 
// ============================================================================
// 函数功能：网口设置TLV参数恢复默认
// 输入参数：无
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================
#if 0
s32 ResetTLV_NET( void )
{
    WORD      *pwTLVEdit;                                       // TLV参数缓冲区基址(编辑区)
    DWORD      dwLoop;                                          // 临时变量
    tagTLVTab       *ptTLVParaTab;                              // 指向TLV参数表基址
    tagTLVParaElement *ptTLVParaElement;
    ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[CN_TLV_PARA_LOC_NET_PORTSET];                                // 指针初始化
    pwTLVEdit = ptTLVParaTab->pwTLVEdit;

    // 恢复TLV参数默认
    for( dwLoop=0; dwLoop<CN_NUM_PORT_NET; dwLoop++ )
    {
        ptTLVParaElement = &(g_tTLVParaModelTab[EN_TLV_PARA_NET_PORTSET].tElement[CN_LONG_TLV_CLASS_NET_PORTSET*dwLoop]);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 0 IP0
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 1 IP1
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 2 IP2
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 3 IP3
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = 0;                           // 4 MAC0
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 5 MAC1
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 6 MAC2
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 7 MAC3
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 8 MAC4
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 9 MAC5
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 10 MASK1 
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 11 MASK2
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 12 MASK3
        ptTLVParaElement += sizeof(tagTLVParaElement);
        *pwTLVEdit++ = ptTLVParaElement->usValDft;  // 13 MASK4
    }
    TLVPara_SaveEditToIIC(EN_TLV_PARA_NET_PORTSET);     
    return TRUE;
}
#endif

// ============================================================================
// 函数功能：通讯端口设置固化函数
// 输入参数：参数数据源标志(0--TLVUsing区， 非0--TLVEdit区)
// 输出参数：无
// 返回值：  TRUE/FALSE
// ============================================================================
s32 ResetTLV_COMM( void )
{
#if (0)
	BYTE    byIP[4];
    tagPParamNet    ptParamNet;
    //       tagPParamSio    ptParamSio;
    tagPPortSet     ptPortSet;
    WORD            *pwTLVEdit;                                         // TLV参数缓冲区基址(编辑区)
    WORD            wTemp;
    DWORD           dwLoop;                                         // 临时变量
    tagTLVTab       *ptTLVParaTab;                              // 指向TLV参数表基址
    tagTLVParaElement *ptTLVParaElement;

    ptTLVParaTab = (tagTLVTab *)&g_tTLVParaTab[CN_TLV_PARA_LOC_COMM_SET];                                // 指针初始化
    pwTLVEdit = ptTLVParaTab->pwTLVEdit;
    // 恢复TLV参数默认
    for( dwLoop=CN_NUM_PORT_SIO; dwLoop<CN_NUM_PORT_SIO+CN_NUM_NET_MON; dwLoop++ )
    {
       ptPortSet = &(g_ptPortSet[dwLoop]);
       for( wTemp=0; wTemp<8; wTemp++ )
           ptPortSet->dwParam[wTemp] = 0;                      // 通讯参数(校验位、停止位等)
       for( wTemp=0; wTemp<CN_LEN_RES_PORT; wTemp++ )
           ptPortSet->dwRes[wTemp] = 0;                        // 通讯口保留参数
       ptParamNet = (tagPParamNet)(ptPortSet->dwParam);

       ptPortSet->wProtocolID = EN_PROTOCOL_PORT_UDP_IEC_103;
       ptPortSet->wPortType = CN_PORT_NET;
       ptPortSet->wPortUsage = EN_PORT_USAGE_FOR_MON;
       ptPortSet->wRunMode  = 0;                               // 运行模式(BasicCAN/PeliCAN, Synch/Asyn, Server/Client)
    //         ptPortSet->wBaudRate = 9600;
    //             ptPortSet->dwAddBase  = 0;                              // 端口硬件地址基准
       ptTLVParaElement = &(g_tTLVParaModelTab[EN_TLV_PARA_COMM_PORTSET].tElement[CN_LONG_TLV_CLASS_COMM_SET*dwLoop]);

       // 主网主机IP地址
       ptParamNet->byMonIP[0] = ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptParamNet->byMonIP[1]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->byMonIP[2]= ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->byMonIP[3]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       // 主网从机IP地址
       ptParamNet->bySlaMonIP[0]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP[1]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP[2]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP[3]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       // 从网主机IP地址
       ptParamNet->byMonIP2[0]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->byMonIP2[1]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->byMonIP2[2]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->byMonIP2[3]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       // 从网从机IP地址
       ptParamNet->bySlaMonIP2[0]= ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP2[1]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP2[2]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
      
       ptParamNet->bySlaMonIP2[3]=  ptTLVParaElement->usValDft;  // 0 IP0
       ptTLVParaElement += sizeof(tagTLVParaElement);
       
       ptParamNet->wNetPort = ptTLVParaElement->usValDft;  //
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptParamNet->bDblMac =  ptTLVParaElement->usValDft;  //
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptParamNet->bDblNet =  ptTLVParaElement->usValDft;  //
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptPortSet->wProtocolID = ptTLVParaElement->usValDft;
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptParamNet->byMasPort =  ptTLVParaElement->usValDft;  //
       ptTLVParaElement += sizeof(tagTLVParaElement);
       ptParamNet->bySlaPort =  ptTLVParaElement->usValDft;  //
       ptTLVParaElement += sizeof(tagTLVParaElement);       
       ptPortSet->wUsed = ptTLVParaElement->usValDft;  
       ptTLVParaElement += sizeof(tagTLVParaElement);
    }
    TLVPara_SaveEditToIIC(EN_TLV_PARA_COMM_PORTSET);        
#endif
    return TRUE;
}

