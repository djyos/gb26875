/*================================================================================
 * 文件名称：EFS_File_Manager.h
 * 文件描述：在EFS简易文件系统中创建文件、删除文件、写文件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间: 
 * 版本修订:
 * 修订人员: 
 *================================================================================*/
 
#ifndef _EFS_FILE_MANAGER_H
#define _EFS_FILE_MANAGER_H

// include 包含关系
//#include "Resource.h"
#include "sunri_types.h"

typedef enum _EN_EFS_TYPE
{
    EN_EFS_TYPE_NETLOG = 0,             // 网络日志文件
    //EN_EFS_TYPE_CHGREC,                 // 充电日志文件
    EN_EFS_TYPE_END,
}enEFSEvent;

enum _EN_EFS_MODE
{
    EN_EFS_MODE_CONTINUOUS = 0,         // 连续第往下写
    EN_EFS_MODE_SEPARATE,               // 单独写，写一条记录就是一个文件
};

typedef struct
{
    char* pDir_Content;
    char* pDir_Prefix;
    char* pDir_Suffix;
    UINT32 dwType;
    UINT32 dwAmount;
    UINT32 dwMode;
    BOOL   bNoContinuous;
    BOOL   bWithDate;
}tag_EFS_Private;

typedef BOOL (*fnEFSEventHook)(enEFSEvent event);

// 外部函数声明
extern BOOL     EFS_Log_Init(fnEFSEventHook fHook);
extern UINT32   EFS_WriteFile(char* pContext, UINT32 dwLen, UINT32 dwType);
extern BOOL     EFS_FormatFile(UINT32 dwType);
extern BOOL     EFS_DelConfig(void);
extern BOOL     EFS_ReadConfig(UINT32* pdwNum, UINT8* pbyHostIP, UINT8* pbyNetGate);
extern BOOL     Standard_IsDigitString(char* pInput);
extern INT32S   Standard_atoi (char* pInput);
extern BOOL     EFS_IsDigitString(char* pInput);
extern UINT32   EFS_atoi (char* pInput);
extern void     EFS_GetSysTime( char *cTime );
extern UINT32   EFS_DelFileWithDate(char* pcFileName, UINT32 dwLen);
extern UINT32 EFS_DelFileWithoutDate(char* pcFileName);
extern UINT32   EFS_SeekFileWithDate(char* pcName, char* pcFileName, char* pDir_Suffix, BOOL bWithDate);
extern UINT32   EFS_SeekFileWithoutDate(char* pcName, char* pcFileName, char* pDir_Suffix);
extern BOOL     EFS_SubStr(char* pcDst, char* pcSrc, UINT32 dwStart, UINT32 dwLen);

/*================================================================================*/
#endif // _EFS_FILE_MANAGER_H
