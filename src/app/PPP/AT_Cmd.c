#include "stdint.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dbug.h>
#include <shell.h>
#include <tcpip/ppp/atcmd.h>
#include <gkernel.h>
#include "PPP_Scan.h"
#include "project_config.h"     //本文件由IDE中配置界面生成，存放在APP的工程目录中。
                                //允许是个空文件，所有配置将按默认值配置。

char *AT_COM_SIMCOM[] = {"AT+CSQ","AT+CICCID"};
char *AT_COM_HUAWEI[] = {"AT+CSQ","AT^ICCID?"};
char *AT_COM_MEIG[]   = {"AT+CSQ","AT+ICCID"};

char AtRcvBuf[50];
char ICCID[21];

// ============================================================================
//功能：获取AT命令。
//参数：function -- 需要AT命令集中的第几个命令；
//返回：该命令
//备注：
// ============================================================================
char *GetAT_Cmd(u32 function)
{
    char *receive;
    char *name[] = {"SIMCOM","HUAWEI","MEIG"};
    receive = CUSTOM_GetInfos();     //获取4G模块的型号
    if(receive == NULL)
    {
        printf("AT : error : no found 4G module .\r\n");
        return NULL;
    }

    if(strstr(receive,name[0]) == NULL)     //根据获取到的型号选择使用的指令集
    {
        if(strstr(receive,name[1]) == NULL)
        {
            if(strstr(receive,name[2]) == NULL)
            {
                printf("AT : error : unsupported modules.\r\n");
                return NULL;
            }
            else
            {
                return AT_COM_MEIG[function];
            }
        }
        else
        {
           return AT_COM_HUAWEI[function];
        }
    }
    else
    {
        return AT_COM_SIMCOM[function];
    }
}

// ============================================================================
//功能：读取信号强度
//参数：无
//返回：-1 -- 读取失败；其他 -- 信号强度；
//备注：
// ============================================================================
s32 GetSignalStrength(void)
{
    u8 i;
    s32 strength;
    u32 good = 0;
    char *argv[6];
    s32 argc;
    char *at_cmd = GetAT_Cmd(0);
    if(at_cmd == NULL)
        return -1;
    if(PppDevLinkIsOk(NET_DEV_NAME))
    {
        memset(AtRcvBuf, 0x0, sizeof(AtRcvBuf));
        argc =AtCmd(AT_DEV_NAME,at_cmd,AtRcvBuf,sizeof(AtRcvBuf),6,argv);
        if(argc > 0)
        {
            for(i = 0; i < (sizeof(AtRcvBuf)-5); i++)
            {
                if((AtRcvBuf[i] == '+') && (AtRcvBuf[i+1] == 'C') &&
                   (AtRcvBuf[i+2] == 'S') && (AtRcvBuf[i+3] == 'Q') &&
                   (AtRcvBuf[i+4] == ':') && (AtRcvBuf[i+5] == ' '))
                {
                    good = i+6;
                    break;
                }
            }
            if(!good)
                return -1;
            i = good;
            for(; i < (sizeof(AtRcvBuf)-i); i++)
            {
                if(AtRcvBuf[i] < '0' || AtRcvBuf [i] > '9')
                {
                    AtRcvBuf[i] = '\0';
                    break;
                }
            }
            if(i == (sizeof(AtRcvBuf)-i))
                return -1;
            strength = atoi((char*)(AtRcvBuf+good));
            return strength;
        }

    }
    return -1;
}

// ============================================================================
//功能：读取ICCID
//参数：bArgC -- 参数数量；第一个参数为设备名，缺省为“uat”
//返回：NULL -- 读取失败；其他 -- 获取到的ICCID（以字符串形式）；
//备注：
// ============================================================================
char *GetICCID(void)
{
    u8 i;
    u32 len,good = 0;
    char *argv[6];
    s32 argc;
    char *at_cmd = GetAT_Cmd(1);
    if(at_cmd == NULL)
        return -1;

    if(PppDevLinkIsOk(NET_DEV_NAME))
    {
        argc =AtCmd(AT_DEV_NAME,at_cmd,AtRcvBuf,sizeof(AtRcvBuf),6,argv);
        if(argc > 0)
        {
            for(i = 0; i < (sizeof(AtRcvBuf) - 8); i++)
            {
                if((AtRcvBuf[i] == '+') || (AtRcvBuf[i] == '^'))
                {
                    if((AtRcvBuf[i+1] == 'I') &&(AtRcvBuf[i+2] == 'C') &&
                       (AtRcvBuf[i+3] == 'C') &&(AtRcvBuf[i+4] == 'I') &&
                       (AtRcvBuf[i+5] == 'D') &&(AtRcvBuf[i+6] == ':') &&
                       (AtRcvBuf[i+7] == ' '))
                    {
                        good = i+8;
                        break;
                    }
                }
            }
            if(!good)
                return NULL;
            i = good;
            for(; i < sizeof(AtRcvBuf); i++)
            {
                if(AtRcvBuf[i] < '0' || AtRcvBuf [i] > '9')
                {
                    if(AtRcvBuf[i] < 'A' || AtRcvBuf [i] > 'Z')
                        break;
                }
            }
            len = i-good;
            if(len != 20)
                return NULL;

            memset(ICCID, 0x0, sizeof(ICCID));
            for(i = 0; i < len; i++ )
            {
        //        ICCID[i] = response[good + i] - '0';           //返回阿拉伯数字
                ICCID[i] = AtRcvBuf[good + i] ;                  //返回ASCII
            }
            ICCID[len] = '\0';
            return (ICCID);
        }
    }
    return NULL;
}


bool_t Sh_GetICCID(char *param)
{
    printf("ICCID = %s \r\n",GetICCID());
    return true;
}

bool_t Sh_GetSignalStrength(char *param)
{
    printf("SignalStrength = %d \r\n",GetSignalStrength());
    return true;
}

ADD_TO_ROUTINE_SHELL(getss,Sh_GetSignalStrength,"获取信号强度 :COMMAND:getss+enter");
ADD_TO_ROUTINE_SHELL(geticcid,Sh_GetICCID,"获取ICCID :COMMAND:geticcid+enter");
