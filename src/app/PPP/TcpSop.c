/*================================================================================
 * 文件描述：103通讯用网络Tcp封装
 * 文件版本: V1.00
 * 开发人员: 姜超
 * 定版时间:
 * 版本修订:
 * 修订人员:
 *================================================================================*/

#include "TcpSop.h"

#if( CN_INCLUDE_TCP )        // 包含UDP
#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include "stdlib.h"
#include "endian.h"
#include "string.h"
#include "os.h"

#include <sys/socket.h>
#include <endian.h>

//#include "InstCell.h"
//#include "DebugShell.h"
#include <os.h>
#include "pppdata.h"


/*================================================================================*/

//#define  CN_TCPSERVERSTART_STACK_LEN    0xC00
//static UINT8  gs_TCPServerStart_Stack[CN_TCPSERVERSTART_STACK_LEN];
#define  CN_TCPMON_TASK_STACK_LEN    0x1000
static UINT8  gs_TcpMon_Task_Stack[CN_TCPMON_TASK_STACK_LEN];
#define  CN_TCPREAD_TASK_STACK_LEN   0x1000
static UINT8  gs_TcpRead_Task_Stack[CN_TCPREAD_TASK_STACK_LEN];
#define  CN_TCPSEND_TASK_STACK_LEN   0x1000
static UINT8  gs_TcpSend_Task_Stack[CN_TCPSEND_TASK_STACK_LEN];

unsigned int g_uiLogTcp = 0;            // for print logmsg

#define CN_TCP_TYPE_IP  ( 0 )            // UDP/IP
#define CN_TCP_TYPE_BD  ( 1 )            // UDP广播<能接收广播报文>
#define CN_TCP_TYPE     ( CN_TCP_TYPE_BD )
static u16 iReadTaskID_Type = 0;     // Tcp读取报文事件类型ID
static u16 iSendTaskID_Type = 0;     // Tcp发送报文事件类型ID

static unsigned int uiModemReset = 0;   // 无线模块重启标志
static unsigned int uiModemReset2 = 0;   // 无线模块重启标志2

#if 0
extern sNum_tagTimeBCD tSend_Protocol_Start_OK[30];
#endif
//extern sNum_tagTimeBCD tSend_Protocol_Start_Fail[60];
//extern sNum_tagTimeBCD tTcp_Start_Succeed[30];
//extern sNum_tagTimeBCD tSend_Protocol_Start_Lib_Fail[30];
//extern sNum_tagTimeBCD tSend_Protocol_Start_Lib_Fail2[60];



extern void Time_DbgSysTime( void );    // 打印系统时间
extern s32 CUSTOM_DeviceReady(void);

//-------each struct reflect a tcp connection------------
struct MTcpCtrlStruc m_TcpCtrlStruc;
// ================================================================================//
// 函数功能:根据无线模组PPP拨号状态，改变全局结构体中nModerm的状态，
//          该状态由TcpMonTask函数读取,决定是否开启客户端连接
// 传入参数:nModerm
// 返回值  :无
//=================================================================================//
void TcpSetModermState( short nModerm )
{

    m_TcpCtrlStruc.nModerm = nModerm;
    
    return;
}

short TcpGetModermState( void )
{    
    return m_TcpCtrlStruc.nModerm;
}

unsigned int TCP_Get_Modem_RstFlag(void)
{
    return (0 == uiModemReset);
}

unsigned int TCP_Get_Modem_RstFlag2(void)
{
    return (0 == uiModemReset2);
}


int TcpGetSendState( short nTcpNo )
{
    struct MTcpConn *pConn;
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState != Using )
    {
        return -1;
    }
    if( pConn->nSendState != SendFree )
    {
        return -2;
    }
    return 1;
}

int TcpGetSockState( short nTcpNo )
{
    struct MTcpConn *pConn;
    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return FALSE;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    return ( Using == pConn->nState );
}

//===========================================================================================
// this function is used to initialize all tcp related global value
// Notice: this function could ONLY be called once!!
// ==========================================================================================
void Tcp_Initial(void)
{
    struct  MTcpConn *pConn;
    short            iLoop;

    m_TcpCtrlStruc.nTcpNo    = 0;
    m_TcpCtrlStruc.nServerNo = 0;

    //初始化
    for( iLoop = 0; iLoop < CN_TCP_NUM_SOCK; iLoop++ )
    {
        pConn            = &( m_TcpCtrlStruc.m_ConnArray[iLoop] );
        pConn->pReadBuff = (char*)( 0 );
        pConn->pSendBuff = (char*)( 0 );
        pConn->pReadPara = (char*)( 0 );

        pConn->nTimeLimit   = 30;
        pConn->iReadBuffLen = 512;//1024;
        pConn->iSendBuffLen = 512;//1024;
        pConn->iReadTaskID  = -1;
        pConn->iSendTaskID  = -1;
        pConn->fd           = -1;
        pConn->iFailNum     = 0;
        pConn->iFailRstFlag = 0;
        Time_TimerBStop( &pConn->tTimerTcpRecv );
        Time_TimerBStop( &pConn->tTimerReConnect );
    }

    m_TcpCtrlStruc.ptSemIDServer = Lock_MutexCreate("TcpCtrlStru-SemSrv");
    m_TcpCtrlStruc.ptSemIDConn   = Lock_MutexCreate("TcpCtrlStru-SemConn");
    return;
}

//======================================================================================================================
// 增加链接
// =====================================================================================================================
short TcpAddConnection( BYTE* ServerIP, BYTE* ClientIP, short nPort, int nType, int ( *ReadFunc )( ), BYTE byDstSlot )
{
    struct MTcpConn *pConn, *pConnTmp;
    char            szServerIP[20], szClientIP[20];
    short           nTcpNo, iLoop, iRt;

    Lock_MutexPend(m_TcpCtrlStruc.ptSemIDConn,CN_TIMEOUT_FOREVER);

    // 检查链接是否已经在列表中
    nTcpNo = m_TcpCtrlStruc.nTcpNo;
    sprintf( szServerIP, "%d.%d.%d.%d", ServerIP[0], ServerIP[1], ServerIP[2], ServerIP[3] );
    sprintf( szClientIP, "%d.%d.%d.%d", ClientIP[0], ClientIP[1], ClientIP[2], ClientIP[3] );
    for( iLoop = 0; iLoop < nTcpNo; iLoop++ )
    {
        pConnTmp = &( m_TcpCtrlStruc.m_ConnArray[iLoop] );
        if( ( pConnTmp->nServerPort == nPort )
            && ( 0 == strcmp( szServerIP, pConnTmp->ServerIP ) )
            && ( 0 == strcmp( szClientIP, pConnTmp->ClientIP ) ) )
        {
            if( g_uiLogTcp )
            {
                printf( "Duplicate Connect[%d]:%s->%s:%d\r\n", nTcpNo, szClientIP, szServerIP, nPort );
            }
            Lock_MutexPost(m_TcpCtrlStruc.ptSemIDConn);
            return iLoop;
        }
    }

    if( nTcpNo >= CN_TCP_NUM_SOCK )
    {
        m_TcpCtrlStruc.nTcpNo = CN_TCP_NUM_SOCK;
        if( g_uiLogTcp )
        {
            printf( "Add Connect[%d]:%s->%s:%d Error,Connect Array is Full!\r\n", nTcpNo, szClientIP, szServerIP, nPort );
        }
        iRt = -1;
    }else
    {
        pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );

        strcpy( pConn->ServerIP, szServerIP );
        strcpy( pConn->ClientIP, szClientIP );
        pConn->nServerPort = nPort;
        pConn->SockType    = nType;
        pConn->FrameGet    = ReadFunc;
        pConn->nState      = unUse;
        pConn->nSendState  = SendFree;
        pConn->nReadState  = ReadFree;
        pConn->nTimeCount  = 0;
        Time_TimerBStart( &pConn->tTimerTcpRecv, 30*1000 );
        Time_TimerBSetExpired( &pConn->tTimerReConnect ); // 初始化为重连时间到
        pConn->fd          = -1;
        pConn->CheckEnable = NULL;                        // 默认为不检查
        pConn->pCheckEPara = NULL;

        //-----------------------------------------------------------
        pConn->iReadBuffLen = 1024;
        pConn->iSendBuffLen = 1024;
        pConn->nTimeLimit   = 30;
        pConn->nConnTimeout = 0;
        pConn->byDstSlot    = byDstSlot;

        m_TcpCtrlStruc.nTcpNo++;
        if( g_uiLogTcp )
        {
            printf( "Add Connect[%d]:%s->%s:%d\r\n", nTcpNo, szClientIP, szServerIP, nPort );
        }
        iRt = nTcpNo;
/*
// 向TcpReadTask和TcpSendTask中pop参数
// 弹入参数，获取事件号
        pConn->iReadTaskID = DJY_EventPop(iReadTaskID_Type,NULL,0,(ptu32_t)nTcpNo,(ptu32_t)NULL,0);  
// 弹入参数，获取事件号
        pConn->iSendTaskID = DJY_EventPop(iSendTaskID_Type,NULL,0,(ptu32_t)nTcpNo,(ptu32_t)NULL,0);

        pConn->pReadSemp       = Lock_SempCreate(1,0,CN_BLOCK_FIFO,NULL);
        pConn->pSendSemp       = Lock_SempCreate(1,0,CN_BLOCK_FIFO,NULL);
        */
    }
    Lock_MutexPost(m_TcpCtrlStruc.ptSemIDConn);

    return ( iRt );
}

//=================================================================================================================
// 设置链接的参数
// ================================================================================================================
void TcpSetConnPara( short nTcpNo, int iReadLen, int iSendLen, char* ReadPara, short nTimeLimit, short nConnTimeout )
{
    struct MTcpConn* pConn;

    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return;
    }

    pConn               = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    pConn->iReadBuffLen = iReadLen;
    pConn->iSendBuffLen = iSendLen;
    pConn->nTimeLimit   = nTimeLimit;
    pConn->nConnTimeout = nConnTimeout;

    free( pConn->pReadBuff );
    free( pConn->pSendBuff );
    // 分配读写缓冲区
    pConn->pReadBuff = (char*)malloc( iReadLen );
    pConn->pSendBuff = (char*)malloc( iSendLen );
    printf("pReadBuff = %p, pSendBuff = %p \r\n", pConn->pReadBuff, pConn->pSendBuff);
    pConn->pReadPara = ReadPara;

    return;
}

// =========================================================================================
// 设置链接的检查函数:
// 如果需要动态启用/停用连接,则必须调用本函数设置连接检查函数和参数
// 否则可不调用
// =======================================================================================
void TcpSetConnCheckEFun( short nTcpNo, int ( *CheckEnable )( void* ), void* pCheckEPara )
{
    struct MTcpConn* pConn;

    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return;
    }
    pConn              = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    pConn->CheckEnable = CheckEnable;
    pConn->pCheckEPara = pCheckEPara;
    return;
}

//===========================================================================================
// 检查连接是否启用
// ==========================================================================================
int TcpCheckEnable( short nTcpNo )
{
    struct MTcpConn* pConn;

    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return FALSE;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );

    if( NULL == pConn->CheckEnable )
    {
        return TRUE; // 不需要检查
    }
    else
    {
        return ( *( pConn->CheckEnable ) )( pConn->pCheckEPara );
    }
}

//==============================================================================================
// 读取TCP字节
// 返回的nRead可能比nLen小,调用函数需要检查返回值
// =============================================================================================
int TcpGetChar( short nTcpNo, char *buf, int nLen )
{
    int             nRead;
    struct MTcpConn *pConn;
    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return -1;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState != Using )
    {
        return -1;
    }
    nRead = recv( pConn->fd, buf, nLen, 0 );
    if( nRead < 0 )
    {
//        if( errnoGet( ) == EWOULDBLOCK || errnoGet( ) == EINTR || errnoGet( ) == EAGAIN ||
//            errnoGet( ) == ETIMEDOUT || errnoGet( ) == EINPROGRESS )
//        {
//            return 0;
//        }
//        else
//        {
            return -1;
//        }
    }
    if( nRead == 0 )
    {
       return -1;
    }
    return nRead;
}

//============================================================================================
// 读取TCP字节
// 返回的nRead可能比nLen小,调用函数需要检查返回值
// ============================================================================================
int tcp_basic_read( short nTcpNo, char *buf, int nLen )
{
    int             nRead;
    struct MTcpConn *pConn;
    
    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return -1;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState != Using )
    {
        return -1;
    }
    memset( buf, 0, nLen );

    nRead = recv( pConn->fd, buf, nLen, 0 );
    if( nRead < 0 )
    {
//        if( errnoGet( ) == EWOULDBLOCK || errnoGet( ) == EINTR || errnoGet( ) == EAGAIN ||
//            errnoGet( ) == ETIMEDOUT || errnoGet( ) == EINPROGRESS )
//        {
//            return 0;
//        }
//        else
//        {
            return -1;
//        }
    }
    if( nRead == 0 )
    {
        return -1;
    }

    return nRead;
}

//=============================================================================================
// 应用层接收调用函数
// ============================================================================================
int TcpRead( short nTcpNo, char* pBuff )
{
    struct MTcpConn     *pConn;

    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return -1;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState != Using )
    {
        return -1;
    }
    if( pConn->nReadState != Reading )
    {
        return 0;
    }
    if( pConn->iReadLen == 0 )
    {
        return 0;
    }
    memcpy( pBuff, pConn->pReadBuff, pConn->iReadLen );
    pConn->nReadState = ReadFree;
    return pConn->iReadLen;
}

//===========================================================================================
// 应用层发送调用函数
// ==========================================================================================
int TcpSend( short nTcpNo, char* pBuff, int nLen )
{
    struct MTcpConn     *pConn;

    if( nTcpNo >= m_TcpCtrlStruc.nTcpNo )
    {
        return -1;
    }
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState != Using )
    {
        if( g_uiLogTcp )
        {
            printf("TcpSend (nTcpNo: %d)发送失败的具体原因 : nState != Using, nState = %d .\r\n", nTcpNo, pConn->nState);
        }
        return -1;
    }
    if( pConn->nSendState != SendFree )
    {
        if( g_uiLogTcp )
        {    
            printf("TcpSend (nTcpNo: %d)发送失败的具体原因 : nSendState != SendFree, nSendState = %d .\r\n", nTcpNo, pConn->nSendState);
        }
        return 0;
    }
    if( nLen > pConn->iSendBuffLen )
    {
        if( g_uiLogTcp )
        {  
            printf("TcpSend (nTcpNo: %d)发送失败的具体原因 : 发送报文过长 .\r\n", nTcpNo);
        }
        return 0; // 发送报文过长
    }
    memcpy( pConn->pSendBuff, pBuff, nLen );
    pConn->iSendLen   = nLen;
    pConn->nSendState = Sending;
    return nLen;
}
// =======================================================================================
//
// ========================================================================================
void CloseOldConnect( short nTcpNo )
{
    struct MTcpConn *pConn;
    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( pConn->nState == Closing )
    {
        return;           // Add 2004-11-10 防止进来多次
    }
    pConn->nState = Closing;
    Time_TimerBStart( &pConn->tTimerReConnect, 30*1000 );

    if( pConn->fd > 0 )
    {
        shutdown( pConn->fd, 2 ); // 关闭连接
        closesocket( pConn->fd );
    }

    pConn->fd     = -1;
    pConn->nState = unUse;
//--add for debug @ 20170413s
    pConn->CheckEnable = NULL;
//--
    if( g_uiLogTcp )
    {
        //Time_DbgSysTime( );
        printk( "CloseOldConnect finished nTcpNo=%d,Fd:%d,iReadTaskID=%d,iSendTaskID=%d\r\n", nTcpNo, pConn->fd, pConn->iReadTaskID, pConn->iSendTaskID );
    }
}

// ================================================================================================
//
// ================================================================================================
void TcpReadTask( void )
{
    int             nRead;
    int             (*ReadFrame)( );
    short           nTcpNo;
    struct MTcpConn *pConn;

    DJY_GetEventPara((ptu32_t*)&nTcpNo,NULL);

    pConn             = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    ReadFrame         = pConn->FrameGet;
    pConn->nReadState = ReadFree;
    pConn->nTimeCount = 0;
    Time_TimerBReset( &pConn->tTimerTcpRecv ); // 重新启动B类定时器

    if( g_uiLogTcp )
    {
        //Time_DbgSysTime( );
        printk( "Start Read Task From Connection %d,Fd:%d\r\n", nTcpNo, pConn->fd );
    }

    while( pConn->nState == Using )
    {
        //taskDelay( 1 );
        DJY_EventDelay(16700);

        if( pConn->fd <= 0 )
        {
            continue;
        }
        if( pConn->nReadState == ReadFree )
        {                                                          // 保证每次只保存一个报文
            nRead = ( *ReadFrame )( nTcpNo, pConn->pReadBuff, pConn->pReadPara );
            if( nRead > 0 )
            {
                pConn->nReadState = Reading;
                pConn->iReadLen   = nRead;
                pConn->nTimeCount = 0;
                Time_TimerBReset( &pConn->tTimerTcpRecv ); // 重新启动B类定时器
            }
            else if( nRead == 0 )
            {
                if( pConn->nState != Using )
                {
                    break;                             // 任务会自动退出 Add 2004-11-07
                }
                pConn->nReadState = ReadFail;
                EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TCPREADDOWN);
                CloseOldConnect( nTcpNo );
                break;
            }
        }
    }
    if( g_uiLogTcp )
    {
        //Time_DbgSysTime( );
        printf( "Exit Read Task From Connection %d,Fd:%d\r\n", nTcpNo, pConn->fd );
    }
    pConn->nReadState = ReadExit;
    pConn->iReadTaskID  = -1;
    //DJY_EvttUnregist(DJY_GetMyEvttId());
}

void TcpSendTask( void )
{
    int              nWrite;
    short            nTcpNo;
    struct  MTcpConn *pConn;

    DJY_GetEventPara((ptu32_t*)&nTcpNo,NULL);
    
    pConn             = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    pConn->nSendState = SendFree;

    if( g_uiLogTcp )
    {
        //Time_DbgSysTime( );
        ;//printf( "Start Send Task From Connection %d,Fd:%d,nState=%d\r\n", nTcpNo, pConn->fd, pConn->nState );
    }
    while( pConn->nState == Using )
    {
        if( pConn->fd <= 0 )
        {
            continue;
        }
        if( pConn->nSendState == Sending )
        {
            nWrite = send( pConn->fd, pConn->pSendBuff, pConn->iSendLen, 0 );

            if(nWrite <= 0)
            {
                Time_DbgSysTime( );
                if( g_uiLogTcp )
                {                      
                    printf("\r\n底层TCP调用库函数发送失败, nWrite = %d \r\n", nWrite);
                }
            }
            if( nWrite == 0 )// send 不返回0值
            {
                EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TCPSENDDOWN);
                CloseOldConnect( nTcpNo );
                pConn->nSendState = SendFail;
                break;
            }
            pConn->nSendState = SendFree;
        }

        //taskDelay( 5 );
        DJY_EventDelay(83500);
    }

    if( g_uiLogTcp )
    {
        //Time_DbgSysTime( );
        printf( "Exit Send Task From Connection %d,Fd:%d\r\n", nTcpNo, pConn->fd );
    }
    pConn->nSendState = SendExit;
    pConn->iSendTaskID  = -1;
    //DJY_EvttUnregist(DJY_GetMyEvttId());
}

void StartNewConnect( short nTcpNo, int newFd )
{
    //char           szTxt[20];
    struct MTcpConn*pConn;
    pConn         = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    pConn->fd     = newFd;
    pConn->nState = Using;

    if( g_uiLogTcp )
    {
        printf( "Start new connect %d,Fd:%d\r\n", nTcpNo, pConn->fd );
    }

    //sprintf( szTxt, "t_read_%d", nTcpNo );    
    //pConn->iReadTaskID = DJY_EvttRegist(EN_CORRELATIVE,gc_u8PriorityPort,0,1,(ptu32_t(*)(void))TcpReadTask,NULL,
    //                           0x800,szTxt);
    DJY_EventPop(iReadTaskID_Type,NULL,0,(ptu32_t)nTcpNo,0,0);
    //sprintf( szTxt, "t_send_%d", nTcpNo );
    //pConn->iSendTaskID =DJY_EvttRegist(EN_CORRELATIVE,gc_u8PriorityPort,0,1,(ptu32_t(*)(void))TcpSendTask,NULL,
    //                               0x800,szTxt);
    DJY_EventPop(iSendTaskID_Type,NULL,0,(ptu32_t)nTcpNo,0,0);

}    
int TcpServerStart( )
{
    int                  sockAddrSize, sockFd, newFd, i;
    struct sockaddr_in   serverAddr, clientAddr;
    struct MTcpConn      * pConn;
    int                  iValue, sockopt = 1;
    short nServer;

    DJY_GetEventPara((ptu32_t *)&nServer,NULL);
    
    struct MTcpServerPort* pServer = &( m_TcpCtrlStruc.m_ServerArray[nServer] );
    printf("TcpServerStart: %d\n", nServer);

    // 生成一个基于TCP的套接字
    if( ( sockFd = socket( AF_INET, SOCK_STREAM, 0 ) ) == -1 )
    {
        return -1;
    }

    // 设置本地地址
    sockAddrSize = sizeof( struct sockaddr_in );
    memset( (char*)&serverAddr,0, sockAddrSize );
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr=htonl(INADDR_ANY);
    serverAddr.sin_port        = htons( pServer->nPort );
#if( CN_TCP_TYPE == CN_TCP_TYPE_BD  )
        serverAddr.sin_addr.s_addr  = htonl ( INADDR_ANY );            // 采用 UDP广播
#else
        serverAddr.sin_addr.s_addr  = inet_addr(pServer->ServerIP);   // 采用 UDP/IP
#endif

    // 将套接字绑定到本地地址
    if( bind( sockFd, (struct sockaddr*)&serverAddr, sockAddrSize ) == -1 )
    {
        iValue = closesocket( sockFd );
        return -1;
    }

    //2012-08-03
    if( setsockopt( sockFd, IPPROTO_TCP, TCP_NODELAY, (char*)&sockopt, sizeof( int ) ) )//Delivering Messages Immediately
    {
        // NOP
    }
    if( setsockopt( sockFd, SOL_SOCKET, SO_KEEPALIVE, (char*)&sockopt, sizeof( int ) ) )//Detecting a Dead Connection
    {
        // NOP

    }

    if( setsockopt( sockFd, SOL_SOCKET, SO_REUSEADDR, (char*)&sockopt, sizeof( int ) ) )
    {
        // NOP
    }

    // 生成客户端请求链接队列
    if( listen( sockFd, CN_TCP_NUM_CONNECT ) == -1 )
    {
        iValue = closesocket( sockFd );
        return -1;
    }

    pServer->Fd = sockFd;
    for(;; )
    {         // 接受新的连接请求并生成一个新的任务来处理链接请求
        if( ( newFd = accept( sockFd, (struct sockaddr*)&clientAddr, &sockAddrSize ) ) == -1)
        { // 接受连接请求出错

            //taskDelay( 30 );
            DJY_EventDelay(501*mS);
            continue;
        }

        //printf("accept client: %s\n", inet_ntoa(clientAddr.sin_addr));

        for( i = 0; i < m_TcpCtrlStruc.nTcpNo; i++ )
        {
            pConn = &( m_TcpCtrlStruc.m_ConnArray[i] );
            if( ( inet_addr( pConn->ClientIP ) != clientAddr.sin_addr.s_addr )
                && ( inet_addr( pConn->ClientIP ) != 0xFFFFFFFF ) )
            {
                continue;
            }
            if( inet_addr( pConn->ServerIP ) != serverAddr.sin_addr.s_addr )
            {
                continue;
            }
            if( pConn->nServerPort != pServer->nPort )
            {
                continue;
            }
            if( pConn->SockType != 0 )
            {
                continue;
            }
            if( FALSE == TcpCheckEnable( i ) )
            {
                continue; // 连接停用
            }

            if( g_uiLogTcp )
            {
                //Time_DbgSysTime( );
                printf( "Server[%d]:Connected by %s:%d,newFd:%d\r\n", nServer, pConn->ClientIP, pServer->nPort, newFd );
            }
            if( pConn->nState == Using )
            {
                CloseOldConnect( i );
            }

            while( pConn->nState != unUse )
            {
                //taskDelay( 10 );
                DJY_EventDelay(167*mS);
            }
            StartNewConnect( i, newFd );
            //taskDelay( 10 ); // Add 2004-11-10 确保读写任务已经启动
            DJY_EventDelay(167*mS);
            break;
        }
        if( i >= m_TcpCtrlStruc.nTcpNo )
        {
            if( g_uiLogTcp )
            {
                //Time_DbgSysTime( );
                //printf( "Server[%d]:The link from %s was not allowed: rejected!\r\n", nServer, inet_ntoa( clientAddr.sin_addr ) );
            }
            iValue = closesocket( newFd );
        }
        memset( (char*)&clientAddr, 0,sockAddrSize );
        newFd = 0;
    }

    if(iValue)
    {
        ;
    }

    pServer->Fd = -1;
    return -1;
}

//=========================================================================================
// 增加服务器
// ServerIP为0.0.0.0表示不限制本地IP地址
// =======================================================================================
short TcpAddServer( BYTE* ServerIP, short nPort )
{
    struct  MTcpServerPort *pServer, *pSvrTmp;
    char                   szTxt[20], szServerIP[20];
    short                  nServerNo, iLoop, iRt;
    u16 EvttId ;
    Lock_MutexPend(m_TcpCtrlStruc.ptSemIDServer,CN_TIMEOUT_FOREVER);

    // 检查服务器是否已经在列表中
    nServerNo = m_TcpCtrlStruc.nServerNo;
    sprintf( szServerIP, "%d.%d.%d.%d", ServerIP[0], ServerIP[1], ServerIP[2], ServerIP[3] );
    for( iLoop = 0; iLoop < nServerNo; iLoop++ )
    {
        pSvrTmp = &( m_TcpCtrlStruc.m_ServerArray[iLoop] );
        if( ( pSvrTmp->nPort == nPort ) && ( 0 == strcmp( szServerIP, pSvrTmp->ServerIP ) ) )
        {
            if( g_uiLogTcp )
            {
                printk( "Duplicate Tcp Server[%d]=%s:%d!\r\n", iLoop, szServerIP, nPort );
            }
            Lock_MutexPost(m_TcpCtrlStruc.ptSemIDServer);
            return iLoop;
        }
    }

    if( nServerNo >= CN_TCP_NUM_SVR )
    {
        m_TcpCtrlStruc.nServerNo = CN_TCP_NUM_SVR;
        if( g_uiLogTcp )
        {
            printf( "Add Tcp Server=%s:%d Error,Server Array is Full!\r\n", szServerIP, nPort );
        }
        iRt = -1;
    }
    else
    {
        pServer = &( m_TcpCtrlStruc.m_ServerArray[nServerNo] );
        strcpy( pServer->ServerIP, szServerIP );
        pServer->nPort = nPort;
        pServer->Fd    = -1;

        m_TcpCtrlStruc.nServerNo++;

        if( g_uiLogTcp )
        {
            printf( "Add Tcp Server[%d]=%s:%d OK!\r\n", nServerNo, szServerIP, nPort );
        }
        // 启动服务器任务
        sprintf( szTxt, "tTcp_Srv%d", nServerNo );
        EvttId = DJY_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,1,(ptu32_t(*)(void))TcpServerStart,NULL,
                                  0xC00,szTxt);
        DJY_EventPop(EvttId,NULL,0,(ptu32_t)nServerNo,0,0);
        iRt = nServerNo;
    }
    Lock_MutexPost(m_TcpCtrlStruc.ptSemIDServer);

    return ( iRt );
}

//===========================================================
// 此任务用于启动客户端的连接
//===========================================================
int TcpClientStart( short nTcpNo )
{
    struct sockaddr_in serverAddr;
    struct MTcpConn    *pConn;
    struct timeval     tv,  *ptv ;
    int                sockAddrSize, sFd;
//    char                cNote[256];
    int                iValue, sockopt = 1;

// 如果无线使用无线模块，若无线模块没有加载成功，就一直等待，不继续执行
#if (CN_EVC_MODEM)
    while(0 != CUSTOM_DeviceReady())
    {
    	DJY_EventDelay(1000*mS);
    }
#endif

    pConn = &( m_TcpCtrlStruc.m_ConnArray[nTcpNo] );
    if( FALSE == Time_TimerBCheck( &pConn->tTimerReConnect ) )
    {
        return -1;
    }
    pConn->nState = Connecting;

    if( ( sFd = socket( AF_INET, SOCK_STREAM, 0 ) ) == -1 )
    {
        if( g_uiLogTcp )
        {
            //printf( "socket Create Error %d,ErroNo:0x%x\n", nTcpNo, errnoGet( ) );
        }
        pConn->nState = unUse;
        return -1;
    }

    //2012-08-03
    if( setsockopt( sFd, IPPROTO_TCP, TCP_NODELAY, (char*)&sockopt, sizeof( int ) ) )//Delivering Messages Immediately
    {
        // NOP
    }
    if( setsockopt( sFd, SOL_SOCKET, SO_KEEPALIVE, (char*)&sockopt, sizeof( int ) ) )//Detecting a Dead Connection
    {
        // NOP
    }

    sockAddrSize = sizeof( struct sockaddr_in );
    //bzero( (char*)&serverAddr, sockAddrSize );
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port   = htons( pConn->nServerPort );

    if( ( serverAddr.sin_addr.s_addr = inet_addr( pConn->ServerIP ) ) == (unsigned)INADDR_NONE )
    {       // 注意ERROR是有符号数,而inet_addr返回的是无符号数
        if( g_uiLogTcp )
        {
            //printf( "Unknow serverName,ErroNo:0x%x\n", errnoGet( ) );
        }
        pConn->nState = unUse;
        iValue        = closesocket( sFd );
        return -1;
    }
    if( !pConn->nConnTimeout )
    {
        ptv = NULL;
    }
    else
    {
        tv.tv_sec  = pConn->nConnTimeout;
        tv.tv_usec = 0;
        ptv        = &tv;
    }
    if( connect( sFd, (struct sockaddr*)&serverAddr, sockAddrSize) == -1 )
    {
        if( g_uiLogTcp )
        {
            printf( "Failure: Start Client sFD:%d,Fd:%d\r\n", sFd, pConn->fd );
        }
        iValue        = closesocket( sFd );
        pConn->nState = unUse;
        return -1;
    }

    if(iValue)
    {
    	;
    }
    if(NULL == ptv)
    {
    	;
    }


    StartNewConnect( nTcpNo, sFd );
    return sFd;
}

//===========================================================
// 此任务用于重新启动被关闭客户端的连接
//===========================================================
void TcpMonTask( void )
{
    struct MTcpConn *pConn;
    unsigned short  wLoop;

#if (CN_EVC_MODEM)
    unsigned int    uiModemRstFlag;
#endif 

    // Moderm State Initialize
    TcpSetModermState( 1 );

    //taskDelay( 10 );
    DJY_EventDelay(30*1000*mS);
    while( 1 )
    {
        //taskDelay( 60 );
        DJY_EventDelay(1002*mS);

// 如果有无线模块，则如果拨号状态为失败，则不执行下面的代码。如果成功，则继续建立新连接
#if (CN_EVC_MODEM)
        // PPP拨号状态 0-成功 非0-失败
        if(m_TcpCtrlStruc.nModerm)
        {
            m_TcpCtrlStruc.nModerm_bak = 1;
            continue;
        }

// 若Modem状态由0变1，则清空“无线模块重启”标志位清零，并将TCP链接建立失败次数清0
        if((1 == m_TcpCtrlStruc.nModerm_bak)&&(0 == m_TcpCtrlStruc.nModerm))
        {
            for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
            {
                pConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
                pConn->iFailRstFlag = 0;
                pConn->iFailNum     = 0;
            }
            m_TcpCtrlStruc.nModerm_bak = 0;
        }
#endif            
        for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
        {
            pConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
            if( ( pConn->SockType == 1 ) && ( pConn->nState == unUse ) && TcpCheckEnable( wLoop ) )
            {
                if(0 == pConn->iFailNum)
                {
                    Time_TimerBStart( &pConn->tTimerEstablish, 300*1000 );// 定时器
                }
                if( g_uiLogTcp )
                {
                    printf( "Start Client nTcpNo:%d,Fd:%d\r\n", wLoop, pConn->fd );
                }
// 若有无线模块，则如果TCP建立失败的次数超过了50次，则将“无线模块重启”标志位置1。
// PPP_Scan函数会执行重启无线模块的操作，并重新拨号
#if (CN_EVC_MODEM)
                if(1 == pConn->iFailRstFlag )
                {
                    continue;
                }
                if(TcpClientStart( wLoop ) < 0)
                {
                	//printf("TcpClientStart---Failure. FailNum = %d. \n\r", pConn->iFailNum);
                    pConn->iFailNum ++;
                    if(pConn->iFailNum > 50)
                    {
                        pConn->iFailRstFlag = 1;       // 无线模块重启标志位置1
                        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TCP50TIMES);
                    }
                    else if(Time_TimerBCheck( &pConn->tTimerEstablish ))
                    {
                        pConn->iFailRstFlag = 1;       // 无线模块重启标志位置1
                        Time_TimerBStop( &pConn->tTimerEstablish );
                        EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TCPOVERTIME);
                    }
                    else
                    {
                        pConn->iFailRstFlag = 0;        // 无线模块重启标志位置0
                    }
                }
                else
                {
                    //printf("Tcp连接建立成功,清空无线模块重置标志位.\r\n");
                    EVC_EFSLog_FlagSet_Ext(EN_EFS_TYPE_NETLOG, EN_NETLOG_TYPE_TCPUP);
                    pConn->iFailRstFlag = 0;
                    pConn->iFailNum     = 0;
                }
#else
// 若没有无线模块，则直接启动客户端即可。
                TcpClientStart( wLoop );
#endif 
            }
        }
// 若有无线模块则遍历所有模块的“无线模块重启标志”，如果都被置1，则将总的“无线模块重启标志位”置1
#if (CN_EVC_MODEM)
        // 如果没有创建tcp连接，则不重启无线模块
        if(m_TcpCtrlStruc.nTcpNo > 0)
        {
            uiModemRstFlag = 1;
            for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
            {
                pConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
                uiModemRstFlag = uiModemRstFlag && pConn->iFailRstFlag;
            }
            uiModemReset = uiModemRstFlag;
            uiModemRstFlag = 1;
            for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
            {
                pConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
                uiModemRstFlag = uiModemRstFlag && pConn->iFailRstFlag2;
            }
            uiModemReset2 = uiModemRstFlag;
        }
        else
        {
            uiModemReset  = 0;
            uiModemReset2 = 0;
        }
#endif 

    }
    return;
}

//===========================================================
// 此任务用于关闭接受报文长期阻塞的连接
// 连接在以下情况会被关闭:
// 1.TCP层超期收不到报文;
// 2.接收的报文应用层超期不取走;
// 3.连接被停用;
//===========================================================
void TcpCountTask( )
{
    struct MTcpConn *pConn;
    unsigned short  wLoop;
//    char            cNote[256];

    //taskDelay( 10 );
    DJY_EventDelay(167*mS);

    while( 1 )
    {
        //taskDelay( 60 );
        DJY_EventDelay(1002*mS);
        for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
        {
            pConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
            if( pConn->nState == Using )
            {
                pConn->nTimeCount++;
                if( Time_TimerBCheck( &pConn->tTimerTcpRecv ) )
                {
                    Time_TimerBStop( &pConn->tTimerTcpRecv );
                    CloseOldConnect( wLoop );

                    //taskDelay( 10 ); //等待readTask sendTask自动退出,167ms   2012-08-06
                    DJY_EventDelay(167*mS);
                    if(( pConn->iReadTaskID != -1)||( pConn->iSendTaskID != -1))
                    {
                        if(pConn->iReadTaskID != -1)
                        {
                            //taskDelete(pConn->iReadTaskID);
                        }
                        if(pConn->iSendTaskID != -1)
                        {
                            //taskDelete(pConn->iSendTaskID);
                        }
                        //taskDelay( 20 );
                        DJY_EventDelay(334*mS);
                    }

                }
                else if( ( FALSE == TcpCheckEnable( wLoop ) ) || ( pConn->nSendState == SendFail ) || ( pConn->nReadState == ReadFail ) )
                {
                    CloseOldConnect( wLoop );

                    //taskDelay( 10 ); //等待readTask sendTask自动退出,167ms   2012-08-06
                    DJY_EventDelay(167*mS);
                    if(( pConn->iReadTaskID != -1)||( pConn->iSendTaskID != -1))
                    {
                        if(pConn->iReadTaskID != -1)
                        {
                            //taskDelete(pConn->iReadTaskID);
                        }
                        if(pConn->iSendTaskID != -1)
                        {
                            //taskDelete(pConn->iSendTaskID);
                        }
                        //taskDelay( 20 );
                        DJY_EventDelay(334*mS);
                    }
                }
            }
        }
    }
    return;
}

//===========================================================
// 启动链接监视任务
//===========================================================
void TcpStartNet( void )
{
    u16 EvttId;

    EvttId = DJY_EvttRegist(EN_CORRELATIVE,CN_PRIO_RRS,0,1,(ptu32_t(*)(void))TcpMonTask,gs_TcpMon_Task_Stack,
                          CN_TCPMON_TASK_STACK_LEN,"tTcp_Mon");
    


    if(EvttId==CN_EVTT_ID_INVALID)
    {
         printf("tTcp_Mon event register failed.\r\n");
    }
    else
    {
         DJY_EventPop(EvttId, NULL, 0, 0, 0, 0);
    }

    // 在此处创建tTcp_Read和tTcp_Send任务，且任务类型为EN_INDEPENDENCE，独立型。
    // 在此事件类型中，最多可以创建CN_TCP_NUM_SOCK个任务。因此，可以建立多个TCP链接。
    iReadTaskID_Type = DJY_EvttRegist(EN_INDEPENDENCE,CN_PRIO_RRS,0,CN_TCP_NUM_SOCK,(ptu32_t(*)(void))TcpReadTask,gs_TcpRead_Task_Stack,
                               CN_TCPREAD_TASK_STACK_LEN,"tTcp_Read");
    
    iSendTaskID_Type = DJY_EvttRegist(EN_INDEPENDENCE,CN_PRIO_RRS,0,CN_TCP_NUM_SOCK,(ptu32_t(*)(void))TcpSendTask,gs_TcpSend_Task_Stack,
                               CN_TCPSEND_TASK_STACK_LEN,"tTcp_Send");

    if(iReadTaskID_Type == CN_EVTT_ID_INVALID)
    {
         printf("tTcp_Read event register failed.\r\n");
    }
    
    if(iSendTaskID_Type == CN_EVTT_ID_INVALID)
    {
         printf("tTcp_Send event register failed.\r\n");
    }
    //DJY_EventPop(EvttId,NULL,0,0,0,0);
}

//===========================================================
// 添加网关,传入的地址为数值型
//===========================================================
s32 ipAddGate( char *pbyDest, char *pbyGate )
{
    char   strDest[20], strGate[20];
    s32 rt;

    sprintf( strDest, "%d.%d.%d.%d", pbyDest[0], pbyDest[1], pbyDest[2], pbyDest[3] );
    sprintf( strGate, "%d.%d.%d.%d", pbyGate[0], pbyGate[1], pbyGate[2], pbyGate[3] );
    rt = -1; //-- routeNetAdd( strDest, strGate );  cys test "routeNetAdd"函数库没有
    if( g_uiLogTcp )
    {
        printf( "Add Route:%s->Destination:%s ", strGate, strDest );
        if( rt == 1 )
        {
            printk( "OK!\r\n" );
        }
        else
        {
            //printk( "ERROR!ErroNo:0x%x\n", errnoGet( ) );
        }
    }
    return rt;
}

//===========================================================
// 调试函数,显示TCP的服务器和客户端链接信息
//===========================================================
void Dbg_ShowTcp( void )
{
    struct  MTcpServerPort *ptSvr;
    struct  MTcpConn       *ptConn;
    short                  iLoop;

    // 显示服务器信息
    printf( "Server List\n" );
    printf( " No         IP         Port  fd \r\n" );
    printf( "----  ---------------  ----  -- \r\n" );
    for( iLoop = 0; iLoop < m_TcpCtrlStruc.nServerNo; iLoop++ )
    {
        ptSvr = &( m_TcpCtrlStruc.m_ServerArray[iLoop] );
        printf( "%02d    %-15s  %-4d  %d \r\n", iLoop, ptSvr->ServerIP, ptSvr->nPort, ptSvr->Fd );
    }

    // 显示连接信息
    printf( "\nConnect List\r\n" );
    printf( " No       SeverIP          ClientIP     Port  Type Status Used fd if \r\n" );
    printf( "----  ---------------  ---------------  ----  ---- ------ ---- -- -- \r\n" );
    for( iLoop = 0; iLoop < m_TcpCtrlStruc.nTcpNo; iLoop++ )
    {
        ptConn = &( m_TcpCtrlStruc.m_ConnArray[iLoop] );
        printf( "%02d    %-15s  %-15s  %-4d    %d    %d    %d    %d %d \r\n",
            iLoop, ptConn->ServerIP, ptConn->ClientIP, ptConn->nServerPort,
            ptConn->SockType, ptConn->nState, TcpCheckEnable( iLoop ), ptConn->fd, ptConn->byIfIndex );
    }
    return;
}

void Dbg_Set_ResetFlag(void)
{
    struct MTcpConn *pConn;
    pConn = &( m_TcpCtrlStruc.m_ConnArray[0] );
    pConn->iFailRstFlag = 1;       // 无线模块重启标志位置1

    printf("手动置TcpSop重启标志位\r\n");
}

void Dbg_Set_g_uiLogTcp(void)
{
    g_uiLogTcp = 1;
    printf("开始打印tcpsop中的打印信息。\r\n");
}
void Dbg_Reset_g_uiLogTcp(void)
{
    g_uiLogTcp = 0;
    printf("停止打印tcpsop中的打印信息。\r\n");
}

void Dbg_Set_nSendState_SendFree(void)
{
    struct MTcpConn *pConn;
    pConn = &( m_TcpCtrlStruc.m_ConnArray[0] );
    pConn->nSendState = SendFree;       // Tcp发送状态
    printf("将Tcp中的nSendState的状态修改为SendFree.\r\n");
}

void Dbg_Set_nSendState_Sending(void)
{
    struct MTcpConn *pConn;
    pConn = &( m_TcpCtrlStruc.m_ConnArray[0] );
    pConn->nSendState = Sending;       // Tcp发送状态
    printf("将Tcp中的nSendState的状态修改为Sending.\r\n");
}

u32 Tcp_Get_Socket_Status(u16 uSocket)
{
    return (Using == (m_TcpCtrlStruc.m_ConnArray[uSocket].nState));
}

void Tcp_Set_iFailRstFlag2(u16 uSocket)
{
    m_TcpCtrlStruc.m_ConnArray[uSocket].iFailRstFlag2 = 1;
}

void Tcp_Reset_iFailRstFlag2(u16 uSocket)
{
    m_TcpCtrlStruc.m_ConnArray[uSocket].iFailRstFlag2 = 0;
}

u32 Tcp_Reset_iFailRstFlag2_All(void)
{
    UINT8 i;
    for(i = 0; i < CN_TCP_NUM_SOCK; i++)
        m_TcpCtrlStruc.m_ConnArray[i].iFailRstFlag2 = 0;

    return 0;
}

//===========================================================
// 检查连接是否正常函数，通过state判断连接是否正常，过滤掉192.168.253.253的ip
// 正确连接 返回FASE,有一个连接异常返回FALSE
//===========================================================
//BOOLEAN Check_ConnectErr( INT8S* pbyIP )
//{
//    struct  MTcpConn       *ptConn;
//    WORD wLoop;
//    INT8S byMsxIP[20]= "192.168.253.253";

//    for( wLoop = 0; wLoop < m_TcpCtrlStruc.nTcpNo; wLoop++ )
//    {
//        ptConn = &( m_TcpCtrlStruc.m_ConnArray[wLoop] );
//        if(0==strncmp(byMsxIP,ptConn->ClientIP,15))
//        {
//            continue;
//    }
//        pbyIP = &ptConn->ServerIP[0];
//        if(Using!=ptConn->nState)  return TRUE;
//    }
//    return FALSE;
//}

//========================================================
// 启动网络发送并显示报文.
// byDbgBit为显示标志，0xFF表示不显示
UINT16 Hard_Tcp_TxStartup( short iTcpSockNo, UINT8 *pbyMsgBuf, UINT16 wMsgLen, UINT8 byPortNo, UINT8 byDbgBit )
{
    if( wMsgLen==TcpSend( iTcpSockNo, (char*)pbyMsgBuf, (int)wMsgLen ) )
    {
        return wMsgLen;
    }
    if( g_uiLogTcp )
            {
        printf("TcpSop的状态机出现问题，导致在Hard_Tcp处发送失败, iTcpSockNo = %d .\r\n", iTcpSockNo);
    }
    return 0;
}

#endif // #if( CONST_INCLUDE_TCP )

