//================================================================================
// 文件描述：103组件底层通信封装模块
// 文件版本: V1.00
// 开发人员: 姜超
// 定版时间:
// 版本修订:
// 修订人员:
//================================================================================
#ifndef _IEC103_COMM_H_
#define _IEC103_COMM_H_

//#include "Resource.h"

#include "uartctrl.h"
//#include "UdpSop.h"
#include "TcpSop.h"
#include "Protocol.h"

/*================================================================================*/
//外部函数声明
//通用公用函数
extern BOOLEAN  Fun_GetPortVariable( INT8U byPortNo, tagPPortVal *pptPortVal );
extern BOOLEAN  Port_CheckCommERR( INT8U byPortNo );
extern BOOLEAN  Port_CheckCommOK( INT8U byPortNo );
extern s32 GetFlashNetIP(UINT8 byNum,UINT8* pbyBuf);        
extern void Init_PortCtrl( UINT8 byPortNo, BOOLEAN bFirst );
extern void Sio_Port_Set(INT8U byPortNo);
//extern void Net_Port_Set(INT8U byPortNo,UINT32 u32oldip);
extern void Comm_Port_Set(INT8U byPortNo);
extern s32 ResetTLV_SIO(void );
extern s32 ResetTLV_NET( void );
extern s32 ResetTLV_COMM( void ) ;


extern void Net_MAC_Init(INT8U byPortNo);


#endif
