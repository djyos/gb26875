#ifndef LOG_INIT_H
#define LOG_INIT_H

//日志类型名称
enum {
    LOG_DEVICE_READ = 0,
//    LOG_STATE_PUSH,
//    LOG_STATE_POP,
    LOG_UNIT_STATE,
    LOG_UPDATE_MESSAGE,
    LOG_DOWN_MESSAGE,
    LOG_SOCKET_STATE,
    LOG_SERIAL_INFO,
    LOG_IP_INFO,
};

struct LogString
{
    char *buff;  //日志内容
    int len;     //日志长度
    int type;    //日志类型
    char *check; //日志校验位
};

/**
 *
 * 日志队列初始化
 *
 */
void log_queue_init(void);

/**
 *
 * 日志内容入队列
 *
 */
void log_queue_push(char *data, int len,int type,char *check);

/**
 *
 * 日志内容出队列
 *
 */
void log_queue_pop(void);

#endif /* LOG_INIT_H */
