/*
 * usr_info_trans_dev.c
 *
 *  Created on: 2020年7月23日
 *      Author: WangXi
 */
#include <stdlib.h>
#include <stdio.h>

#include "queue.h"
#include "map.h"

#include "usr_info_trans_dev.h"
#include "protocol_GB26875.h"

#include "../common/djyutil.h"
#include "../uitd_reporter.h"

#define UITD_LOG 0

#if UITD_LOG
#include "../trans_log/log.h"
#endif



void __UITD_Status(u8 *param);
u8 __UITD_ProtocolWork(struct SerialConfig *serial_config, u8 *data_buff, u32 data_size);
void __UITD_ProtocolHandler(u8 type, struct FireCtrlInfo *info);
void __UITD_DownloadConfirm(u16 no);


struct String
{
    u8 *buff;
    int len;
};

#if UITD_AUTO_HEARTBEAT
#define UITD_HB_TIMEOUT     31000   //  心跳超时时间
s32 FIRST_HB_FLAG;
s64 LAST_HB_TIME;       //  最后一次心跳时间
#endif

QueueInfo_st * FC_UPLOAD_QUEUE;    //  主动上传信息体队列
#define UPLOAD_BUFF_LEN          0x120     //  最大信息体长度
static u8 upload_buff[UPLOAD_BUFF_LEN];  //  主动上传信息体缓存
int upload_size;                   //  信息体缓存长度

#if UITD_AUTO_RELOAD
struct MapInfo *UITD_RELOADMAP;
#endif


// 全局变量 记录参数
// static u64 g_UitdAliveTime = 0; //  开机运行时长
static u64 g_UitdConnCnt = 0; //  连接次数

#if UITD_AUTO_RELOAD
static u64 g_UitdReloadCnt = 0; //  重传次数
static u64 g_UitdReloadSize = 0; //  重传数据包大小
#endif

static u64 g_UitdUploadErrorCnt = 0; // 上传失败计数
#if UITD_AUTO_UPLOAD
static u64 g_UitdUploadCnt = 0; // 上传次数
static u64 g_UitdUploadSize = 0; // 上传数据包大小
#endif

#if UITD_AUTO_HEARTBEAT
static u64 g_UitdHbCnt = 0; // 心跳次数
static u64 g_UitdHbSize = 0; // 心跳数据包大小
#endif

static u64 g_UitdDownloadCnt = 0; // 下载次数
static u64 g_UitdDownloadSize = 0; // 下载数据包大小

//todo:应该使用静态数组
struct FireCtrlSystem *FC_SYSTEMS[UITD_SERIAL_NUM];    //  系统管理缓存 采用线型数组 最多0xFF个系统
struct MapInfo *FC_UNIT_MAP[UITD_SERIAL_NUM];//  部件Map指针数组 每个系统都有相应的Map 最多0xFF
struct UserInfoTransDev FC_DEV;    //  用户信息传输装置数据结构缓存

u8 UITD_init_flag = false;      //  初始化标志

int UIRD_test_i = 0;        //  测试变量

#if UITD_PROTOCOL_FUNC      //  装置下游通讯模块

/* 串口协议配置等缓存 */
struct SerialConfig FC_SERIAL_CONFIG[UITD_SERIAL_NUM];
void *serial_devices[UITD_SERIAL_NUM] = {0};

void UITD_ProtocolThread(u8 serial_addr)
{
#if UITD_VERSION_MODEL
    serial_addr = 0;
#endif
    struct SerialConfig *serial_config;
    u8 *data_buff = NULL;
    u32 data_buff_len = 0x400;
    s32 data_size = 0;
    s32 ret = -1;
    void *dev;  //  定义串口指针
    struct ProtocolVTable *protocol = NULL;
    serial_config = UITD_GetSerialConfig(serial_addr);
	// printf("串口[%d]获取配置\r\n", serial_addr);
    if(serial_config == NULL)
    {
		printf("串口[%d]配置为空\r\n", serial_addr);
        return;
    }
    protocol = serial_config->protocol;
	// printf("串口[%d]获取协议\r\n", serial_addr);
    if(protocol == NULL)
    {
		printf("串口[%d]协议配置为空\r\n", serial_addr);
        return;
    }

    //得到设备号
    dev = UITD_GetDevice(serial_config->addr);  //  根据地址映射串口指针
	// printf("串口[%d]获取设备映射\r\n", serial_addr);
    if(dev == (void *)0xFFFFFFFF)
    {
        printf("串口[%d]映射地址错误\r\n", serial_config->addr);
        return;
    }
	// printf("串口[%d]走通讯 缓冲大小: %d\r\n", serial_addr, protocol->data_size_max);
    if(protocol->data_size_max > 0) data_buff_len = protocol->data_size_max;
    data_buff = (u8 *)malloc(data_buff_len);
    memset(data_buff, 0, data_buff_len);

    if(data_buff == NULL)
    {
        printf("data_buff malloc error\r\n");
        return;
    }
	// printf("串口[%d]协议类型:%d\r\n", serial_addr, protocol->type);
    if(protocol->type == UITD_PROTOCOL_TYPE_1)  //  通讯协议类型1: 消防系统作为主机主动发送数据 信息传输装置作为从机被动接受 在接受后需发送回码
    {
        // 1. 先读 从串口中读数据
        data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
        if(data_size <= 0)
        {
            goto uitd_protocol_end;
        }
//        printf("TYPE_1 dev [%d] read done (%d)\r\n", (int)dev, data_size);
        // 2. 处理业务
        ret = __UITD_ProtocolWork(serial_config, data_buff,  data_size);
        // 3. 处理完业务发送响应包
		
		if(serial_config->protocol->build_response != NULL)
		{
			data_size = 0;
			memset(data_buff, 0, data_buff_len);

			data_size = protocol->build_response(serial_config->addr, serial_config->dev_addr, ret, data_buff);    //  格式化回码
			ret = 0;
			if(data_size > 0)
			{
				ret = UITD_DeviceWrite(dev, data_buff, data_size ,serial_config->addr);  //  发送回码
			}
			if(ret <= 0)
			{
				goto uitd_protocol_end;
			}
//			printf("TYPE_1 dev [%d] write done (%d)\r\n", (int)dev, ret);
		}
		else
		{
			printf("COM[%d] TYPE_1 dev [%d] response error\r\n", serial_addr, (int)dev);
		}
    }
    else if(protocol->type == UITD_PROTOCOL_TYPE_2) //  通讯协议类型2: 信息传输装置作为主机主动发送询码 消防设施作为从机被动回复数据
    {
        // 1. 发送查询包
        if(protocol->build_request != NULL)
        {
            data_size = protocol->build_request(serial_config->addr, serial_config->dev_addr, data_buff);    //  格式化询码
        }
        if(data_size > 0)
        {
            ret = UITD_DeviceWrite(dev, data_buff, data_size,serial_config->addr);  //  发送询码
        }
        if(ret <= 0)
        {
            goto uitd_protocol_end;
        }
//        printf("TYPE_2 dev [%d] write done (%d)\r\n", (int)dev, ret);
        // 2. 读响应数据
        data_size = 0;
        memset(data_buff, 0, data_buff_len);
        data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
        if(data_size <= 0)
        {
			printf("COM[%d] TYPE_2 dev [%d] read error\r\n", serial_addr, (int)dev);
            goto uitd_protocol_end;
        }
//        printf("TYPE_2 dev [%d] read done (%d)\r\n", (int)dev, data_size);
        // 3. 处理业务
        __UITD_ProtocolWork(serial_config, data_buff,  data_size);
    }
    else if(protocol->type == UITD_PROTOCOL_TYPE_3) //  通讯协议类型3: 信息传输装置被动接收消防设施的数据 不需要回码
    {
        // 1. 读打印数据
        data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
        if(data_size <= 0)
        {
            goto uitd_protocol_end;
        }
//        printf("TYPE_3 dev [%d] read done (%d)\r\n", (int)dev, data_size);
        // 2. 处理业务
        __UITD_ProtocolWork(serial_config, data_buff,  data_size);
    }
    else if(protocol->type == UITD_PROTOCOL_TYPE_4) //  通讯协议类型4: 全双工
    {
        // 1. 读打印数据
        data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
        if(data_size <= 0)
        {
            // 如果没有读到数据 则主动发送查询包
            if(protocol->build_request != NULL)
            {
                data_size = protocol->build_request(serial_config->addr, serial_config->dev_addr, data_buff);    //  格式化询码
                if(data_size > 0)
                {
                    ret = UITD_DeviceWrite(dev, data_buff, data_size,serial_config->addr);  //  发送询码
                }
                if(ret <= 0)
                {
                    goto uitd_protocol_end;
                }
                printf("COM[%d] TYPE_4 dev [%d] write done (%d)\r\n", serial_addr, (int)dev, ret);
                // goto uitd_protocol_end;
                data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
            }
        }
        if(data_size > 0)
        {
            printf("COM[%d] TYPE_4 dev [%d] read done (%d)\r\n", serial_addr, (int)dev, data_size);
            // 2. 处理业务
            __UITD_ProtocolWork(serial_config, data_buff,  data_size);
            if(serial_config->protocol->build_response != NULL)
            {
                data_size = 0;
                memset(data_buff, 0, data_buff_len);
                data_size = protocol->build_response(serial_config->addr, serial_config->dev_addr, ret, data_buff);    //  格式化回码
                ret = 0;
                if(data_size > 0)
                {
                    ret = UITD_DeviceWrite(dev, data_buff, data_size,serial_config->addr);  //  发送回码
                }
                    if(ret <= 0)
                    {
                        goto uitd_protocol_end;
                    }
                //          printf("TYPE_4 dev [%d] write done (%d)\r\n", (int)dev, ret);
                }
                else
                {
                    printf("COM[%d] TYPE_4 dev [%d] response error\r\n", serial_addr, (int)dev);
                }
        }
    }
    else if(protocol->type == UITD_PROTOCOL_TYPE_5) //  通讯协议类型3: 信息传输装置被动接收消防设施的数据 不需要回码
    {
        // 1. 读打印数据
        data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
        if(data_size <= 0)
        {
            goto uitd_protocol_end;
        }
        if(protocol->build_response != NULL)
        {
            ret = protocol->build_response(serial_config->addr, serial_config->dev_addr, data_buff, data_size);
        }
        if(ret == true)
        {
            // 则主动发送查询包
            if(protocol->build_request != NULL)
            {
                data_size = protocol->build_request(serial_config->addr, serial_config->dev_addr, data_buff);    //  格式化询码
                if(data_size > 0)
                {
                    ret = UITD_DeviceWrite(dev, data_buff, data_size,serial_config->addr);  //  发送询码
                }
                if(ret <= 0)
                {
                    goto uitd_protocol_end;
                }
                data_size = UITD_DeviceRead(dev, data_buff, data_buff_len, serial_config->addr, protocol->timeout);
                if(data_size <= 0)
                {
                    goto uitd_protocol_end;
                }
                //        printf("TYPE_5 dev [%d] read done (%d)\r\n", (int)dev, data_size);
                // 2. 处理业务
                __UITD_ProtocolWork(serial_config, data_buff,  data_size);
            }
        }
        __UITD_ProtocolWork(serial_config, data_buff,  data_size);
    //        printf("TYPE_3 dev [%d] read done (%d)\r\n", (int)dev, data_size);
    }
    else
    {
        printf("protocol type is invalid\r\n");
    }
uitd_protocol_end:
    free(data_buff);
}

u8 __UITD_ProtocolWork(struct SerialConfig *serial_config, u8 *data_buff, u32 data_size)
{
   /* 下游通讯缓存 */
    struct FireCtrlSystem sys;  //  消防系统信息缓存
    struct FireCtrlUnit unit[UITD_UNIT_SIZE_MAX];   //  消防部件信息缓存
    struct FireCtrlUnit *unit_p[UITD_UNIT_SIZE_MAX];    //  消防部件信息缓存指针
    struct FireCtrlInfo info;   //  消防信息传输缓存

    s32 ret = -1;
    s32 i;
    if(serial_config == NULL)
    {
        printf("serial config is NULL\r\n");
        return false;
    }

     // 初始化信息缓存空间
    memset(&sys, 0, sizeof(struct FireCtrlSystem));
    memset(unit, 0, sizeof(struct FireCtrlUnit) * UITD_UNIT_SIZE_MAX);

    sys.addr = serial_config->addr; //  初始给一个系统地址
    sys.type = serial_config->protocol->sys_type;

    for(i=0; i<UITD_UNIT_SIZE_MAX; i++)
    {
        unit[i].sys_addr = sys.addr;   //  初始给一个系统地址
        unit_p[i] = &unit[i];
    }

    info.sys = &sys;
    info.unit = unit_p;
    info.unit_size = 0;

    if(serial_config->protocol == NULL)
    {
        printf("protocol is NULL\r\n");
        return false;
    }
    if(data_buff == NULL)
    {
        printf("data_buff is NULL\r\n");
        return false;
    }
    if(data_size == 0)
    {
        printf("data_size is 0\r\n");
        return false;
    }
    if(serial_config->protocol->parse_response != NULL)
    {
        ret = serial_config->protocol->parse_response(serial_config->addr, serial_config->dev_addr, data_buff, data_size, &info);    //  解析下游数据为标准格式
    }
    if(ret < 1)
    {
        printf("[%d] protocol recv error\r\n",serial_config->addr);
#if UITD_LOG
        log_queue_push(data_buff,data_size,LOG_DEVICE_READ,"NULL");
#endif
        return false;
    }
#if UITD_LOG
     log_queue_push(data_buff,data_size,LOG_DEVICE_READ,"NULL");
#endif
    if(ret < UITD_DATA_TYPE_FAIL)   //  如解析失败则跳过处理
    {
        //3.控件内容入栈，进行国标处理
        __UITD_ProtocolHandler(ret, &info);
		return true;
    }
	return false;
}

//更新状态或者操作
void __UITD_ProtocolHandler(u8 type, struct FireCtrlInfo *info)
{
    int i;
    if(info == NULL) return;
    // TODO 多线程是否需要锁
    if(info->sys != NULL)
    {
		if(info->sys->type != 0 && UITD_FindSys(info->sys->addr)->type == 0)
		{
			UITD_FindSys(info->sys->addr)->type = info->sys->type;
		}
        if ((type & UITD_DATA_TYPE_SYS_STS) == UITD_DATA_TYPE_SYS_STS)
        {
            UITD_UpdateSysSts(info->sys->addr, info->sys->status);
        }
        if ((type & UITD_DATA_TYPE_SYS_OPT) == UITD_DATA_TYPE_SYS_OPT)
        {
            UITD_UpdateSysOpt(info->sys->addr, info->sys->opt, info->sys->opt_id);
        }
        if ((type & UITD_DATA_TYPE_SYS_VER) == UITD_DATA_TYPE_SYS_VER)
        {
            UITD_UpdateSysVer(info->sys->addr, info->sys->version, info->sys->version_user);
        }
        if ((type & UITD_DATA_TYPE_SYS_CFG) == UITD_DATA_TYPE_SYS_CFG)
        {
            UITD_UpdateSysCfg(info->sys->addr, info->sys->cfg, info->sys->cfg_len);
        }
    }
    if(info->unit_size > 0)
    {
        for(i=0; i<info->unit_size; i++)
        {
			if(UITD_FindUnit(info->unit[i]->sys_addr, info->unit[i]->addr) == NULL)
			{
				UITD_AddUnit(info->unit[i]->sys_addr, info->unit[i]->type,info->unit[i]->addr);
			}
            if ((type & UITD_DATA_TYPE_UNIT_STS) == UITD_DATA_TYPE_UNIT_STS)
            {
                UITD_UpdateUnitSts(info->unit[i]->sys_addr, info->unit[i]->addr, info->unit[i]->status);
            }
            if ((type & UITD_DATA_TYPE_UNIT_CFG) == UITD_DATA_TYPE_UNIT_CFG)
            {
                UITD_UpdateUnitCfg(info->unit[i]->sys_addr, info->unit[i]->addr, info->unit[i]->dsc);
            }
            if ((type & UITD_DATA_TYPE_UNIT_AD) == UITD_DATA_TYPE_UNIT_AD)
            {
                UITD_UpdateUnitAd(info->unit[i]->sys_addr, info->unit[i]->addr, info->unit[i]->ad_type, info->unit[i]->ad_val);
            }
		}
    }

//    UITD_PrintInfo(type,info);
}

void UITD_SetSerialConfig(u8 addr, struct SerialConfig *cfg)
{
    if(addr >= UITD_SERIAL_NUM)
    {
        printf("serial addr error\r\n");
        return;
    }
    printf("set before dev %d: %x \r\n", addr, (ucpu_t)serial_devices[addr-1]);
    if(serial_devices[addr] != NULL)  //  如果该串口已有配置
    {
        UITD_DeviceClose(addr); //  则先关闭串口 然后重新配置
        serial_devices[addr] = NULL;
    }
	if(cfg == NULL || cfg->bitrate == 0)
	{
		printf("串口[%d]通讯配置 为空\r\n", addr);
		return;
	}
    serial_devices[addr] = UITD_DeviceOpen(addr, cfg);

    printf("set dev %d: %x \r\n", addr, (ucpu_t)serial_devices[addr]);
    if((int)serial_devices[addr] == -1)
    {
        printf("serial open error\r\n");
        return;
    }
    //  保存配置
    FC_SERIAL_CONFIG[addr].addr = addr;
    FC_SERIAL_CONFIG[addr].bitrate = cfg->bitrate;
    FC_SERIAL_CONFIG[addr].data_bit = cfg->data_bit;
    FC_SERIAL_CONFIG[addr].parity_bit = cfg->parity_bit;
    FC_SERIAL_CONFIG[addr].stop_bit = cfg->stop_bit;
    FC_SERIAL_CONFIG[addr].dev_addr = cfg->dev_addr;
    FC_SERIAL_CONFIG[addr].protocol = NULL;//cfg->protocol;
//    printf("set dev %d: %x \r\n", addr, FC_SERIAL_CONFIG[addr]);
}

struct SerialConfig * UITD_GetSerialConfig(u8 addr)
{
    if(addr >= UITD_SERIAL_NUM)
    {
        printf("serial addr error\r\n");
        return NULL;
    }
    /*
    if(serial_config[addr].addr != addr)
    {
        return NULL;
    }
    */
    return &FC_SERIAL_CONFIG[addr];
}

void UITD_SetSerialProtocol(u8 addr, struct ProtocolVTable *ptc)
{
    struct SerialConfig* config;
    if(addr >= UITD_SERIAL_NUM)
    {
        printf("serial addr error\r\n");
        return;
    }
    config = UITD_GetSerialConfig(addr);
    if(config == NULL)
    {
        return;
    }
	if(ptc == NULL)
	{
		printf("删除串口[%d]协议配置\r\n", addr);
		UITD_DeleteSys(addr);
		config->protocol = NULL;
		return;
	}
    config->protocol = ptc;
	UITD_DeleteSys(addr);
	UITD_AddSys(ptc->sys_type, addr);
}

struct ProtocolVTable * UITD_GetSerialProtocol(u8 addr)
{
    struct SerialConfig* config;
    if(addr >= UITD_SERIAL_NUM)
    {
        printf("serial addr error\r\n");
        return NULL;
    }
    config = UITD_GetSerialConfig(addr);
    if(config == NULL)
    {
        return NULL;
    }
    return config->protocol;
}

void *UITD_GetDevice(u8 addr)
{
    if(addr >= UITD_SERIAL_NUM)
    {
        printf("serial addr error\r\n");
        return (void *)0xFFFFFFFF;
    }
    return serial_devices[addr];
}

#endif

void UITD_Init(void)
{
    if(UITD_init_flag)
    {
        printf("UITD Already Init\r\n");
        return;
    }
    printf("UITD Init\r\n");
    memset(FC_SYSTEMS, 0, UITD_SERIAL_NUM * 4);
    memset(FC_UNIT_MAP, 0, UITD_SERIAL_NUM * 4);

#if UITD_AUTO_UPLOAD
    // 初始化上传队列
    if(FC_UPLOAD_QUEUE == NULL)
    {
        FC_UPLOAD_QUEUE = Queue_CreateQueue();
        printf("Queue Create Success:%x\r\n", (ucpu_t)FC_UPLOAD_QUEUE);
    }
    if(FC_UPLOAD_QUEUE == NULL)
    {
        printf("Queue Create Failure.\r\n");
    }
#endif
#if UITD_AUTO_RELOAD
    UITD_RELOADMAP = Map_New(); //  初始化重传数据表
#endif
#if UITD_AUTO_HEARTBEAT
    LAST_HB_TIME = UITD_GetTimestamp();
    FIRST_HB_FLAG = false;
#endif
#if UITD_PROTOCOL_FUNC
    memset(&FC_SERIAL_CONFIG, 0, UITD_SERIAL_NUM * sizeof(struct SerialConfig));
    memset(serial_devices, 0, UITD_SERIAL_NUM * 4);

    UITD_InitSerial();  //  初始化本地串口及协议配置
#endif

    UITD_init_flag = true;  //  设置初始化标志
}

struct FireCtrlSystem * UITD_AddSys(u8 sys_type, u8 sys_addr)
{
    struct FireCtrlSystem *fcsys;
    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return NULL;
    }
    if(FC_SYSTEMS[sys_addr] != NULL)
    {
        return NULL;
    }
    fcsys = (struct FireCtrlSystem *)malloc(sizeof(struct FireCtrlSystem));
    if(fcsys == NULL)
    {
        return NULL;
    }
    fcsys->type = sys_type;
    fcsys->addr = sys_addr;
    fcsys->cfg_len = 0;
    memset(fcsys->cfg, 0, 0xFF);
    FC_SYSTEMS[sys_addr] = fcsys;

    FC_UNIT_MAP[sys_addr] = Map_New();
    printf("UITD_AddSys type[%x] addr[%x]\r\n", sys_type, sys_addr);
    return fcsys;
}

struct FireCtrlUnit *UITD_AddUnit(u8 sys_addr, u8 unit_type, u32 unit_addr)
{
    struct FireCtrlUnit *unit;
    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return NULL;
    }

    if(FC_SYSTEMS[sys_addr] == NULL)
    {
        return NULL;
    }
    unit = (struct FireCtrlUnit *)malloc(sizeof(struct FireCtrlUnit));
    if(unit == NULL)
    {
        return NULL;
    }

    unit->type = unit_type;
    unit->addr = unit_addr;
    unit->sys_addr = sys_addr;
    unit->ad_val = 0;
    memset(unit->dsc, 0, 31);
    if(Map_Put(FC_UNIT_MAP[sys_addr], unit_addr, unit))
    {
        printf("UITD_AddUnit sys_addr[%x] type[%x] addr[%x]\r\n", sys_addr, unit_type, unit_addr);
        return unit;
    }
    return NULL;
}

void UITD_DeleteSys(u8 sys_addr)
{

    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return;
    }
    if(FC_UNIT_MAP[sys_addr] != NULL)
    {
        Map_Clear(FC_UNIT_MAP[sys_addr]);
        free(FC_UNIT_MAP[sys_addr]);
        FC_UNIT_MAP[sys_addr] = NULL;
    }

    if(FC_SYSTEMS[sys_addr] != NULL)
    {
        free(FC_SYSTEMS[sys_addr]);
        FC_SYSTEMS[sys_addr] = NULL;
    }
}

void UITD_DeleteUnit(u8 sys_addr, u32 unit_addr)
{
    struct FireCtrlUnit *unit_old;
    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return;
    }

    if(FC_UNIT_MAP[sys_addr] != NULL)
    {
        unit_old = Map_Get(FC_UNIT_MAP[sys_addr], unit_addr);
        if(unit_old != NULL) free(unit_old);
        Map_Remove(FC_UNIT_MAP[sys_addr], unit_addr);
    }
}

struct FireCtrlSystem * UITD_FindSys(u8 sys_addr)
{
    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return NULL;
    }
    return FC_SYSTEMS[sys_addr];
}

struct FireCtrlUnit *UITD_FindUnit(u8 sys_addr, u32 unit_addr)
{
    if(sys_addr >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return NULL;
    }

    // printf("UITD_FindUnit sys_addr[%x] addr[%x]\r\n", sys_addr, unit_addr);
    if(FC_UNIT_MAP[sys_addr] == NULL)
    {
        return NULL;
    }
    return (struct FireCtrlUnit *)Map_Get(FC_UNIT_MAP[sys_addr], unit_addr);
}

struct UserInfoTransDev *UITD_FindDev(void)
{
    return &FC_DEV;
}

int UITD_UpdateSysAddr(u8 sys_addr_old, u8 sys_addr_new)
{
    struct FireCtrlSystem *fcsys;
    struct FireCtrlSystem *sys_new;
    if(sys_addr_old >= UITD_SERIAL_NUM || sys_addr_new >= UITD_SERIAL_NUM)
    {
        printf("sys addr is invailed\r\n");
        return false;
    }
    if(sys_addr_new == sys_addr_old)
        return true;
    fcsys = UITD_FindSys(sys_addr_old);
    if(fcsys == NULL)
        return false;

    sys_new = UITD_FindSys(sys_addr_new);
    if(sys_new != 0)
        return false;

    fcsys->addr = sys_addr_new; // FIXME
    FC_SYSTEMS[sys_addr_old] = NULL;
    FC_SYSTEMS[sys_addr_new] = fcsys;
    return true;
}

int UITD_UpdateUnitAddr(u8 sys_addr, u32 unit_addr_old, u32 unit_addr_new)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    struct FireCtrlUnit *unit;
    struct FireCtrlUnit *unit_new;
    struct FireCtrlUnit *unit_old;
    if(sys == NULL)
        return false;

    if(unit_addr_old == unit_addr_new)
        return true;

    unit = UITD_FindUnit(sys_addr, unit_addr_old);
    if(unit == NULL)
        return false;

    unit_new = UITD_FindUnit(sys_addr, unit_addr_new);
    if(unit_new != NULL)
        return false;

    unit_new = (struct FireCtrlUnit *)malloc(sizeof(struct FireCtrlUnit));
    memcpy(unit_new, unit, sizeof(struct FireCtrlUnit));
    unit_new->addr = unit_addr_new;

    unit_old = Map_Get(FC_UNIT_MAP[sys_addr], unit_addr_old);
    if(unit_old != NULL) free(unit_old);
    Map_Remove(FC_UNIT_MAP[sys_addr], unit_addr_old);

    Map_Put(FC_UNIT_MAP[sys_addr], unit_addr_new, unit_new);
    return true;
}

void UITD_UpdateSysSts(u8 sys_addr, u16 sys_sts)
{
    char log_information[256]={0};
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    // printf("UITD_UpdateSysSts \r\n");
    if(sys == NULL)
    {
        printf("UITD_Sys is NULL\r\n");
        return;
    }
    if(((sys->status & GB26875_SYS_STS_CB) != GB26875_SYS_STS_CB)
            && sys->status == sys_sts)
    {
        return;
    }
    sys->status = sys_sts;

    snprintf(log_information,sizeof(log_information),"系统地址:%d,系统状态:%d",sys->addr, sys->status);
    printf("系统地址: %d, 系统状态: %d\r\n", sys->addr, sys->status);
    SetLogPageInf("系统",log_information);


#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_SYS_STS + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_SYS_STS;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildSysSts(sys, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateSysOpt(u8 sys_addr, u8 opt_flag, u8 opt_id)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    // printf("UITD_UpdateSysOpt \r\n");
    if(sys == NULL)
    {
        printf("UITD_Sys is NULL\r\n");
        return;
    }
    sys->opt = opt_flag;
    sys->opt_id = opt_id;

    printf("系统地址: %d, 系统操作: %d, 系统操作员id: %d\r\n", sys->addr, sys->opt, sys->opt_id);

#if UITD_AUTO_UPLOAD
    //upload_buff = (u8 *)malloc(GB26875_OBJLEN_SYS_OPT + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_SYS_OPT;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildSysOpt(sys, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateSysVer(u8 sys_addr, u8 ver_main, u8 ver_user)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    if(sys == NULL)
    {
       return;
    }
    sys->version = ver_main;
    sys->version_user = ver_user;

    printf("系统地址: %d, 系统版本: %d, 系统次版本: %d\r\n", sys->addr, sys->version, sys->version_user);

#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_SYS_VER + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_SYS_VER;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildSysVer(sys, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateSysCfg(u8 sys_addr, u8 *cfg, u8 cfg_len)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    // printf("UITD_UpdateSysCfg %x:(%.*s)\r\n", sys_addr, cfg_len, cfg);
    if(sys == NULL)
    {
        printf("UITD_Sys is NULL \r\n");
        return;
    }
    if(cfg != NULL)
    {
        memcpy(sys->cfg, cfg, cfg_len);
        sys->cfg_len = cfg_len;
    }
    else
    {
        memset(sys->cfg, 0, 0xFF);
        sys->cfg_len = 0;
    }

    printf("系统地址: %d, 系统配置: %.*s\r\n", sys->addr, sys->cfg_len, sys->cfg);

#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_SYS_CFG + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_SYS_CFG;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildSysCfg(sys, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateUnitSts(u8 sys_addr, u32 unit_addr, u16 unit_sts)
{
    char log_information[256]={0};
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    struct FireCtrlUnit *unit = UITD_FindUnit(sys_addr, unit_addr);
    u8 unit_date[56]={0};
    // printf("UITD_UpdateUnitSts %x-%x:(%d)\r\n", sys_addr, unit_addr, unit_sts);
#if UITD_LOG
    Log_GetUnit(unit_date,sys_addr,unit_addr,unit_sts);
#endif
    if(sys == NULL || unit == NULL)
    {
#if UITD_LOG
        log_queue_push(unit_date,strlen(unit_date),LOG_UNIT_STATE,"0");
#endif
        printf("UITD_SysUnit is NULL \r\n");
        return;
    }

    if(((unit->status & GB26875_UNIT_STS_CB) != GB26875_UNIT_STS_CB)
            && unit->status == unit_sts)
    {
//        printf("UITD_ad_val is SAME \r\n");
        return;
    }

#if UITD_LOG
    log_queue_push(unit_date,sizeof(unit_date),LOG_UNIT_STATE,"1");
#endif
    unit->status = unit_sts;

    snprintf(log_information,sizeof(log_information),"系统地址:%d,部件地址:%d,部件状态:%d",unit->sys_addr, unit->addr, unit->status);
     printf("系统地址: %d, 部件地址: %d, 部件状态: %d\r\n", unit->sys_addr, unit->addr, unit->status);
    SetLogPageInf("部件",log_information);
#if UITD_AUTO_UPLOAD
    //  注释为什么+10: 标志位1+对象数目1+时间标签6+容错2=10
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_UNIT_STS + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_UNIT_STS;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildUnitSts(sys, unit, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

static s64 UITD_LAST_UPD_UNIT_AD_TIME = 0;
void UITD_UpdateUnitAd(u8 sys_addr, u32 unit_addr, u8 ad_type, u16 ad_val)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    struct FireCtrlUnit *unit = UITD_FindUnit(sys_addr, unit_addr);
    s64 now = DJY_GetSysTime();
    // printf("更新模拟量值 系统[%x]部件[%x]类型:[%x]值(%d)\r\n", sys_addr, unit_addr, ad_type, ad_val);
    if(sys == NULL || unit == NULL)
    {
        printf("UITD_SysUnit is NULL \r\n");
        return;
    }

    if(now - UITD_LAST_UPD_UNIT_AD_TIME < 180 * 1000000)
    {
        if(unit->ad_val == ad_val)
        {
    //        printf("UITD_ad_val is SAME \r\n");
            return;
        }
    }
    else if(now - UITD_LAST_UPD_UNIT_AD_TIME > 190 * 1000000)
    {
        UITD_LAST_UPD_UNIT_AD_TIME = now;
    }

    unit->ad_type = ad_type;
    unit->ad_val = ad_val;

    // printf("系统地址: %d, 部件地址: %d, 部件模拟值类型: %d, 部件模拟值: %d\r\n", unit->sys_addr, unit->addr, unit->ad_type, unit->ad_val);

#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_UNIT_AD + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_UNIT_AD;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildUnitAd(sys, unit, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateUnitCfg(u8 sys_addr, u32 unit_addr, u8 *cfg)
{
    struct FireCtrlSystem *sys = UITD_FindSys(sys_addr);
    struct FireCtrlUnit *unit = UITD_FindUnit(sys_addr, unit_addr);
    int cfg_len = 0;
    printf("UITD_UpdateUnitCfg \r\n");
    if(sys == NULL || unit == NULL)
    {
        printf("UITD_SysUnit is NULL \r\n");
        return;
    }
    cfg_len = cfg == NULL ? 0 : strlen(cfg);
    cfg_len = cfg_len > 31 ? 31 : cfg_len;
    memcpy(unit->dsc, cfg, cfg_len);
#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_UNIT_CFG + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_UNIT_CFG;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildUnitCfg(sys, unit, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateDevSts(u8 status)
{
    struct UserInfoTransDev *dev = UITD_FindDev();
    // printf("UITD_UpdateDevSts \r\n");
    dev->status = status;
#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_DEV_STS + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_DEV_STS;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildDevSts(dev, &upload_buff[2]);
    // UITD_AddToUploadBuff(upload_buff, upload_size + 2);      //  目前发现程序不停地在改变装置状态 暂时取消上传该类信息
#endif
}

void UITD_UpdateDevOpt(u8 opt, u8 opt_id)
{
    struct UserInfoTransDev *dev = UITD_FindDev();
    // printf("UITD_UpdateDevOpt \r\n");
    dev->opt = opt;
    dev->opt_id = opt_id;
#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_DEV_OPT + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_DEV_OPT;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildDevOpt(dev, &upload_buff[2]);
    // UITD_AddToUploadBuff(upload_buff, upload_size + 2);      //  目前发现程序不停地在改变装置操作信息 暂时取消上传该类信息
#endif
}

void UITD_UpdateDevVer(u8 ver_main, u8 ver_user)
{
    struct UserInfoTransDev *dev = UITD_FindDev();
    // printf("UITD_UpdateDevVer \r\n");
    dev->version = ver_main;
    dev->version_user = ver_user;
#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_DEV_VER + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_DEV_VER;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildDevVer(dev, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_UpdateDevCfg(u8 *cfg, u8 cfg_len)
{
    struct UserInfoTransDev *dev = UITD_FindDev();
    // printf("UITD_UpdateDevCfg \r\n");
    if(cfg != NULL)
    {
        memcpy(dev->cfg, cfg, cfg_len);
        dev->cfg_len = cfg_len;
    }
    else
    {
        memset(dev->cfg, 0, 0xFF);
        dev->cfg_len = 0;
    }
#if UITD_AUTO_UPLOAD
    // upload_buff = (u8 *)malloc(GB26875_OBJLEN_DEV_CFG + 10);
    memset(upload_buff, 0, UPLOAD_BUFF_LEN);
    upload_buff[0] = GB26875_UPLOAD_DEV_CFG;                    //  类型标志
    upload_buff[1] = 1;                                         //  信息体对象数目
    upload_size = GB26875_BuildDevCfg(dev, &upload_buff[2]);
    UITD_AddToUploadBuff(upload_buff, upload_size + 2);
#endif
}

void UITD_AddToUploadBuff(u8 *data, int len)
{
    struct String *str;
    if(FC_UPLOAD_QUEUE == NULL)
    {
        return;
    }
    str = (struct String *)malloc(sizeof(struct String));
    if(str != NULL)
    {
        str->buff = (u8 *)malloc(len);
//        printf("++++++++++ 上传队列入队申请 str: [%x],buff: [%x]\r\n", str, str->buff);
        if(str->buff != NULL)
        {
            memcpy(str->buff, data, len);
            str->len = len;
            //  实际存的是格式化后的应用数据单元数据
            if(Queue_Push(FC_UPLOAD_QUEUE, (ElementType)str))
            {
        //#if UITD_LOG
        //        log_queue_push(str->buff,str->len,LOG_STATE_PUSH,"1");
        //#endif
        //        printf("Queue_Push value:%x len:%d\r\n", str->buff, str->len);
            }
            else
            {
        //#if UITD_LOG
        //        log_queue_push("NULL",0,LOG_STATE_PUSH,"0");
        //#endif
                free((void *)str->buff);
                free((void *)str);
            }
        }
        else free((void *)str);
    }
}

u16 info_no=1;   //  流水号

void UITD_ResetInfoNo()
{
    g_UitdConnCnt ++;
    info_no = 1;
}

void UITD_UploadThread(void)
{
    struct String *str;
    u8 send_buff[0x420];
    int send_len = 0;
    struct GB26875Info info;
//	printf("尝试上传数据\r\n");
    //  实际取的是格式化后的应用数据单元数据
    if(Queue_Top(FC_UPLOAD_QUEUE, (ElementType *)&str) == FAILURE)
    {
		// printf("上传队列为空\r\n");
        return;
    }
    if(str == NULL)
    {
		// printf("队列弹出后内容为空\r\n");
		Queue_Pop(FC_UPLOAD_QUEUE, (ElementType *)&str);
        // free(str->buff);
        // free(str);
        return;
    }
//#if UITD_LOG
//    log_queue_push(str->buff,str->len,LOG_STATE_POP,"1");
//#endif
    memset(send_buff, 0, 0x420);
    info.no = info_no;                       // 业务流水号 应全局变量累加
    info.data_len = str->len;               //  应用数据单元长度
    memcpy(info.data, str->buff, str->len); //  应用数据单元数据
    info.opt = ControlComm_2_SendInfo;      //  命令字节
	UITD_GetClientIp(info.src);             // 源地址
    memset(info.dct, 0, 6);                 // FIXME 目标地址
    GB26875_FormatTimeFlag(info.time);      // 时间标签
    info.version = 0x0101;                  // 协议版本号 目前为1
    // info.check_sum = GB26875_CheckSum(&info);
    send_len = GB26875_BuildData(&info, send_buff); //  完善校验和前后协议格式

#if UITD_LOG
    log_queue_push(send_buff,send_len,LOG_UPDATE_MESSAGE,"NULL");
#endif
    // printf("这是设备上传 业务流水号: %d\r\n", info_no);
    if(UITD_Upload(send_buff, send_len, 0))    //  上传数据 需阻塞
    {
        g_UitdUploadErrorCnt = 0;
#if UITD_AUTO_UPLOAD
        g_UitdUploadCnt ++;
        g_UitdUploadSize += send_len;
#endif
        Queue_Pop(FC_UPLOAD_QUEUE, (ElementType *)&str);
//        printf("---------- 上传队列出队释放str: [%x],buff: [%x]\r\n", str, str->buff);
        free(str->buff);
        free(str);
//        printf("上传完成 业务流水号: %d\r\n",info_no);
#if UITD_AUTO_RELOAD
//        printf("新增到重传缓冲，流水号: %d (%02x %02x)\r\n", info_no, send_buff[2], send_buff[3]);
        UITD_AddToReloadBuff((u8 *)send_buff, send_len, info_no);
#endif
        if(info_no >= 0xFFFF)
        {
            info_no = 0;
        }
        info_no++;
    }
    // 上传失败
    else
    {
        g_UitdUploadErrorCnt ++;
        if(g_UitdUploadErrorCnt > 3)
        {
            Queue_Pop(FC_UPLOAD_QUEUE, (ElementType *)&str);
            free(str->buff);
            free(str);
        }
    }
}

#if UITD_AUTO_RELOAD
void UITD_AddToReloadBuff(u8 *data, u32 len, u16 no)
{
    struct ReloadInfo *reload_info;
    // ret = GB26875_FormatData(data, len, &info_st);  //  此步骤可优化（获取业务流水号）

    reload_info = (struct ReloadInfo *)malloc(sizeof(struct ReloadInfo));
    if(reload_info != NULL)
    {
        reload_info->buff = NULL;
        reload_info->buff = (u8 *)malloc(len);
        // printf("++++++++++ 重传 申请 info[%x]->buff[%x] \r\n", reload_info, reload_info->buff);
        if(reload_info->buff != NULL)
        {
            // memset(reload_buff, 0, len);
            memcpy(reload_info->buff, data, len);
            reload_info->no = no;

            reload_info->buff_size = len;
            reload_info->count = 1;
            reload_info->timestamp = UITD_GetTimestamp();
            if(!Map_Put(UITD_RELOADMAP, reload_info->no, reload_info))
            {
                // printf("---------- 重传 失败释放 info[%x]->buff[%x] \r\n", reload_info, reload_info->buff);
                free(reload_info->buff);
                free(reload_info);
            }
        }
        else
        {
            printf("重传 流水号[%d] 申请内存 失败!\r\n", no);
            free(reload_info);
        }
    }
}

/**
 * 遍历重传数据表回调函数
 *
 * @author WangXi (2020/9/1)
 *
 * @param key 业务流水号
 * @param val 重传数据
 */
bool_t UITD_Reload(u32 key, void *val)
{
    struct ReloadInfo *reload_info;
    s64 timestamp = 0;
    if(val == NULL) //  过滤非法数据
    {
        return false;
    }
    reload_info = (struct ReloadInfo *)val;
    if(reload_info->count - 1 > UITD_RELOAD_COUNT) //  如果重传次数已超过上限 则标记为失败
    {
        // TODO 记录本次通讯失败
        printf("重传业务流水号: %d 失败 移出重传缓冲\r\n", key);
        // printf("---------- 重传 超时释放 info[%x]->buff[%x] \r\n", reload_info, reload_info->buff);
        free(reload_info->buff);
        free(reload_info);
        // Map_Remove(UITD_RELOADMAP, key);
        return true;
    }
    timestamp = UITD_GetTimestamp();
    if(timestamp - reload_info->timestamp >= UITD_RELOAD_TIMEOUT)   //  如果超过10秒还没确认通讯 则重传
    {
        printf("\r\n重传业务流水号: %d 第%d次\r\n", key, reload_info->count);
        // printf("这是重发机制上传 业务流水号: %d\r\n", reload_info->no);

        GB26875_FormatTimeFlag(reload_info->buff + 6);
        reload_info->buff[reload_info->buff_size-3] = GB26875_CheckSum(reload_info->buff, 2, reload_info->buff_size-4);

        if(UITD_Upload(reload_info->buff, reload_info->buff_size, 0))
        {
            g_UitdReloadCnt ++;
            g_UitdReloadSize += reload_info->buff_size;
            reload_info->count++;
            reload_info->timestamp = UITD_GetTimestamp();
        }
    }
    return false;
}

void __UITD_PrintReload(u32 key, void *val)
{
    struct ReloadInfo *reload_info;
    s64 timestamp = 0;
    if(val == NULL) //  过滤非法数据
    {
        return;
    }
    reload_info = (struct ReloadInfo *)val;
//    printf("\r\n重传队列数据: 流水号[%d][%d]", reload_info->buff[2], reload_info->buff[3]);
}

void UITD_ReloadThread()
{
    Map_ForeachWithRemove(UITD_RELOADMAP, UITD_Reload);   //  遍历重传数据表
}
#endif

#if UITD_AUTO_HEARTBEAT
int UITD_HeartBeat()
{
    u8 hb_buff[GB26875_OBJLEN_DEV_STS + 10];                 //  主动上传信息体缓存
    int hb_size;                   //  信息体缓存长度

    u8 send_buff[0x420];
    int send_len = 0;
    struct GB26875Info info;
	/*
    struct UserInfoTransDev *dev = UITD_FindDev();
    dev->status = dev->status | GB26875_DEV_STS_NORMAL;
    memset(hb_buff, 0, GB26875_OBJLEN_DEV_STS + 10);
    hb_buff[0] = GB26875_UPLOAD_DEV_STS;                    //  类型标志
    hb_buff[1] = 1;                                         //  信息体对象数目
    hb_size = GB26875_BuildDevSts(dev, &hb_buff[2]) + 2;
	*/
    memset(send_buff, 0, 0x420);
    info.no = info_no;                       // FIXME 业务流水号 应全局变量累加
    // info.data_len = hb_size;               //  应用数据单元长度
    // memcpy(info.data, hb_buff, hb_size); //  应用数据单元数据
	info.data_len = 0;

    info.opt = 7;      //  命令字节 心跳
	UITD_GetClientIp(info.src);
    memset(info.dct, 0, 6);          // FIXME 目标地址
    GB26875_FormatTimeFlag(info.time);      // 时间标签
    info.version = 0x0101;                  // FIXME 协议版本号 目前为1
    // info.check_sum = GB26875_CheckSum(&info);
    send_len = GB26875_BuildData(&info, send_buff); //  完善校验和前后协议格式
    if(UITD_Upload(send_buff, send_len, 0))
    {
     	SeticStatues(GetConnectNumb(),1);
		
        g_UitdHbCnt ++;
        g_UitdHbSize += send_len;
//        printf("heart is ");
//        for(int i =0;i<send_len;i++)
//        {
//            printf("%02x ",send_buff[i]);
//        }
//        printf("\r\n");
        info_no ++;
        return true;
    }
    return false;
}
void tra_breakout(void);
void UITD_HeartBeatThread()
{
    s64 timestamp;
    timestamp = UITD_GetTimestamp();
    if(!FIRST_HB_FLAG || timestamp - LAST_HB_TIME < UITD_HB_TIMEOUT)
    {
        if(!FIRST_HB_FLAG)
        {
            FIRST_HB_FLAG = true;
            LAST_HB_TIME = UITD_GetTimestamp();
        }
        UITD_HeartBeat();
    }
	else
	{
	    FIRST_HB_FLAG = false;
	    tra_breakout();
	}
}
#endif

int UITD_QuerySysSts(struct FireCtrlSystem sys[], int obj_cnt, u8 *info_data)
{
    int i, index = 0;
    struct FireCtrlSystem *s;
    u8 info_obj[0x100];
    int obj_len = 0;
    if(info_data == NULL)
    {
        return 0;
    }
    info_data[0] = GB26875_DOWNLOAD_SYS_STS;
    info_data[1] = 0;
    index = 2;
    for(i=0; i<obj_cnt; i++)
    {
        memset(info_obj, 0, 0x100);
        s = UITD_FindSys(sys[i].addr);
        obj_len = GB26875_BuildSysSts(s, info_obj);
        info_data[1] += (obj_len > 0);
        memcpy(&info_data[index], info_obj, obj_len);
        index += obj_len;
    }
    printf("UITD_QuerySysSts 3=%d\r\n", index);
    return index;
}

int UITD_QuerySysOpt(struct FireCtrlSystem *sys, int opt_cnt, u8 start[6], u8 *info_data)
{
    struct FireCtrlSystem *s;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_SYS_OPT;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    s = UITD_FindSys(sys->addr);
    // FIXME 需完善业务 opt_cnt为查询条数 start为开始时间
    obj_len = GB26875_BuildSysOpt(s, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QuerySysVer(struct FireCtrlSystem *sys, u8 *info_data)
{
    struct FireCtrlSystem *s;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_SYS_VER;
    // info_data[1] = 1;

    memset(info_obj, 0, 0x100);
    s = UITD_FindSys(sys->addr);
    obj_len = GB26875_BuildSysVer(s, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QuerySysCfg(struct FireCtrlSystem sys[], int obj_cnt, u8 *info_data)
{
    int i, index = 0;
    struct FireCtrlSystem *s;
    u8 info_obj[0x120];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_SYS_CFG;
    // info_data[1] = obj_cnt;
    info_data[1] = 0;
    index = 2;
    for(i=0; i<obj_cnt; i++)
    {
        memset(info_obj, 0, 0x120);
        s = UITD_FindSys(sys[i].addr);
        obj_len = GB26875_BuildSysCfg(s, info_obj);
        info_data[1] += (obj_len > 0);
        memcpy(&info_data[index], info_obj, obj_len);
        index += obj_len;
    }
    return index;
}

int UITD_QuerySysTime(struct FireCtrlSystem *sys, u8 *info_data)
{
    struct FireCtrlSystem *s;
    u8 info_obj[0x120];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_SYS_TIME;
    // info_data[1] = 1;

    memset(info_obj, 0, 0x120);
    s = UITD_FindSys(sys->addr);
    obj_len = GB26875_BuildSysTime(s, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryUnitSts(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data)
{
    int i, index = 0;
    struct FireCtrlSystem *s;
    struct FireCtrlUnit *u;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_UNIT_STS;
    // info_data[1] = obj_cnt;
    info_data[1] = 0;
    index = 2;
    for(i=0; i<obj_cnt; i++)
    {
        memset(info_obj, 0, 0x100);
        s = UITD_FindSys(unit[i].sys_addr);
        u = UITD_FindUnit(unit[i].sys_addr, unit[i].addr);
        obj_len = GB26875_BuildUnitSts(s, u, info_obj);
        info_data[1] += (obj_len > 0);
        memcpy(&info_data[index], info_obj, obj_len);
        index += obj_len;
    }
    return index;
}

int UITD_QueryUnitCfg(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data)
{
    int i, index = 0;
    struct FireCtrlSystem *s;
    struct FireCtrlUnit *u;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_UNIT_CFG;
    // info_data[1] = obj_cnt;
    info_data[1] = 0;
    index = 2;
    for(i=0; i<obj_cnt; i++)
    {
        memset(info_obj, 0, 0x100);
        s = UITD_FindSys(unit[i].sys_addr);
        u = UITD_FindUnit(unit[i].sys_addr, unit[i].addr);
        obj_len = GB26875_BuildUnitCfg(s, u, info_obj);
        info_data[1] += (obj_len > 0);
        memcpy(&info_data[index], info_obj, obj_len);
        index += obj_len;
    }
    return index;
}

int UITD_QueryUnitAd(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data)
{
    int i, index = 0;
    struct FireCtrlSystem *s;
    struct FireCtrlUnit *u;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_UNIT_AD;
    // info_data[1] = obj_cnt;
    info_data[1] = 0;
    index = 2;
    for(i=0; i<obj_cnt; i++)
    {
        memset(info_obj, 0, 0x100);
        s = UITD_FindSys(unit[i].sys_addr);
        u = UITD_FindUnit(unit[i].sys_addr, unit[i].addr);
        obj_len = GB26875_BuildUnitAd(s, u, info_obj);
        info_data[1] += (obj_len > 0);
        memcpy(&info_data[index], info_obj, obj_len);
        index += obj_len;
    }
    return index;
}

int UITD_QueryDevSts(u8 *info_data)
{
    struct UserInfoTransDev *d;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_DEV_STS;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    d = UITD_FindDev();
    obj_len = GB26875_BuildDevSts(d, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryDevOpt(int opt_cnt, u8 start[6], u8 *info_data)
{
    struct UserInfoTransDev *d;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_DEV_OPT;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    d = UITD_FindDev();
    // FIXME 需完善业务 opt_cnt为查询条数 start为开始时间
    obj_len = GB26875_BuildDevOpt(d, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryDevVer(u8 *info_data)
{
    struct UserInfoTransDev *d;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_DEV_VER;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    d = UITD_FindDev();
    obj_len = GB26875_BuildDevVer(d, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryDevCfg(u8 *info_data)
{
    struct UserInfoTransDev *d;
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_DEV_CFG;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    d = UITD_FindDev();
    obj_len = GB26875_BuildDevCfg(d, info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryDevTime(u8 *info_data)
{
    u8 info_obj[0x100];
    int obj_len = 0;

    info_data[0] = GB26875_DOWNLOAD_DEV_TIME;
    // info_data[1] = 1;
    memset(info_obj, 0, 0x100);
    obj_len = GB26875_BuildDevTime(info_obj);
    info_data[1] = (obj_len > 0);
    memcpy(&info_data[2], info_obj, obj_len);
    return obj_len + 2;
}

int UITD_QueryInfo(u8 *info_body, int len, u8 *result)
{
    struct GB26875InfoBody infobody_st;
    struct GB26875InfoBody *info = &infobody_st;
    struct FireCtrlSystem *sys;
    struct FireCtrlUnit *unit;
    int user_cnt = 0;
    u8 user_str[0xFF] = {0};
    u8 info_buff[1024] = {0};     //  输出格式化后的数据缓存
    int info_len = 0;
    int ret = 0;

    memset(info, 0, sizeof(struct GB26875InfoBody));
    memset(info_buff, 0, 1024);
    if(info_body == NULL || len < 1)
    {
        printf("info_body is NULL\r\n");
        return 0;
    }
    ret = GB26875_FormatInfoBody(info_body, len, info);      //  解析出信息单元数据结构
    if(!ret)
    {
        printf("GB26875_FormatInfoBody is NULL\r\n");
        return 0;
    }
    sys = (struct FireCtrlSystem *)malloc(info->obj_cnt * sizeof(struct FireCtrlSystem));
    unit = (struct FireCtrlUnit *)malloc(info->obj_cnt * sizeof(struct FireCtrlUnit));
    switch(info->type)
    {
    case GB26875_DOWNLOAD_SYS_STS:
        GB26875_FormatSysSts(info->data, info->data_len, info->obj_cnt, sys);
        info_len = UITD_QuerySysSts(sys, info->obj_cnt, info_buff);
        printf("UITD_QuerySysSts %d\r\n", info_len);
        break;
    case GB26875_DOWNLOAD_UNIT_STS:
        GB26875_FormatUnitSts((u8 *)info->data, info->data_len, info->obj_cnt, sys, unit);
        info_len = UITD_QueryUnitSts(unit, info->obj_cnt, info_buff);
        break;
    case GB26875_DOWNLOAD_UNIT_AD:
        GB26875_FormatUnitAd(info->data, info->data_len, info->obj_cnt, sys, unit);
        info_len = UITD_QueryUnitAd(unit, info->obj_cnt, info_buff);
        break;
    case GB26875_DOWNLOAD_SYS_OPT:
        user_cnt = GB26875_FormatSysOpt(info->data, info->data_len, sys, user_str);
        info_len = UITD_QuerySysOpt(sys, user_cnt, user_str, info_buff);
        break;
    case GB26875_DOWNLOAD_SYS_VER:
        GB26875_FormatSysVer(info->data, info->data_len, sys);
        info_len = UITD_QuerySysVer(sys, info_buff);
        break;
    case GB26875_DOWNLOAD_SYS_CFG:
        GB26875_FormatSysCfg(info->data, info->data_len, info->obj_cnt, sys);
        info_len = UITD_QuerySysCfg(sys, info->obj_cnt, info_buff);
        break;
    case GB26875_DOWNLOAD_UNIT_CFG:
        GB26875_FormatUnitCfg(info->data, info->data_len, info->obj_cnt, sys, unit);
        info_len = UITD_QueryUnitCfg(unit, info->obj_cnt, info_buff);
        break;
    case GB26875_DOWNLOAD_SYS_TIME:
        GB26875_FormatSysTime(info->data, info->data_len, sys);
        info_len = UITD_QuerySysTime(sys, info_buff);
        break;
    case GB26875_DOWNLOAD_DEV_STS:
        // GB26875_FormatDevSts(info->data, info->data_len);
        info_len = UITD_QueryDevSts(info_buff);
        break;
    case GB26875_DOWNLOAD_DEV_OPT:
        user_cnt = GB26875_FormatDevOpt(info->data, info->data_len, user_str);
        info_len = UITD_QueryDevOpt(user_cnt, user_str, info_buff);
        break;
    case GB26875_DOWNLOAD_DEV_VER:
        // GB26875_FormatDevVer(info->data, info->data_len);
        info_len = UITD_QueryDevVer(info_buff);
        break;
    case GB26875_DOWNLOAD_DEV_CFG:
        // GB26875_FormatDevCfg(info->data, info->data_len);
        info_len = UITD_QueryDevCfg(info_buff);
        break;
    case GB26875_DOWNLOAD_DEV_TIME:
        // GB26875_FormatDevTime(info->data, info->data_len);
        info_len = UITD_QueryDevTime(info_buff);
        break;
    default:
        break;
    }
    free(sys);
    free(unit);
    memcpy(result, info_buff, info_len);
    return info_len;
}

/**
 * 发送/确认模式下 控制中心回复确认消息
 *
 * @author WangXi (2020/9/1)
 *
 * @param no 业务流水号
 */
void __UITD_DownloadConfirm(u16 no)
{
#if UITD_AUTO_RELOAD
    void *entry;
    struct ReloadInfo *reload_info;
    if(Map_Lock(UITD_RELOADMAP, no, 0x400))
    {
        entry = Map_Get(UITD_RELOADMAP, no);  //  key可能会有变动
        if(entry != NULL)
        {
            // printf("业务流水号[%d] 确认回复 移出重传缓冲 \r\n", no);
            reload_info = (struct ReloadInfo *)entry;
            // printf("---------- 重传 应答释放 info[%x]->buff[%x] \r\n", reload_info, reload_info->buff);
            free(reload_info->buff);
            free(entry);
            Map_Remove(UITD_RELOADMAP, no);
        }
        else
        {
            // TODO 记录该流水号无缓存 错误类型
        }
        Map_Unlock(UITD_RELOADMAP, no);
    }
#endif
}

#if UITD_DEBUG_REPORT
bool_t UITD_DEBUG_PRINT_FLAG = false;
#endif
void UITD_DownloadThread(void)
{
    // 上游通讯数据缓冲
    char outstr[512];
    u8 recv_buff[0x420];
    int recv_len = 0;
    struct GB26875Info info_st;
    struct GB26875Info *info = &info_st;
    int ret = 0;

    u8 addr[6] = {0}; //  源地址与目标地址交换缓存

    memset(info,  0, sizeof(struct GB26875Info));
    memset(recv_buff, 0, 0x420);
    recv_len = UITD_Download(recv_buff, 5000);        //  读下行数据
    // printf("GB26875_RecvData Len(%d)\r\n", recv_len);
    if(recv_len <= 0)
    {
        // printf("warning: 下载数据长度为0\r\n");
        strcpy(outstr, "data_download_size_is_0");
        return;
    }
    g_UitdDownloadCnt ++;
    g_UitdDownloadSize += recv_len;
    ret = GB26875_FormatData(recv_buff, recv_len, info);     //  格式化数据为国标格式数据结构
    if(!ret)
    {
        printf("warning: 下载数据格式解析错误\r\n");
        strcpy(outstr, "data_download_format_error");

#if UITD_DEBUG_REPORT
        // FIXME 调试用
        if(UITD_DEBUG_PRINT_FLAG)
        {
            UITD_DEBUG_PRINT_FLAG = false;
            // __UITD_Status(NULL);
            UITD_ReportPush(outstr, UITD_GetTimestamp()/1000, Util_GetHeapSize());
        }
#endif
        return;
    }
    switch(info->opt)       //  筛选命令字节
    {
    case ControlComm_1_TimeSync:
        if(info->data_len >= 8)
        {
            if(info->data[0] == GB26875_SYNC_DEV_TIME)
            {
                strcpy(outstr, "time_sync_with_infobody");
                UITD_SyncTime(&(info->data[2]));
            }
            else if(info->data[0] == GB26875_CHECK_DEV)
            {
                // TODO 查岗 (void)(info->data[2]);
            }
        }
        else
        {
            info->time[5];
            UITD_SyncTime(info->time);
            strcpy(outstr, "time_sync");
        }
        break;
    case ControlComm_3_Confirm:
        __UITD_DownloadConfirm(info->no);
        strcpy(outstr, "flow_id_reply");
        break;
    case ControlComm_4_Request:     //  查询信息命令
        memset(recv_buff, 0, 0x420);        //  清零数据缓冲区
        info->opt = ControlComm_5_Respond;
        GB26875_FormatTimeFlag(info->time);
        memcpy(addr, info->src, 6);     //  //  交换源地址和目标地址
        memcpy(info->src, info->dct, 6);
        memcpy(info->dct, addr, 6);
        info->data_len = UITD_QueryInfo(info->data, info->data_len, info->data);        //  查询并组装信息单元数据
        // info->check_sum = GB26875_CheckSum(info);
        recv_len = GB26875_BuildData(info, recv_buff);      //  组装数据包
#if UITD_LOG
        log_queue_push(recv_buff,recv_len,LOG_UPDATE_MESSAGE,"NULL");
#endif
        printf("这是查询回复上传\r\n");
        UITD_Upload(recv_buff, recv_len, 0);        //  数据上行
        break;
    case 7:
#if UITD_AUTO_HEARTBEAT
        LAST_HB_TIME = UITD_GetTimestamp();
#endif
        break;
    default:
        strcpy(outstr, "other_cmd_byte");
        break;
    }

#if UITD_DEBUG_REPORT
    // FIXME 调试用
    if(UITD_DEBUG_PRINT_FLAG)
    {
        UITD_DEBUG_PRINT_FLAG = false;
        // __UITD_Status(NULL);
        UITD_ReportPush(outstr, UITD_GetTimestamp()/1000, Util_GetHeapSize());
    }
#endif
}

void UITD_PrintInfo(u8 type, struct FireCtrlInfo *info)
{
    int i;
    switch(type)
    {
    case UITD_DATA_TYPE_SYS_STS:
        printf("系统地址: %d, 系统状态: %d\r\n", info->sys->addr, info->sys->status);
        break;
    case UITD_DATA_TYPE_SYS_OPT:
        printf("系统地址: %d, 系统操作: %d, 系统操作员id: %d\r\n", info->sys->addr, info->sys->opt, info->sys->opt_id);
        break;
    case UITD_DATA_TYPE_SYS_VER:
        printf("系统地址: %d, 系统版本: %d, 系统次版本: %d\r\n", info->sys->addr, info->sys->version, info->sys->version_user);
        break;
    case UITD_DATA_TYPE_SYS_CFG:
        printf("系统地址: %d, 系统配置: %.*s\r\n", info->sys->addr, info->sys->cfg_len, info->sys->cfg);
        break;
    case UITD_DATA_TYPE_UNIT_STS:
        for(i=0; i<info->unit_size; i++)
        {
            printf("系统地址: %d, 部件地址: %d, 部件状态: %d\r\n", info->unit[i]->sys_addr, info->unit[i]->addr, info->unit[i]->status);
        }
        break;
    case UITD_DATA_TYPE_UNIT_CFG:
        break;
    case UITD_DATA_TYPE_UNIT_AD:
        for(i=0; i<info->unit_size; i++)
        {
            printf("系统地址: %d, 部件地址: %d, 部件模拟值类型: %d, 部件模拟值: %d\r\n", info->unit[i]->sys_addr, info->unit[i]->addr, info->unit[i]->ad_type, info->unit[i]->ad_val);
        }
        break;
    }
}

void UITD_SyncTime(u8 time[6])
{
    // TODO 时间同步
    extern void set_datetime(s32 year,s32 month,s32 day,s32 hour,s32 min,s32 sec);
    set_datetime(time[5]+2000, time[4], time[3], time[2], time[1], time[0]);
}

u32 UITD_CRC_U16_MODBUS(u8 *buff, u32 len)
{
	u16 tmp = 0xffff;
    u16 ret1 = 0;
	u32 i,n;
	for(n = 0; n < len; n++){/*此处的6 -- 要校验的位数为6个*/
        tmp = buff[n] ^ tmp;
        for(i = 0;i < 8;i++){  /*此处的8 -- 指每一个char类型又8bit，每bit都要处理*/
            if(tmp & 0x01){
                tmp = tmp >> 1;
                tmp = tmp ^ 0xa001;
            }   
            else{
                tmp = tmp >> 1;
            }   
        }   
    }
	return tmp;
}
#if !NANXIAO
/****************************************************************************
*   函数名称 : U16CheckCRC
*
*   返回类型 : u8
*
*   参数类型 : unsigned char* buf, int len
*----------------------------------------------------------------------------
*   功能概述 : CRC校验和计算
*
*   设计时间 : 2018-11-28
****************************************************************************/
int U16CheckCRC(char* buf, int len)
{
    unsigned int crc=0xffff;
    int i,j,k;
    for(i=0;i<len;i++)
    {
        crc =crc ^ buf[i];
        for(j=0;j<8;j++)
        {
            k=crc & 01;
            crc=crc >> 1;
            if (k==0) continue;
            crc =crc ^ 0xA001;
        }
    }
    return crc;
}
#endif

u8 UITD_U16CheckCRC(u8 *read_buf, u32 len)
{
    u8 ret = 0;
    u16 checkcrc = 0;

    //  数据长度校验
    if(len < 5)
    {
        return 0;
    }
    if(read_buf[2] <= len-5)
    {
        checkcrc = UITD_CRC_U16_MODBUS(read_buf, read_buf[2]+3);
        if(read_buf[read_buf[2]+3] == (checkcrc&0xff)
        && read_buf[read_buf[2]+4] == ((checkcrc>>8)&0xff))
        {
            ret = 1;
        }
    }
    return ret;
}

void UITD_Test(char *param)
{
    if(UIRD_test_i == 0)
    {
        UITD_AddSys(SysType_1_FireAlarm, 1);
        UITD_AddUnit(1, 11, 0x1AA);
        UITD_AddUnit(1, 21, 0x2AA);
        UITD_AddUnit(1, 31, 0x3AA);

        UITD_AddSys(SysType_0_Currency, 2);
        UITD_AddUnit(2, 12, 0x1BB);
        UITD_AddUnit(2, 22, 0x2BB);
        UITD_AddUnit(2, 32, 0x3BB);

        UITD_AddSys(SysType_10_FireLinkage, 3);
        UITD_AddUnit(3, 13, 0x1CC);
        UITD_AddUnit(3, 23, 0x2CC);
        UITD_AddUnit(3, 33, 0x3CC);
    }

    else if(UIRD_test_i % 3 == 0)
    {
        UITD_UpdateSysSts(1, GB26875_SYS_STS_FIRE_ALARM);
        UITD_UpdateSysOpt(1, GB26875_SYS_OPT_ALARM, 0);
        UITD_UpdateSysVer(1, 0xAA, 0xBB);
        UITD_UpdateSysCfg(1, "xxxxyyyyzzzz", 12);
    }

    else if(UIRD_test_i % 3 == 1)
    {
        UITD_UpdateUnitSts(2, 0x1BB, GB26875_UNIT_STS_FIRE_ALARM);
        UITD_UpdateUnitAd(2, 0x2BB, GB26875_UNIT_AD_TYPE_CNT, 0xCCCC);
        UITD_UpdateUnitCfg(2, 0x3BB, "uuuuvvvvwwww");
    }

    else if(UIRD_test_i % 3 == 2)
    {
        UITD_UpdateDevSts(GB26875_DEV_STS_FIRE_ALARM);
        UITD_UpdateDevOpt(GB26875_DEV_OPT_ALARM, 0xDD);
        UITD_UpdateDevVer(0xAA, 0xBB);
        UITD_UpdateDevCfg("eeeeffffgggg", 12);
    }

    UIRD_test_i++;
}

void UITD_TestUpload(u8 *param)
{
    UITD_UploadThread();
}


int UITD_PrintUploadQueue()
{
    Queue_Print(FC_UPLOAD_QUEUE);
    return 1;
}

#if UITD_AUTO_RELOAD
int UITD_PrintResendMap()
{
    // Map_Foreach(UITD_RELOADMAP, __UITD_PrintReload);   //  遍历重传数据表
    Map_Print(UITD_RELOADMAP);
    return 1;
}
#endif

void __UITD_Status(u8 *param)
{
    u64 data_size = 0;
    printf("===================\r\n");
    extern void print_nowDatetime();
    print_nowDatetime();
    printf("= 已经运行: %llu 秒\r\n\r\n", UITD_GetTimestamp()/1000);
    printf("= 第 %llu 次连接 \r\n", g_UitdConnCnt);
#if UITD_AUTO_UPLOAD
    printf("\r\n= 已上传数据 %llu 次\r\n", g_UitdUploadCnt);
    printf("= 已发送上传数据包 %llu b\r\n", g_UitdUploadSize);
    data_size += g_UitdUploadSize;
#endif

#if UITD_AUTO_RELOAD
    printf("\r\n= 已重传数据 %llu 次\r\n", g_UitdReloadCnt);
    printf("= 已发送重传数据包 %llu b\r\n\r\n", g_UitdReloadSize);
    data_size += g_UitdReloadSize;
#endif

#if UITD_AUTO_HEARTBEAT
    printf("= 已心跳 %llu 次\r\n", g_UitdHbCnt);
    printf("= 已发送心跳包 %llu b\r\n\r\n", g_UitdHbSize);
    data_size += g_UitdHbSize;
#endif
    printf("= 总发送数据包 %llu kb\r\n\r\n", data_size/1000);

    printf("= 已下载数据 %llu 次\r\n", g_UitdDownloadCnt);
    printf("= 总下载数据包 %llu kb\r\n\r\n", g_UitdDownloadSize/1000);

    printf("= 当前系统总动态内存 %lu \r\n", Util_GetHeapSize());

    printf("==================\r\n");
}

#if 1
#include "shell.h"
// ADD_TO_ROUTINE_SHELL(uitd, UITD_Test, "用户信息传输装置测试");
ADD_TO_ROUTINE_SHELL(uitd_up, UITD_TestUpload, "用户信息传输装置 上传测试");

ADD_TO_ROUTINE_SHELL(prtuploadqueue, UITD_PrintUploadQueue, "用户信息传输装置 重传缓冲打印");
#if UITD_AUTO_RELOAD
ADD_TO_ROUTINE_SHELL(prtresendmap, UITD_PrintResendMap, "用户信息传输装置 重传缓冲打印");
#endif

ADD_TO_ROUTINE_SHELL(uitd_status, __UITD_Status, "用户信息传输装置运行状态");
#endif
