#ifndef TRANS_DEV_H_
#define TRANS_DEV_H_

#include "stdint.h"
#include "../trans_dev/usr_info_trans_dev.h"
struct Est3GeneralInfo      // EST3一般性信息
{
    u8 info_content[4+1];            //  信息内容
    u8 time[8+1];                    //  时间
    u8 date[10+1];                   //  日期
    u8 plate_number[9+1];            //  盘号
    u8 card_number[5+1];             //  卡号
    u8 device_number[9+1];           //  器件号
    u8 device[8+1];                  //  设备号
};

struct Est3OperationInfo      // EST3控制面板操作信息
{
    u8 operation_info[10+1];            //  控制面板操作信息
    u8 time[8+1];                       //  时间
    u8 date[10+1];                      //  日期
    u8 operation_content[24+1];         //  操作内容
    u8 all_number[256+1];               //  全部器件号
    u8 device[8+1];                     //  设备号
};

enum {
    TRANS_GENERAL_INFORMATION = 0,   //一般性信息
    TRANS_CONTROL_INFORMATION,       //控制性信息
    TRANS_ERROR_INFORMATION,         //错误信息
};
/**
 *
 * 获取换行符的位置
 *
 * @param data 要处理的数据
 */
void Get_Wrap(u8 *data);

/**
 *
 * 获取两段数据分隔符的位置
 *
 * @param data   要处理的数据
 */
u32 Get_Spaces(u8 *data);
/**
 *
 * 获取冒后后的数据
 *
 * @param data   要处理的数据
 * @param result 处理返回的结果
 */
void Get_Colon(u8 *data,u8 *result);

/**
 *
 * 获取一般性信息的数据结构
 *
 * @param data   要处理的数据
 */
void Get_General_Info(u8 *data);

/**
 *
 * 获取一般性信息的部件状态
 *
 */
u16 Get_Unit_Sts(void);

/**
 *
 * 获取一般性信息的部件编号
 *
 */
u32 Get_Unit_Numb(void);

/**
 *
 * 获取控制面板操作信息的数据结构
 *
 * @param data   要处理的数据
 */
void Get_Operation_Info (u8 *data);

/**
 *
 * 获取控制面板操作信息的部件状态
 *
 */
u8 Get_Opt_Sts(void);

/**
 *
 * 获取控制面板操作信息的部件编号
 *
 */
u32 Get_Opt_Numb(void);

/**
 *
 * 设备控件初始化
 *
 */
void Trans_UITD_Init(void);


void SetProtocol(u32 serial,u8 protocol,struct SerialConfig trans_config);
void dev_protocol_init(u32 protocol0,u32 protocol1,u32 protocol2,u32 protocol3,u32 protocol4);
#endif /* TRANS_DEV_H_ */
