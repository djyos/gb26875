/*
 * queue.h
 *
 *  Created on: 2020年7月23日
 *      Author: WangXi
 */

#ifndef __MAP_H__
#define __MAP_H__

#include "stdint.h"

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

typedef uint32_t u32;

struct MapInfo;     //  Map结构

struct Entry;       //  Map中存放的值

/**
 * 创建一个Map
 * 返回一个Map指针
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 *
 * @return struct MapInfo* Map指针
 */
struct MapInfo * Map_New(void);

int Map_Lock(struct MapInfo *map, u32 key, u32 timeout);

void Map_Unlock(struct MapInfo *map, u32 key);

void Map_Foreach(struct MapInfo *map, void (*Map_ForeachHandler)(u32 key, void *val));

void Map_ForeachWithRemove(struct MapInfo *map, bool_t (*Map_ForeachHandler)(u32 key, void *val));
/**
 * 向Map里添加键值对
 *
 * @author WangXi (2020/7/31)
 *
 * @param map 目标Map指针
 * @param key 键
 * @param value 值
 */
int Map_Put(struct MapInfo *map, u32 key, void *value);

/**
 * 根据键查询目标Map中的值
 *
 * @author WangXi (2020/7/31)
 *
 * @param map 目标Map指针
 * @param key 键
 *
 * @return void* 值指针
 */
void *Map_Get(struct MapInfo *map, u32 key);

/**
 * 从目标Map中删除某个键值对
 *
 * @author WangXi (2020/7/31)
 *
 * @param map 目标Map针指
 * @param key 要删除的键
 */
void Map_Remove(struct MapInfo *map, u32 key);

/**
 * 清空目标Map
 *
 * @author WangXi (2020/7/31)
 *
 * @param map 目标Map指针
 */
void Map_Clear(struct MapInfo *map);

void Map_Print(struct MapInfo* map);
#endif // !__MAP_H__
