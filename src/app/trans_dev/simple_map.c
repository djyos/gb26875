/*
 * simple_map.c
 *
 *  Created on: 2020年7月23日
 *      Author: WangXi
 */

#include "stdlib.h"
#include "string.h"
#include "usr_info_trans_dev.h"
#if DJYOS
#include "../trans_dev/map.h"
#else
#include "map.h"
#endif

#define MAP_SIZE    1024

enum MapLock
{
    MAP_UNLOCK = 0,
    MAP_LOCK
};

struct MapInfo
{
    //todo: 改为动态分配
    struct Entry *array[MAP_SIZE];
    u32 index;   //  空位下标
};

struct Entry
{
    enum MapLock lock;
    u32 key;
    void *value;
};

struct MapInfo * Map_New(void)
{
    struct MapInfo *map = (struct MapInfo *)malloc(sizeof(struct MapInfo));
    memset((void *)(&map->array[0]), 0, MAP_SIZE * 4);
    map->index = 0;
    return map;
}

int __Map_Lock(struct Entry *e, u32 timeout)
{
    s64 timestamp = 0;
    timestamp = UITD_GetTimestamp();
    while(e != NULL && e->lock != MAP_UNLOCK)
    {
        UITD_Sleep(100);
        if(UITD_GetTimestamp() - timestamp >= timeout)
        {
            return false;
        }
    }
    if(e != NULL)
    {
        e->lock = MAP_LOCK;
    }
    return true;
}

/**
 * 对Map某元素加锁
 *
 * @author WangXi (2020/9/1)
 *
 * @param map 数据表
 * @param key 索引
 * @param timeout 加锁超时 单位毫秒
 *
 * @return int 上锁结果
 */
int Map_Lock(struct MapInfo *map, u32 key, u32 timeout)
{
	u32 i;
    struct Entry *e;
	if(map == NULL)
    {
        return false;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
       e = map->array[i];
       if(e == NULL)
       {
           continue;
       }
       if(e->key == key)
       {
           break;
       }
    }
    if(i == MAP_SIZE)
    {
        return false;
    }
   return __Map_Lock(e, timeout);
}

void __Map_Unlock(struct Entry *e)
{
    if(e != NULL)
    {
        e->lock = MAP_UNLOCK;
    }
}

void Map_Unlock(struct MapInfo *map, u32 key)
{
    u32 i;
    struct Entry *e;
	if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
       e = map->array[i];
       if(e == NULL)
       {
           continue;
       }
       if(e->key == key)
       {
           break;
       }
    }
    if(i == MAP_SIZE)
    {
        return;
    }
    __Map_Unlock(e);
}

void Map_Foreach(struct MapInfo *map, void (*Map_ForeachHandler)(u32 key, void *val))
{
    int i;
    struct Entry *e;
    if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
        e = map->array[i];
        if(__Map_Lock(e, 0x400))
        {
            e = map->array[i];
            if(e != NULL)
            {
                Map_ForeachHandler(e->key, e->value);
            }
            __Map_Unlock(e);
        }
    }
}

void Map_ForeachWithRemove(struct MapInfo *map, bool_t (*Map_ForeachHandler)(u32 key, void *val))
{
    int i;
    struct Entry *e;
    bool_t ret = false;
    if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
        ret = false;
        e = map->array[i];
        if(__Map_Lock(e, 0x400))
        {
            e = map->array[i];
            if(e != NULL)
            {
                ret = Map_ForeachHandler(e->key, e->value);
            }
            __Map_Unlock(e);
            if(ret)
            {
                // free(e->value);  // 由分配者释放
                e->value = NULL;
                // printf("---------- Map[%d] free [%x]\r\n", i, map->array[i]);
                free(map->array[i]);
                map->array[i] = NULL;
            }
        }
    }
}

int Map_Put(struct MapInfo *map, u32 key, void *value)
{
    int i;
    struct Entry *e = NULL;
    if(map == NULL)
    {
        return false;
    }
    // 检查key是否已存在
    for(i=0; i<MAP_SIZE; i++)
    {
        e = NULL;
        e = map->array[i];
        if(e == NULL)
        {
            continue;
        }
        if(e->key == key)
        {
            // e->value = value;
            return false;
        }
    }

    if(map->array[map->index] != NULL)
    {
        for(i=0; i<MAP_SIZE; i++)
        {
            e = NULL;
            e = map->array[i];
            if(e == NULL)
            {
                map->index = i;
                break;
            }
        }

        if(i == MAP_SIZE)
        {
            printf("Map[%x]已满\r\n", map);
            Map_Print(map);
            return false;
        }
    }
    //todo：注释说明在哪里释放内存
    map->array[map->index] = (struct Entry *)malloc(sizeof(struct Entry));
    // printf("+++++++++++ Map[%d] malloc [%x]\r\n", map->index, map->array[map->index]);
    map->array[map->index]->key = key;    map->array[map->index]->value = value;
    map->array[map->index]->lock = MAP_UNLOCK;
    // map->index ++;
    return true;
}

void *Map_Get(struct MapInfo *map, u32 key)
{
    int i;
    struct Entry *e;
    if(map == NULL)
    {
        return NULL;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
       e = map->array[i];
       if(e == NULL)
       {
           continue;
       }
       if(e->key == key)
       {
           return e->value;
       }
    }
    return NULL;
}

void Map_Remove(struct MapInfo *map, u32 key)
{
    int i;
    struct Entry *e = NULL;
    if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
        e = NULL;
       e = map->array[i];
       if(e == NULL)
       {
           continue;
       }
       if(e->key == key)
       {
           // free(e->value);  // 由分配者释放
           e->value = NULL;
           // printf("---------- Map[%d] free [%x]\r\n", i, map->array[i]);
           free(map->array[i]);           map->array[i] = NULL;
           return;
       }
    }
}

void Map_Clear(struct MapInfo *map)
{
    int i;
    struct Entry *e;
    if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
       e = map->array[i];
       if(e == NULL)
       {
           continue;
       }
       // free(e->value);  // 由分配者释放
       e->value = NULL;
       free(map->array[i]);
       map->array[i] = NULL;
    }
    map->index = 0;
}

void Map_Print(struct MapInfo* map)
{
    int i;
    struct Entry *e;
    if(map == NULL)
    {
        return;
    }
    for(i=0; i<MAP_SIZE; i++)
    {
        e = map->array[i];
        if(__Map_Lock(e, 0x400))
        {
            e = map->array[i];
            if(e != NULL)
            {
                printf("%02d):[0x%x]{%d, 0x%x}\r\n", i+1, e, e->key, e->value);
            }
            __Map_Unlock(e);
        }
    }
}
