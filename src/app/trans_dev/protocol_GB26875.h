
#ifndef PROTOCOL_GB26875_H
#define PROTOCOL_GB26875_H

#include "stdlib.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"

/* 预定义 */

#define NANXIAO 0

#if NANXIAO
#include "nanxiao.h"
#include "lincon.h"
#include "wenjian.h"
#define NANXIAO_MAC 1   //  MAC加密计算开关
#endif

#if DJYOS
#include "djyos.h"
#endif

/****************************************************************************
*   宏定义
****************************************************************************/

#define ControlComm_128_HeartBeat       (128)     /*  心跳                                         */

#if !NANXIAO    //  南消头文件宏定义重定义
#define ALARM_CHAR_MAX      (200)   /* 报警信息字符串不能大于200*/
#define ALARM_TIME_MAX      (40)    /* 固定值，不建议修改        */
#define ALARM_TYPE_MAX      (10)    /* 报警类型                 */
#define ALARM_NUM__MAX      (400)   /* 每种报警类型最大条数       */


#define CHECK_OK 0
#define CHECK_ERR -1


#define Return_OK           (0)     /* 函数运行正常，返回0       */
#define RETURN_TYPE_ERR0R   (-3)
#define RETURN_ADDR_ERROR   (-7)


static unsigned long mk[4] = {0xA1234567, 0xB1234567, 0xC1234567, 0xD1234567};



#define YEAR    (6)     /*  年                           */
#define MONTH   (5)     /*  月                           */
#define DAY     (4)     /*  天                           */
#define HOUR    (3)     /*  时                           */
#define MINUTE  (2)     /*  分                           */
#define SECOND  (1)     /*  秒                           */


/*  控制单元命令          Control Unit Command  */
#define ControlComm_0                   (0)     /*  预留                                          */
#define ControlComm_1_TimeSync          (1)     /*  时间同步                                        */
#define ControlComm_2_SendInfo          (2)     /*  发送火灾报警和建筑消防设施运行状态等信息        */
#define ControlComm_3_Confirm           (3)     /*  对控制命令和发送消息的确认回答                 */
#define ControlComm_4_Request           (4)     /*  查询火灾报警和建筑消防设施运行状态等信息        */
#define ControlComm_5_Respond           (5)     /*  返回查询信息                                  */
#define ControlComm_6_Repudiate         (6)     /*  对控制命令和发送信息的否认回答                 */
#define ControlComm_7_127               (0)     /*  预留                                          */
#define ControlComm_128_255             (0)     /*  自定义                                              */



/*  类型代码       type code   */
#define TypeCode_UP_0                   (0)     /*  预留                                          */
#define TypeCode_UP_1_SystemSts         (1)     /*  上行      上传建筑消防设施系统状态            */
#define TypeCode_UP_2_UnityRunSts       (2)     /*  上行      上传建筑消防设施部件运行状态      */
#define TypeCode_UP_3_AnalogValue       (3)     /*  上行      上传建筑消防设施部件模拟量值      */
#define TypeCode_UP_4_OperateInfo       (4)     /*  上行      上传建筑消防设施操作信息            */
#define TypeCode_UP_5_SWVersion         (5)     /*  上行      上传建筑消防设施软件版本            */
#define TypeCode_UP_6_SystemConfig      (6)     /*  上行      上传建筑消防设施系统配置情况      */
#define TypeCode_UP_7_UnityConfig       (7)     /*  上行      上传建筑消防设施部件配置情况      */
#define TypeCode_UP_8_SystemTime        (8)     /*  上行      上传建筑消防设施系统时间            */
#define TypeCode_UP_9_20                (0)     /*  预留                                          */
#define TypeCode_UP_21_J700RunSts       (21)    /*  上行      上传用户信息传输装置运行状态      */
#define TypeCode_UP_22                  (0)     /*  预留                                          */
#define TypeCode_UP_23                  (0)     /*  预留                                          */
#define TypeCode_UP_24_J700Operate      (24)    /*  上行      上传用户信息传输装置操作信息      */
#define TypeCode_UP_25_J700Version      (25)    /*  上行      上传用户信息传输装置软件版本      */
#define TypeCode_UP_26_J700Config       (26)    /*  上传      上传用户信息传输装置配置情况      */
#define TypeCode_UP_27                  (0)     /*  预留                                          */
#define TypeCode_UP_28_J700Time         (28)    /*  上行      上传用户信息传输装置系统时间      */
#define TypeCode_UP_29_40               (0)     /*  预留                                          */
#define TypeCode_UP_41_60               (0)     /*  预留                                          */

#define TypeCode_Down_61_SystemSts      (61)    /*  下行          读建筑消防设施系统状态             */
#define TypeCode_Down_62_UnityRunSts    (62)    /*  下行      读建筑消防设施部件运行状态           */
#define TypeCode_Down_63_AnalogValue    (63)    /*  下行      读建筑消防设施部件模拟量值           */
#define TypeCode_Down_64_OperateInfo    (64)    /*  下行      读建筑消防设施操作信息             */
#define TypeCode_Down_65_SWVersion      (65)    /*  下行      读建筑消防设施软件版本             */
#define TypeCode_Down_66_SystemConfig   (66)    /*  下行      读建筑消防设施系统配置情况           */
#define TypeCode_Down_67_UnityConfig    (67)    /*  下行      读建筑消防设施部件配置情况           */
#define TypeCode_Down_68_SystemTime     (68)    /*  下行      读建筑消防设施系统时间             */
#define TypeCode_Down_69_80             (0)     /*  预留                                          */
#define TypeCode_Down_81_J700RunSts     (81)    /*  下行      读用户信息传输装置运行状态           */
#define TypeCode_Down_82                (0)     /*  预留                                          */
#define TypeCode_Down_83                (0)     /*  预留                                          */
#define TypeCode_Down_84_J700Operate    (84)    /*  下行      读用户信息传输装置操作信息           */
#define TypeCode_Down_85_J700Version    (85)    /*  下行      读用户信息传输装置软件版本           */
#define TypeCode_Down_86_J700Config     (86)    /*  下行      读用户信息传输装置配置情况           */
#define TypeCode_Down_87                (0)     /*  预留                                          */
#define TypeCode_Down_88_J700Time       (88)    /*  下行      读用户信息传输装置系统时间           */
#define TypeCode_Down_89_InitJ700       (89)    /*  下行      初始化用户信息传输装置             */
#define TypeCode_Down_90_J700TimeSync   (90)    /*  下行      同步用户信息传输装置时钟            */
#define TypeCode_Down_91_CheckComm      (91)    /*  下行      查岗命令                            */
#define TypeCode_Down_92_127            (0)     /*  预留                                          */
/*  国网自定义 */
#define TypeCode_UP_128_EarlyWarn       (128)   /*  上行      上传消防设施预警信息              */
#define TypeCode_UP_129_Blackout        (129)   /*  上行      上传电力设备断电信息              */
#define TypeCode_UP_134_RemoteBoot      (134)   /*  上行      上传遥控应急启动灭火反馈            */
#define TypeCode_UP_136_BlackoutF       (136)   /*  上行      上传电力设备断电失效反馈            */

#define TypeCode_Down_130_EarlyWarn     (130)   /*  下行      读消防设施预警信息                   */
#define TypeCode_Down_131_Blackout      (131)   /*  下行      读电力设备断电信息                   */
#define TypeCode_Down_132_RemoteBoot    (132)   /*  下行      遥控应急启动灭火装置              */
#define TypeCode_Down_133_RemoteBootF   (133)   /*  下行      读遥控应急启动灭火反馈             */
#define TypeCode_Down_135_BlackoutInval (135)   /*  下行      电力设备断电失效确认              */
#define TypeCode_Down_137_BlackoutF     (137)   /*  下行      读电力设备断电失效反馈             */

#define TypeCode_138_254                (0)     /*  自定义                                         */


#define SysType_0_Currency              (0)     /*  通用                                          */
#define SysType_1_FireAlarm             (1)     /*  火灾报警控制器                                 */
#define SysType_2_9                     (0)     /*  预留                                          */
#define SysType_10_FireLinkage          (10)    /*  消防联动控制器                                 */
#define SysType_11_FireHydrant          (11)    /*  消火栓系统                                       */
#define SysType_12_AutomaticSprinkler   (12)    /*  自动喷水灭火系统                                */
#define SysType_13_GasOutfire           (13)    /*  气体灭火系统                                  */
#define SysType_14_WaterSpray           (14)    /*  水喷雾灭火系统(泵启动方式)                      */
#define SysType_15_                     (15)    /*  */
#define SysType_16_                     (16)    /*  */
#define SysType_17_                     (17)    /*  */
#define SysType_18_                     (18)    /*  */
#define SysType_19_                     (19)    /*  */
#define SysType_20_                     (20)    /*  */
#define SysType_21_                     (21)    /*  */
#define SysType_22_                     (22)    /*  */
#define SysType_23_                     (23)    /*  */
#define SysType_24_                     (24)    /*  */
#define SysType_25_127                  (0)     /*  */
#define SysType_128_                    (128)   /*  */
#define SysType_129_                    (129)   /*  */
#define SysType_130_                    (130)   /*  */
#define SysType_131_                    (131)   /*  */
#define SysType_132_                    (132)   /*  */
#define SysType_133_                    (133)   /*  */

#define SysType_133_255                 (0)     /*  */


typedef enum
{
    HJ = 0,
    GZ = 1,
    LD = 2,
    JG = 3,
    PB=4
} Alarm_type;

#endif  //  南消头文件宏定义重定义

/* 类型标志定义 */
#define GB26875_UPLOAD_SYS_STS            1                   // 上传建筑消防设施系统状态
#define GB26875_UPLOAD_UNIT_STS           2                   // 上传建筑消防设施部件状态
#define GB26875_UPLOAD_UNIT_AD            3                   // 上传建筑消防设施部件模拟量
#define GB26875_UPLOAD_SYS_OPT            4                   // 上传建筑消防设施操作信息
#define GB26875_UPLOAD_SYS_VER            5                   // 上传建筑消防设施软件版本
#define GB26875_UPLOAD_SYS_CFG            6                   // 上传建筑消防设施配置情况
#define GB26875_UPLOAD_UNIT_CFG           7                   // 上传建筑消防设施部件配置情况
#define GB26875_UPLOAD_SYS_TIME           8                   // 上传建筑消防设施系统时间
#define GB26875_UPLOAD_FC_OTHER           9                   // (9-20)预留
#define GB26875_UPLOAD_DEV_STS            21                  // 上传用户信息传输装置运行状态
#define GB26875_UPLOAD_DEV_OPT            24                  // 上传用户信息传输装置操作信息
#define GB26875_UPLOAD_DEV_VER            25                  // 上传用户信息传输装置软件版本
#define GB26875_UPLOAD_DEV_CFG            26                  // 上传用户信息传输装置配置情况
#define GB26875_UPLOAD_DEV_TIME           28                  // 上传用户信息传输装置系统时间
#define GB26875_DOWNLOAD_SYS_STS          61                  // 读取建筑消防设施系统状态
#define GB26875_DOWNLOAD_UNIT_STS         62                  // 读取建筑消防设施部件状态
#define GB26875_DOWNLOAD_UNIT_AD          63                  // 读取建筑消防设施部件模拟量
#define GB26875_DOWNLOAD_SYS_OPT          64                  // 读取建筑消防设施操作信息
#define GB26875_DOWNLOAD_SYS_VER          65                  // 读取建筑消防设施软件版本
#define GB26875_DOWNLOAD_SYS_CFG          66                  // 读取建筑消防设施配置情况
#define GB26875_DOWNLOAD_UNIT_CFG         67                  // 读取建筑消防设施部件配置情况
#define GB26875_DOWNLOAD_SYS_TIME         68                  // 读取建筑消防设施系统时间
#define GB26875_DOWNLOAD_FC_OTHER         69                  // (9-20)预留
#define GB26875_DOWNLOAD_DEV_STS          81                  // 读取用户信息传输装置运行状态
#define GB26875_DOWNLOAD_DEV_OPT          84                  // 读取用户信息传输装置操作信息
#define GB26875_DOWNLOAD_DEV_VER          85                  // 读取用户信息传输装置软件版本
#define GB26875_DOWNLOAD_DEV_CFG          86                  // 读取用户信息传输装置配置情况
#define GB26875_DOWNLOAD_DEV_TIME         88                  // 读取用户信息传输装置系统时间
#define GB26875_INIT_DEV                  89                  // 初始化用户信息传输装置
#define GB26875_SYNC_DEV_TIME             90                  // 同步用户信息传输装置系统时间
#define GB26875_CHECK_DEV                 91                  // 查岗

/* 消防设施状态类型 */
#define GB26875_SYS_STS_NORMAL            1<<0                //  正常
#define GB26875_SYS_STS_FIRE_ALARM        1<<1                //  火警
#define GB26875_SYS_STS_ERROR             1<<2                //  故障
#define GB26875_SYS_STS_SHIELD            1<<3                //  屏蔽
#define GB26875_SYS_STS_WTC               1<<4                //  监管
#define GB26875_SYS_STS_START             1<<5                //  启动
#define GB26875_SYS_STS_CB                1<<6                //  反馈
#define GB26875_SYS_STS_DELAY             1<<7                //  延时
#define GB26875_SYS_STS_DC_ERROR          1<<8                //  主电故障
#define GB26875_SYS_STS_DC2_ERROR         1<<9                //  备电故障
#define GB26875_SYS_STS_BUS_ERROR         1<<10               //  总线故障
#define GB26875_SYS_STS_MANUAL            1<<11               //  手动状态
#define GB26875_SYS_STS_CFG_CHANGE        1<<12               //  配置改变
#define GB26875_SYS_STS_RESET             1<<13               //  复位
#define GB26875_SYS_STS_OTHER             0                   //  预留

/* 消防设置部件状态类型 */
#define GB26875_UNIT_STS_NORMAL            1<<0                //  正常
#define GB26875_UNIT_STS_FIRE_ALARM        1<<1                //  火警
#define GB26875_UNIT_STS_ERROR             1<<2                //  故障
#define GB26875_UNIT_STS_SHIELD            1<<3                //  屏蔽
#define GB26875_UNIT_STS_WTC               1<<4                //  监管
#define GB26875_UNIT_STS_START             1<<5                //  启动
#define GB26875_UNIT_STS_CB                1<<6                //  反馈
#define GB26875_UNIT_STS_DELAY             1<<7                //  延时
#define GB26875_UNIT_STS_DC_ERROR          1<<8                //  电源故障
#define GB26875_UNIT_STS_OTHER             0                   //  预留

/* 消防设施操作类型 */
#define GB26875_SYS_OPT_RESET              1<<0                //  复位
#define GB26875_SYS_OPT_SILIENCE           1<<1                //  消音
#define GB26875_SYS_OPT_ALARM              1<<2                //  手动报警
#define GB26875_SYS_OPT_NO_ALARM           1<<3                //  报警消除
#define GB26875_SYS_OPT_CHECK              1<<4                //  自检
#define GB26875_SYS_OPT_CONFIRM            1<<5                //  确认
#define GB26875_SYS_OPT_TEST               1<<6                //  测试
#define GB26875_SYS_OPT_OTHER              0<<7                //  预留

/* 消防设施部件模拟量值类型 */
#define GB26875_UNIT_AD_TYPE               0                   //  未用
#define GB26875_UNIT_AD_TYPE_CNT           1                   //  事件计数
#define GB26875_UNIT_AD_TYPE_HIGHT         2                   //  高度
#define GB26875_UNIT_AD_TYPE_TEMP          3                   //  温度
#define GB26875_UNIT_AD_TYPE_PRES_M        4                   //  压力兆帕
#define GB26875_UNIT_AD_TYPE_PRES_K        5                   //  压力千帕
#define GB26875_UNIT_AD_TYPE_GAS           6                   //  气体浓度
#define GB26875_UNIT_AD_TYPE_SEC           7                   //  事件
#define GB26875_UNIT_AD_TYPE_VOLTAGE       8                   //  电压
#define GB26875_UNIT_AD_TYPE_ELE           9                   //  电流


/* 用户信息传输装置状态类型 */
#define GB26875_DEV_STS_NORMAL            1<<0                //  正常
#define GB26875_DEV_STS_FIRE_ALARM        1<<1                //  火警
#define GB26875_DEV_STS_ERROR             1<<2                //  故障
#define GB26875_DEV_STS_DC_ERROR          1<<3                //  主电故障
#define GB26875_DEV_STS_DC2_ERROR         1<<4                //  备电故障
#define GB26875_DEV_STS_WTC_ERROR         1<<5                //  监控中心故障
#define GB26875_DEV_STS_CON_ERROR         1<<6                //  连接故障
#define GB26875_DEV_STS_OTHER             0                   //  预留

/* 用户信息传输装置操作类型 */
#define GB26875_DEV_OPT_RESET              1<<0                //  复位
#define GB26875_DEV_OPT_SILIENCE           1<<1                //  消音
#define GB26875_DEV_OPT_ALARM              1<<2                //  手动报警
#define GB26875_DEV_OPT_NO_ALARM           1<<3                //  报警消除
#define GB26875_DEV_OPT_CHECK              1<<4                //  自检
#define GB26875_DEV_OPT_REPLY              1<<5                //  查岗应答
#define GB26875_DEV_OPT_TEST               1<<6                //  测试
#define GB26875_DEV_OPT_OTHER              0<<7                //  预留

#define GB26875_OBJLEN_SYS_STS             4                    //  系统状态类信息对象数据包长度
#define GB26875_OBJLEN_SYS_OPT             4                    //  系统操作类信息对象数据包长度
#define GB26875_OBJLEN_SYS_VER             4                    //  系统版本类信息对象数据包长度
#define GB26875_OBJLEN_SYS_CFG             0xFF+3               //  系统配置类信息对象数据包长度
#define GB26875_OBJLEN_UNIT_STS            40                   //  部件状态类信息对象数据包长度
#define GB26875_OBJLEN_UNIT_AD             10                   //  部件模拟量类信息对象数据包长度
#define GB26875_OBJLEN_UNIT_CFG            38                   //  部件说明类信息对象数据包长度
#define GB26875_OBJLEN_DEV_STS             1                    //  装置状态类信息对象数据包长度
#define GB26875_OBJLEN_DEV_OPT             2                    //  装置操作类信息对象数据包长度
#define GB26875_OBJLEN_DEV_VER             2                    //  装置版本类信息对象数据包长度
#define GB26875_OBJLEN_DEV_CFG             0xFF+1               //  装置配置类信息对象数据包长度
/* 预定义结束 */

/****************************************************************************
*   结构定义
****************************************************************************/

struct GB26875Info          //  国标数据结构
{
    u8 time[6];           //  时间标志
    u8 src[6];            //  源地址
    u8 dct[6];            //  目标地址
    u8 opt;                 //  命令字节
    u8 check_sum;           //  校验和
    u16 no;                 //  业务流水号
    u16 version;            //  协议版本号
    u16 data_len;           //  信息数据单元长度
    u8 data[1024];        //  信息数据单元数据
};

struct GB26875InfoBody      //  国标数据信息数据单元数据结构
{
    u8 type;                //  数据单元类型标志
    u8 obj_cnt;             //  单元对象数目
    u16 data_len;           //  单元对象数据长度
    u8 data[1024];        //  单元对象数据
};

/**
 * 261字节
 */
struct UserInfoTransDev     //  用户信息传输装置数据结构
{
    u8 status;              //  装置状态
    u8 opt;                 //  操作标志类型
    u8 opt_id;              //  操作员id
    u8 version;             //  主版本号
    u8 version_user;        //  用户版本号
    u8 cfg_len;             //  配置情况长度
    u8 cfg[0xFF];         //  配置情况
};

/**
 * 270字节
 */
struct FireCtrlSystem       //  消防设施系统数据结构
{
    u8 time[6];
    u8 type;                //  类型标志
    u8 addr;                //  地址
    u8 opt;                 //  操作类型
    u8 opt_id;              //  操作员id
    u8 version;             //  主版本号
    u8 version_user;        //  次版本号
    u8 cfg_len;             //  配置长度
    u16 status;             //  状态
    u8 cfg[0xFF];         //  配置情况
};

/**
 * 42字节
 */
struct FireCtrlUnit         //  消防设施部件数据结构
{
    u8 type;                //  部件类型
    u8 sys_addr;            //  所属系统地址
    u8 ad_type;             //  模拟量类型
    u16 ad_val;             //  模拟量值
    u16 status;             //  部件状态
    u8 dsc[31];           //  部件说明
    u32 addr;               //  部件地址
};

/****************************************************************************
*   功能定义
****************************************************************************/

/**
 * 构建国标数据包
 *
 * @author WangXi (2020/7/31)
 *
 * @param info 国标数据结构指针
 * @param send_buf 输出国标格式数据
 *
 * @return int 返回数据长度
 */
int GB26875_BuildData(struct GB26875Info *info, u8 *send_buf);

/**
 * 构建消防建筑设施系统状态信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildSysSts(struct FireCtrlSystem *sys, u8 *str);

/**
 * 构建消防建筑设施系统操作记录信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildSysOpt(struct FireCtrlSystem *sys, u8 *str);

/**
 * 构建消防建筑设施系统版本号信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildSysVer(struct FireCtrlSystem *sys, u8 *str);

/**
 * 构建消防建筑设施系配置情况信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildSysCfg(struct FireCtrlSystem *sys, u8 *str);

/**
 * 构建消防建筑设施系时间信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildSysTime(struct FireCtrlSystem *sys, u8 *str);

/**
 * 构建消防建筑设施部件状态信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param unit 消防建筑设施部件数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildUnitSts(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str);

/**
 * 构建消防建筑设施部件模拟量值信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param unit 消防建筑设施部件数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildUnitAd(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str);

/**
 * 构建消防建筑设施部件配置说明信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数据结构指针
 * @param unit 消防建筑设施部件数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildUnitCfg(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str);


/**
 * 构建用户信息传输装置状态信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param dev 用户信息传输装置数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildDevSts(struct UserInfoTransDev *dev, u8 *str);

/**
 * 构建用户信息传输装置操作记录信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param dev 用户信息传输装置数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildDevOpt(struct UserInfoTransDev *dev, u8 *str);

/**
 * 构建用户信息传输装置版本号信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param dev 用户信息传输装置数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildDevVer(struct UserInfoTransDev *dev, u8 *str);

/**
 * 构建用户信息传输装置配置情况信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param dev 用户信息传输装置数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildDevCfg(struct UserInfoTransDev *dev, u8 *str);

/**
 * 构建用户信息传输装置时间信息对象
 *
 * @author WangXi (2020/7/31)
 *
 * @param dev 用户信息传输装置数据结构指针
 * @param str 输出信息对象字符串
 *
 * @return int 返回输出字符串长度
 */
int GB26875_BuildDevTime(u8 *str);

/**
 * 国标协议校验和计算方法
 *
 * @author WangXi (2020/7/31)
 *
 * @param buf 源数据
 * @param firstbyte 开始计算的位置
 * @param end 终止计算的位置
 *
 * @return u8 校验和
 */
u8 GB26875_CheckSum(u8 *buf, int firstbyte, int end);

/**
 * 国标协议获取格式化的时间标签字符串
 *
 * @author WangXi (2020/7/31)
 *
 * @param time 时间标签字符串
 */
void GB26875_FormatTimeFlag(u8 time[6]);

/**
 * 解析国标协议格式的数据为国标数据结构
 *
 * @author Wang Xi
 *
 * @param data 输入数据
 * @param len 输入长度
 *
 * @return struct GB26875InfoBody* 国标数据结构指针
 */
int GB26875_FormatData(u8 *data, int len, struct GB26875Info *recv_info);

/**
 * 解析国标协议中信息数据单元格式的数据为国标数据单元结构
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param len 输入长度
 *
 * @return struct GB26875InfoBody* 国标数据单元结构指针
 */
int GB26875_FormatInfoBody(u8 *data, int len, struct GB26875InfoBody *recv_info_body);

/**
 * 解析国标数据单元中的系统状态类单元对象数据为消防建筑设施系统数据结构
 * 提取下行数据中所需系统索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param obj_cnt 单元对象数目
 * @param sys 输出数据结构数组
 */
void GB26875_FormatSysSts(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys);

/**
 * 解析国标数据单元中的系统操作记录类单元对象数据为消防建筑设施系统数据结构
 * 提取下行数据中所需系统索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param sys 输出数据结构指针
 * @param start_time 操作记录起始查询时间
 *
 * @return u8 返回记录数目
 */
u8 GB26875_FormatSysOpt(u8 *data, int data_len, struct FireCtrlSystem *sys, u8 start_time[6]);

/**
 * 解析国标数据单元中的系统版本号类单元对象数据为消防建筑设施系统数据结构
 * 提取下行数据中所需系统索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param sys 输出数据结构指针
 */
void GB26875_FormatSysVer(u8 *data, int data_len, struct FireCtrlSystem *sys);

/**
 * 解析国标数据单元中的系统配置情况类单元对象数据为消防建筑设施系统数据结构
 * 提取下行数据中所需系统索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param obj_cnt 单元对象数目
 * @param sys 输出数据结构数组
 */
void GB26875_FormatSysCfg(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys);

/**
 * 解析国标数据单元中的系统时间类单元对象数据为消防建筑设施系统数据结构
 * 提取下行数据中所需系统索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param sys 输出数据结构指针
 */
void GB26875_FormatSysTime(u8 *data, int data_len, struct FireCtrlSystem *sys);

/**
 * 解析国标数据单元中的部件状态类单元对象数据为消防建筑设施部件数据结构
 * 提取下行数据中所需系统及部件索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param obj_cnt 单元对象数目
 * @param sys 输出数据结构数组
 * @param unit 输出数据结构数组
 */
void GB26875_FormatUnitSts(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit);

/**
 * 解析国标数据单元中的部件模拟量值类单元对象数据为消防建筑设施部件数据结构
 * 提取下行数据中所需系统及部件索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param obj_cnt 单元对象数目
 * @param sys 输出数据结构数组
 * @param unit 输出数据结构数组
 */
void GB26875_FormatUnitAd(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit);

/**
 * 解析国标数据单元中的部件配置说明类单元对象数据为消防建筑设施部件数据结构
 * 提取下行数据中所需系统及部件索引信息
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 输入数据
 * @param data_len 输入长度
 * @param obj_cnt 单元对象数目
 * @param sys 输出数据结构数组
 * @param unit 输出数据结构数组
 */
void GB26875_FormatUnitCfg(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit);

u8 GB26875_FormatDevSts(u8 *data, int data_len);
u8 GB26875_FormatDevOpt(u8 *data, int data_len, u8 start_time[6]);
u8 GB26875_FormatDevVer(u8 *data, int data_len);
u8 GB26875_FormatDevCfg(u8 *data, int data_len);
u8 GB26875_FormatDevTime(u8 *data, int data_len);


#endif
