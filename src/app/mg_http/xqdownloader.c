/*
 * xqdownloader.c
 *
 *  Created on: May 21, 2020
 *      Author: WangXi
 */

#include <dbug.h>
#include <stdio.h>
#include <lock.h>
#include "xqdownloader.h"
struct MutexLCB *xq_download_lock = NULL; // 下载业务锁

u16 xq_download_event_id = 0;


void XqLocal_SaveFile(const char *file_name, char *file_data, int file_size)
{
    remove(file_name);

    FILE *file = NULL;
    int file_open_over_time = 0;
    while(file_open_over_time < 10)
    {
        file = fopen(file_name, "wb+");
        if(file == NULL)
        {
//            debug_printf(XQ_LOCAL, "[写时]找不到该文件.[%s]", file_name);
            DJY_EventDelay(1000*1000);
            // ModuleInstall_SDCARD();
        }
        else break;
        file_open_over_time++;
    }
    // open file error
    if(file == NULL || file_open_over_time >= 10)
    {
//        debug_printf(XQ_LOCAL, "打开文件 <%s> 失败!", file_name);
        return;
    }

    int write_size = 0;
    while(write_size < file_size)
    {
        size_t w_s = fwrite(file_data + write_size, 1, file_size - write_size, file);
        write_size += w_s;
    }
    fflush(file);
    if(file)
    {
        fclose(file);
    }
}

bool_t __XqDownloader_DownloadFile(const char *file_url, const char *file_name)
{
   const char XQ_UPDATE_HEADER[] =
           "Accept: */*\r\n"
           "Connection: keep-alive\r\n"
           "User-Agent: Mongoose/6.15\r\n\r\n"; // header

   struct http_message *resp = MG_HttpRequestOpt(MG_HTTP_METHOD_GET, file_url, NULL, NULL, 30);

   if(resp->resp_code == 0xFFFF)
   {
       debug_printf(XQ_DOWNLOADER, "请求失败!!");
       M_Free((void *)resp);
       return false;;
   }

   if(resp == NULL || resp->body.len == 0)
   {
       debug_printf(XQ_DOWNLOADER, "http返回消息为空!!\r\n");
       M_Free((void *)resp);
       return false;;
   }
   if(resp->body.len != 0)
   {
       XqLocal_SaveFile(file_name, resp->body.p, (int)resp->body.len);

       // XqLog_OsLog("xq_env", "下载完毕");

       debug_printf(XQ_DOWNLOADER, "文件\"%s\"下载完毕", file_name);

   }
   M_Free((void *)resp);
   return true;
}

void __XqDownloader_DownloadFileEvent()
{
    struct XqDownloaderSession *session;

    DJY_GetEventPara((ptu32_t*)&session, NULL);

    if(session == NULL)
    {
        return;
    }

    debug_printf(XQ_DOWNLOADER, "开始下载事件:[从:\"%s\"下载到\"%s\"]", session->file_url, session->file_name);

    if(__XqDownloader_DownloadFile(session->file_url, session->file_name))
    {
        session->process = 100;
    }
    else
    {
        session->process = -1;
    }
    Lock_MutexPost(xq_download_lock);
    if(session->handler != NULL)
    {
        session->handler(session);
    }
}

void XqDownloader_DownloadFileBase(struct XqDownloaderSession *session)
{
    if(xq_download_lock == NULL)
    {// 初始化下载业务锁
        xq_download_lock = Lock_MutexCreate(NULL);
    }
/*
    if(xq_download_event_id == 0)
    {
        xq_download_event_id = DJY_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS-2, 0, 0,\
                __XqDownloader_DownloadFileEvent, NULL, 0x1800, "xq_download");
       DJY_EventDelay(100 * mS);
    }
    if(xq_download_event_id != 0 && xq_download_event_id != CN_EVENT_ID_INVALID)
    {
        Lock_MutexPend(xq_download_lock, CN_TIMEOUT_FOREVER);
        session->process = 0;
        DJY_EventPop(xq_download_event_id, 0, 0, session, NULL, 0);
    }
    else
    {
        session->process = -1;
        debug_printf(XQ_DOWNLOADER, "弹出下载事件失败!");
    }
    */
    Lock_MutexPend(xq_download_lock, CN_TIMEOUT_FOREVER);
    session->process = 0;
    debug_printf(XQ_DOWNLOADER, "开始下载事件:[从:\"%s\"下载到\"%s\"]", session->file_url, session->file_name);

    if(__XqDownloader_DownloadFile(session->file_url, session->file_name))
    {
       session->process = 100;
    }
    else
    {
       printf("下载失败\r\n");
       session->process = -1;

    }
    Lock_MutexPost(xq_download_lock);
    if(session->handler != NULL)
    {
       session->handler(session);
    }
}

void __XqDownloader_FreeSession(struct XqDownloaderSession *session)
{
    if(session)
    {
        M_Free((void*)session);
    }
}

void XqDownloader_DownloadFile(const char *file_url, const char *file_name, XqDownloader_Handler handler)
{
    struct XqDownloaderSession *session = (struct XqDownloaderSession *)malloc(sizeof(struct XqDownloaderSession));
    session->file_name = file_name;
    session->file_url = file_url;
    if(handler == NULL)
    {
        session->handler = __XqDownloader_FreeSession;
    }
    else
    {
        session->handler = handler;
    }
    XqDownloader_DownloadFileBase(session);
}


void __XqDownloader_Shell(char *param)
{
   int argc = 2;
   char *argv[2];

   string2arg(&argc, argv, param);

   if(argc < 2)
       return;

   char file_name[64];

   sprintf(file_name, "/yaf2/%s", argv[1]);
   XqDownloader_DownloadFile(argv[0], file_name, NULL);
}

#include <shell.h>
ADD_TO_ROUTINE_SHELL(app_download, __XqDownloader_Shell,"下载文件,+ url, + file");
