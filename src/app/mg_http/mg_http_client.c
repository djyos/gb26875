/*
 * mg_http_client.c
 *
 *  Created on: May 18, 2020
 *      Author: WangXi
 */

#include "mg_http_client.h"
#include "mongoose.h"

#include <stdlib.h>
#include <stdarg.h>
#include <osarch.h>

struct MgHttpSession
{
    struct MgHttpRequest  *request;
    struct http_message *response;
    bool_t exit_flag;
};

static void __MG_HttpHandler(struct mg_connection *nc, int ev, void *ev_data)
{
  struct http_message *hm = (struct http_message *) ev_data;
  struct MgHttpSession *mg_session = (struct MgHttpSession *)nc->user_data;

  switch (ev) {
    case MG_EV_CONNECT:
      if (*(int *) ev_data != 0)
      {
        fprintf(stderr, "http connect() failed: %s\r\n", strerror(*(int *) ev_data));
        mg_session->exit_flag = true;
      }
      else
      {
          // todo http connect success
      }
      break;
    case MG_EV_HTTP_REPLY:
      nc->flags |= MG_F_CLOSE_IMMEDIATELY;
      memcpy(mg_session->response, hm, sizeof(struct http_message));
      mg_session->exit_flag = true;
      break;
    case MG_EV_CLOSE:
      if (!mg_session->exit_flag) {
        printf("Server closed connection\n");
        mg_session->exit_flag = true;
      }
      break;
    default:
      break;
  }
}

void __MG_HttpProcess(struct MgHttpRequest *request,\
                      struct http_message *response,\
                      u64 timeout)
{
    struct MgHttpSession mg_session;
    mg_session.request = request;
    mg_session.response = response;

    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);

    mg_session.exit_flag = false;

    struct mg_connection *mg_nc = mg_connect_http(&mgr, __MG_HttpHandler,\
            request->url, request->headers, request->data);
    if(mg_nc == NULL)
    {
        mg_mgr_free(&mgr);
        response->resp_code = MG_HTTP_STATUS_ERROR;
        printf("\r\nmg_http: mg connect error!");
        return;
    }

    mg_nc->user_data = &mg_session;

    u64 ts_start = (u64)mg_time(); // FIXME 时间单位改为毫秒更有意义
    while (!mg_session.exit_flag)
    {
        u64 ts_curr = (u64)mg_time();
        if(ts_curr - ts_start > timeout)
        {
            printf("\r\nmg_http: poll over time!");
            mg_nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            mg_mgr_poll(&mgr, 100);
            break;
        }
        mg_mgr_poll(&mgr, 100);
    }
    mg_mgr_free(&mgr);
}

struct http_message *MG_HttpRequestBase(struct MgHttpRequest *request, u64 timeout)
{
    struct http_message *response = NULL;// http响应体, 需调用者手动释放
    response = (struct http_message *)malloc(sizeof(struct http_message));
    memset(response, 0, sizeof(struct http_message));
    response->resp_code = MG_HTTP_STATUS_ERROR;
    request->response = response;

    if(request->url == NULL || strlen(request->url) == 0)
    {
        return response;
    }

    __MG_HttpProcess(request, response, timeout);

    return response;
}


struct http_message *MG_HttpRequestOpt(const char *method, const char *url, char *param, char *data, u64 timeout)
{
    struct MgHttpRequest *request = NULL;// http请求体
    request = (struct MgHttpRequest *)malloc(sizeof(struct MgHttpRequest));
    memset(request, 0, sizeof(struct MgHttpRequest));

    request->url = url;

    if(method == NULL || strlen(method) == 0)
    {
        request->method = MG_HTTP_METHOD_GET;
    }
    else
    {
        request->method = method;
    }

    request->params = param;
    request->data = data;

    const char XQ_UPDATE_HEADER[] =
               "Accept: */*\r\n"
               "Connection: keep-alive\r\n"
               "User-Agent: Mongoose/6.15\r\n\r\n"; // header
    request->headers = XQ_UPDATE_HEADER;

    struct http_message *response = MG_HttpRequestBase(request, timeout);

    M_Free((void *)request);

    return response;
}

struct http_message *MG_HttpRequest(const char *method, const char *url, char *param, char *data)
{
    return MG_HttpRequestOpt(method, url, param, data, 5);
}

void __MG_HttpShell(char *param)
{
   int argc = 5;
   char *argv[5];

   string2arg(&argc, argv, param);

   if(argc < 2)
       return;
   struct http_message *response = NULL;
   switch(argc)
   {
   case 2:
       argv[2] = NULL;
       argv[3] = NULL;
       break;
   case 3:
       argv[3] = NULL;
       break;
   case 4:
       break;
   default:
       break;
   }
   response = MG_HttpRequest(argv[0], argv[1], argv[2], argv[3]);
   // 最多打印100个字节
   if(response->body.len > 100)
   {
       response->body.len = 100;
   }
   printf("\r\nresp code:[%d]\r\nresp body:[%.*s]",\
           response->resp_code, response->body.len, response->body.p);

   M_Free((void *)response);
}

#include <shell.h>
ADD_TO_ROUTINE_SHELL(mghttp, __MG_HttpShell,"mongoose http,+ 请求方法,+ url,+ 参数");
