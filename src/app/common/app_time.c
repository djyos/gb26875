/*
 * app_time.c
 *
 *  Created on: 2020年12月18日
 *      Author: WangXi
 */
#include <time.h>
#include <stdio.h>
#include "app_flash.h"
#include "../mg_http/mg_http_client.h"
#include <shell.h>

#define STR_FMT_TIME_SYNC_INFO         "time_type=%s,server_time=%d,last_local_timestamp=%s"

const char TIME_MONTHS[12][3] = {
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
};

/**
 * 根据月份首三位字母返回月数
 */
u16 Time_Mon2Int(const char mon[])
{
    for(int i=0; i<12; i++)
    {
        char m[4];
        memcpy(m, TIME_MONTHS[i], 3);
        m[3] = '\0';
        s32 ss = strcmp(mon, m);
        if(ss == 0)
            return i+1;
    }
    return 0xFFFF;
}

bool_t Time_InfoSave(struct TimeInfoSt *time_info_in)
{
    char str_time_info[256] = {0};
    s32 time_info_len = 0;
    if(time_info_in == NULL)
        return false;
    time_info_len = snprintf(str_time_info, 256, STR_FMT_TIME_SYNC_INFO,\
            time_info_in->time_type, time_info_in->server_time,\
            time_info_in->last_local_timestamp);
    if(time_info_len <= 0)
        return false;
    return File_SetNameValueFs(CFG_TIME_INFO_FILE_NAME, str_time_info, time_info_len);
}

bool_t Time_InfoLoad(struct TimeInfoSt *time_info_out)
{
    char str_time_info[256] = {0};
    s32 time_info_len = 0;
    s32 ret = 0;
    if(time_info_out == NULL)
        return false;
    time_info_len = File_GetNameValueFs(CFG_TIME_INFO_FILE_NAME, str_time_info, 256);
    if(time_info_len <= 0)
        return false;
    ret = sscanf(str_time_info, STR_FMT_TIME_SYNC_INFO,\
            time_info_out->time_type, time_info_out->server_time,\
            time_info_out->last_local_timestamp);
    return ret == 3;
}

bool_t Time_InfoErase()
{
    return File_RmNameValueFs(CFG_TIME_INFO_FILE_NAME);
}

/**
 * 基于mg http获取gmt时间
 */
bool_t Time_NetGmtTime(const char *url, char *str_gmt_time, size_t str_gmt_time_len)
{
    struct http_message *response = MG_HttpGet(url);
    struct mg_str *date;
    if(response == NULL)
    {
        printf("__NetGmtTime error!\r\n");
        return false;
    }
    date = mg_get_http_header(response, "Date");
    if(date != NULL)
    {
        if(date->len < str_gmt_time_len)
            str_gmt_time_len = date->len;
        memcpy(str_gmt_time, date->p, str_gmt_time_len);
        M_Free((void *)response);
        return true;
    }
    M_Free((void *)response);
    return false;
}

/**
 * http date gmt时间格式化为tm
 */
void __Time_Gmt2Ymd(char *gmt_str, struct tm *time)
{
    if(gmt_str == NULL)
        return;

    char year_str[5];
    strncpy(year_str, gmt_str+12, 4);
    year_str[4] = '\0';
    time->tm_year = atoi(year_str);

    char month_str[4];
    strncpy(month_str, gmt_str+8, 3);
    month_str[3] = '\0';
    time->tm_mon = Time_Mon2Int(month_str);

    char day_str[3];
    strncpy(day_str, gmt_str+5, 2);
    day_str[2] = '\0';
    time->tm_mday = atoi(day_str);

    char hour_str[3];
    strncpy(hour_str, gmt_str+17, 2);
    hour_str[2] = '\0';
    time->tm_hour = atoi(hour_str);

    char min_str[3];
    strncpy(min_str, gmt_str+20, 2);
    min_str[2] = '\0';
    time->tm_min = atoi(min_str);

    char sec_str[3];
    strncpy(sec_str, gmt_str+23, 2);
    sec_str[2] = '\0';
    time->tm_sec = atoi(sec_str);

    time->tm_us = 0;
}

/**
 * 当前日历
 * 不需做任何加减处理
 */
struct tm *Time_GetDatetime(struct tm *datetime)
{
    s64 temp_time;
    Time_Time(&temp_time);
//    temp_time -= (9 * 60); // FIXME 时间快9分钟
    Time_LocalTime_r(&temp_time, datetime);
    datetime->tm_year += 1900;
    return datetime;
}

/**
 * 配置时间
 */
void Time_SetDatetime(s32 year,s32 month,s32 day,s32 hour,s32 min,s32 sec)
{
    struct tm datetime;
    datetime.tm_year=year;
    datetime.tm_mon=month-1;
    datetime.tm_mday=day;
    datetime.tm_hour=hour;
    datetime.tm_min=min;
    datetime.tm_sec=sec;

    datetime.tm_year -= 1900;

    Time_SetDateTime(&datetime);
}

void Time_PrintDatetime(struct tm rtc_datetime)
{
    printf("\r\n时间:%04d-%02d-%02d %02d:%02d:%02d 星期%d\r\n",
            rtc_datetime.tm_year, rtc_datetime.tm_mon + 1, rtc_datetime.tm_mday,
            rtc_datetime.tm_hour, rtc_datetime.tm_min, rtc_datetime.tm_sec, rtc_datetime.tm_wday);
}

void Time_PrintNowDatetime()
{
    struct tm date;
    Time_GetDatetime(&date);
    Time_PrintDatetime(date);
}

void Time_InfoInit()
{
    struct TimeInfoSt timeInfoSt;
    if(Time_InfoLoad(&timeInfoSt))
    {
        if(timeInfoSt.time_type == TIME_HTTP)
        {
            char gmt_time[0x20];
            struct tm gmt_datetime;
            if(Time_NetGmtTime(timeInfoSt.server_time, gmt_time, 0x20))
            {
                if(gmt_time == NULL)
                    return;
                __Time_Gmt2Ymd(gmt_time, &gmt_datetime);
                if(gmt_datetime.tm_hour > 30 ||gmt_datetime.tm_year>3000 || gmt_datetime.tm_min > 60)
                    return;
                gmt_datetime.tm_hour += 8;
                gmt_datetime.tm_year -= 1900;

                Time_SetDateTime(&gmt_datetime);
            }
        }
        else if(timeInfoSt.time_type == TIME_NTP)
        {
            unsigned int timestamp = 0;
            GetTimeStamp(&timestamp, 5000);
        }
        else
        {
            // nothing;
        }
    }
    else
    {
        unsigned int timestamp = 0;
        GetTimeStamp(&timestamp, 5000);
    }

    Time_PrintNowDatetime();
}

bool_t Time_InfoCfg(u8 type, char *server, char *local_time)
{
    struct TimeInfoSt timeInfoSt;
    if(type >= 3) return false;
    timeInfoSt.time_type = type;
    if(type == TIME_LOCAL)
    {
        if(local_time != NULL)
            strncpy(timeInfoSt.last_local_timestamp, local_time, CFG_TIME_INFO_LEN);
    }
    else
    {
        if(server != NULL)
            strncpy(timeInfoSt.server_time, server, CFG_SERVER_ADDR_LEN);
    }
    Time_InfoInit();
    return Time_InfoSave(&timeInfoSt);
}

bool_t __Shell_TimeInfoCfg(char *param)
{
    char type[10];
    char server[255];
    char time[255];
    if(sscanf(param, "%s %s %s", type, server, time) == 3)
    {
        return Time_InfoCfg(atoi(type), server, time);
    }
    return false;
}


ADD_TO_ROUTINE_SHELL(time_config, __Shell_TimeInfoCfg, "配置时间同步参数\r\n时间同步类型[0:NTP 1:HTTP头 2:本地时间]\r\n[同步类型, 时间服务器地址, 本地时间]");
