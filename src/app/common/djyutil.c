/*
 * djyutil.c
 *
 *  Created on: 2020年10月29日
 *      Author: WangXi
 */

#include "djyutil.h"
#include "project_config.h"
#include "driver_info_to_fs.h"
#include "app_flash.h"

extern struct EventECB g_tECB_Table[];

//extern struct EventType g_tEvttTable[];

extern struct EventECB  *s_ptEventFree; //空闲链表头,不排序

#include <sys/socket.h>

bool_t __Set_StaticIp(const char* my_ipv4, const char* my_gatway, const char* my_dns, const char* my_submask)
{
    u32 hop,subnet,ip,submask,dns,broad;
    tagHostAddrV4  ipv4addr;
    tagRouterPara para;
    //we use the static ip we like
//  memset((void *)&ipv4addr,0,sizeof(ipv4addr));
    memset(&para,0,sizeof(para));
    ip      = inet_addr(my_ipv4);
    submask = INADDR_ANY;
    hop  = inet_addr(my_gatway);
    dns     = inet_addr(my_dns);
    broad   = inet_addr("192.168.0.255");

//  hop = INADDR_ANY;
    subnet = ip & submask;
    para.ver = EN_IPV_4;
    para.host = &ip;
    para.mask = &submask;
    para.broad = &broad;
    para.hop = &hop;
    para.net = &subnet;
    para.prior = CN_ROUT_PRIOR_UNI;
    para.ifname = CFG_SELECT_NETCARD;

    RouterCreate(&para);

    memset(&para,0,sizeof(para));
    ip      = inet_addr(my_ipv4);
    submask = inet_addr(my_submask);
//  hop  = inet_addr(my_gatway);
    dns     = inet_addr(my_dns);
    broad   = inet_addr("192.168.0.255");

    DNS_Set(EN_IPV_4,&dns, 0);

    hop = INADDR_ANY;
    subnet = ip & submask;
    para.ver = EN_IPV_4;
    para.host = &ip;
    para.mask = &submask;
    para.broad = &broad;
    para.hop = &hop;
    para.net = &subnet;
    para.prior = CN_ROUT_PRIOR_UNI;
    para.ifname = CFG_SELECT_NETCARD;

    if(RouterCreate(&para))
    {
        printk("%s:CreateRout:%s:%s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
        return true;
    }
    else
    {
        printk("%s:CreateRout:%s:%s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
        return false;
    }
}
#if 0
bool_t Set_StaticIp(char *param)
{
    tagRouterPara para;

    u32 hop,subnet,ip,submask,dns,broad;

    char buff_ip[32] = {0};
    char buff_gatway[32] = {0};
    char buff_submask[32] = {0};
    char buff_dns[32] = {0};

    if(param == NULL)
    {
        printf("ip地址 子网掩码 网关 DNS\r\n");
        return false;
    }

    sscanf(param, "%s %s %s %s",&buff_ip, &buff_submask, &buff_gatway, &buff_dns);

    if(strlen(buff_ip) == 0 || strlen(buff_submask) == 0 || strlen(buff_gatway) == 0 || strlen(buff_dns) == 0)
        return false;
    if(File_SetNameValueFs(CFG_STATIC_IP_INFO_FILE_NAME, param, strlen(param)));
    {
        return __Set_StaticIp(buff_ip, buff_gatway, buff_dns, buff_submask);
    }
    return false;
}
#endif
u32 Util_GetHeapSize()
{
    u16 pl_ecb;
    u32 heapsize=0;
    for(pl_ecb = 0; pl_ecb < CFG_EVENT_LIMIT; pl_ecb++)
    {
      if(g_tECB_Table[pl_ecb].previous !=
                      (struct EventECB*)&s_ptEventFree)
      {
          heapsize += g_tECB_Table[pl_ecb].HeapSize;
      }
    }

    return heapsize;
}

#if 0
ADD_TO_ROUTINE_SHELL(setstaticip, Set_StaticIp, "设置本机静态ip");
#endif
