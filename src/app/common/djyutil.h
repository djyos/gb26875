/*
 * djyutil.h
 *
 *  Created on: 2020��10��29��
 *      Author: WangXi
 */

#ifndef APP_DJYUTIL_H_
#define APP_DJYUTIL_H_

#include "stdint.h"
#include "board-config.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "object.h"
#include "lock.h"
#include "int.h"
#include "systime.h"
#include "djyos.h"
#include "shell.h"
#include "blackbox.h"
#include "dbug.h"

u32 Util_GetHeapSize();

bool_t Set_StaticIp(char *param);

#endif /* APP_DJYUTIL_H_ */
