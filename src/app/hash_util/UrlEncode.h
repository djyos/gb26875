/*
 * UrlEncode.h
 *
 *  Created on: 2020��3��6��
 *      Author: Administrator
 */

#ifndef APP_KDXF_URLENCODE_H_
#define APP_KDXF_URLENCODE_H_

int hex2dec(char c);
char dec2hex(short int c);
void urlencode(char url[]);
void urldecode(char url[]);


#endif /* APP_KDXF_URLENCODE_H_ */
