/*
 * sha256.h
 *
 *  Created on: 2020��2��28��
 *      Author: Administrator
 */

#ifndef APP_KDXF_SHA256_H_
#define APP_KDXF_SHA256_H_

#include <stdint.h>
char* StrSHA256(const char* str, long long length, char* sha256);
void longs2char(long *num,const int len, unsigned char *cs);
char* StrSHA256Base64(const char* str, long long length, char* sha256);
#endif /* APP_KDXF_SHA256_H_ */
