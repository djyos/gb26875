/* ====================================================================
 * Copyright (c) 1999-2007 The OpenSSL Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit. (http://www.OpenSSL.org/)"
 *
 * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. For written permission, please contact
 *    licensing@OpenSSL.org.
 *
 * 5. Products derived from this software may not be called "OpenSSL"
 *    nor may "OpenSSL" appear in their names without prior written
 *    permission of the OpenSSL Project.
 *
 * 6. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by the OpenSSL Project
 *    for use in the OpenSSL Toolkit (http://www.OpenSSL.org/)"
 *
 * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 */


#include <string.h>
#include "md5.h"


#define INIT_DATA_A (MD5_LONG)0x67452301
#define INIT_DATA_B (MD5_LONG)0xefcdab89
#define INIT_DATA_C (MD5_LONG)0x98badcfe
#define INIT_DATA_D (MD5_LONG)0x10325476

#define HOST_c2l(c,l) ((l)=*((const unsigned int *)(c)), (c)+=4, (l))
#define HOST_l2c(l,c) (*((unsigned int *)(c))=(l), (c)+=4, (l))

#define ROTATE(a,n)   (((a)<<(n))|(((a)&0xffffffff)>>(32-(n))))

#define F(b,c,d)      ((((c) ^ (d)) & (b)) ^ (d))
#define G(b,c,d)      ((((b) ^ (c)) & (d)) ^ (c))
#define H(b,c,d)      ((b) ^ (c) ^ (d))
#define I(b,c,d)      (((~(d)) | (b)) ^ (c))

#define R0(a,b,c,d,k,s,t) { \
        a+=((k)+(t)+F((b),(c),(d))); \
        a=ROTATE(a,s); \
        a+=b; };\

#define R1(a,b,c,d,k,s,t) { \
        a+=((k)+(t)+G((b),(c),(d))); \
        a=ROTATE(a,s); \
        a+=b; };

#define R2(a,b,c,d,k,s,t) { \
        a+=((k)+(t)+H((b),(c),(d))); \
        a=ROTATE(a,s); \
        a+=b; };

#define R3(a,b,c,d,k,s,t) { \
        a+=((k)+(t)+I((b),(c),(d))); \
        a=ROTATE(a,s); \
        a+=b; };

#define HASH_MAKE_STRING(c,s)   do {    \
        unsigned int ll;               \
        ll=(c)->A; (void)HOST_l2c(ll,(s));      \
        ll=(c)->B; (void)HOST_l2c(ll,(s));      \
        ll=(c)->C; (void)HOST_l2c(ll,(s));      \
        ll=(c)->D; (void)HOST_l2c(ll,(s));      \
        } while (0)


int MD5_Init(MD5_CTX *c)
{
    memset(c, 0, sizeof(*c));
    c->A = INIT_DATA_A;
    c->B = INIT_DATA_B;
    c->C = INIT_DATA_C;
    c->D = INIT_DATA_D;
    return 1;
}

void md5_block_data_order(MD5_CTX *c, const void *data_, size_t num)
{
    const unsigned char *data = data_;
    register unsigned MD32_REG_T A, B, C, D, l;
# ifndef MD32_XARRAY
    /* See comment in crypto/sha/sha_locl.h for details. */
    unsigned MD32_REG_T XX0, XX1, XX2, XX3, XX4, XX5, XX6, XX7,
        XX8, XX9, XX10, XX11, XX12, XX13, XX14, XX15;
#  define X(i)   XX##i
# else
    MD5_LONG XX[MD5_LBLOCK];
#  define X(i)   XX[i]
# endif

    A = c->A;
    B = c->B;
    C = c->C;
    D = c->D;

    for (; num--;) {
        HOST_c2l(data, l);
        X(0) = l;
        HOST_c2l(data, l);
        X(1) = l;
        /* Round 0 */
        R0(A, B, C, D, X(0), 7, 0xd76aa478);
        HOST_c2l(data, l);
        X(2) = l;
        R0(D, A, B, C, X(1), 12, 0xe8c7b756);
        HOST_c2l(data, l);
        X(3) = l;
        R0(C, D, A, B, X(2), 17, 0x242070db);
        HOST_c2l(data, l);
        X(4) = l;
        R0(B, C, D, A, X(3), 22, 0xc1bdceee);
        HOST_c2l(data, l);
        X(5) = l;
        R0(A, B, C, D, X(4), 7, 0xf57c0faf);
        HOST_c2l(data, l);
        X(6) = l;
        R0(D, A, B, C, X(5), 12, 0x4787c62a);
        HOST_c2l(data, l);
        X(7) = l;
        R0(C, D, A, B, X(6), 17, 0xa8304613);
        HOST_c2l(data, l);
        X(8) = l;
        R0(B, C, D, A, X(7), 22, 0xfd469501);
        HOST_c2l(data, l);
        X(9) = l;
        R0(A, B, C, D, X(8), 7, 0x698098d8);
        HOST_c2l(data, l);
        X(10) = l;
        R0(D, A, B, C, X(9), 12, 0x8b44f7af);
        HOST_c2l(data, l);
        X(11) = l;
        R0(C, D, A, B, X(10), 17, 0xffff5bb1);
        HOST_c2l(data, l);
        X(12) = l;
        R0(B, C, D, A, X(11), 22, 0x895cd7be);
        HOST_c2l(data, l);
        X(13) = l;
        R0(A, B, C, D, X(12), 7, 0x6b901122);
        HOST_c2l(data, l);
        X(14) = l;
        R0(D, A, B, C, X(13), 12, 0xfd987193);
        HOST_c2l(data, l);
        X(15) = l;
        R0(C, D, A, B, X(14), 17, 0xa679438e);
        R0(B, C, D, A, X(15), 22, 0x49b40821);
        /* Round 1 */
        R1(A, B, C, D, X(1), 5, 0xf61e2562);
        R1(D, A, B, C, X(6), 9, 0xc040b340);
        R1(C, D, A, B, X(11), 14, 0x265e5a51);
        R1(B, C, D, A, X(0), 20, 0xe9b6c7aa);
        R1(A, B, C, D, X(5), 5, 0xd62f105d);
        R1(D, A, B, C, X(10), 9, 0x02441453);
        R1(C, D, A, B, X(15), 14, 0xd8a1e681);
        R1(B, C, D, A, X(4), 20, 0xe7d3fbc8);
        R1(A, B, C, D, X(9), 5, 0x21e1cde6);
        R1(D, A, B, C, X(14), 9, 0xc33707d6);
        R1(C, D, A, B, X(3), 14, 0xf4d50d87);
        R1(B, C, D, A, X(8), 20, 0x455a14ed);
        R1(A, B, C, D, X(13), 5, 0xa9e3e905);
        R1(D, A, B, C, X(2), 9, 0xfcefa3f8);
        R1(C, D, A, B, X(7), 14, 0x676f02d9);
        R1(B, C, D, A, X(12), 20, 0x8d2a4c8a);
        /* Round 2 */
        R2(A, B, C, D, X(5), 4, 0xfffa3942);
        R2(D, A, B, C, X(8), 11, 0x8771f681);
        R2(C, D, A, B, X(11), 16, 0x6d9d6122);
        R2(B, C, D, A, X(14), 23, 0xfde5380c);
        R2(A, B, C, D, X(1), 4, 0xa4beea44);
        R2(D, A, B, C, X(4), 11, 0x4bdecfa9);
        R2(C, D, A, B, X(7), 16, 0xf6bb4b60);
        R2(B, C, D, A, X(10), 23, 0xbebfbc70);
        R2(A, B, C, D, X(13), 4, 0x289b7ec6);
        R2(D, A, B, C, X(0), 11, 0xeaa127fa);
        R2(C, D, A, B, X(3), 16, 0xd4ef3085);
        R2(B, C, D, A, X(6), 23, 0x04881d05);
        R2(A, B, C, D, X(9), 4, 0xd9d4d039);
        R2(D, A, B, C, X(12), 11, 0xe6db99e5);
        R2(C, D, A, B, X(15), 16, 0x1fa27cf8);
        R2(B, C, D, A, X(2), 23, 0xc4ac5665);
        /* Round 3 */
        R3(A, B, C, D, X(0), 6, 0xf4292244);
        R3(D, A, B, C, X(7), 10, 0x432aff97);
        R3(C, D, A, B, X(14), 15, 0xab9423a7);
        R3(B, C, D, A, X(5), 21, 0xfc93a039);
        R3(A, B, C, D, X(12), 6, 0x655b59c3);
        R3(D, A, B, C, X(3), 10, 0x8f0ccc92);
        R3(C, D, A, B, X(10), 15, 0xffeff47d);
        R3(B, C, D, A, X(1), 21, 0x85845dd1);
        R3(A, B, C, D, X(8), 6, 0x6fa87e4f);
        R3(D, A, B, C, X(15), 10, 0xfe2ce6e0);
        R3(C, D, A, B, X(6), 15, 0xa3014314);
        R3(B, C, D, A, X(13), 21, 0x4e0811a1);
        R3(A, B, C, D, X(4), 6, 0xf7537e82);
        R3(D, A, B, C, X(11), 10, 0xbd3af235);
        R3(C, D, A, B, X(2), 15, 0x2ad7d2bb);
        R3(B, C, D, A, X(9), 21, 0xeb86d391);

        A = c->A += A;
        B = c->B += B;
        C = c->C += C;
        D = c->D += D;
    }
}

int MD5_Update(MD5_CTX *c, const void *data_, size_t len)
{
    const unsigned char *data = data_;
    unsigned char *p;
    MD5_LONG l;
    size_t n;

    if (len == 0)
        return 1;

    l = (c->Nl + (((MD5_LONG) len) << 3)) & 0xffffffff;
    /*
     * 95-05-24 eay Fixed a bug with the overflow handling, thanks to Wei Dai
     * <weidai@eskimo.com> for pointing it out.
     */
    if (l < c->Nl)              /* overflow */
        c->Nh++;
    c->Nh += (MD5_LONG) (len >> 29); /* might cause compiler warning on
                                       * 16-bit */
    c->Nl = l;

    n = c->num;
    if (n != 0) {
        p = (unsigned char *)c->data;

        if (len >= MD5_CBLOCK || len + n >= MD5_CBLOCK) {
            memcpy(p + n, data, MD5_CBLOCK - n);
            md5_block_data_order(c, p, 1);
            n = MD5_CBLOCK - n;
            data += n;
            len -= n;
            c->num = 0;
            /*
             * We use memset rather than OPENSSL_cleanse() here deliberately.
             * Using OPENSSL_cleanse() here could be a performance issue. It
             * will get properly cleansed on finalisation so this isn't a
             * security problem.
             */
            memset(p, 0, MD5_CBLOCK); /* keep it zeroed */
        } else {
            memcpy(p + n, data, len);
            c->num += (unsigned int)len;
            return 1;
        }
    }

    n = len / MD5_CBLOCK;
    if (n > 0) {
        md5_block_data_order(c, data, n);
        n *= MD5_CBLOCK;
        data += n;
        len -= n;
    }

    if (len != 0) {
        p = (unsigned char *)c->data;
        c->num = (unsigned int)len;
        memcpy(p, data, len);
    }
    return 1;
}

int MD5_Final(unsigned char *md, MD5_CTX *c)
{
    unsigned char *p = (unsigned char *)c->data;
    size_t n = c->num;

    p[n] = 0x80;                /* there is always room for one */
    n++;

    if (n > (MD5_CBLOCK - 8)) {
        memset(p + n, 0, MD5_CBLOCK - n);
        n = 0;
        md5_block_data_order(c, p, 1);
    }
    memset(p + n, 0, MD5_CBLOCK - 8 - n);

    p += MD5_CBLOCK - 8;
#if defined(DATA_ORDER_IS_BIG_ENDIAN)
    (void)HOST_l2c(c->Nh, p);
    (void)HOST_l2c(c->Nl, p);
#elif defined(DATA_ORDER_IS_LITTLE_ENDIAN)
    (void)HOST_l2c(c->Nl, p);
    (void)HOST_l2c(c->Nh, p);
#endif
    p -= MD5_CBLOCK;
    md5_block_data_order(c, p, 1);
    c->num = 0;
    memset(p, 0, MD5_CBLOCK);

#ifndef HASH_MAKE_STRING
# error "HASH_MAKE_STRING must be defined!"
#else
    HASH_MAKE_STRING(c, md);
#endif

    return 1;
}
