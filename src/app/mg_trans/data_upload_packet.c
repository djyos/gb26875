#include "djyos.h"
#include <stdlib.h>
#include <stdio.h>
#include "../trans_dev/usr_info_trans_dev.h"
ptu32_t socket_upload_packet_trans(void)
{
    while(1)
    {
        //数据出队列并上传报文
        UITD_UploadThread();

        DJY_EventDelay(10*1000);
    }
    return 0;
}
u16 evtt_upload_packet = CN_EVTT_ID_INVALID;
void trans_socket_upload_init()
{
    evtt_upload_packet = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS-1, 0, 0,
            socket_upload_packet_trans, NULL, 0x1000, "socket upload packet trans");

    if (CN_EVTT_ID_INVALID != evtt_upload_packet)
    {
        DJY_EventPop(evtt_upload_packet, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_upload_packet error\r\n");
    }
}

s64 UITD_GetTimestamp()
{
    return DJY_GetSysTime()/1000;
}

void UITD_Sleep(s32 timeout)
{
    DJY_EventDelay(timeout*1000);
}
