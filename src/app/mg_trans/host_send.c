#include "stdlib.h"
#include "stdint.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "host_send.h"
#include "../trans_log/log_init.h"
#include "mongoose.h"
#include "../trans_dev/queue.h"
#include "../trans_dev/usr_info_trans_dev.h"
#include "../GuiWin/Info/WinSwitch.h"
////浙消正式服务器
#define SOCKET_TCP_URL      "120.55.103.0:12345"

////浙消测试服务器
//#define SOCKET_TCP_URL      "121.89.220.164:12345"


//#define SOCKET_TCP_URL      "192.168.2.107:12345"

//南消
//#define SOCKET_TCP_URL      "47.114.128.216:8888"

//#define SOCKET_TCP_URL      "192.168.0.109:8888"

// 监狱
// #define SOCKET_TCP_URL      "30.1.99.173:12345"

// #define SOCKET_TCP_URL      "1.tcp.vip.cpolar.cn:20561"

static bool_t socket_flag=false; //socket连接标志位
static bool_t reast_flag=false;       //socket重连标志位
static SocketLock socket_lock_flag = SOCKET_TYPE_PREPARE; //socket锁的标志位

struct QueueInfo *page_queues=NULL;
//struct QueueInfo *send_error_queues=NULL;
static struct StTransObj sg_stSocketState={0};
static char SendDate[1024]={0};
static char tcp_url[64]={0};
extern struct GuiIp GuiIpConfig[5];
void SetTcpUrl(u32 addr)
{
    if((strlen(GuiIpConfig[addr].gip_address)) < 5)
    {
        strcpy(tcp_url, SOCKET_TCP_URL);
    }
    else
    {
        snprintf(tcp_url,sizeof(tcp_url),"%s:%s",GuiIpConfig[addr].gip_address,GuiIpConfig[addr].gport);
    }
}
bool_t GetSocketState(void)
{
    return socket_flag;
}
struct RecvString
{
    u8 *buff;
    int len;
};

//mongoose socket 操作句柄
static void cb_tra_ev_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    u8 *buf = 0;
    struct StTransObj *SocketState = 0;
    struct RecvString *str = 0;
    if (nc->user_data == NULL)
    {
        sg_stSocketState.status = CONNECT_STA_ERROR;
        printf("error: cb_http_ev_handler, nc->user_data is NULL!\r\n");
        return;
    }
    SocketState = (struct StTransObj *)nc->user_data;
    switch (ev)
    {
        case MG_EV_CONNECT:
            if (*(int *)ev_data != 0)
            {
#if UITD_LOG
                log_queue_push("socket连接失败",32,LOG_SOCKET_STATE,"NULL");
#endif
                printf("warning: %s MG_EV_CONNECT failed: %d!\r\n", __FUNCTION__, *(int *)ev_data);
                nc->flags |= MG_F_CLOSE_IMMEDIATELY;
                SocketState->status = CONNECT_STA_ERROR;
            }
            else
            {
#if UITD_LOG
                log_queue_push("socket连接成功",32,LOG_SOCKET_STATE,"NULL");
#endif
                socket_flag=true;

                SocketState->status = CONNECT_STA_SUCESS;

                printf("MG_EV_CONNECT\r\n");
            }
            break;
        case MG_EV_CLOSE:
            printf("MG_EV_CLOSE\r\n");
#if UITD_LOG
            log_queue_push("socket断开连接",32,LOG_SOCKET_STATE,"NULL");
#endif
            SocketState->status = CONNECT_STA_CLOSED;

            break;
        case MG_EV_SEND:

//            printf("MG_EV_SEND\r\n");
            SocketState->timemark = DJY_GetSysTime() / 1000;
#if UITD_LOG
            log_queue_push("socket上传数据",32,LOG_SOCKET_STATE,"NULL");
#endif
//            error_queue_pop();
            socket_lock_flag=SOCKET_TYPE_COMPLETE;
            SocketState->status = CONNECT_STA_SUCESS;
            break;
        case MG_EV_RECV:

//            printf("MG_EV_RECV\r\n");
#if UITD_LOG
            log_queue_push("socket读取数据",32,LOG_SOCKET_STATE,"NULL");
#endif
            buf=malloc(nc->recv_mbuf.len);
            memcpy(buf,nc->recv_mbuf.buf,nc->recv_mbuf.len);
#if UITD_LOG
            log_queue_push(buf,nc->recv_mbuf.len,LOG_DOWN_MESSAGE,"NULL");
#endif
            str = (struct RecvString *)malloc(sizeof(struct RecvString));
            str->buff = buf;
            str->len = nc->recv_mbuf.len;
            Queue_Push(page_queues, (s32)str);
//            printf("recv buf is %s\r\n",buf);
            // 必须remove 否则内存不释放
            struct mbuf *io = &nc->recv_mbuf;
            mbuf_remove(io, io->len);

//            UITD_DownloadThread();
//            error_queue_pop();
//            socket_lock_flag=SOCKET_TYPE_COMPLETE;
//            sg_stSocketState.status = CONNECT_STA_WS_SUCESS;

            break;
        default:
            break;
    }
}


#define SOCKET_TCP_TIMEOUT      60*1000



void tra_breakout(void)
{
    sg_stSocketState.is_break = 1;
    SeticStatues(GetConnectNumb(),0);
}

void tra_reast(void)
{
    sg_stSocketState.len = 0;
    sg_stSocketState.is_break = 0;
    sg_stSocketState.status = CONNECT_STA_INIT;
    sg_stSocketState.timemark = DJY_GetSysTime() / 1000;
}

bool_t uploading = false;
ptu32_t socket_tcp_trans(void)
{
    printf("this is socket_tcp_trans_init\r\n");
#if UITD_LOG
    log_queue_push("socket初始化",32,LOG_SOCKET_STATE,"NULL");
#endif
    int ret = 0;

    //每次打开重置
    tra_reast();

    //初始mongoose
    struct mg_mgr mgr;
    struct mg_connection *nc;
    mg_mgr_init(&mgr, NULL);

    nc = mg_connect(&mgr, tcp_url, cb_tra_ev_handler);

    UITD_ResetInfoNo();
	
    if (nc)
    {
        nc->user_data = &sg_stSocketState;
    }
    else
    {
#if UITD_LOG
        log_queue_push("socket连接失败",32,LOG_SOCKET_STATE,"NULL");
#endif
        mg_mgr_free(&mgr);
        return 0;
    }
    while (1)
    {
        if (sg_stSocketState.status == CONNECT_STA_INIT || sg_stSocketState.status == CONNECT_STA_RECV)
        {
            if (DJY_GetSysTime() / 1000 - sg_stSocketState.timemark > 10000)
            {
                printf("warning: %s:%d timeout!\r\n", __FUNCTION__, __LINE__);
                ret = TRA_CONNECT_BREAK;
                break;
            }
        }
        else if (sg_stSocketState.status == CONNECT_STA_SEND)
        {
            mg_send(nc,SendDate,sg_stSocketState.len);

            sg_stSocketState.timemark = DJY_GetSysTime() / 1000;

            sg_stSocketState.status = CONNECT_STA_RECV;
        }
        else if (sg_stSocketState.status == CONNECT_STA_ERROR)
        {
            printf("error: TRA_CONNECT_ERROR %s:%d !\r\n", __FUNCTION__, __LINE__);
            ret = TRA_CONNECT_ERROR;
            break;
        }
        else if (sg_stSocketState.status == CONNECT_STA_CLOSED)
        {
            printf("error: CONNECT_STA_WS_CLOSED %s:%d !\r\n", __FUNCTION__, __LINE__);
            ret = TRA_CONNECT_CLOSE;
            break;
        }

        if (sg_stSocketState.is_break == 1)
        {
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            printf("warning: %s break waiting, exit now!\r\n", __FUNCTION__);
            ret = TRA_CONNECT_BREAK;
            break;
        }

        mg_mgr_poll(&mgr, 500);
    }

    socket_lock_flag=SOCKET_TYPE_ERROR;
    socket_flag=false;
    reast_flag=true;
    SeticStatues(GetConnectNumb(),0);
    mg_mgr_free(&mgr);
    return 0;
}

void socket_tcp_trans_send(u8 *send_data,int len)
{
    sg_stSocketState.len=len;
    sg_stSocketState.status = CONNECT_STA_SEND;

    memset(SendDate,0,sizeof(SendDate));
    memcpy(SendDate,send_data,len);
}


u16 evtt_socket = CN_EVTT_ID_INVALID;
static struct MutexLCB *upload_lock = NULL;

void trans_socket_tcp_init()
{
    if(page_queues == NULL)
    {
        page_queues = Queue_CreateQueue();
        printf("page_queues Create Success:%x\r\n", (ucpu_t)page_queues);
    }
    if(page_queues == NULL)
    {
        printf("Socket_Eueues Create Failure.\r\n");
    }

    upload_lock = Lock_MutexCreate("upload_lock");
    if (NULL == upload_lock)
    {
        debug_printf("host_send","Create upload_lock fail.\r\n");
        Lock_MutexDelete(upload_lock);
    }

//    if(send_error_queues == NULL)
//    {
//        send_error_queues = Queue_CreateQueue();
//        printf("Send_Error_Queues Create Success:%x\r\n", send_error_queues);
//    }
//    if(send_error_queues == NULL)
//    {
//        printf("Send_Error_Queues Create Failure.\r\n");
//    }
    evtt_socket = DJY_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS-2, 0, 0,
                socket_tcp_trans, NULL, 0x2C00, "socket tcp trans");

    if (CN_EVTT_ID_INVALID != evtt_socket)
    {
        DJY_EventPop(evtt_socket, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_socket error\r\n");
    }
}

int UITD_Download(u8 *data, s32 timeout)
{
    if(socket_flag)
    {
        struct RecvString *str;
        int len=0;
        if(Queue_Pop(page_queues, (s32 *)&str) == 0)
        {
//            printf("Queue_Pop Error\r\n");
            return 0;
        }
        if(str == NULL)
        {
//            printf("Queue_Pop is NULL\r\n");
            return 0;
        }
//        printf("buf is %s\r\n",buf);
        if(str->len > 0x420)
        {
            str->len = 0x420;
        }
        memcpy(data,str->buff,str->len);
        len=str->len;
        free(str->buff);
        free(str);
        return len;
    }
    else
    {
        return 0;
    }
}

int UITD_Upload(u8 *data, int len, u8 *recv)
{
    Lock_MutexPend(upload_lock,CN_TIMEOUT_FOREVER);
    uploading = true;
//    s64 timeout=DJY_GetSysTime()/1000;
    if(socket_flag)
    {
        socket_tcp_trans_send(data,len);
        while(1)
        {
            if(socket_lock_flag == SOCKET_TYPE_COMPLETE)
            {
                socket_lock_flag=SOCKET_TYPE_PREPARE;
                Lock_MutexPost(upload_lock);
                uploading = false;
                return 1;
            }
            else if(socket_lock_flag == SOCKET_TYPE_ERROR)
            {
                printf("upload is error\r\n");
                socket_lock_flag=SOCKET_TYPE_PREPARE;
                Lock_MutexPost(upload_lock);
                uploading = false;
                return 0;
            }
//            else if(DJY_GetSysTime()/1000 - timeout > 5000)
//            {
//
//                printf("upload is timeout\r\n");
//                socket_lock_flag=SOCKET_TYPE_PREPARE;
//                Lock_MutexPost(upload_lock);
//                return 0;
//            }
            DJY_EventDelay(10*1000);
        }
    }
    else
    {
        socket_lock_flag=SOCKET_TYPE_PREPARE;
        Lock_MutexPost(upload_lock);
        uploading = false;
        return 0;
    }

}

struct tm *get_datetime(struct tm *datetime);
void UITD_GetCurrentTime(u8 time[6])
{
    struct tm curr_datetime;
    get_datetime(&curr_datetime);

    time[0]=curr_datetime.tm_year%100;
    time[1]=curr_datetime.tm_mon+1;
    time[2]=curr_datetime.tm_mday;
    time[3]=curr_datetime.tm_hour;
    time[4]=curr_datetime.tm_min;
    time[5]=curr_datetime.tm_sec;

//    printf("time[0] is %d\r\n",time[0]);
//    printf("time[1] is %d\r\n",time[1]);
//    printf("time[2] is %d\r\n",time[2]);
//    printf("time[3] is %d\r\n",time[3]);
//    printf("time[4] is %d\r\n",time[4]);
//    printf("time[5] is %d\r\n",time[5]);

}

int reast_socket_trans(void)
{
    if(reast_flag && (CN_EVTT_ID_INVALID != evtt_socket))
    {
        DJY_EventPop(evtt_socket, NULL, 0, 0, 0, 0);
        reast_flag=false;
        return 1;
    }
    return 0;
}

//void error_queue_push(char *data, int len)
//{
//    struct ErrorString *str;
//    str = (struct ErrorString *)malloc(sizeof(struct ErrorString));
//
//    char *buf=malloc(len);
//    memcpy(buf,data,len);
//    str->buff = buf;
//    str->len = len;
//    if(!Queue_Push(send_error_queues, (ElementType)str))
//    {
//        free((void *)str);
//        free(buf);
//    }
//}
//
//void error_queue_pop(void)
//{
//    struct ErrorString *str;
//    if(Queue_Pop(send_error_queues, (ElementType *)&str) == 0)
//    {
//        return;
//    }
//    if(str == NULL)
//    {
//        return;
//    }
//
//    free(str->buff);
//    free(str);
//}
//
//s32 error_queue_top(void)
//{
//    struct ErrorString *str;
//    if(Queue_Top(send_error_queues, (ElementType *)&str) == 0)
//    {
//        return 0;
//    }
//    if(str == NULL)
//    {
//        return 0;
//    }
//
//    socket_tcp_trans_send(str->buff,str->len);
//
//    return 1;
//}
