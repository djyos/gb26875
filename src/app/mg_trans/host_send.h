#ifndef __HOST_SEND_H__
#define __HOST_SEND_H__


enum {
    TRA_SEND_START = 0, //开始发送数据
    TRA_SEND_CONTINUE,  //继续发送数据。
    TRA_SEND_ENDED,     //结束发送数据
};


enum {
    TRA_CONNECT_OK = 0,
    TRA_CONNECT_ERROR = -1,
    TRA_CONNECT_BREAK = -2,
    TRA_CONNECT_CLOSE = -3,
};

enum {
    CONNECT_STA_INIT = 0,   //socket初始化，开始建立连接
    CONNECT_STA_SUCESS,  //socket连接成功
    CONNECT_STA_RECV,    //socket读取数据
    CONNECT_STA_SEND,    //socket发送数据
    CONNECT_STA_CLOSED,  //socket断开连接
    CONNECT_STA_ERROR,   //socket连接错误
};

#include "mongoose.h"

struct StTransObj
{
    u32 len;
    s32 status;
    s32 is_break;
    s64 timemark;

};

struct ErrorString
{
    char *buff;
    int len;
};

typedef enum __SocketLock //阻塞标志位
{
    SOCKET_TYPE_PREPARE = 0,         // 开始发送数据
    SOCKET_TYPE_COMPLETE ,           // 成功发送数据
    SOCKET_TYPE_ERROR,               // 发送数据失败
} SocketLock;

/**
 *
 * 事件初始化
 */
ptu32_t socket_tcp_trans(void);

/**
 *
 * socket发送数据
 */
void socket_tcp_trans_send(u8 *send_data,int len);

/**
 *
 * 客户端主动断开socket连接
 */
void tra_breakout(void);

/**
 *
 * 断开连接尝试重连标志
 */
int reast_socket_trans(void);

bool_t GetSocketState(void);
void SetTcpUrl(u32 addr);
#endif
