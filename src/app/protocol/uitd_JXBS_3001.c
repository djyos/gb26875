#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"

#define UITD_JXBS_3001 1

#if UITD_JXBS_3001

#define UITD_JXBS_3001_UNIT_NUM 1
#define UITD_JXBS_3001_READ_NUM 1

int JXBS_3001SetSendData(u8 serial_addr, u8 COM_ID, u8 *send_data)
{
    int checkcrc = 0;       /*crc校验*/

    char read_addr_L = 0;
    char read_addr_H = 0;

    read_addr_H = (0>>8)&0xff;
    read_addr_L = (0>>0)&0xff;

    send_data[0] = COM_ID;              /*从机地址              */
    send_data[1] = 03;                      /*功能码                */
    send_data[2] = read_addr_H;             /*起始地址高字节            */
    send_data[3] = read_addr_L;             /*起始地址低字节            */
    send_data[4] = 0;                       /*寄存器数量高字节          */
    send_data[5] = UITD_JXBS_3001_READ_NUM;         /*寄存器数量低字节          */
    checkcrc = UITD_CRC_U16_MODBUS(send_data, 6);       /*计算CRC校验值         */
    send_data[6] = checkcrc&0xff;           /*CRC校验低字节         */
    send_data[7] = (checkcrc>>8)&0xff;      /*CRC校验高字节         */
    return 8;
}

int JXBS_3001SetRecvData(u8 serial_addr, u8 COM_ID, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
    u32 read_addr = 0;
    u16 checkcrc = 0;
    s32 ret = -1;

    if(UITD_U16CheckCRC(read_buf, len)) //  CRC校验
    {
        if(read_buf[0] == COM_ID && read_buf[1] == 3)   //  功能码和从机地址校验
        {
            ret = 0;
        }
    }
    if(ret == 0)
    {
        info->unit_size = 1;
        // info->unit[0]->sys_addr = COM_ID; // COM_ID这里表示从机地址 系统地址在外部已赋值
        info->unit[0]->addr = 0;
        info->unit[0]->type = 134;  //  部件类型134用户自定义类型 液位传感器
        info->unit[0]->ad_type = GB26875_UNIT_AD_TYPE_HIGHT;    //  液位值 高度
        info->unit[0]->ad_val = (((u16)read_buf[3])<<8) + read_buf[4];
        return UITD_DATA_TYPE_UNIT_AD;
    }
    return UITD_DATA_TYPE_NULL;
}

struct ProtocolVTable PROTOCOL_JXBS_3001 = {\
    UITD_PROTOCOL_TYPE_2,\
    0,\
    5000,\
    128,\
    0,\
    JXBS_3001SetSendData,\
    0,\
    JXBS_3001SetRecvData,\
    1000,
};

#endif
