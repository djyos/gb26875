#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"

/******6125和6126******/

#define U6125_TYPE_FIR        135      //  火灾报警系统报警数据
#define U6125_TYPE_ERR        138      //  火灾报警系统回路故障信息
#define U6125_TYPE_RST        139      //  控制器复位
#define U6125_TYPE_XJFH       140      //  巡检返回
#define U6125_TYPE_OTHER      141      //  其它报警信息
#define U6125_TYPE_REF        200      //  读火灾报警系统信息（刷新 命令）
#define U6125_TYPE_XJKZQ      206      //  巡检控制器
#define U6125_TYPE_ACK        250      //  应答包(ACK)


#define U6125_FIR_1       0x01     //  火警
#define U6125_FIR_16      0x16     //  感烟火警
#define U6125_FIR_17      0x17     //  感温火警
#define U6125_FIR_2       0x02     //  盗警
#define U6125_FIR_3       0x03     //  预警
#define U6125_FIR_4       0x04     //  摘头故障
#define U6125_FIR_5       0x05     //  老化故障
#define U6125_FIR_7       0x07     //  短路隔离故障
#define U6125_FIR_8       0x08     //  未编故障
#define U6125_FIR_A       0x0A     //  动作
#define U6125_FIR_B       0x0B     //  隔离
#define U6125_FIR_C       0x0C     //  正在启动（总线模块）
#define U6125_FIR_D       0x0D     //  泄漏
#define U6125_FIR_E       0x0E     //  输入模块火警
#define U6125_FIR_F       0x0F     //  控制模块正在延时
#define U6125_FIR_18      0x18     //  烟传感故障
#define U6125_FIR_19      0x19     //  温传感故障
#define U6125_FIR_1A      0x1A     //  地址重复
#define U6125_FIR_1B      0x1B     //  24V 断线
#define U6125_FIR_2A      0x2A     //  输入输出口越界
#define U6125_FIR_2B      0x2B     //  信号线断线
#define U6125_FIR_34      0x34     //  多线启动
#define U6125_FIR_35      0x35     //  多线反馈
/*********文档没有***********/
#define U6125_FIR_30      0x30     //  监管
/**********************/

#define U6125_OTHER_13      0x13     //  直接按键输出断路
#define U6125_OTHER_14      0x14     //  直接按键输出短路
#define U6125_OTHER_15      0x15     //  24V 电源输出短路
#define U6125_OTHER_1C      0x1C     //  液晶显示盘主板故障
#define U6125_OTHER_1F      0x1F     //  联动键盘主板故障
#define U6125_OTHER_20      0x20     //  直接按键板通讯故障
#define U6125_OTHER_21      0x21     //  联动键盘板通讯故障
#define U6125_OTHER_28      0x28     //  主电故障
#define U6125_OTHER_29      0x29     //  备电故障
#define U6125_OTHER_33      0x33     //  区域火警
#define U6125_OTHER_36      0x36     //  多线启动短路
#define U6125_OTHER_37      0x37     //  多线启动断路
#define U6125_OTHER_38      0x38     //  多线停止短路
#define U6125_OTHER_39      0x39     //  多线停止断路
#define U6125_OTHER_3B      0x3B     //  声光通讯故障
#define U6125_OTHER_3C      0x3C     //  总线板故障
#define U6125_OTHER_3E      0x3E     //  声光启动
#define U6125_OTHER_3F      0x3F     //  声光短路
#define U6125_OTHER_40      0x40     //  声光断路
/*********文档没有***********/
#define U6125_OTHER_34      0x34     //  启动
#define U6125_OTHER_35      0x35     //  反馈
/**********************/

#define U6125_ERR_10      0x10     //  短路故障
#define U6125_ERR_11      0x11     //  断路故障

#define U6125_RESET     0x80    //  故障减少 复位

static u8 UType=0;
static u8 UAddr=0;

u16 __U6125GetSysstatus(u8 type,u8 data)
{
    u16 status=GB26875_SYS_STS_NORMAL;

    if(type == U6125_TYPE_FIR)
    {
        if(data == U6125_FIR_1 || data == U6125_FIR_16 || data == U6125_FIR_17 ||
           data == U6125_FIR_E)
        {
            status|=GB26875_SYS_STS_FIRE_ALARM;
        }
        else if(data == U6125_FIR_4 || data == U6125_FIR_5 || data == U6125_FIR_7 ||
                data ==U6125_FIR_8 || data ==U6125_FIR_18 || data ==U6125_FIR_19 ||
                data ==U6125_FIR_2A || data ==U6125_FIR_1B || data == U6125_FIR_2B ||
                data ==U6125_FIR_D)
        {
                status|=GB26875_SYS_STS_ERROR;
        }
        else if(data == U6125_FIR_C || data == U6125_FIR_34)
        {
            status|=GB26875_SYS_STS_START;
        }
        else if(data == U6125_FIR_35)
        {
            status|=GB26875_SYS_STS_CB;
        }
        else if(data == U6125_FIR_B)
        {
            status|=GB26875_SYS_STS_SHIELD;
        }
        else if(data == U6125_FIR_A || data == U6125_FIR_2 || data == U6125_FIR_3 || data ==U6125_FIR_F)
        {
            status|=GB26875_SYS_STS_CB;
        }
        else if(data == U6125_FIR_30)
        {
            status|=GB26875_SYS_STS_WTC;
        }
        else
        {
            status|=GB26875_SYS_STS_NORMAL;   //TODO还有些状态不是很清楚归为哪类
        }
    }
    else if(type == U6125_TYPE_ERR)
    {
        status|=GB26875_SYS_STS_ERROR;

        if((data & U6125_RESET) == U6125_RESET)   // 最高位是1表示减少故障
        {
            status|=GB26875_SYS_STS_RESET;
        }
    }
    else if(type == U6125_TYPE_OTHER)
    {
        if(data == U6125_OTHER_33)
        {
            status|=GB26875_SYS_STS_FIRE_ALARM;
        }
        else if(data == U6125_OTHER_28)
        {
            status|=GB26875_SYS_STS_DC_ERROR;
        }
        else if(data == U6125_OTHER_29)
        {
            status|=GB26875_SYS_STS_DC2_ERROR;
        }
        else if(data == U6125_OTHER_3E)
        {
            status|=GB26875_SYS_STS_START;
        }

        else if(data == U6125_OTHER_13 || data == U6125_OTHER_14 || data == U6125_OTHER_15 ||
                data ==U6125_OTHER_1C || data ==U6125_OTHER_1F || data ==U6125_OTHER_20 ||
                data ==U6125_OTHER_21 || data ==U6125_OTHER_36 || data == U6125_OTHER_37 ||
                data ==U6125_OTHER_38 || data ==U6125_OTHER_39 || data == U6125_OTHER_3B ||
                data ==U6125_OTHER_3C || data ==U6125_OTHER_3F || data == U6125_OTHER_40)
        {
            status|=GB26875_SYS_STS_ERROR;
        }
        else if(data == U6125_OTHER_34)
        {
            status|=GB26875_SYS_STS_START;
        }
        else if(data == U6125_OTHER_35)
        {
            status|=GB26875_SYS_STS_CB;
        }
        else
        {
            status|=GB26875_SYS_STS_NORMAL;
        }

        if((data & U6125_RESET) == U6125_RESET)   // 最高位是1表示减少故障
        {
            status|=GB26875_SYS_STS_RESET;
        }
    }
    return status;
}

u16 __U6125GetUnitstatus(u8 type,u8 data)
{
    u16 status=GB26875_UNIT_STS_NORMAL;

    if(type == U6125_TYPE_FIR)
    {
        if(data == U6125_FIR_1 || data == U6125_FIR_16 || data == U6125_FIR_17 ||
           data == U6125_FIR_E)
        {
            status|=GB26875_UNIT_STS_FIRE_ALARM;
        }
        else if(data == U6125_FIR_4 || data == U6125_FIR_5 || data == U6125_FIR_7 ||
                data ==U6125_FIR_8 || data ==U6125_FIR_18 || data ==U6125_FIR_19 ||
                data ==U6125_FIR_2A || data ==U6125_FIR_1B || data == U6125_FIR_2B ||
                data ==U6125_FIR_D)
        {
            status|=GB26875_UNIT_STS_ERROR;
        }
        else if(data == U6125_FIR_C || data == U6125_FIR_34)
        {
            status|=GB26875_UNIT_STS_START;
        }
        else if(data == U6125_FIR_35)
        {
            status|=GB26875_UNIT_STS_CB;
        }
        else if(data == U6125_FIR_B)
        {
            status|=GB26875_UNIT_STS_SHIELD;
        }
        else if(data == U6125_FIR_A || data == U6125_FIR_2 || data == U6125_FIR_3 || data ==U6125_FIR_F)
        {
            status|=GB26875_UNIT_STS_CB;    // 反馈
        }
        else if(data == U6125_FIR_30)
        {
            status|=GB26875_UNIT_STS_WTC;
        }
        else
        {
            status|=GB26875_UNIT_STS_NORMAL;   //TODO还有些状态不是很清楚归为哪类
        }

    }
    else if(type == U6125_TYPE_ERR)
    {
        status|=GB26875_UNIT_STS_ERROR;

        if((data & U6125_RESET) == U6125_RESET)   // 最高位是1表示减少故障
        {
            status|=GB26875_UNIT_STS_NORMAL;
        }
    }
    else if(type == U6125_TYPE_OTHER)
    {
        if(data == U6125_OTHER_33)
        {
            status|=GB26875_UNIT_STS_FIRE_ALARM;
        }
        else if(data == U6125_OTHER_28)
        {
            status|=GB26875_UNIT_STS_DC_ERROR;
        }
        else if(data == U6125_OTHER_29)
        {
            status|=GB26875_UNIT_STS_DC_ERROR;
        }
        else if(data == U6125_OTHER_3E)
        {
            status|=GB26875_UNIT_STS_START;
        }

        else if(data == U6125_OTHER_13 || data == U6125_OTHER_14 || data == U6125_OTHER_15 ||
                data ==U6125_OTHER_1C || data ==U6125_OTHER_1F || data ==U6125_OTHER_20 ||
                data ==U6125_OTHER_21 || data ==U6125_OTHER_36 || data == U6125_OTHER_37 ||
                data ==U6125_OTHER_38 || data ==U6125_OTHER_39 || data == U6125_OTHER_3B ||
                data ==U6125_OTHER_3C || data ==U6125_OTHER_3F || data == U6125_OTHER_40)
        {
            status|=GB26875_UNIT_STS_ERROR;
        }
        else if(data == U6125_OTHER_34)
        {
            status|=GB26875_UNIT_STS_START;
        }
        else if(data == U6125_OTHER_35)
        {
            status|=GB26875_UNIT_STS_CB;
        }
        else
        {
            status|=GB26875_UNIT_STS_NORMAL;
        }

        if((data & U6125_RESET) == U6125_RESET)   // 最高位是1表示减少故障
        {
            status|=GB26875_UNIT_STS_NORMAL;
        }
    }
    return status;
}

static s64 U6125Timeout=0;
int U6125SetSendData(u8 serial_addr, u8 COM_ID, u8 *send_data)
{
    send_data[0] = 0x68;           //起始字节
    send_data[1] = 0x09;           //字节长度
    send_data[2] = 0x09;           //字节长度
    send_data[3] = 0x68;           //起始字节
    send_data[4] = 0x00;           //计算机机位号低位
    send_data[5] = 0x00;           //计算机机位号高位
    send_data[6] = UAddr;    //控制器机位号低位  //TODO这个是主机号
    send_data[7] = 0x00;           //控制器机位号高位
    send_data[8] = 0x00;           //固定值
    send_data[9] = 0x10;           //版本号
    send_data[10] = U6125_TYPE_XJKZQ;                   //类型标志符
    send_data[11] = 0x01;                               //信息对象数目
    send_data[12] = 0x00;                               //巡检数据包类型
    send_data[13] = GB26875_CheckSum(&send_data[3],2,9);    //校验和
    send_data[14] = 0x16;                               //结束字节

    if (DJY_GetSysTime() / 1000 - U6125Timeout > 20000)
    {
        U6125Timeout=DJY_GetSysTime() / 1000;
//        printf("Send data: ");
//        for(int i=0;i<15;i++)
//        {
//            printf("%02x ", send_data[i]);
//        }
//        printf("\r\n");
        return 15;
    }
    else
    {
        return 0;
    }

}

int U6125SetRespData(u8 serial_addr, u8 COM_ID, u32 ret, u8 *send_data)
{
    send_data[0] = 0x68;           //起始字节
    send_data[1] = 0x09;           //字节长度
    send_data[2] = 0x09;           //字节长度
    send_data[3] = 0x68;           //起始字节
    send_data[4] = 0x00;           //计算机机位号低位
    send_data[5] = 0x00;           //计算机机位号高位
    send_data[6] = UAddr;    //控制器机位号低位  //TODO这个是主机号
    send_data[7] = 0x00;           //控制器机位号高位
    send_data[8] = 0x00;           //固定值
    send_data[9] = 0x10;           //版本号
    send_data[10] = U6125_TYPE_ACK;                      //类型标志符
    send_data[11] = 0x01;                                //信息对象数目
    send_data[12] = UType;                               //要应答的数据包类型
    send_data[13] = GB26875_CheckSum(&send_data[3],2,9);     //校验和
    send_data[14] = 0x16;                                //结束字节

//    printf("Resp data: ");
//    for(int i=0;i<15;i++)
//    {
//        printf("%02x ", send_data[i]);
//    }
//    printf("\r\n");

    return 15;
}

int U6125SetRecvData(u8 serial_addr, u8 COM_ID, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
    int ret = UITD_DATA_TYPE_NULL;
    u8 quantity = 0;
    int i = 0;
    printf("U6125 recv data: ");
    for(i=0;i<len;i++)
    {
        printf("%02x ", read_buf[i]);
    }
    printf("\r\n");

    //检查数据头部和尾部是否合法
    if((read_buf[0] != 0x68) && (read_buf[3] != 0x68) && (read_buf[len] != 0x16))
    {
        return UITD_DATA_TYPE_NULL;
    }
    //检查数据校验和
    if(GB26875_CheckSum(&read_buf[3],2,len-6) != read_buf[len-2])
    {
        return UITD_DATA_TYPE_NULL;
    }

    UAddr = read_buf[4];
    UType = read_buf[10];
    quantity = read_buf[11];

    if(UType == U6125_TYPE_FIR)
    {
        for(int i=0;i<quantity;i++)
        {
            info->unit[info->unit_size]->sys_addr =serial_addr;
            info->unit[info->unit_size]->addr = read_buf[13 + i*36] + (read_buf[12 + i*36]<<8);     //部件地址read_buf[12 + i*36]回路号
            info->unit[info->unit_size]->type = 199;
            if(read_buf[15] == 0)
            {
                info->unit[info->unit_size]->status = GB26875_UNIT_STS_NORMAL;
            }
            else
            {
                info->unit[info->unit_size]->status = __U6125GetUnitstatus(UType,read_buf[14 + i*36]);
            }
            info->unit_size ++;
        }
        ret |= UITD_DATA_TYPE_UNIT_STS;
    }
    else if(UType == U6125_TYPE_ERR)
    {
        for(int i=0;i<quantity;i++)
        {
            if(0 == read_buf[14 + i*5])
            {
                info->sys->status = __U6125GetSysstatus(UType,read_buf[12 + i*5]);
                ret |= UITD_DATA_TYPE_SYS_STS;
            }
            else
            {
                info->unit[info->unit_size]->sys_addr =serial_addr;
                info->unit[info->unit_size]->addr = read_buf[14 + i*5]<<8;      //部件地址read_buf[14 + i*5]回路号
                info->unit[info->unit_size]->type = 199;
                info->unit[info->unit_size]->status = __U6125GetUnitstatus(UType,read_buf[12 + i*5]);
                info->unit_size ++;
                ret |= UITD_DATA_TYPE_UNIT_STS;
            }
        }
    }
    else if(UType == U6125_TYPE_RST)
    {
//        info->sys->type = 1;
        info->sys->opt = GB26875_SYS_OPT_RESET;
        ret |= UITD_DATA_TYPE_SYS_OPT;
    }
    else if(UType == U6125_TYPE_OTHER)
    {
        for(int i=0;i<quantity;i++)
        {
            if(0 == read_buf[14 + i*5])
            {
                info->sys->status = __U6125GetSysstatus(UType,read_buf[12 + i*5]);
                ret |= UITD_DATA_TYPE_SYS_STS;
            }
            else
            {
                 //主机号read_buf[13 + i*5]
                 info->unit[info->unit_size]->sys_addr =serial_addr;
                 info->unit[info->unit_size]->addr = read_buf[14 + i*5];      //部件地址read_buf[14 + i*5]回路号
                 info->unit[info->unit_size]->type = 199;
                 info->unit[info->unit_size]->status = __U6125GetUnitstatus(UType,read_buf[12 + i*5]);
                 info->unit_size ++;
                 ret |= UITD_DATA_TYPE_UNIT_STS;
            }
        }
    }
    else
    {
        return UITD_DATA_TYPE_NULL;
    }
    return ret;
}


struct ProtocolVTable PROTOCOL_U6125 = {\
    UITD_PROTOCOL_TYPE_4,\
    1,\
    5000,\
    256,\
    0,\
    U6125SetSendData,\
    U6125SetRespData,\
    U6125SetRecvData,\
    500\
};
