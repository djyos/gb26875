#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"

#define UITD_P300_UNIT_MAX 1
#define UITD_P300_READ_NUM 4
#define UITD_P300_TOLERANCE_VALUE 1

static u16 Read_Value[60]={0};

bool_t P300_IfUpd(u8 com_id, u16 addr, u16 value)
{
    u16 result=0;
    u16 big=0;
    u16 small=0;
    big = Read_Value[addr]>value?Read_Value[addr]:value;
    small = Read_Value[addr]<value?Read_Value[addr]:value;
    if(com_id >= UITD_SERIAL_NUM)
    {
        return false;
    }

    result=big-small;

    if(result > UITD_P300_TOLERANCE_VALUE)
    {
        Read_Value[addr] = value;
        return true;
    }

    return false;
}
u32 P300_SetSendData(u8 serial_addr,u8 COM_ID, u8 *send_data)
{
	int checkcrc = 0;		/*crc校验*/

	u8 read_addr_L = (0>>8)&0xff;
	u8 read_addr_H = (0>>0)&0xff;

	send_data[0] = COM_ID;				              /*从机地址				*/
	send_data[1] = 03;						          /*功能码				*/
	send_data[2] = read_addr_H;				           /*起始地址高字节			*/
	send_data[3] = UITD_P300_READ_NUM;		          /*起始地址低字节			*/
	send_data[4] = 0;						          /*寄存器数量高字节			*/
	send_data[5] = UITD_P300_UNIT_MAX;			      /*寄存器数量低字节			*/
	checkcrc = UITD_CRC_U16_MODBUS(send_data, 6);     /*计算CRC校验值                            */
	send_data[6] = checkcrc&0xff;			          /*CRC校验低字节			*/
	send_data[7] = (checkcrc>>8)&0xff;		          /*CRC校验高字节			*/

	return 8;
}

u32 P300_SetRecvData(u8 serial_addr,u8 COM_ID, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
	s32 ret = -1;
	u16 data_addr = 0;
	u16 read_val = 0;
	if(UITD_U16CheckCRC(read_buf, len))	//	CRC校验
	{
		if(read_buf[0] == COM_ID && read_buf[1] == 3)	//	功能码和从机地址校验
		{
			ret = 0;
		}
	}
	if(ret == 0)
	{
	    info->unit_size = 0;
        for(data_addr = 0; data_addr < UITD_P300_UNIT_MAX; data_addr++)
        {
            read_val = (((u16)read_buf[3+2*data_addr])<<8) + read_buf[4+2*data_addr];
            if(true)//P300_IfUpd(serial_addr,data_addr,read_val))
            {
                info->unit[info->unit_size]->addr = data_addr;
                info->unit[info->unit_size]->type = 135;  //  部件类型135用户自定义类型 压力监控装置
                info->unit[info->unit_size]->ad_type = GB26875_UNIT_AD_TYPE_PRES_K;   //  压力值 兆帕
                info->unit[info->unit_size]->ad_val = read_val * 10;
                info->unit_size++;
            }
        }
		return UITD_DATA_TYPE_UNIT_AD;
	}
	return UITD_DATA_TYPE_NULL;
}

/******压力监控装置******/
struct ProtocolVTable PROTOCOL_P300 = {
	UITD_PROTOCOL_TYPE_2,
	135,
	5000,
	1024,
	0,
	P300_SetSendData,
	0,
	P300_SetRecvData,
	500
};

