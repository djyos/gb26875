/*
 * uitd_4100.c
 *
 *  Created on: 2020年8月15日
 *      Author: cwj
 */

#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"


#define UITD_4100_UNIT_NUM   800
#define UITD_4100_READ_NUM   50
#define UITD_4100_HUILU_NUM  200

int units_4100_index[UITD_SERIAL_NUM] = {0};


int J4100SetSendData(u8 serial_addr, u8 COM_ID, u8 *send_data)
{
    int checkcrc = 0;       /*crc校验*/

    char read_addr_L = 0;
    char read_addr_H = 0;

    read_addr_H = (units_4100_index[serial_addr]>>8)&0xff;
    read_addr_L = (units_4100_index[serial_addr]>>0)&0xff;

    send_data[0] = COM_ID;              /*从机地址              */
    send_data[1] = 03;                      /*功能码               */
    send_data[2] = read_addr_H;             /*起始地址高字节           */
    send_data[3] = read_addr_L;             /*起始地址低字节           */
    send_data[4] = 0;                       /*寄存器数量高字节          */
    send_data[5] = UITD_4100_READ_NUM;          /*寄存器数量低字节          */
    checkcrc = UITD_CRC_U16_MODBUS(send_data, 6);        /*计算CRC校验值          */
    send_data[6] = checkcrc&0xff;           /*CRC校验低字节          */
    send_data[7] = (checkcrc>>8)&0xff;      /*CRC校验高字节          */

    return 8;
}

u16 J4100GetStatus(u8 sts)
{
    u16 ret = GB26875_UNIT_STS_NORMAL;
    if(sts == 1){ret |= GB26875_UNIT_STS_FIRE_ALARM;}    //火警            火警
    else if(sts == 2){ret |= GB26875_UNIT_STS_ERROR;} //故障           故障
    else if(sts == 3){ret |= GB26875_UNIT_STS_CB;} //动作          反馈
    else if(sts == 4){ret |= 0;} //维护
    else if(sts == 5){ret |= GB26875_UNIT_STS_START;} //启动           启动
    else if(sts == 7){ret |= GB26875_UNIT_STS_SHIELD;} //隔离          屏蔽
    else if(sts == 11){ret |= GB26875_UNIT_STS_WTC;} //监管
    else ret = GB26875_UNIT_STS_NORMAL;
    return ret;
}

int J4100SetRecvData(u8 serial_addr,u8 COM_ID, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
    int data_addr = 0;
    char ret = -1;
    if(len < 5)
    {
        return 0;
    }
    if(serial_addr >= UITD_SERIAL_NUM)
    {
        serial_addr = UITD_SERIAL_NUM - 1;
    }

    if(read_buf[0] == COM_ID && read_buf[1] == 3 && read_buf[2] == len-5)
    {
            ret = 0;
    }
    if(ret == 0)
    {
        info->unit_size = 0;
        for(data_addr = 1; data_addr <= UITD_4100_READ_NUM; data_addr++)
        {
            info->unit[data_addr]->sys_addr = serial_addr;
            info->unit[data_addr]->addr = data_addr + units_4100_index[serial_addr] + (1<<8); // FIXME 获取回路地址
            info->unit[data_addr]->type = 0;
            info->unit[data_addr]->status = J4100GetStatus(read_buf[data_addr*2+2]);
            info->unit_size ++;
        }

        if(units_4100_index[serial_addr] < UITD_4100_HUILU_NUM)
        {
            units_4100_index[serial_addr] += UITD_4100_READ_NUM;
        }
        else
        {
            units_4100_index[serial_addr] = 0;
        }
        return UITD_DATA_TYPE_UNIT_STS;
    }
    else
    {
        return UITD_DATA_TYPE_NULL;
    }
}

struct ProtocolVTable PROTOCOL_J4100 = {
    UITD_PROTOCOL_TYPE_2,
    1,
    5000,
    1024,
    0,
    J4100SetSendData,
    0,
    J4100SetRecvData,
    50,
};

