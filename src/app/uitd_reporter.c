/*
 * uitd_reporter.c
 *
 *  Created on: 2020年10月29日
 *      Author: WangXi
 */
#include "uitd_reporter.h"

struct UITD_ReportBody
{
    char output[128];
    u32 runtime;
    u32 memsize;
    s64 timestamp;
};

u16 uitd_report_evtt = 0;

QueueInfo_st * g_report_queue = NULL;

#define UITD_REPORT_URL_FMT         "http://192.168.1.29:8080/report/status?src=%s&devDesc=%s&output=%s&runTime=%d&memSize=%d&timestamp=%llu"

#define UITD_DEV_CFG                "dev_cfg_valid"

void __UITD_ReportStatusEvent(void);

void UITD_ReportStatusInit()
{
    if(g_report_queue == NULL)
    {
        g_report_queue = Queue_CreateQueue();
    }
    if(uitd_report_evtt == 0)
    {
        uitd_report_evtt = DJY_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,\
                (ptu32_t)__UITD_ReportStatusEvent, NULL, 0x1000, "uitd_reporter");
        DJY_EventDelay(100 * mS);
    }
    if(uitd_report_evtt != 0 && uitd_report_evtt != CN_EVENT_ID_INVALID)
    {
        DJY_EventPop(uitd_report_evtt, 0, 0, NULL, NULL, 0);
    }
    else
    {
        printf("弹出报告状态事件失败\r\n");
    }
}

void UITD_ReportPush(char *str, u32 r, u32 m)
{
    struct UITD_ReportBody *body = (struct UITD_ReportBody *)malloc(sizeof(struct UITD_ReportBody));
    memset(body->output, 0, 128);
    strncpy(body->output, str, 128);
    body->runtime = r;
    body->memsize = m;

    extern s64 get_timestamp();
    body->timestamp = get_timestamp();
    if(g_report_queue != NULL)
    {
        if(Queue_Push(g_report_queue,  (ElementType)body)){}
        else free((void *)body);
    }
    else
    {
        free((void *)body);
    }
}

void __UITD_ReportStatusEvent()
{
    struct UITD_ReportBody *body;
    char url[512];
    struct http_message *response;
    char UITD_SRC[12] = {0};
    char UITD_SRCIP[6] = {0};
    while(true)
    {
        body = NULL;
        if(Queue_Top(g_report_queue, (ElementType *)&body) == FAILURE)
        {
            // printf("上传队列为空\r\n");

        }
        else if(body == NULL)
        {
            // printf("队列弹出后内容为空\r\n");
            Queue_Pop(g_report_queue, (ElementType *)&body);
            free((void *)body);
        }
        else
        {
            memset(url, 0, 512);
            UITD_GetClientIp(UITD_SRCIP);
            snprintf(UITD_SRC, 12, "%02X%02X%02X%02X%02X%02X", UITD_SRCIP[0],UITD_SRCIP[1],UITD_SRCIP[2],UITD_SRCIP[3],UITD_SRCIP[4],UITD_SRCIP[5]);
            snprintf(url, 512, UITD_REPORT_URL_FMT, UITD_SRC, UITD_DEV_CFG, body->output, body->runtime, body->memsize, body->timestamp);
            response = MG_HttpGet(url);
            Queue_Pop(g_report_queue, (ElementType *)&body);
            free((void *)body);
            free((void *)response);
        }
        DJY_EventDelay(1000000);
    }
}
